/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
require('dotenv').config();

const { getDefaultConfig } = require('metro-config');

const isDevelop = Boolean(process.env.DEVELOPER_MODE);

module.exports = (async () => {
  const { resolver } = await getDefaultConfig();
  const config = {
    transformer: {
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: true,
        },
      }),
    },
  };

  if (isDevelop) {
    const assets = extensionBuilder(resolver.assetExts, isDevelop);
    const sourceExts = extensionBuilder(resolver.sourceExts, isDevelop);
    console.log(
      'Developer Menu Activated -> every file with ".develop.[js,ts,png, ... ]" will get added to the build',
    );
    config.resolver = {
      ...resolver,
      assetExts: assets,
      sourceExts: [...sourceExts, 'cjs'],
    };
  }

  return config;
})();

function extensionBuilder(exts, addDevelop) {
  return exts.reduce((prev, ext) => {
    addDevelop && prev.push(`develop.${ext}`);
    prev.push(ext);
    return prev;
  }, []);
}
