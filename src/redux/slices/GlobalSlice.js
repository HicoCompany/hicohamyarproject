import { createSlice } from '@reduxjs/toolkit';
import { PURGE } from 'redux-persist';

const initialState = {
  version: null,
  privacyLink: null,
  guideLink: null,
  marketLink: null,
  loading: true,
  error: false,
  credits: {
    remain: '...',
    points: '...',
    requestsDeliveryCount: '...',
    deliveredCount: '...',
    loading: true,
    error: false,
    message: null,
  },
  notificationsCount: {
    suggestion: null,
    notices: null,
    lessons: null,
    loading: false,
  },
};

const GlobalSlice = createSlice({
  name: 'global',
  initialState,
  reducers: {
    /**
     *
     * @param {{payload: {version: string,privacyLink: string,guideLink: string,marketLink: string,hasUpdate: boolean}}} action
     */
    setDetails(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    /**
     * @param {import('@reduxjs/toolkit').PayloadAction<{loading: boolean,error: string | boolean}>} action
     */
    setDetailsLoading(state, action) {
      state.loading = action.payload.loading;
      state.error = action.payload.error;
    },
    /**
     *
     * @param {{payload: {points: number,remain: boolean,error: boolean,loading: boolean}}} action
     */
    setCredits(state, action) {
      state.credits = {
        ...state.credits,
        ...action.payload,
      };
    },
    /**
     * @param {{payload: boolean}} action
     */
    setNotificationStatus(state, action) {
      state.notificationsCount = {
        ...state.notificationsCount,
        loading: action.payload,
      };
    },

    /**
     * @param {{payload: {lessons: number,notices: number, suggestions: number}}} action
     */
    setNotificationCount(state, action) {
      state.notificationsCount = {
        ...state.notificationsCount,
        ...action.payload,
      };
    },
    extraReducers: (builder) => {
      builder.addCase(PURGE, () => {
        return initialState;
      });
    },
  },
});

export const Actions = GlobalSlice.actions;
export default GlobalSlice.reducer;
