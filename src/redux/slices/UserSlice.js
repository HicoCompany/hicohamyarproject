import { createSlice } from '@reduxjs/toolkit';
import { PURGE } from 'redux-persist';

const initialState = {
  auth: {
    token: null,
    refreshToken: null,
    tokenExp: null,
    refreshTokenExp: null,
    active: false,
  },
  profile: {
    firstName: '',
    lastName: '',
    nationalId: '',
    gender: '',
    phoneNumber: '',
    // familyCount: '',
  },
  //TODO old fields -> remove after changing all the components to new fields
  sdi_api_key: '',
  isSeenIntroduce: false,
  isSeenTour: false,
  isLogin: false,
  isSeenWelComeMessage: false,
  mobileNumber: '',
  codeNumber: '',
  mapid: '-1',
  address: '',
  user: {
    AccountNumber: '',
    AccountOwner: '',
    Adress: '',
    BankID: -1,
    BankTitle: '',
    BoothId: -1,
    CharityID: -1,
    Code: '',
    DeliveryWasteID: -1,
    DeviceID: '',
    DiscountCardId: -1,
    Email: '',
    Family: '',
    HelperId: -1,
    LatitudeLocation: null,
    LongitudeLocation: null,
    MainAreaID: -1,
    MainAreaTitle: '',
    Mobile: '',
    Name: '',
    NationalCode: '',
    NoticesID: -1,
    Page: -1,
    Password: null,
    Payment: -1,
    ReagentCode: -1,
    Sex: -1,
    ShabaNumber: '',
    UserName: null,
    WareId: -1,
    WasteTypeID: -1,
  },
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser(state, action) {
      state.user = action.payload;
    },
    setMapId(state, action) {
      state.mapid = action.payload;
    },
    setMapAddress(state, action) {
      state.address = action.payload;
    },
    setIsLogin(state, action) {
      state.isLogin = action.payload ?? true;
    },
    setIsSeenIntroduce(state) {
      state.isSeenIntroduce = true;
    },
    setIsSeenTour(state) {
      state.isSeenTour = true;
    },
    setIsSeenWelComeMessage(state) {
      state.isSeenWelComeMessage = true;
    },
    setMobileNumber(state, action) {
      state.mobileNumber = action.payload;
    },

    setSdiKey(state, action) {
      state.sdi_api_key = action.payload;
    },
    /**
     *
     * @param {{payload: {token: string,refreshToken: string,tokenExp: string,refreshTokenExp: string,active: boolean}}} action
     */
    setAuthToken(state, action) {
      state.auth = {
        ...state.auth,
        ...action.payload,
      };
    },

    // setCodeNumber(state, action) {
    //   state.codeNumber = action.payload;
    // },
  },
  extraReducers: (builder) => {
    builder.addCase(PURGE, () => {
      return initialState;
    });
  },
});
export const exitApp = () => ({ type: PURGE });

export const {
  setAuthToken,
  setIsLogin,
  setIsSeenIntroduce,
  setIsSeenTour,
  setMapAddress,
  setMapId,
  setMobileNumber,
  setSdiKey,
  setUser,
  setIsSeenWelComeMessage,
} = userSlice.actions;

export default userSlice.reducer;
