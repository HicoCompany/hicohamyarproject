import { createSlice } from '@reduxjs/toolkit';
import { nanoid } from 'nanoid';

const initialState = {
  requests: [],
};

const DevSlice = createSlice({
  name: 'dev',
  initialState,
  reducers: {
    /**
     * @param {import('@reduxjs/toolkit').PayloadAction<{ url, request, response, error, code, method,responseStr: string,requestStr: string }>} action
     */
    addRequest(state, action) {
      state.requests.unshift({
        key: nanoid(8),
        url: action.payload.url,
        response: action.payload.response,
        request: action.payload.request,
        responseStr: action.payload.responseStr,
        requestStr: action.payload.requestStr,
        error: action.payload.error,
        code: action.payload.code,
        time: new Date().toUTCString(),
        method: action.payload.method,
      });
    },
    clearRequests(state) {
      state.requests = [];
    },
  },
});

export const { addRequest, clearRequests } = DevSlice.actions;
export default DevSlice.reducer;
