import { createSlice } from '@reduxjs/toolkit';
import { PURGE } from 'redux-persist';

const initialState = {
  list: [],
  selected: null,
  loading: false,
  error: false,
  message: false,
};
/**
 * @typedef {{id: number | string,street: string,postalCoe: string,area: {id: number,name: string},homeNo: string | null,buildingUnit: number,position:{latitude: number,longitude: number}}} AddressParam
 * @typedef {{id: string} & AddressParam} Address
 */
const AddressesSlice = createSlice({
  name: 'addresses',
  initialState: initialState,
  reducers: {
    /**
     * @param {import('@reduxjs/toolkit').PayloadAction<Address[]>} action
     */
    setAddressList(state, action) {
      // state.list = action.payload.map((el) => ({
      //   id: el?.HelperAddressId,
      //   // userId: el?.HelperId,
      //   position: {
      //     latitude: el?.LatitudeLocation,
      //     longitude: el?.LongitudeLocation,
      //   },
      //   area: {
      //     id: el?.MainAreaId,
      //     name: el?.MainAreaTitle,
      //   },
      //   street: el?.Street,
      //   homeNo: el?.HomeNo,
      //   isDeleted: el?.IsDeleted,
      // }));
      state.list = action.payload;
    },
    /**
     * @param {import('@reduxjs/toolkit').PayloadAction<Address>} action
     */
    addAddress(state, action) {
      const data = action.payload;
      state.selected = data;
      state.list.push(data);
    },
    /**
     *
     * @param {{payload: {id: string,valuesToEdit: AddressParam}}} action
     */
    editAddress(state, action) {
      state.list = state.list.map((el) =>
        el.id === action.payload.id
          ? { ...el, ...action.payload.valuesToEdit }
          : el,
      );
    },
    /**
     * @description payload is id of address
     * @param {{payload: string}} action
     */
    removeAddress(state, action) {
      state.list = state.list.map((el) =>
        el.id === action.payload ? { ...el, isDeleted: true } : el,
      );
      if (action.payload == state.selected?.id) {
        state.selected = null;
      }
    },

    /**
     * @description payload is id of address
     * @param {{payload: string}} action
     */
    selectAddress(state, action) {
      if (!action.payload) return state;
      state.selected = state.list.find((el) => el.id == action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(PURGE, () => {
      return initialState;
    });
  },
});

export const {
  addAddress,
  editAddress,
  removeAddress,
  selectAddress,
  setAddressList,
} = AddressesSlice.actions;
export default AddressesSlice.reducer;
