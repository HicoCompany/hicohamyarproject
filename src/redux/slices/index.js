//reducers
import GlobalReducer from './GlobalSlice';
import UserReducer from './UserSlice';
import DevReducer from './development';
import AddressesReducer from './AddressesSlice';
import { combineReducers } from 'redux';

export const rootReducer = combineReducers({
  global: GlobalReducer,
  dev: DevReducer,
  userData: UserReducer,
  addresses: AddressesReducer,
});
