import { getGlobalDetails } from '@src/api/modules/App';
import Notification from '@src/api/modules/Notification';
import { getWasteRequestsInformation } from '@src/api/modules/Waste';
import { config } from '@src/utility/Constants';
import { Actions } from '../slices/GlobalSlice';

export const {
  setCredits,
  setDetails,
  setDetailsLoading,
  setNotificationCount,
  setNotificationStatus,
} = Actions;

export const fetchCredits = () => async (dispatch, get) => {
  dispatch(setCredits({ loading: true }));
  try {
    const { data } = await getWasteRequestsInformation();

    dispatch(
      setCredits({
        loading: false,
        remain: data.Price,
        points: data.TotalPoint,
        requestsDeliveryCount: data.RequestDeliveryCount,
        deliveredCount: data.DeliveryCount,
      }),
    );
  } catch (e) {
    dispatch(setCredits({ loading: false, error: true, message: e.message }));
  }
};

export const fetchNotificationCount = () => async (dispatch) => {
  dispatch(setNotificationStatus(true));
  try {
    const { data } = await Notification.GetCountOfNotViewdItem()();

    dispatch(
      setNotificationCount({
        lessons: data.LearningFileNotView,
        notices: data.NotificationNotView,
        suggestions: data.SuggestionsNotVisited,
        loading: false,
      }),
    );
  } catch (e) {
    dispatch(setNotificationStatus(false));
  }
};

/**
 * @typedef {'notices' | 'lessons' | 'suggestions'} NotificationsCountType
 * @param {NotificationsCountType} countOf
 * @param {(countOf: number) => number} modifier
 */
export const modifyNotificationCount = (countOf, modifier) => (
  dispatch,
  getState,
) => {
  const notificationsCount = getState().global.notificationsCount;
  dispatch(
    setNotificationCount({
      ...notificationsCount,
      [countOf]: modifier?.(notificationsCount[countOf]),
    }),
  );
};

export const fetchGlobalDetails = () => (dispatch) => {
  dispatch(setDetailsLoading({ loading: true, error: false }));
  getGlobalDetails()
    .then(({ data }) => {
      dispatch(
        setDetails({
          version: data?.HelperVersion,
          privacyLink: data.PrivacyLink,
          guideLink: data?.HelperGuideLink,
          marketLink: data?.HelperMarketLink,
          welcomeMessage: data?.HelperWelcomeMessage,
          hasUpdate:
            data?.HelperVersion &&
            parseInt(data?.HelperVersion, 10) > config.VERSION_CODE,
          minWeightOnDeliveryRequest: data?.MinWeightOnDeliveryRequest ?? 0,
          wasteChangeVelocityOnDeliveryRequest:
            data?.WasteChangeVelocityOnDeliveryRequest ?? 0.1,
          loading: false,
        }),
      );
    })
    .catch((e) => {
      dispatch(setDetailsLoading({ loading: false, error: e.message }));
    });
};
