// import { applyMiddleware, compose, createStore } from 'redux';
// import thunk from 'redux-thunk';
import { configureStore, combineReducers } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-community/async-storage';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';

import { rootReducer } from '../slices';
const config = {
  key: 'primary',
  storage: AsyncStorage,
  blacklist: ['global', 'dev'],
};

const reducer = persistReducer(config, rootReducer);

const store = configureStore({
  reducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const Persistor = persistStore(store);
// Persistor.
export default store;
