import { Dimensions } from 'react-native';
import { NavigationActions } from '@react-navigation/native';
import env from 'react-native-config';

export const SpinnerType = {
  CircleFlip: 'CircleFlip',
  Bounce: 'Bounce',
  Wave: 'Wave',
  WanderingCubes: 'WanderingCubes',
  Pulse: 'Pulse',
  ChasingDots: 'ChasingDots',
  ThreeBounce: 'ThreeBounce',
  Circle: 'Circle',
  CubeGrid: '9CubeGrid',
  WordPress_IOS: 'WordPress',
  FadingCircle: 'FadingCircle',
  FadingCircleAlt: 'FadingCircleAlt',
  Arc_IOS: 'Arc',
  ArcAlt_IOS: 'ArcAlt',
};

// export const CedarMapSettings = {
//   ClientId: 'wastemanager01-14099576306087092750',
//   clientSecret:
//     'by_qIndhc3RlbWFuYWdlcjAxniHS1veCg3E2PyjD7EOAiWmqI4uLYcp-St5Bw7csvG8=',
// };

export const Navigate = (context, routeName, params) => {
  if (context && context.props && context.props.navigation) {
    context.props.navigation.navigate({ routeName, params, key: Date.now() });
  }
};

export const setParamsAction = (screen, params) =>
  NavigationActions.setParams({
    params,
    key: screen,
  });

export const GetDim = () => {
  return Dimensions.get('window');
};

export const dimensions = {
  screen: Dimensions.get('screen'),
  window: Dimensions.get('window'),
};

const Constants = {
  UPLOAD_STATUS: {
    IDLE: 0,
    UPLOADING: 1,
    DOWNLOADING: 2,
    ERROR: 3,
    DONE: 4,
  },
  PageSize: 10,
  Offset: 0,
  UPLOAD_URL: '',
  SUPPORT_CENTER_PHONE_NUMBER: '+98513000313',
  SOUND_HORN: 'horn.mp3',
  DEFAULT_LOCATION: {
    latitude: 31.31498219402185,
    longitude: 48.67934335899528,
    latitudeDelta: 0.18390431844822075,
    longitudeDelta: 0.10068612092825902,
  },
  GENDERS: {
    male: 1,
    female: 2,
  },
  Fonts: {
    IR_L: 'IRANYekanLightMobile(FaNum)',
    IR_M: 'IRANYekanRegularMobile(FaNum)',
    IR_B: 'IRANYekanMobileBold(FaNum)',
    IOS_IR: 'IRANYekanMobile',
  },
};

const hash = __DEV__ ? env.APP_DEBUG_HASH : env.APP_HASH;

// Je9HWzjTP4S
/**
 * @type {{APP_NAME: string, BASE_URL: string,API_URL: string,VERSION: string,VERSION_CODE: number,BUILD: string,FLAVOR: string,OTHER: {flavorFa: string},THUMBNAIL_CACHE_NAME_PREFIX: string}}
 */
export const config = {
  APP_NAME: env.APP_NAME,
  BASE_URL: env.APP_API_HOST,
  API_URL: `${env.APP_API_HOST}${env.APP_API_BASE_ENDPOINT}`,
  VERSION: env.APP_VERSION,
  VERSION_CODE: env.APP_VERSION_CODE,
  FLAVOR: env.APP_FILE_FOR_FLAVOR,
  OTHER: {
    flavorFa: env.FLAVOR_FA,
    hash,
  },
  THUMBNAIL_CACHE_NAME_PREFIX: `tafkik_hamyar_thumbnail`,
};

console.log('configs: ', config);
export const Months = [
  'فروردين',
  'ارديبهشت',
  'خرداد',
  'تير',
  'مرداد',
  'شهريور',
  'مهر',
  'آبان',
  'آذر',
  'دي',
  'بهمن',
  'اسفند',
];

export default Constants;
