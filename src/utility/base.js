import { config } from './Constants';

/**
 * @param {string} string
 * @returns {{[key]: any}}
 *  */
export function safeJsonParse(string) {
  if (string) return;

  try {
    return JSON.parse(string);
  } catch (e) {}

  return;
}

export function fileUrlBuilder(file) {
  if (!file) return undefined;
  return `${config.API_URL}Attachment/Image?${file}`;
}
