import { DefaultModalActions } from '@src/components/GlobalAlert';
import { PermissionsAndroid } from 'react-native';
import RNPermissions from 'react-native-permissions';
// import GPSState from 'react-native-gps-state';

export const permissionPrompt = (name, persianName = '', Alert) =>
  new Promise((res, rej) => {
    // const title = `درخواست مجوز ${persianName}`;
    const message = `برای دسترسی به ${persianName} لطفا مجوز را تایید کنید`;
    const restrictionMessage = `برای استفاده از ${persianName} لطفا به تنظیمات رفته و مجوز های ${persianName} را تایید کنید.`;

    Alert.setAction(
      DefaultModalActions.Ask({
        message,
        mode: 'success',
        ConfirmLabel: 'تایید',
        DenyLabel: 'لغو',
        onConfirm: async () => {
          const permission = await RNPermissions.request(name);
          if (permission === 'denied') {
            permissionPrompt(name);
          } else if (permission === 'restricted') {
            Alert.setError(
              DefaultModalActions.Error({
                message: restrictionMessage,
              }),
            );
            rej('restricted');
          } else if (permission === 'authorized') {
            res('authorized');
          }
        },
        onDenied: () => rej('canceled'),
      }),
    );
  });

export async function requestReadSmsPermission() {
  return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_SMS, {
    title: 'دسترسی به پیام تایید',
    message:
      'لطفا برای دریافت کد اعتبار سنجی از پیام ها اجازه دسترسی به پیام ها را بدهید.',
    buttonPositive: 'اجازه می دهم',
    buttonNegative: 'اجازه نمی دهم',
  });
}
export async function checkReadSmsPermission() {
  return PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_SMS);
}
