const Colors = {
  disabled: '#dadee0',
  white: '#ffffff',

  black: '#000000',
  grayHard: '#4e505a',
  purple: '#675beb',
  gray: '#757575',
  green: '#4caf50',
  red: '#de1315',
  blue: '#2b8cf3',
  starRankColor: '#FDD835',
  greenbackgroundNotice: '#cae8cb',
  greenbackground: '#95d097',
  backgroundColor: '#ffffff',
  textGray: '#a8a8a8',
  textBlack: '#212121',
  badge: '#e53935',
  toolbar: '#016b51',
  // statusBar: '#015641',
  statusBar: '#4caf50',
  button: '#307240', //307240
  buttonlightdark: '#285728',
  border: '#757575',
  buttondark: '#102310',

  // main
  text: '#222222',
  whiteText: '#efefef',
  backdropColor: 'rgba(0,0,0,0.4)',
  backdropStatusBarColor: '#19391A',
  backdropLighterStatusBarColor: '#3E6737',
  //states
  // error: '#ff190c',
  error: '#CA0B00',
  warn: '#ffcc00',
  success: '#4caf50',
  info: '#2b8cf3',
};

export default Colors;
