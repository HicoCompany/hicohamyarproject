const _HEADERS = { 'Content-Type': 'application/json' };
const _REQUEST_TIMEOUT = 55000;
const _UPLOAD_TIMEOUT = 240000;

const WebService = {
  _tryParse(value) {
    let result = {};
    try {
      result = JSON.parse(value);
    } catch (ignored) {}
    return result;
  },

  _requestSync(params, url, options = {}, onProgress) {
    options = {
      timeout: options.timeout || _REQUEST_TIMEOUT,
      headers: options.headers || _HEADERS,
      method: options.method || 'POST',
    };

    const headerKeys = Object.keys(options.headers);
    const headerVals = Object.values(options.headers);

    if (options.method === 'GET') {
      if (params && Object.keys(params).length > 0) {
        url = `${url}?${Object.keys(params)
          .map(
            (k) => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`,
          )
          .join('&')}`;
      }
    }

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open(options.method, url);
      xhr.timeout = options.timeout;

      for (const index in headerKeys) {
        xhr.setRequestHeader(headerKeys[index], headerVals[index]);
      }

      if (headerKeys['Content-Type'] === undefined) {
        xhr.setRequestHeader('Content-Type', 'application/json');
      }

      if (xhr.upload && onProgress) {
        xhr.upload.onprogress = onProgress;
      }

      xhr.onload = (event) => {
        resolve(WebService._tryParse(event.target.responseText));
      };

      xhr.onerror = (error) => {
        reject(error);
      };

      xhr.onabort = () => {
        reject('abort');
      };

      xhr.ontimeout = () => {
        reject('timeout');
      };

      if (options.method === 'GET') {
        xhr.send();
      }

      if (options.method === 'POST') {
        xhr.send(JSON.stringify(params));
      }

      xhr.stop = xhr.abort;

      setTimeout(() => {
        if (xhr && !xhr._aborted) {
          xhr.abort();
          reject('abort');
        }
      }, options.timeout);
    });
  },

  _upload(params, url, options = {}, onProgress) {
    options = {
      timeout: options.timeout || _UPLOAD_TIMEOUT,
      headers: options.headers || _HEADERS,
      method: 'POST',
    };

    const headerKeys = Object.keys(options.headers);
    const headerVals = Object.values(options.headers);

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open(options.method, url);
      xhr.timeout = options.timeout;

      for (const index in headerKeys) {
        xhr.setRequestHeader(headerKeys[index], headerVals[index]);
      }

      if (xhr.upload && onProgress) {
        xhr.upload.onprogress = onProgress;
      }

      xhr.onload = (event) => {
        resolve(WebService._tryParse(event.target.responseText));
      };

      xhr.onerror = (error) => {
        reject(error);
      };

      xhr.onabort = () => {
        reject('abort');
      };

      xhr.ontimeout = () => {
        reject('timeout');
      };

      xhr.send(params);

      xhr.stop = xhr.abort;
    });
  },
};

export default WebService;
