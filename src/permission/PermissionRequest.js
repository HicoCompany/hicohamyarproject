import Permissions from 'react-native-permissions';

export default class PermissionRequest {

    static authorized = 'authorized';
    static denied = 'denied';
    static restricted = 'restricted';
    static undetermined = 'undetermined';

    static async location (callback) // android and ios
    {
        return Permissions.request('location');//.then(callback);
    }
    static async camera (callback) // android and ios
    {
        return Permissions.request('camera');//.then(callback);
    }

    static microphone (callback) // android and ios
    {
        Permissions.request('microphone').then(callback);
    }
    static photo (callback) // android and ios
    {
        Permissions.request('photo').then(callback);
    }
    static contacts (callback) // android and ios
    {
        Permissions.request('contacts').then(callback);
    }
    static event (callback) // android and ios
    {
        Permissions.request('event').then(callback);
    }
    static bluetooth (callback) // just ios
    {
        Permissions.request('bluetooth').then(callback);
    }
    static reminder (callback) // just ios
    {
        Permissions.request('reminder').then(callback);
    }
    static notification (callback) // just ios
    {
        Permissions.request('notification').then(callback);
    }
    static backgroundRefresh (callback) // just ios
    {
        Permissions.request('backgroundRefresh').then(callback);
    }
    static speechRecognition (callback) // just ios
    {
        Permissions.request('speechRecognition').then(callback);
    }
    static mediaLibrary (callback) // just ios
    {
        Permissions.request('mediaLibrary').then(callback);
    }
    static motion (callback) // just ios
    {
        Permissions.request('motion').then(callback);
    }
    static storage (callback) // just android
    {
        Permissions.request('storage').then(callback);
    }
    static callPhone (callback) // just android
    {
        Permissions.request('callPhone').then(callback);
    }
    static readSms (callback) // just android
    {
        Permissions.request('readSms').then(callback);
    }
    static receiveSms (callback)  // just android
    {
        Permissions.request('receiveSms').then(callback);
    }

}
