import main_logo from './main_logo.png';
import splash_background from './splash_background.jpg';
import about_logo from './about_logo.png';
import organization_logo from './organization_logo.png';
import intro_first from './intro/intro_first.png';
import intro_second from './intro/intro_second.png';
import intro_third from './intro/intro_third.png';

export {
  main_logo,
  splash_background,
  about_logo,
  organization_logo,
  intro_first,
  intro_second,
  intro_third,
};
