import React, { Fragment } from 'react';

//redux
import { Provider } from 'react-redux';
import Store, { Persistor } from './redux/store';

//ui components
import { StyleProvider } from 'native-base';
import { Provider as PaperProvider } from 'react-native-paper';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import { theme } from './theme';

//custom contexts
import AlertProvider from './components/GlobalAlert';
import { PersistGate } from 'redux-persist/integration/react';

export default function Providers({ children }) {
  return (
    <PaperProvider theme={theme}>
      <AlertProvider>
        <Provider store={Store}>
          <PersistGate loading={null} persistor={Persistor}>
            <StyleProvider style={getTheme(material)}>
              <Fragment>{children}</Fragment>
            </StyleProvider>
          </PersistGate>
        </Provider>
      </AlertProvider>
    </PaperProvider>
  );
}
