import Axios from 'axios';
import globalStore, { Persistor } from '@src/redux/store';
import { saveToDev } from '@src/screens/dev/Requests/utils';
import { config } from '@src/utility/Constants';
import { getGlobalAlert } from '@src/components/GlobalAlert';
import NetInfo from '@react-native-community/netinfo';

export const axios = Axios.create({
  baseURL: config.API_URL,
  timeout: 15000,
  headers: {
    'Cache-Control': 'no-cache',
  },
  timeoutErrorMessage: 'در حال حاضر امکان دسترسی به سرور وجود ندارد.',
});

function handleError(error) {
  if (error.response) {
    try {
      const data = JSON.parse(error?.response?.data?.Message?.trim?.());
      return Promise.reject({
        ...error?.response,
        message: data.summary,
        data,
        error: error,
      });
    } catch (e) {}

    return Promise.reject({
      ...error?.response,
      message: error?.response?.data?.Message,
      response: error?.response?.data,
      error: error,
    });
  }

  return Promise.reject({
    message: error.message,
    data: undefined,
    error: error,
  });
}

// to only show one alert every 2 second
let loginAlertTimeout;

axios.interceptors.request.use(async (req) => {
  const { isInternetReachable, isConnected } = await NetInfo.fetch();
  if (isConnected && isInternetReachable) {
    const state = globalStore.getState();
    if (state.userData.auth?.active) {
      // eslint-disable-next-line dot-notation
      req.headers['Authorization'] = `Bearer ${state.userData.auth?.token}`;
    }

    return req;
  } else {
    throw Promise.reject({
      response: { data: { Message: 'لطفا اینترنت خود را بررسی کنید.' } },
      message: 'لطفا اینترنت خود را بررسی کنید.',
    });
  }
}, handleError);

axios.interceptors.response.use(
  (res) => {
    saveToDev({
      url: res.config.url,
      request: {
        data: res.config.data ?? 'nothing sent in body',
        params: res.config.params ?? 'nothing sent as query string',
      },
      response: res.data,
      code: res.status,
      error: false,
      method: res.config.method,
    });
    return res;
  },
  (err) => {
    if (err.response) {
      saveToDev({
        url: err.config.url,
        request: {
          data: err.config.data ?? 'nothing sent in body',
          params: err.config.params ?? 'nothing sent as query string',
        },
        response: err.data,
        code: err?.status,
        error: true,
        method: err.config.method,
      });
      if (loginAlertTimeout) {
        clearTimeout(loginAlertTimeout);
      }
      loginAlertTimeout = setTimeout(() => {
        if (err.response.status === 401) {
          getGlobalAlert().addToast({
            message: 'لطفا مجددا وارد شوید',
            type: 'error',
            timeout: 'long',
          });
          Persistor.purge();
        }
      }, 1000);
    }
    return handleError(err);
  },
);

// axios.interceptors.response.use(undefined, (error) => {
//   return Promise.reject(JSON.parse(error.response.Message));
// });

// if (config.FLAVOR == 'dev') {
//   axios.interceptors.response.use(
//     (res) => {
//       // config.config.
// saveToDev({
//   url: res.config.url,
//   request: res.config.data,
//   response: res.data,
//   code: res.status,
//   error: false,
//   method: res.config.method,
// });
//       return res;
//     },
//     /**
//      * @param {import('axios').AxiosError} error
//      */
//     (error) => {
//       saveToDev({
//         url: error.config.url,
//         request: error.request,
//         response: error.response,
//         code: error.code,
//         error: error.message,
//         method: error.config.method,
//       });
//       throw error;
//     },
//   );
// }

// const useAxios = makeUseAxios({
//   axios,
// });
