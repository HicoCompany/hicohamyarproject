import React, { Component } from 'react';
import {
  Alert,
  Image,
  Keyboard,
  ScrollView,
  StatusBar,
  View,
  Linking,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { setMobileNumber } from '@src/redux/actions';

import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import { Button } from 'react-native-paper';
import { hp, normalize, wp } from '@src/responsive';

import { Checkbox } from 'react-native-paper';
import TextInput from '@components/TextInput';
import Label from '@components/Label';

//assets
import { main_logo } from '@assets/image';
import { requestCode } from '@src/api/modules/Account';
import { config } from '@src/utility/Constants';

class LoginScreen extends Component {
  FromName = 'ورود';
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      isLoadingButton: false,
      Mobile: '',
      isSelected: false,
      link: '',
    };

    this.onConfirm = this.onConfirm.bind(this);
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  componentWillUnmount = () => {
    //NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  };

  onConfirm = () => {
    Keyboard.dismiss();
    const { isConnected, Mobile, isSelected } = this.state;
    this.setState({ error: '' });
    if (isConnected) {
      if (isSelected === false) {
        this.setState({
          error:
            'شرایط  استفاده از خدمات و حریم خصوصی را مطالعه و تایید نمایید',
        });
      } else if (Mobile.length === 0) {
        this.setState({ error: 'شماره همراه خود را وارد کنید.' });
      } else if (Mobile.length < 11) {
        this.setState({ error: 'شماره همراه خود را کامل وارد کنید.' });
      } else {
        this.setState(
          {
            error: '',
            isLoadingButton: true,
          },
          () => {
            this.phoneRegisterNaturalPersonApi();
          },
        );
      }
    } else {
      Alert.alert(
        'اینترنت گوشی خود را بررسی کنید',
        '',
        [
          { text: 'انصراف' },
          { text: 'تلاش مجدد', onPress: this.onConfirm },
          { text: 'باشه' },
        ],
        { cancelable: true },
      );
    }
  };
  toggleSelection = () => {
    this.setState((state) => ({
      ...state,
      isSelected: !state.isSelected,
    }));
  };

  linkUrl() {
    if (this.props.privacyLink) {
      Linking.canOpenURL(this.props.privacyLink)
        .then(async (value) => {
          if (value) {
            try {
              await Linking.openURL(this.props.privacyLink);
            } catch {}
          }
        })
        .catch(() => {});
    }
  }

  async phoneRegisterNaturalPersonApi() {
    try {
      const response = await requestCode({
        phoneNumber: this.state.Mobile,
        hash: config.OTHER.hash,
      });
      this.props.setMobileNumber(Utils.toEnglishDigits(this.state.Mobile));

      this.props.navigation.navigate('LoginConfirm', {
        verifyId: response.data.VerifyId,
        beforeCodeCharCount: response.data.BeforeCodeCharCount,
      });
    } catch (error) {
      this.setState({
        error: error.message ?? 'خطایی رخ داد لطفا اینترنت خود را بررسی کنید.',
      });
    } finally {
      this.setState({ isLoadingButton: false });
    }
  }

  render() {
    return (
      <ScrollView style={styles.container} contentContainerStyle={styles.grow}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <Image style={styles.logo} source={main_logo} resizeMode={'contain'} />
        <TextInput
          placeholder={'شماره همراه خود را وارد کنید'}
          selectionColor={Colors.green}
          keyboardType={'phone-pad'}
          autoFocus
          style={styles.input}
          maxLength={11}
          value={this.state.Mobile}
          accessible={false}
          onChangeText={(Mobile) => {
            if (Mobile.length == 11) {
              Keyboard.dismiss();
            }
            this.setState({
              Mobile,
              error: '',
            });
          }}
        />
        <Label style={styles.error}>{this.state.error}</Label>
        <View style={styles.checkboxContainer}>
          <TouchableOpacity
            style={styles.privacyContainer}
            onPress={() => this.linkUrl()}>
            <Label style={styles.label}>
              {'شرایط استفاده از خدمات و حریم خصوصی را می پذیرم'}
            </Label>
          </TouchableOpacity>
          {this.props.loadingDetails ? (
            <ActivityIndicator />
          ) : (
            <Checkbox
              status={this.state.isSelected ? 'checked' : 'unchecked'}
              style={styles.checkbox}
              onPress={this.toggleSelection}
              color={Colors.green}
            />
          )}
        </View>
        <Button
          loading={this.state.isLoadingButton}
          style={styles.button}
          mode="contained"
          labelStyle={this.state.isSelected ? styles.buttonLabel : undefined}
          disabled={!this.state.isSelected}
          onPress={this.onConfirm}>
          {this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'دریافت کد '}
        </Button>
      </ScrollView>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },

  checkboxContainer: {
    flexDirection: 'row',
    // marginBottom: 10,
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 6,
    // borderColor: '#035e6f',
    // borderWidth: 1,
    // padding: 2,
    // ma
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    margin: 8,
    textDecorationLine: 'underline',
    textDecorationColor: Colors.green,
    color: Colors.green,
  },
  logo: {
    height: hp('40%'),
    width: wp('50%'),
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop: hp('3%'),
    marginBottom: hp('3%'),
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },
  desc: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.white,
    textAlign: 'center',
  },
  separate: {
    marginBottom: 20,
    marginTop: 20,
    height: 0.8,
    width: wp('70%'),
    backgroundColor: Colors.textGray,
    alignSelf: 'center',
  },
  input: {
    height: hp('6%'),
    width: wp('75%'),
    alignSelf: 'center',
    backgroundColor: Colors.white,
    textAlign: 'center',
  },
  error: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.error,
    textAlign: 'center',
  },

  button: {
    width: 200,
    height: 45,
    borderWidth: 1,
    color: '#ffffff',
    borderRadius: 30,
    margin: 0,
    alignSelf: 'center',
    // ma
  },
  buttonLabel: {
    color: '#ffffff',
  },
  privacyContainer: {
    flexDirection: 'row',
  },
  grow: { flex: 1 },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    loadingDetails: state.global.loadingDetails,
    privacyLink: `${config.BASE_URL}${state.global.privacyLink}`,
    global: state.global,
  };
};

const mapActionToProps = {
  setMobileNumber,
};

export default connect(mapStateToProps, mapActionToProps)(LoginScreen);
