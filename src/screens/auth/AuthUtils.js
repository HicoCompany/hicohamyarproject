import { useNavigation } from '@react-navigation/native';
import { CommonActions } from '@react-navigation/routers';
import { sendFireBaseIdToServer } from '@src/notification';
import { setAuthToken } from '@src/redux/actions';
import { useDispatch } from 'react-redux';

export function useAuthAndNavigateToHome() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  return async (data) => {
    dispatch(
      setAuthToken({
        token: data.TokenDto.Token,
        refreshToken: data.TokenDto.RefreshToken,
        tokenExp: data.TokenDto.AccessTokenExpireDateUtc,
        refreshTokenExp: data.TokenDto.RefreshTokenExpireDateUtc,
        active: true,
      }),
    );

    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: 'Dashboard',
          },
        ],
      }),
    );

    // send the user firebase id to server after login
    sendFireBaseIdToServer();
  };
}
