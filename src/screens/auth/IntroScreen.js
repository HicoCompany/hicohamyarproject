import React, { useCallback, useMemo } from 'react';
import { Image, StatusBar, View } from 'react-native';

//redux
import { useDispatch } from 'react-redux';
import { setIsSeenIntroduce } from '@src/redux/actions';

//ui components
import AppIntroSlider from 'react-native-app-intro-slider';
import { Label } from '@src/components';

//assets
import { intro_first, intro_second, intro_third } from '@assets/image';
import Colors from '@src/utility/Colors';
import { config, dimensions } from '@src/utility/Constants';

//utils
import { ScaledSheet } from 'react-native-size-matters';

function IntroScreen({ navigation }) {
  const dispatch = useDispatch();

  const slides = useMemo(
    () => [
      {
        key: 'first',
        title: `به سامانه تفکیک از مبدا شهرداری ${config.OTHER.flavorFa} (${config.APP_NAME}) خوش آمدید`,
        text: `شهروند عزیز، شما میتوانید به راحتی در اپلیکیشن ${config.APP_NAME} ثبت نام کرده و به عنوان همیار محیط زیست دوست شناخته شوید.`,
        image: intro_first,
      },
      {
        key: 'second',
        title: 'ثبت درخواست ',
        text:
          'همیار عزیز، شما می توانید زباله های قابل بازیافت(خشک) خود را از بین زمان های پیشنهاد شده، تحویل سفیران پاکی محله خود دهید و در ازای آن اعتبار دریافت کنید.',
        image: intro_second,
      },
      {
        key: 'third',
        title: 'جمع آوری',
        text:
          'سفیران پاکی در زمانی که مشخص نموده اید به محل زندگی شما مراجعه کرده و پسماند تفکیک شده ی شما را تحویل می گیرد .علاوه بر آن به ازای پسماند تحویل داده شده، امتیاز دریافت می کنید و میتوانید این امتیازات رو خرج کنید',
        image: intro_third,
      },
    ],
    [],
  );

  const renderItem = useCallback((item) => {
    return (
      <View style={styles.itemContainer}>
        <Image source={item.image} style={styles.imageStyle} />
        <View style={styles.itemInner}>
          <Label weight="bold" style={styles.titleStyle}>
            {item.title}
          </Label>
          <Label style={styles.textStyle}>{item.text}</Label>
        </View>
      </View>
    );
  }, []);

  const renderSkipButton = useCallback(() => {
    return (
      <View style={styles.skipContainer}>
        <Label style={styles.skipText}>{'گذشتن از صفحات'}</Label>
      </View>
    );
  }, []);

  const renderDoneButton = useCallback(() => {
    return (
      <View style={styles.skipContainer}>
        <Label style={styles.skipText}>{'ورود به نرم افزار'}</Label>
      </View>
    );
  }, []);

  const handleDone = useCallback(() => {
    dispatch(setIsSeenIntroduce());
    navigation.navigate('Login');
  }, [dispatch, navigation]);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.statusBar} barStyle="light-content" />
      <AppIntroSlider
        slides={slides}
        showSkipButton
        showPrevButton={false}
        hideNextButton
        hideDoneButton={false}
        bottomButton
        renderItem={renderItem}
        renderSkipButton={renderSkipButton}
        renderDoneButton={renderDoneButton}
        onDone={handleDone}
        onSkip={handleDone}
        dotStyle={styles.dot}
        activeDotStyle={styles.dotActive}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  dot: {
    backgroundColor: Colors.greenbackground,
  },
  dotActive: {
    backgroundColor: Colors.white,
  },
  skipContainer: {
    height: '45@ms',
    width: '200@ms',
    borderRadius: '16@ms',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: Colors.white,
    color: Colors.white,
  },
  skipText: {
    fontSize: '15@s',
    color: Colors.green,
  },

  itemContainer: {
    flex: 1,
    width: dimensions.window.width,
  },
  imageStyle: {
    width: '60%',
    flex: 3.5,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  itemInner: {
    flex: 1,
    paddingBottom: 150,
    padding: 16,
    width: dimensions.window.width,
    backgroundColor: Colors.green,
    alignSelf: 'center',
  },
  titleStyle: {
    marginVertical: 10,
    fontSize: '18@s',
    textAlign: 'center',
    color: Colors.white,
  },
  textStyle: {
    fontSize: '14@s',
    color: Colors.white,
  },
});

export default IntroScreen;
