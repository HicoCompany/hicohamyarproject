/* eslint-disable react-hooks/exhaustive-deps */
import { Label, RadioButton, Select } from '@src/components';
import TextInput from '@src/components/TextInput';
import Colors from '@src/utility/Colors';
import Constants from '@src/utility/Constants';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  View,
  KeyboardAvoidingView,
  SafeAreaView,
  Platform,
  Image,
  StatusBar,
  Keyboard,
} from 'react-native';

//ui components
import ViewPager from 'react-native-pager-view';
import { Button, HelperText } from 'react-native-paper';
//utils
import { s, ScaledSheet } from 'react-native-size-matters';

import Noise from '@assets/noise.png';
import { useFormik } from 'formik';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import { ClassLabel } from '@src/components/Label';
import { useMainGuild, useSubGuild } from '@src/api/modules/Guild';
import { useRegisterHelper } from '@src/api/modules/Helper';
import { useAuthAndNavigateToHome } from './AuthUtils';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import AddressInput from '@src/components/Addresses/AddressInput';

const english = /^[A-Za-z0-9]*$/;
/**
 *  Register
 * first/last name
 * national id
 * gender
 * area
 * family count
 * invitation code
 *
 */

/**
 *
 * Update Profile
 * first/last name
 * national id
 * gender
 * area
 * family count
 * email
 * birthday date
 */

const AnimatedLabel = Animated.createAnimatedComponent(ClassLabel);

export default function RegisterScreen({ route }) {
  const pagerRef = useRef();
  const subGuildSelectRef = useRef();
  const [currentPage, setCurrentPage] = useState(0);

  const Alert = useAlert();
  const setAuthAndNavigateToHome = useAuthAndNavigateToHome();

  //animated
  const titleFontSize = useSharedValue(24);
  const isKeyboardVisible = useSharedValue(false);

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      nationalId: '',
      gender: -1,
      familyMembers: '',
      invitationCode: '',
      address: {},
      //guild
      guildMain: null,
      guildSub: undefined,
    },

    onSubmit: () => {
      submitHelper
        .handleFetch()
        .then(({ data }) => {
          setAuthAndNavigateToHome(data);
        })
        .catch((e) => {
          Alert.setError(
            DefaultModalActions.Error({
              message: e.message,
            }),
          );
        });
    },
    validate: ({ firstName, lastName }) => {
      const errors = {};
      if (english.test(firstName)) {
        errors.firstName = 'نام خود را با حروف فارسی وارد کنید.';
      }

      if (english.test(lastName)) {
        errors.lastName = 'نام خانوادگی خود را با حروف فارسی وارد کنید.';
      }

      return errors;
    },
  });
  const values = formik.values;
  const [submitState, submitHelper] = useRegisterHelper({
    name: values.firstName,
    family: values.lastName,
    gender: values.gender,
    verifyId: route.params?.verifyId,
    reagentCode: values.invitationCode,
    guildId: values.guildSub?.GuildId,
    mainAreaId: values.address?.area?.id,
  });

  const [mainGuildState] = useMainGuild({
    initialData: [],
    onSuccess: ({ data }) => {
      formik.setFieldValue('guildMain', data[0].GuildId);
      subGuildHelpers
        .handleFetch({
          params: {
            parentGuildId: data[0].GuildId,
          },
        })
        .then(() => {
          subGuildSelectRef.current.setVisibility(true);
        });
    },
  });

  const [subGuildState, subGuildHelpers] = useSubGuild({
    initialData: [],
  });
  const titleStyle = useAnimatedStyle(() => ({
    fontSize: titleFontSize.value,
    flex: isKeyboardVisible.value ? undefined : 1,
    paddingTop: isKeyboardVisible.value ? 50 : 55,
    fontWeight: 'bold',
  }));

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => {
      titleFontSize.value = withSpring(s(20));

      isKeyboardVisible.value = true;
    });
    Keyboard.addListener('keyboardDidHide', () => {
      titleFontSize.value = withSpring(s(24));
      isKeyboardVisible.value = false;
    });

    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  }, []);

  const handleNextPage = useCallback(async () => {
    if (currentPage < 2) {
      const errors = await formik.validateForm();
      if (errors.firstName || errors.lastName) return;
      pagerRef.current.setPage(currentPage + 1);
      setCurrentPage(currentPage + 1);
    } else {
      formik.submitForm();
    }
  }, [currentPage]);

  const nextButtonLabel = useMemo(() => {
    if (currentPage == 0) {
      return 'مرحله بعد';
    } else if (currentPage == 1) {
      return 'تکمیل پروفایل';
    } else if (currentPage == 2 && formik.values.invitationCode) {
      return 'ثبت کد معرف';
    } else {
      return 'پایان ثبت نام';
    }
  }, [formik.values.invitationCode, currentPage]);

  const handlePrevPage = useCallback(() => {
    if (currentPage > 0) {
      pagerRef.current.setPage(currentPage - 1);
      setCurrentPage(currentPage - 1);
    }
  }, [currentPage]);

  const nextDisabled = useMemo(() => {
    const { firstName, lastName, gender, guildSub, guildMain } = formik.values;
    if (currentPage == 0) {
      if (
        firstName.trim().length < 2 ||
        lastName.trim().length < 2 ||
        gender == -1
      ) {
        return true;
      }
      return false;
    } else if (currentPage == 1) {
      if (!guildMain || !guildSub) {
        return true;
      }
      return false;
    }
    return false;
  }, [formik.values, currentPage]);

  const handleInputs = (name) => (value) =>
    formik.handleChange({ target: { value, name } });

  const handleGuildChange = useCallback(async (value) => {
    await formik.setFieldValue('guildMain', value);
    formik.setFieldValue('guildSub', undefined);
    await subGuildHelpers.handleFetch({
      params: {
        parentGuildId: value,
      },
    });
    subGuildSelectRef.current.setVisibility(true);
  }, []);

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.OS == 'ios' ? 'padding' : undefined}>
      <StatusBar translucent backgroundColor="transparent" />
      <Image source={Noise} style={styles.noise} resizeMode="cover" />

      <SafeAreaView style={styles.content}>
        <AnimatedLabel style={[styles.title, titleStyle]} weight="bold">
          به خانواده دوست داران محیط زیست خوش آمدید
        </AnimatedLabel>

        <ViewPager
          style={styles.pager}
          showPageIndicator
          scrollEnabled={false}
          ref={pagerRef}>
          <View key="name" style={styles.slides}>
            <Label style={styles.help} weight="bold" fontSize={14}>
              برای ادامه لطفا نام و نام خانوادگی و جنسیت خود را وارد کنید.
            </Label>

            <TextInput
              style={styles.input}
              placeholder="نام"
              autoFocus
              value={formik.values.firstName}
              onChangeText={handleInputs('firstName')}
            />

            {values.firstName.length && formik.errors.firstName ? (
              <HelperText style={styles.error} type="error">
                {formik.errors.firstName}
              </HelperText>
            ) : null}

            <TextInput
              style={styles.input}
              placeholder="نام خانوادگی"
              value={formik.values.lastName}
              onChangeText={handleInputs('lastName')}
            />

            {values.lastName.length && formik.errors.lastName ? (
              <HelperText style={styles.error} type="error">
                {formik.errors.lastName}
              </HelperText>
            ) : null}

            <RadioButton.Group
              value={formik.values.gender}
              onChange={handleInputs('gender')}
              style={styles.gender}>
              <RadioButton
                value={Constants.GENDERS.female}
                label="زن"
                side="left"
                style={styles.genderItem}
                checkedColor={Colors.purple}
                labelStyle={styles.genderItemLabel}
              />
              <RadioButton
                value={Constants.GENDERS.male}
                label="مرد"
                side="right"
                checkedColor={Colors.purple}
                style={styles.genderItem}
                labelStyle={styles.genderItemLabel}
              />
            </RadioButton.Group>

            <AddressInput
              value={formik.values.address}
              onChange={(value) => {
                handleInputs('address')(value);
              }}
            />
          </View>
          <View key="guild" style={styles.slides}>
            <Label style={{ color: Colors.whiteText }}>نوع کاربری:</Label>
            <RadioButton.Group
              style={styles.guild}
              value={formik.values.guildMain}
              onChange={handleGuildChange}>
              {mainGuildState.data.map((el, i, arr) => (
                <RadioButton
                  label={el.Title}
                  key={el.GuildId}
                  value={el.GuildId}
                  style={styles.guildItem}
                  side={
                    (arr.length - 1 == i && 'left') ||
                    (i == 0 && 'right') ||
                    undefined
                  }
                  labelStyle={styles.guildItemLabel}
                  checkedColor={Colors.purple}
                />
              ))}
            </RadioButton.Group>

            <Select
              data={subGuildState.data}
              labelKey="Title"
              valueKey="GuildId"
              loading={subGuildState.loading}
              value={formik.values.guildSub}
              onChange={handleInputs('guildSub')}
              style={styles.guildInput}
              inputStyle={styles.selectInput}
              placeholder="کاربر"
              selectRef={subGuildSelectRef}
            />

            {formik.values.guildMain === 1 ? (
              <TextInput
                style={styles.input}
                placeholder="تعداد افراد خانواده"
                value={formik.values.familyMembers}
                onChangeText={handleInputs('familyMembers')}
                keyboardType="numeric"
              />
            ) : null}
          </View>
          <View key="reagentCode" style={styles.slides}>
            <Label style={styles.invitationTitle} weight="bold">
              در صورتی که کسی اپ را به شما معرفی کرده است کد معرف را وارد کنید.
            </Label>
            <TextInput
              style={styles.input}
              placeholder="کد معرف"
              value={formik.values.invitationCode}
              onChangeText={handleInputs('invitationCode')}
              keyboardType="numeric"
            />
          </View>
        </ViewPager>

        <View style={styles.actionBar}>
          {currentPage > 0 ? (
            <Button color={Colors.grayHard} onPress={handlePrevPage}>
              برگشت
            </Button>
          ) : (
            <View />
          )}
          <Button
            onPress={handleNextPage}
            disabled={
              nextDisabled || formik.errors.firstName || formik.errors.lastName
            }
            loading={submitState.loading}>
            {nextButtonLabel}
          </Button>
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
}

const styles = ScaledSheet.create({
  container: { flex: 1, backgroundColor: Colors.green },
  content: {
    flex: 1,
  },
  title: {
    flex: 1,
    textAlignVertical: 'center',
    padding: '16@s',
    color: Colors.whiteText,
  },
  help: {
    textAlignVertical: 'center',
    padding: '5@s',
    color: Colors.whiteText,
  },
  pager: {
    width: '100%',
    flex: 4,
  },
  slides: {
    flex: 1,
    padding: '16@s',
  },
  actionBar: {
    width: '100%',
    height: '50@s',
    backgroundColor: '#efefef',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: '12@s',
  },
  guildInput: {
    height: '45@s',
    marginBottom: '12@s',
  },
  input: {
    height: '45@s',
    marginBottom: '12@s',
    zIndex: 0,
    elevation: 0,
  },
  inputLabel: {
    color: 'white',
    fontSize: '14@s',
    textAlign: 'right',
  },
  invitationTitle: {
    color: 'white',
    fontSize: '20@s',
    textAlign: 'right',
    height: '140@s',
    paddingTop: 50,
  },
  gender: {
    alignSelf: 'center',
    height: '45@s',
    overflow: 'hidden',
  },
  genderItem: {
    flex: 1,
    height: '40@s',
  },
  genderItemLabel: {
    fontSize: '12@s',
  },
  noise: {
    position: 'absolute',
    top: 0,
    flex: 1,
    opacity: 0.5,
  },
  guild: {
    alignSelf: 'center',
    width: '100%',
    height: '45@s',
    flexDirection: 'row-reverse',
  },
  guildItem: {
    flex: 1,
  },
  guildItemLabel: {
    fontSize: '11@s',
  },

  selectInput: {
    height: '45@s',
  },

  error: {
    backgroundColor: Colors.white,
    textAlign: 'center',
    borderRadius: 6,
    alignSelf: 'center',
    marginBottom: 10,
  },
});
