import React, { useCallback, useEffect, useState } from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  View,
} from 'react-native';
import { useSelector } from 'react-redux';

// ui components
import { Button, HelperText } from 'react-native-paper';
import Label from '@components/Label';
import ConfirmCodeInput from '@components/ConfirmCodeInput';

//assets
import { main_logo } from '@assets/image';

//utils
import { ScaledSheet } from 'react-native-size-matters';
import Colors from '@src/utility/Colors';
import { requestCode, useVerifyUser } from '@src/api/modules/Account';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import { useAuthAndNavigateToHome } from './AuthUtils';

import SmsRetriever from 'react-native-sms-retriever';
import { config } from '@src/utility/Constants';

let timer = null;
export default function LoginConfirmScreen({ navigation, route }) {
  const Alert = useAlert();
  const [timeoutCount, setTimeoutCount] = useState(60);
  const phoneNumber = useSelector((store) => store.userData.mobileNumber);
  const [{ beforeCodeCharCount, verifyId }, setParams] = useState({
    verifyId: null,
    beforeCodeCharCount: 0,
  });

  // const verifyId = route.params.verifyId;
  const setAuthAndNavigateToHome = useAuthAndNavigateToHome();
  const formik = useFormik({
    initialValues: '',
    validationSchema: yup
      .string()
      .length(5)
      .required('کد ارسال شده به شماره همراه خود را وارد کنید.'),
    validateOnChange: false,
    validateOnBlur: false,
    validateOnMount: false,
    onSubmit() {
      helper
        .handleFetch()
        .then(({ data }) => {
          if (data.IsRegister) {
            navigation.navigate('Register', { verifyId: verifyId });
          } else {
            setAuthAndNavigateToHome(data);
          }
        })
        .catch((error) => {
          Alert.setError(DefaultModalActions.Error({ message: error.message }));
        });
    },
  });
  const [state, helper] = useVerifyUser({
    verifyId,
    code: formik.values,
  });

  useEffect(() => {
    startTimer();

    handleListenToSms();
    return () => {
      if (timer) {
        clearInterval(timer);
      }

      SmsRetriever.removeSmsListener();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setParams({
      verifyId: route.params.verifyId,
      beforeCodeCharCount: route.params.beforeCodeCharCount,
    });
  }, [route.params]);

  const handleListenToSms = async () => {
    try {
      const registered = await SmsRetriever.startSmsRetriever();
      if (registered) {
        SmsRetriever.addSmsListener((event) => {
          const code = event.message?.slice?.(
            beforeCodeCharCount,
            beforeCodeCharCount + 5,
          );
          formik.setValues(code);
        });
      }
    } catch (error) {}
  };

  const startTimer = useCallback(() => {
    let tempTime = 60;
    if (!timer) {
      setTimeoutCount(60);
    }

    timer = setInterval(() => {
      if (tempTime < 1) {
        clearInterval(timer);
        timer = null;
      } else {
        setTimeoutCount((time) => time - 1);
        tempTime = tempTime - 1;
      }
    }, 1000);
  }, []);

  const handleResendCode = useCallback(() => {
    requestCode({ phoneNumber, hash: config.OTHER.hash }).then((response) => {
      setParams({
        verifyId: response.data.VerifyId,
        beforeCodeCharCount: response.data.BeforeCodeCharCount,
      });
      startTimer();
    });
  }, [phoneNumber, startTimer]);

  // const onConfirm = () => {
  //   Keyboard.dismiss();
  //   this.setState({ error: '' });
  //   if (this.state.smsToken.length === 0) {
  //     this.setState({ error: 'کد فعال سازی خود را وارد کنید.' });
  //   } else if (this.state.smsToken.length < 5) {
  //     this.setState({ error: 'کد فعال سازی خود را کامل وارد کنید.' });
  //   } else {
  //     this.setState(
  //       {
  //         error: '',
  //         isLoadingButton: true,
  //       },
  //       () => {
  //         this.phoneLoginNaturalPersonApi();
  //       },
  //     );
  //   }
  // };

  return (
    <ScrollView style={styles.container}>
      <StatusBar backgroundColor={Colors.statusBar} barStyle="light-content" />
      <Image style={styles.logo} source={main_logo} resizeMode={'contain'} />

      <Label style={styles.desc}>
        {`کد فعال سازی به شماره زیر ارسال خواهد شد \n ${phoneNumber}`}
      </Label>

      <ConfirmCodeInput value={formik.values} onChange={formik.setValues} />

      <Button
        mode="contained"
        loading={state.loading}
        style={styles.button}
        disabled={!formik.values}
        labelStyle={formik.values ? styles.buttonLabel : undefined}
        onPress={formik.handleSubmit}>
        {state.loading ? 'لطفا منتظر بمانید' : 'ورود به نرم افزار'}
      </Button>

      {timeoutCount !== 0 ? (
        <Label style={styles.timer}>
          {`${timeoutCount} ‌ثانیه تا ارسال مجدد `}
        </Label>
      ) : (
        <View style={styles.inputTitleContainer}>
          <TouchableOpacity activeOpacity={0.6} onPress={handleResendCode}>
            <Label style={styles.sendCodeAgain}>{'ارسال دوباره کد...'}</Label>
          </TouchableOpacity>
        </View>
      )}
      <HelperText style={styles.error} type="error" visible={state.error}>
        {state.message}
      </HelperText>
    </ScrollView>
  );
}

// class LoginConfirmScreen extends Component {
//   FormName = 'تایید ورود';
//   constructor(props) {
//     super(props);

//     this.state = {
//       isLoadingButton: false,
//       smsToken: '',
//       error: '',
//       timeCount: 60,
//     };

//     this.onConfirm = this.onConfirm.bind(this);
//     this.phoneRegisterNaturalPersonApi = this.phoneRegisterNaturalPersonApi.bind(
//       this,
//     );
//   }

//   componentDidMount() {
//     this.startCounterDown();
//   }

//   shouldComponentUpdate(nextProps, nextState) {
//     return Utils.shallowCompare(this, nextProps, nextState);
//   }

//   componentWillUnmount() {
//     if (this.timer) {
//       clearInterval(this.timer);
//     }
//   }

//   async phoneLoginNaturalPersonApi() {
//     try {
//       const response = await confirmCode({
//         code: Utils.toEnglishDigits(this.state.smsToken),
//         phoneNumber: this.props.userData.mobileNumber,
//       });

//       this.setState({ isLoadingButton: false });
//       this.props.setCodeNumber(Utils.toEnglishDigits(this.state.smsToken));
//       if (response.ResultID === 100) {
//         this.props.navigation.navigate('Register');
//       } else if (response.ResultID === 200) {
//         this.props.setIsLogin();
//         this.props.navigation.dispatch(
//           StackActions.reset({
//             index: 0,
//             key: null,
//             actions: [NavigationActions.navigate({ routeName: 'dashboard' })],
//           }),
//         );
//       }
//     } catch (error) {
//       this.setState({
//         isLoadingButton: false,
//         error: error.data?.Text ?? 'ارتباط دستگاه خود را بررسی کنید.',
//       });
//     }
//   }

//   async phoneRegisterNaturalPersonApi() {
//     const deviceId = DeviceInfo.getUniqueID();
//     const params = {
//       Mobile: this.props.userData.mobileNumber,
//       DeviceID: deviceId,
//     };
//     try {
//       const response = await Repository.PhoneRegisterNaturalPersonApi(params);
//       this.setState({ isLoadingButton: false });
//       if (response.Success === 1) {
//         this.startCounterDown();
//       } else {
//         this.setState({ error: response.Text });
//       }
//     } catch (error) {
//       this.setState({
//         isLoadingButton: false,
//         error: 'ارتباط دستگاه خود را بررسی کنید.',
//       });
//     }
//   }

//   render() {
//     return (

//     );
//   }
// }

const styles = ScaledSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },
  logo: {
    height: '200@vs',
    width: '200@s',
    alignSelf: 'center',
    resizeMode: 'contain',
    marginVertical: '20@s',
  },
  title: {
    fontSize: '12@s',
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: '10@ms',
  },
  desc: {
    fontSize: '15@ms',
    textAlign: 'center',
  },
  inputTitleContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    margin: '12@ms',
  },
  sendCodeAgain: {
    fontSize: '13@ms',
    textDecorationLine: 'underline',
    color: Colors.green,
    textAlign: 'center',
    width: '100%',
  },
  error: {
    fontSize: '12@ms',
    textAlign: 'center',
  },
  button: {
    width: '220@s',
    height: '40@s',
    borderWidth: 1,
    color: '#ffffff',
    borderRadius: 0,
    borderBottomLeftRadius: '12@ms',
    borderBottomRightRadius: '12@ms',
    margin: 0,
    alignSelf: 'center',
  },
  buttonLabel: {
    color: '#ffffff',
  },
  timer: {
    fontSize: '13@ms',
    alignSelf: 'center',
    margin: '8@ms',
  },
});

// const mapStateToProps = (state) => {
//   return {
//     userData: state.userData,
//   };
// };

// const mapActionToProps = {
//   setIsLogin,
//   setCodeNumber,
// };

// export default connect(mapStateToProps, mapActionToProps)(LoginConfirmScreen);
