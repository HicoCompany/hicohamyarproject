import Login from './LoginScreen';
import LoginConfirm from './LoginConfirmScreen';
import Register from './RegisterScreen';
import ReagentCode from './ReagentCodeScreen';
import Intro from './IntroScreen';

export const AuthScreens = {
  Login,
  LoginConfirm,
  Register,
  ReagentCode,
  Intro,
};
