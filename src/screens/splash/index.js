import React, { Component } from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  StatusBar,
  View,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '@src/utility/Colors';
import { normalize } from '@src/responsive';
import { splash_background } from '@assets/image';
import { Label } from '@components';
import { fetchGlobalDetails } from '@src/redux/actions';
import { Button } from 'react-native-paper';

class SplashScreen extends Component {
  componentDidMount() {
    this.props.handleRefresh();
  }

  _renderLoading = (
    <>
      <ActivityIndicator
        size={'small'}
        color={Colors.green}
        style={styles.loading}
        animating={true}
      />
      <Label style={styles.loadingText}>{'لطفا صبر کنید'}</Label>
    </>
  );

  _renderRetry = (
    <Button
      icon={'refresh'}
      labelStyle={styles.buttonLabel}
      onPress={this.props.handleRefresh}>
      تلاش مجدد
    </Button>
  );
  render() {
    return (
      <ImageBackground style={styles.container} source={splash_background}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <View style={styles.loadingContainer}>
          {this.props.loading ? this._renderLoading : null}
          {!this.props.loading && this.props.error ? this._renderRetry : null}
        </View>
      </ImageBackground>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  loadingContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  loading: {
    marginRight: 10,
  },
  loadingText: {
    fontSize: normalize(13),
    color: '#333',
  },
  buttonLabel: {
    fontSize: 18,
    color: Colors.textBlack,
  },
});

const mapStateToProps = (state) => {
  return {
    loading: state.global.loading,
    error: state.global.error,
  };
};

const mapActionToProps = {
  handleRefresh: fetchGlobalDetails,
};

export default connect(mapStateToProps, mapActionToProps)(SplashScreen);
