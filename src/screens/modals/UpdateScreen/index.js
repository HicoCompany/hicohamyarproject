import React, { useCallback } from 'react';
import {
  Modal,
  View,
  StyleSheet,
  BackHandler,
  StatusBar,
  Linking,
} from 'react-native';

import { useSelector } from 'react-redux';

//ui
import { BlurView } from '@react-native-community/blur';
import Label from '@components/Label';
import { Button } from 'react-native-paper';

export default function UpdateScreen() {
  const { hasUpdate, marketLink, loadingDetails } = useSelector(
    (store) => store.global,
  );

  const handleClose = useCallback(() => {
    BackHandler.exitApp();
  }, []);

  const updateTheApp = useCallback(async () => {
    if (marketLink) {
      (await Linking.canOpenURL(marketLink)) && Linking.openURL(marketLink);
    }
  }, [marketLink]);

  if (hasUpdate && !loadingDetails) {
    return (
      <>
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="light-content"
          animated
        />
        <BlurView
          blurType="dark"
          blurAmount={100}
          blurRadius={5}
          reducedTransparencyFallbackColor="white"
          style={styles.absolute}
        />
        <Modal
          transparent
          animationType="fade"
          onRequestClose={handleClose}
          visible={true}>
          <View style={styles.container}>
            <View style={styles.dialog}>
              <Label style={styles.title} weight="bold">
                به روزرسانی نرم افزار
              </Label>
              <View style={styles.messages}>
                <Label>نسخه ی جدیدی از نرم افزار ارائه شده است.</Label>
                <Label>برای استفاده لطفا نرم افزار را به روزرسانی کنید.</Label>
              </View>

              <View style={styles.groupButton}>
                <Button
                  labelStyle={styles.updateLabel}
                  mode="contained"
                  style={styles.update}
                  onPress={updateTheApp}>
                  به روزرسانی
                </Button>
                <Button
                  style={styles.cancel}
                  color="#333"
                  onPress={handleClose}>
                  فعلا نه
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </>
    );
  }
  return null;
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  dialog: {
    width: 300,
    height: 200,
    borderRadius: 6,
    padding: 16,
    backgroundColor: '#ffffff',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  messages: {
    alignItems: 'center',
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  title: {
    fontSize: 16,
  },

  groupButton: {
    flexDirection: 'row',
  },
  update: {
    flex: 1,
  },
  updateLabel: {
    color: '#fff',
  },
  cancel: {
    width: 100,
  },
});
