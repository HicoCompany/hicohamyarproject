/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// screens
import MainScreen from './MainScreen';
import NoticeScreen from './NoticeScreen';
import AdvertisementScreen from './AdvertisementScreen';
import ManageUserScreen from './ManageUserScreen';
import DeliveryTypeScreen from './creditManagementScreen/DeliveryTypeScreen';

//utils
import Colors from '@src/utility/Colors';
import {
  AppState,
  BackHandler,
  InteractionManager,
  Platform,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { fetchNotificationCount } from '@src/redux/actions';
import { REQUESTS } from '@src/api';
import moment from 'jalali-moment';
import { ScaledSheet } from 'react-native-size-matters';
import { useRef } from 'react';
import { useAlert } from '@src/components/GlobalAlert';
import { useAndroidBackHandler } from 'react-navigation-backhandler';

const Tab = createBottomTabNavigator();

const tabStyles = ScaledSheet.create({
  tab: {
    backgroundColor: '#fff',
    height: Platform.OS === 'android' ? '52@s' : '50@s',
    direction: 'rtl',
  },
  tabItem: {},
});

/**
 *
 * @param {{navigation: import('@react-navigation/stack').StackNavigationProp<any,''>}} param0
 * @returns
 */
export default function Dashboard({ navigation }) {
  const dispatch = useDispatch();
  const Alert = useAlert();
  const backPressCounter = useRef(0);
  useAndroidBackHandler(() => {
    if (backPressCounter.current < 1) {
      backPressCounter.current += 1;
      InteractionManager.runAfterInteractions(() => {
        Alert.addToast({
          message: 'برای خروج از برنامه مجددا دکمه بازگشت را لمس کنید.',
          timeout: 1000,
        });
      });

      const timer = setTimeout(() => {
        backPressCounter.current = 0;
        clearTimeout(timer);
      }, 1000);
    } else {
      BackHandler.exitApp();
    }

    if (navigation.canGoBack()) {
      return false;
    }
    return true;
  });

  useEffect(() => {
    // fetch on mount
    handleAppActive(AppState.currentState);
    //fetch on app is in foreground
    AppState.addEventListener('change', handleAppActive);
    return () => {
      AppState.removeEventListener('change', handleAppActive);
    };
  }, []);

  const handleAppActive = useCallback((state) => {
    //check if app is in foreground
    if (state === 'active') {
      //fetch the count of notification that not viewed
      dispatch(fetchNotificationCount());
      // fetch list of wasteCollection request that completed but user didn't take survey
      REQUESTS.Waste.Survey.notSurveyedList()
        .then(({ data }) => {
          if (data.Success === 1) {
            const items = data.Result ?? [];
            // filter the statuses that aren't (3 === collected)
            const list = items.filter((el) => el.Status === 3);

            if (list.length) {
              //filter the items aren't collected less than 2 days ago
              const collectedLessThanTwoDaysAgo = list.filter(
                (el) =>
                  moment
                    .from(el.CollectionDateStr, 'fa', 'YYYY/MM/DD')
                    .diff(moment.now(), 'days') >= -2,
              );
              const DeliveryWasteID =
                collectedLessThanTwoDaysAgo[0]?.DeliveryWasteID;
              if (DeliveryWasteID) {
                navigation.navigate('WasteCollectionSurvey', {
                  DeliveryWasteID,
                });
              }
            }
          }
        })
        .catch((e) => {});
    }
  }, []);

  return (
    <Tab.Navigator
      initialRouteName="main"
      backBehavior="initialRoute"
      tabBarOptions={{
        style: tabStyles.tab,
        inactiveTintColor: Colors.gray,
        activeTintColor: Colors.green,
        showLabel: false,
        tabStyle: tabStyles.tabItem,
      }}>
      <Tab.Screen
        name="notice"
        component={NoticeScreen}
        options={NoticeScreen.navigationOptions}
      />
      <Tab.Screen
        name="advertise"
        component={AdvertisementScreen}
        options={AdvertisementScreen.navigationOptions}
      />

      <Tab.Screen
        name="user"
        component={ManageUserScreen}
        options={ManageUserScreen.navigationOptions}
      />
      <Tab.Screen
        name="delivery"
        component={DeliveryTypeScreen}
        options={DeliveryTypeScreen.navigationOptions}
      />

      <Tab.Screen
        name="main"
        component={MainScreen}
        options={MainScreen.navigationOptions}
      />
    </Tab.Navigator>
  );
}
