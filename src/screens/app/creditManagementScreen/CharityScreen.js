import React, { Component } from 'react';
import { View, StatusBar, TextInput } from 'react-native';
import { connect } from 'react-redux';

//ui components
import { Portal, Dialog, Button } from 'react-native-paper';
import { Label } from '@components';
import HeaderBack from '@components/HeaderBack';
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Icon,
  Picker,
} from 'native-base';

//utils
import Colors from '@src/utility/Colors';
import { hp, normalize, wp } from '@src/responsive';
import Utils from '@src/utility/Utils';
import EStyleSheet from 'react-native-extended-stylesheet';

//api
import Repository from '@src/repository/Repository';
import ComingSoon from '@src/components/ComingSoon';

class CharityScreen extends Component {
  FormName = 'خیریه';
  constructor(props) {
    super(props);
    this.state = {
      Payment: 0,
      CharityID: 0,
      CharityList: [],
      apiSuccess: false,
      apiSuccessDesc: '',
      apiSuccessTitle: '',
      apiError: false,
      apiErrorDesc: '',
      isLoadingButton: false,
    };
  }

  componentDidMount() {
    // this.getCharityApi();
  }

  async getCharityApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
    };
    try {
      const response = await Repository.PhoneGetCharityApi(params, headers);
      if (response.Success === 1) {
        const list = response.Result;
        this.setState({
          CharityList: list,
        });
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
      this.setState({
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };
  onConfirm = () => {
    // CharityID
    const Payment = this.state.Payment;

    let error = false;

    if (Payment.length === 0) {
      error = true;
      this.setState({
        errorPayment: 'لطفا مبلغ مورد نظر را وارد نمایید.',
      });
    }

    if (error) {
      return;
    }
    if (!error) {
      this.setState(
        {
          isLoadingButton: true,
        },
        () => {
          this.PhoneAddCharityPaymentApi();
        },
      );
    }
  };
  async PhoneAddCharityPaymentApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      Payment: this.state.Payment,
      CharityID: this.state.CharityID,
      FormName: this.FormName,
    };
    try {
      const response = await Repository.PhoneAddCharityPaymentApi(
        params,
        headers,
      );
      if (response.Success === 1) {
        this.setState({
          isLoadingButton: false,
          apiSuccess: true,
          apiSuccessDesc: response.Text,
          apiSuccessTitle: response.Titel,
          Payment: '',
        });
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
    } catch (error) {
      if (this.mounted) {
        this.setState({
          isLoadingButton: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
        });
      }
    }
  }

  renderCharityPickerItem() {
    return this.state.CharityList.map((Charity) => (
      <Picker.Item
        key={Charity.CharityID.toString()}
        label={`${Charity.Title}`}
        value={Charity.CharityID}
      />
    ));
  }

  handleRefresh = () => {
    this.setState(
      {
        apiError: false,
        isLoadingPage: true,
        list: [],
      },
      this.getCharityApi,
    );
  };

  renderArrowDownIcon = () => (
    <Icon type="MaterialIcons" name="keyboard-arrow-down" style={styles.icon} />
  );

  magnifyIcon = () => (
    <Icon type="FontAwesome" name="search" style={styles.icon} />
  );

  handleCharityChange = (CharityID) => {
    this.setState({ CharityID });
  };

  handleValueChange = (value) => {
    const Payment = parseInt(value, 10);
    this.setState({
      Payment: isNaN(Payment) ? 0 : Payment,
      errorPayment: '',
    });
  };

  render() {
    return (
      <>
        <Container>
          <Content>
            <StatusBar
              backgroundColor={Colors.statusBar}
              barStyle="light-content"
            />
            <HeaderBack headerTitle={' خیریه '} />
            <View style={[styles.Center]}>
              <View style={styles.remain}>
                <Label style={styles.remainText} weight="bold">
                  {this.state.Payment
                    ? 'مانده حساب بعد از کسر مبلغ'
                    : 'مانده حساب'}
                </Label>
                <Label style={styles.remainText} weight="bold">
                  {(this.props.credits.remain ?? 0) - this.state.Payment} تومان
                </Label>
              </View>
              <View style={styles.inputTotal}>
                {this.magnifyIcon()}

                <Picker
                  style={styles.pickerStyle}
                  textStyle={styles.pickerTextStyle}
                  iosHeader="خیریه مورد نظر را انتخاب کنید"
                  headerBackButtonText="بستن"
                  iosIcon={this.renderArrowDownIcon}
                  headerStyle={styles.pickerHeaderStyle}
                  headerTitleStyle={styles.pickerHeaderTextStyle}
                  headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                  itemStyle={styles.pickerItemStyle}
                  itemTextStyle={styles.pickerItemTextStyle}
                  selectedValue={this.state.CharityID}
                  onValueChange={this.handleCharityChange}
                  mode={'dropdown'}>
                  <Picker.Item label={'انتخاب کنید'} value={null} />
                  {this.renderCharityPickerItem()}
                </Picker>
              </View>
            </View>
            <View style={[styles.Center]}>
              <View style={styles.inputTotal}>
                <Label style={[styles.text, styles.icon]}>تومان</Label>
                <TextInput
                  placeholder="مبلغ مورد نظر "
                  style={styles.input}
                  placeholderTextColor={'#50b3ae'}
                  value={this.state.Payment}
                  keyboardType="numeric"
                  onChangeText={this.handleValueChange}
                />
                <Icon type="Entypo" name="database" style={styles.icon} />
              </View>
            </View>

            <Portal>
              <Dialog
                visible={this.state.apiError}
                style={styles.dialogContainer}
                onDismiss={this.props.navigation.goBack}
                dismissable={true}>
                <Dialog.Content>
                  <Label style={styles.dialogText}>
                    {this.state.apiErrorDesc}
                  </Label>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button onPress={this.handleRefresh}>تلاش مجدد</Button>
                </Dialog.Actions>
              </Dialog>
              <Dialog
                visible={this.state.apiSuccess}
                style={styles.dialogContainer}
                onDismiss={() => {
                  this.setState({
                    apiSuccess: false,
                  });
                }}>
                <Dialog.Title style={styles.dialogTitleText}>
                  {this.state.apiSuccessTitle}
                </Dialog.Title>
                <Dialog.Content style={styles.dialogContent}>
                  <Label style={styles.dialogText}>
                    {this.state.apiSuccessDesc}
                  </Label>
                  <Button
                    onPress={() => {
                      this.setState({
                        apiSuccess: false,
                      });
                    }}>
                    بستن
                  </Button>
                </Dialog.Content>
              </Dialog>
            </Portal>
          </Content>
          <Footer style={styles.footer}>
            <FooterTab style={styles.footerContent}>
              <Button
                onPress={this.onConfirm}
                style={styles.BtnFooter}
                disabled={this.state.isLoadingButton}
                loading={this.state.isLoadingButton}
                labelStyle={styles.textFooter}>
                {this.state.isLoadingButton
                  ? 'لطفا منتظر بمانید'
                  : 'پرداخت مبلغ خیریه'}
              </Button>
            </FooterTab>
          </Footer>
        </Container>
        <ComingSoon />
      </>
    );
  }
}

export const styles = EStyleSheet.create({
  footer: { height: 70, width: '100%' },
  footerContent: { backgroundColor: Colors.green },
  header: {
    backgroundColor: '#50b3ae',
  },
  text: {
    color: 'black',
    fontSize: 12,
  },
  inputTotal: {
    borderColor: '#b3afaf',
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    marginTop: 20,
    paddingHorizontal: 10,
    width: '90%',
  },
  input: {
    fontSize: 12,
    color: 'black',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    flex: 1,
    paddingHorizontal: 10,
    textAlign: 'center',
  },
  textFooter: {
    color: 'white',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    fontSize: 14,
  },
  BtnFooter: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  TotalText: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  BtnShow: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: '#50b3ae',
    borderRadius: 5,
    padding: 10,
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pickerContainer: {
    height: hp('6%'),
    width: wp('70%'),
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
  pickerStyle: {
    width: wp('50%'),
  },
  pickerHeaderStyle: {
    backgroundColor: Colors.purple,
  },
  pickerHeaderTextStyle: {
    width: wp('50%'),
    backgroundColor: 'transparent',
    fontSize: 16,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '300',
    },
    '@media android': {
      fontFamily: '$IR_L',
    },
    color: 'white',
    textAlign: 'right',
  },
  pickerBackHeaderTextStyle: {
    width: wp('50%'),
    backgroundColor: 'transparent',
    fontSize: 16,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '300',
    },
    '@media android': {
      fontFamily: '$IR_L',
    },
    color: 'white',
    textAlign: 'left',
  },
  pickerTextStyle: {
    fontSize: 16,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '300',
    },
    '@media android': {
      fontFamily: '$IR_L',
    },
    color: '#34495e',
    textAlign: 'center',
  },
  pickerItemStyle: {
    width: wp('90%'),
    backgroundColor: 'transparent',
  },
  pickerItemTextStyle: {
    fontSize: 16,
    width: wp('65%'),
    backgroundColor: 'transparent',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    color: '#34495e',
    textAlign: 'right',
  },
  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
    fontWeight: 'bold',
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.textBlack,
  },
  icon: {
    color: '#50b3ae',
  },
  remain: {
    flexDirection: 'row-reverse',
    backgroundColor: Colors.green,
    width: '95%',
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 50,
    margin: 10,
  },
  remainText: {
    color: '#ffffff',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    credits: state.global.credits,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(CharityScreen);
