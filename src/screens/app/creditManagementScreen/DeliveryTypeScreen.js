import React, { Component } from 'react';
import { View, StatusBar, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

//ui components
import { Container, Content } from 'native-base';
import TabItem from '@src/components/TabItem';
import { Label } from '@components';
//utils
import Colors from '@src/utility/Colors';
import { normalize } from '@src/responsive';
import Section from '@src/components/sections';
import { fetchCredits } from '@src/redux/actions';

// assets
import WASTE from '@src/assets/image/ic_waste.png';
import HelpToKheyrieh from '@src/assets/image/HelpToKheyrieh.png';
import SPECIAL_ICON from '@src/assets/image/SpecialService.png';
import PRODUCT_ICON from '@src/assets/image/Kala.png';
import PRODUCT_REQUESTS_ICON from '@src/assets/image/RKala.png';

class DeliveryTypeScreen extends Component {
  static navigationOptions = () => ({
    tabBarIcon: ({ color }) => (
      <TabItem name={'مدیریت اعتبار'} source={WASTE} color={color} />
    ),
  });

  componentDidMount() {
    this.props.fetchCredits();
  }

  handleNavigationToCharityScreen = () => {
    this.props.navigation.navigate('CharityScreen');
  };
  handleNavigationToSpecialServiceScreen = () => {
    this.props.navigation.navigate('SpecialServiceScreen');
  };
  handleNavigationToProductScreen = () => {
    this.props.navigation.navigate('ProductScreen');
  };
  handleNavigationToRequestProductScreen = () => {
    this.props.navigation.navigate('RequestProductScreen');
  };

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <StatusBar
              backgroundColor={Colors.statusBar}
              barStyle="light-content"
            />
            <View style={styles.header}>
              <View style={styles.headerItems}>
                <View style={styles.headerItem}>
                  <Label style={styles.headerItemTitle} color="placeholder">
                    مانده حساب من:
                  </Label>
                  {this.props.credits.loading ? (
                    <ActivityIndicator color={Colors.green} />
                  ) : (
                    <Label style={styles.headerItemValue} weight="bold">
                      {`${this.props.credits.remain ?? '...'} تومان`}
                    </Label>
                  )}
                </View>
                <View style={styles.headerItem}>
                  <Label style={styles.headerItemTitle} color="placeholder">
                    امتیاز من:
                  </Label>
                  {this.props.credits.loading ? (
                    <ActivityIndicator color={Colors.green} />
                  ) : (
                    <Label style={styles.headerItemValue} weight="bold">
                      {this.props.credits.points ?? '...'}
                    </Label>
                  )}
                </View>
              </View>
            </View>

            <View style={styles.inlineBetween}>
              <Section
                label="کمک به خیریه"
                source={HelpToKheyrieh}
                onPress={this.handleNavigationToCharityScreen}
              />
              <Section
                label="خدمات ویژه "
                source={SPECIAL_ICON}
                onPress={this.handleNavigationToSpecialServiceScreen}
              />
              <Section
                label="ارائه کالا"
                source={PRODUCT_ICON}
                onPress={this.handleNavigationToProductScreen}
              />
              <Section
                label="لیست درخواست ها"
                source={PRODUCT_REQUESTS_ICON}
                onPress={this.handleNavigationToRequestProductScreen}
              />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  badge: {
    position: 'absolute',
    top: 8,
    right: 10,
  },
  BtnImg: {
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  bgProduct: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#fafafa',
    borderBottomWidth: 1,
    borderBottomColor: '#f0f0f0',
  },
  BoxProduct: {
    width: 160,
    height: 160,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  ImgProduct: {
    width: '85%',
    height: 80,
    resizeMode: 'contain',
  },

  textRed: {
    color: '#e53935',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    textDecorationColor: '#000',
  },
  BtnProduct: {
    borderWidth: 1,
    borderColor: '#f0f0f0',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  BtnProductOne: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: 'green',
  },
  BtnProductTwo: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
  },
  textBtnProduct: {
    color: 'white',
    fontSize: 11,
  },
  BoxBtnSort: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#bdbbbb',
    width: 330,
  },
  BtnSort: {
    width: 160,
    height: 40,
    paddingHorizontal: 4,
    backgroundColor: '#fafafa',
    alignItems: 'center',
  },
  header: {
    backgroundColor: Colors.green,
    minHeight: 80,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    padding: 16,
    alignItems: 'flex-end',
    marginBottom: 30,
  },
  headerItems: {
    // backgroundColor: '#fff',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 20,
    position: 'absolute',
    top: 20,
    // elevation: 2,
    // height: 50,
    // paddingBottom: 10,
    flexDirection: 'row-reverse',
    flexWrap: 'wrap',
  },
  headerItem: {
    backgroundColor: '#fff',
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    margin: 5,
    marginBottom: 2,
    elevation: 3,
    flex: 1,
    height: 80,
  },

  headerItemTitle: {
    fontSize: normalize(10),
    marginStart: 10,
    position: 'absolute',
    top: 5,
    right: 10,
  },
  headerItemValue: {
    fontSize: normalize(16),
    textAlign: 'right',
    alignSelf: 'center',
    color: Colors.green,
  },
  inlineBetween: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 16,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    credits: state.global.credits,
  };
};

const mapActionToProps = {
  fetchCredits,
};

export default connect(mapStateToProps, mapActionToProps)(DeliveryTypeScreen);
