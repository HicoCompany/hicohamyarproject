import React, { useCallback, useEffect, useMemo } from 'react';
import {
  View,
  FlatList,
  RefreshControl,
  StyleSheet,
  Platform,
  Linking,
} from 'react-native';

//ui components
import HeaderBack from '@src/components/HeaderBack';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableRipple } from 'react-native-paper';
import BoothAccordion from '@components/BoothComponents/BoothAccordion';
import Label from '@components/Label';
import Colors from '@src/utility/Colors';

import BOOTH_ICON from '@src/assets/image/boothIcon.png';
import { useBoothes } from '@src/api/modules/Booth';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import { Loading } from '@src/components';
import EmptyList from '@src/components/EmptyList';

/**
 *
 * @param {{navigation: import('@react-navigation/stack').NavigationStackScreenProps}} param0
 */
export default function BoothsScreen({ navigation }) {
  const Alert = useAlert();
  const [state, handler] = useBoothes();

  useEffect(() => {
    if (state.error) {
      Alert.setError(
        DefaultModalActions.Error({
          message: state.message,
          onDismiss: handler.handleRefreshing,
        }),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.error, state.message]);

  const handleBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const renderRefreshControl = useMemo(
    () => (
      <RefreshControl
        refreshing={state.refreshing}
        colors={[Colors.green]}
        onRefresh={handler.handleRefreshing}
      />
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [state.refreshing],
  );

  const handleOpenInMap = (lat, lng) => async () => {
    const scheme = Platform.select({
      ios: 'maps:0,0?q=',
      android: 'geo:0,0?q=',
    });
    const latlng = `${lng},${lat}`;

    const url = Platform.select({
      ios: `${scheme}@${latlng}`,
      android: `${scheme}${latlng}`,
    });

    (await Linking.canOpenURL(url))
      ? Linking.openURL(url)
      : Alert.addToast({
          message: 'امکان نمایش آدرس روی نقشه وجود ندارد.',
          type: 'error',
        });
  };

  const handleRenderItem = useCallback(
    ({ item }) => (
      <BoothAccordion
        style={styles.item}
        title={item.Title}
        IconSource={BOOTH_ICON}>
        <Label color="placeholder">پرسنل</Label>
        <Label>{item.PersonlName}</Label>
        <Label color="placeholder">روز های فعالیت غرفه</Label>
        <Label>{item.BoothWorkTime || ''}</Label>
        <Label color="placeholder">آدرس</Label>
        <Label style={styles.lastItem}>{item.Address}</Label>
        <TouchableRipple
          style={styles.button}
          onPress={handleOpenInMap(
            item.LatitudeLocation,
            item.LongitudeLocation,
          )}>
          <Label style={styles.button}>نمایش روی نقشه</Label>
        </TouchableRipple>
      </BoothAccordion>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const handleExtractKey = useCallback((item) => `booth-${item.BoothId}`, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <HeaderBack onBackPress={handleBack} headerTitle={'غرفه ها'} />
      </View>
      {state.loading ? (
        <Loading />
      ) : (
        <FlatList
          data={state.data}
          contentContainerStyle={styles.listContent}
          style={styles.listContainer}
          refreshControl={renderRefreshControl}
          renderItem={handleRenderItem}
          keyExtractor={handleExtractKey}
          ListEmptyComponent={
            <EmptyList
              message={'غرفه ایی برای نمایش وجود ندارد.'}
              icon="tent"
            />
          }
        />
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
  header: {
    zIndex: 10,
  },
  listContainer: {
    width: '100%',
  },
  listContent: {
    paddingVertical: 16,
  },
  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  item: {},
  button: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.green,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderRadius: 6,
    color: 'white',
    alignSelf: 'center',
  },
  lastItem: {
    marginBottom: 10,
  },
});
