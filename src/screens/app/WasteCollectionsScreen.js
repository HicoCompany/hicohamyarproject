import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  StatusBar,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
} from 'react-native';
import Colors from '@src/utility/Colors';
import HeaderBack from '@components/HeaderBack';
import { Dialog, Portal } from 'react-native-paper';
import Utils from '@src/utility/Utils';
import { normalize, wp } from '@src/responsive';
import { Button, Empty, Loading } from '@src/components';
import { RequestHeader, CardItem } from '@src/components/RequestComponents';
import { SafeAreaView } from 'react-native-safe-area-context';

import ActionDialog from '@src/components/ActionDialog';
import { Label } from 'native-base';
import {
  getWasteRequestsInformation,
  getWasteCollectionRequests,
  cancelWasteCollectionRequest,
} from '@src/api/modules/Waste';
import { fetchCredits } from '@src/redux/actions';

class RequestListScreen extends Component {
  FormName = 'درخواست های جمع آوری پسماند';

  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      listRequest: [],
      refreshing: false,
      loadingData: false,
      apiError: false,
      apiErrorDesc: '',
      deleteDialog: false,
      deleteLoading: false,
    };
  }

  componentDidMount = () => {
    this.props.fetchCredits();
    this.getApi();
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };

  getApi = async () => {
    await this.PhoneGetDeliverWasteHelperApi();
  };

  async PhoneGetDeliverWasteHelperApi() {
    try {
      const { data } = await getWasteCollectionRequests();

      this.setState({
        listRequest: data,
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
        deleteDialog: false,
      });
    }
  }

  async PhoneDelRequestDeliverWasteApi(DeliveryWasteID) {
    try {
      await cancelWasteCollectionRequest({
        deliveryWasteId: DeliveryWasteID,
      });

      this.setState({
        ApiSuccess: true,
        ApiTitleSuccess: `درخواست شماره ${DeliveryWasteID} حذف شد.`,
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        deleteLoading: false,
        deleteDialog: false,
      });
    } catch (error) {
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
        deleteLoading: false,
        deleteDialog: false,
      });
    }
  }
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };
  handleLoadMore = () => {
    // if (this.state.countList > 0) {
    //     this.setState({
    //         offset: this.state.offset + this.state.pageSize, loadingData: true
    //     }, async () => {
    //         await this.phoneGetNotificationForHelperApi();
    //     });
    // }
  };

  handleRefresh = () => {
    this.setState({
      refreshing: true,
      isLoadingPage: true,
    });
    this.PhoneGetDeliverWasteHelperApi();
  };

  renderEmpty = () => {
    return this.state.isLoadingPage ? (
      <View style={styles.grow}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.grow}>
        <Empty />
      </View>
    );
  };

  renderFooter = () => {
    if (!this.state.loadingData) {
      return null;
    }
    return <ActivityIndicator size={'small'} color={'black'} />;
  };

  dismissDeleteDialog = () => {
    this.setState((state) => ({ ...state, deleteDialog: false }));
  };
  openDeleteDialog = (DeliveryWasteID) => () => {
    this.setState((state) => ({
      ...state,
      deleteDialog: true,
      deleteId: DeliveryWasteID,
    }));
  };

  handleDelete = () => {
    if (!this.state.deleteId.toString()) return;
    this.setState(
      {
        ApiSuccess: false,
        isLoadingPage: true,
        deleteLoading: true,
      },
      async () => {
        await this.PhoneDelRequestDeliverWasteApi(this.state.deleteId);
        await this.PhoneGetDeliverWasteHelperApi();
      },
    );
  };

  handleDetail = (DeliveryWasteID) => () => {
    this.props.navigation.navigate('WasteCollectionDetail', {
      DeliveryWasteID,
      apiSuccess: 0,
    });
  };

  handleEdit = (item) => () => {
    this.props.navigation.navigate('WasteCollectionEditForm', {
      request: item,
      sendResult: () => {
        this.handleRefresh();
      },
    });
  };

  handleSurvey = (DeliveryWasteID) => () => {
    this.props.navigation.navigate('WasteCollectionSurvey', {
      DeliveryWasteID: DeliveryWasteID,
      onDone: this.handleRefresh,
    });
  };

  renderItem = ({ item }) => {
    const {
      DeliverDateCreate,
      CreateDateStr,
      CreateTimeStr,
      DeliveryDateStr,
      SettingCollectFromAndToTime,
      DeliveryWasteID,
      StatusStr,
      Status,
      DeliveryCode,
      CollectionDateStr,
      Editable,
      Cancelable,
      Commentable,
    } = item;

    return (
      <CardItem
        deliverDate={`${DeliveryDateStr} ${SettingCollectFromAndToTime}`}
        date={DeliverDateCreate}
        status={StatusStr}
        statusID={Status}
        onDetailClick={this.handleDetail(DeliveryWasteID)}
        onDelete={this.openDeleteDialog(DeliveryWasteID)}
        onEdit={this.handleEdit(item)}
        onSurveyPress={this.handleSurvey(DeliveryWasteID)}
        style={styles.item}
        code={DeliveryCode}
        createdDate={`${CreateDateStr} - ${CreateTimeStr}`}
        collectedDate={CollectionDateStr}
        editable={Editable}
        cancelable={Cancelable}
        commentable={Commentable}
      />
    );
  };

  handleKeyExtractor = (item, i) => `${item.DeliveryWasteID}-${i}`;

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={'لیست درخواست ها'} />
        <View style={styles.content}>
          <RequestHeader
            requestCount={this.props.credit.requestsDeliveryCount}
            deliveryCount={this.props.credit.deliveredCount}
            points={this.props.credit.points}
          />
        </View>
        <View>
          <FlatList
            data={this.state.listRequest}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderEmpty}
            ListFooterComponent={this.renderFooter}
            keyExtractor={this.handleKeyExtractor}
            refreshControl={
              <RefreshControl
                colors={[Colors.green]}
                refreshing={this.state.refreshing}
                onRefresh={this.handleRefresh}
              />
            }
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.08}
            contentContainerStyle={
              this.state.listRequest.length > 0
                ? styles.listContent
                : styles.grow
            }
          />
        </View>

        <Portal>
          <Dialog
            visible={this.state.apiError}
            style={styles.dialogContainer}
            dismissable
            onDismiss={() => {
              this.setState({
                apiError: false,
              });
            }}>
            <Dialog.Content>
              <Label style={styles.dialogText}>{this.state.apiErrorDesc}</Label>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                buttonText={'تلاش مجدد'}
                onPress={() => {
                  this.setState(
                    {
                      apiError: false,
                      isLoadingPage: true,
                      list: [],
                    },
                    () => {
                      this.getApi();
                    },
                  );
                }}
                style={styles.dialogButton}
              />
              <Button
                buttonText={'کنسل'}
                onPress={() => {
                  this.setState({
                    apiError: false,
                  });
                }}
                style={styles.dialogButton}
              />
            </Dialog.Actions>
          </Dialog>
          {/* <Dialog
            visible={this.state.ApiSuccess}
            style={styles.dialogContainer}
            dismissable
            onDismiss={() => {
              this.setState({
                ApiSuccess: false,
              });
            }}>
            <Dialog.Content>
              <Label style={styles.dialogText}>
                {this.state.ApiTitleSuccess}
              </Label>
            </Dialog.Content>
          </Dialog> */}
          <ActionDialog
            onDismiss={this.dismissDeleteDialog}
            onSubmit={this.handleDelete}
            visible={this.state.deleteDialog}
            message={'آیا از حذف این درخواست مطمئن هستید؟'}
            loading={this.state.deleteLoading}
            submitColor="danger"
          />
        </Portal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: 6,
  },
  listContent: {
    padding: 16,
    paddingBottom: 300,
  },
  item: {
    marginBottom: 16,
  },
  grow: { flex: 1 },
  header: {
    backgroundColor: '#50b3ae',
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    credit: state.global.credits,
  };
};

const mapActionToProps = {
  fetchCredits,
};
export default connect(mapStateToProps, mapActionToProps)(RequestListScreen);
