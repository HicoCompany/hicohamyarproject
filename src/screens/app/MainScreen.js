/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect } from 'react';
import { View, StatusBar, BackHandler } from 'react-native';

//redux
import { useDispatch, useSelector } from 'react-redux';
import { setIsSeenTour, setIsSeenWelComeMessage } from '@src/redux/actions';

//ui components
import { Container, Content } from 'native-base';
import HeaderMain from '@components/HeaderMain';
import Section from '@components/sections';
import TabItem from '@components/TabItem';

// utils
import Colors from '@src/utility/Colors';

//assets
import BOOTH_ICON from '@src/assets/image/boothIcon.png';
import WASTE_ICON from '@src/assets/image/waste.png';
import REPORT_ICON from '@src/assets/image/Report.png';
import REQUEST_ICON from '@src/assets/image/Request.png';
import RESIDUE_ICON from '@src/assets/image/ic_residueR.png';

import EStyleSheet from 'react-native-extended-stylesheet';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';

function MainPageScreen({ navigation }) {
  // const closeAppOnNextBackPress = useRef(false);
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.userData);
  const welcomeMessage = useSelector((state) => state.global.welcomeMessage);
  const Alert = useAlert();

  useEffect(() => {
    if (!userData?.isSeenWelComeMessage && welcomeMessage) {
      Alert.setAction(
        DefaultModalActions.Message({
          message: welcomeMessage,
          ConfirmLabel: 'ورود',
          dismissable: true,
        }),
      );
      dispatch(setIsSeenWelComeMessage());
    }
  }, [welcomeMessage]);

  useEffect(() => {
    if (!userData?.isSeenTour) {
      dispatch(setIsSeenTour());
    }
  }, []);

  const handleBoothsScreenNavigation = useCallback(() => {
    navigation.navigate('BoothsScreen');
  }, []);

  const handleDeliveryScreenNavigation = useCallback(() => {
    navigation.navigate('WasteSelectionList');
  }, []);
  const handleReportScreenNavigation = useCallback(() => {
    navigation.navigate('Report');
  }, []);

  const handleRequestScreenNavigation = useCallback(() => {
    navigation.navigate('WasteCollections');
  }, []);

  const handleUsedProduct = useCallback(() => {
    navigation.navigate('UsedGoods');
  }, []);

  return (
    <Container>
      <StatusBar backgroundColor={Colors.statusBar} barStyle="light-content" />
      <HeaderMain
        onMenuPress={BackHandler.exitApp}
        onPinPress={handleBoothsScreenNavigation}
        headerTitle="صفحه اصلی"
      />
      <Content>
        <View style={styles.container}>
          <Section
            label="تحویل وسایل مستعمل"
            source={RESIDUE_ICON}
            onPress={handleUsedProduct}
          />
          <Section
            label="تحویل پسماند خشک"
            source={WASTE_ICON}
            onPress={handleDeliveryScreenNavigation}
          />
          <Section label="درخواست تحویل پکیج" source={RESIDUE_ICON} disabled />
          <Section
            label="غرفه ها"
            source={BOOTH_ICON}
            onPress={handleBoothsScreenNavigation}
          />

          <Section
            label="گزارشات"
            source={REPORT_ICON}
            onPress={handleReportScreenNavigation}
          />
          <Section
            label="درخواست ها"
            source={REQUEST_ICON}
            onPress={handleRequestScreenNavigation}
          />
        </View>
      </Content>
    </Container>
  );
}

MainPageScreen.navigationOptions = () => {
  return {
    tabBarIcon: ({ color }) => {
      return (
        <TabItem
          name={'صفحه اصلي'}
          source={require('@src/assets/image/ic_home.png')}
          color={color}
        />
      );
    },
  };
};

export const styles = EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 16,
  },
});

export default MainPageScreen;
