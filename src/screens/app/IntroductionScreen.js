import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, Share } from 'react-native';
import Colors from '@src/utility/Colors';
import HeaderBack from '@components/HeaderBack';
import Repository from '@src/repository/Repository';

import { View } from 'react-native';
import { Container, Content, Text, Card, CardItem } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { ActivityIndicator, Button } from 'react-native-paper';

//ui components
import Label from '@components/Label';
import ErrorDialog from '@src/components/ErrorDialog';
import { getWasteRequestsInformation } from '@src/api/modules/Waste';
import { ScaledSheet } from 'react-native-size-matters';

class IntroductionScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      horizontal: false,
      ReagentCode: undefined,
      apiError: false,
      apiErrorDesc: '',
      isLoadingPage: true,
    };
  }
  onShare = async () => {
    try {
      const code = `کد دعوت: ${this.state.ReagentCode}`;
      const message =
        'اپلیکیشن همیار زیست دوست رو نصب و به پاکی محیط زیست کمک کنید و از مزایای ویژه برخودار شوید.';
      const result = await Share.share({
        title: 'دعوت از دوستان',
        message: `${message}\n${code}${
          this.props.marketLink ? '\n' + this.props.marketLink : ''
        }`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      // alert(error.message);
    }
  };

  componentDidMount = () => {
    this.getApi();
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };
  getApi = async () => {
    await this.PhoneSettingsForHelperApi();
  };
  async PhoneSettingsForHelperApi() {
    try {
      this.setState({
        ReagentCode: (await getWasteRequestsInformation()).data.ReagentCode,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  handleDismissError = () => {
    this.setState({
      apiError: false,
      apiErrorDesc: '',
    });
  };

  render() {
    return (
      <>
        <Container>
          <StatusBar
            backgroundColor={Colors.statusBar}
            barStyle="light-content"
          />
          <HeaderBack headerTitle={'دعوت از دوستان '} />
          <View style={styles.container}>
            <View style={styles.codeContainer}>
              <Label style={styles.codeLabel}>کد معرف</Label>

              {this.state.isLoadingPage ? (
                <ActivityIndicator
                  style={styles.code}
                  color="white"
                  size={'small'}
                />
              ) : (
                <Label style={styles.code}>
                  {this.state.ReagentCode || '...'}
                </Label>
              )}

              <Label style={styles.codeMessage}>
                {'با معرفی به دوستان و آشنایان خود , به حفظ محیط زیست کمک کنید'}
              </Label>
            </View>

            <View style={styles.shareButtonContainer}>
              <Button
                style={styles.shareButton}
                onPress={this.onShare}
                mode="contained"
                labelStyle={styles.shareButtonLabel}>
                ارسال کد
              </Button>
            </View>
          </View>
        </Container>
        <ErrorDialog
          open={this.state.apiError}
          message={this.state.apiErrorDesc}
          handleClose={this.handleDismissError}
        />
      </>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  codeContainer: {
    flex: 1,
    backgroundColor: Colors.green,
    justifyContent: 'center',
    alignItems: 'center',
  },
  codeLabel: {
    fontSize: 30,
    color: '#fff',
  },
  code: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff',
    height: '50@s',
  },
  codeMessage: {
    color: '#fff',
    position: 'absolute',
    bottom: 10,
    fontSize: 14,
    flexWrap: 'wrap',
    textAlign: 'center',
  },
  shareButtonContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  shareButton: {
    // backgroundColor: Colors.green,
    height: 50,
    justifyContent: 'center',
    width: '50%',
    alignSelf: 'center',
    borderRadius: 10,
  },
  shareButtonLabel: {
    color: '#fff',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    marketLink: state.global.marketLink,
  };
};

const mapActionToProps = {};
export default connect(mapStateToProps, mapActionToProps)(IntroductionScreen);
