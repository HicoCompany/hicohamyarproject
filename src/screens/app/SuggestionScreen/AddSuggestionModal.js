import { Label } from '@src/components';
import TextInput from '@src/components/TextInput';
import Colors from '@src/utility/Colors';
import React, { useCallback, useRef, useState } from 'react';
import { Dimensions, ScrollView, StyleSheet, View } from 'react-native';
import { Button } from 'react-native-paper';

import Modal from 'react-native-modal';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';

import { useSubmitNewSuggestion } from '@src/api/modules/Suggestion';

export default function AddSuggestionModal({
  visible = false,
  onDismiss,
  onSubmit,
}) {
  const Alert = useAlert();
  const [state, setState] = useState({ title: '', message: '' });

  const inputRef = useRef();

  const [submitState, submitHandlers] = useSubmitNewSuggestion({
    ...state,
    formName: AddSuggestionModal.FormName,
  });

  const handleChangeTitle = useCallback((title) => {
    setState((prev) => ({ ...prev, title }));
  }, []);

  const handleChangeMessage = useCallback((message) => {
    setState((prev) => ({ ...prev, message }));
  }, []);

  const handleConfirmModalOpening = ({ loading }) => {
    Alert.setAction(
      DefaultModalActions.Ask({
        onConfirm: handleSubmit,
        onDenied: Alert.clearAction,
        message: 'آیا از ارسال پیام خود مطمئن هستید؟',
        loading,
      }),
    );
  };

  const handleSubmit = () => {
    handleConfirmModalOpening({ loading: true });
    submitHandlers
      .handleFetch()
      .then(() => {
        setState({ title: '', message: '' });
        Alert.addToast({
          message: 'با موفقیت انجام شد',
          type: 'success',
          timeout: 'short',
        });
        onSubmit();
        onDismiss();
      })
      .catch((error) => {
        if (error.done) {
          Alert.setError(
            DefaultModalActions.Error({
              message: error.message,
              onDismiss: Alert.clearError,
            }),
          );
        } else {
          Alert.setAction(
            DefaultModalActions.RetryOrBack({
              onRefresh: handleSubmit,
              message: error.message,
              onDismiss: Alert.clearAction,
            }),
          );
        }
      });
  };

  const handleFocusOnMessage = useCallback(() => {
    inputRef.current.focus();
  }, [inputRef]);

  return (
    <Modal
      isVisible={visible}
      swipeDirection="down"
      onSwipeComplete={onDismiss}
      animationIn="slideInUp"
      backdropOpacity={0.1}
      onBackdropPress={onDismiss}
      onBackButtonPress={onDismiss}
      style={styles.modal}
      avoidKeyboard
      onDismiss={onDismiss}>
      <View style={styles.modalContent}>
        <View style={styles.line} />

        <ScrollView>
          <View style={styles.row}>
            <Label style={styles.cardTitleText} color="placeholder">
              عنوان :
            </Label>
            <TextInput
              placeholder="عنوان"
              autoFocus
              mode="none"
              value={state.title}
              blurOnSubmit={false}
              onChangeText={handleChangeTitle}
              onSubmitEditing={handleFocusOnMessage}
              style={styles.inputs}
            />
          </View>
          <TextInput
            placeholder="پیام خود را بنویسید"
            mode=""
            onChangeText={handleChangeMessage}
            value={state.message}
            inputRef={inputRef}
            style={styles.inputs}
            onSubmitEditing={handleConfirmModalOpening}
          />
        </ScrollView>
        <View style={styles.row}>
          <Button
            onPress={handleConfirmModalOpening}
            loading={submitState.loading}>
            ارسال
          </Button>
          <Button color={Colors.error} onPress={onDismiss}>
            لغو
          </Button>
        </View>
      </View>
    </Modal>
  );
}

const { height } = Dimensions.get('window');
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  cardTitleText: {
    fontSize: 16,
    textAlign: 'right',
    paddingRight: 10,
  },
  cardTitle: {
    minHeight: 20,
  },

  line: {
    // borderWidth: 1,
    width: '20%',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 15,
    marginTop: -5,
    height: 2,
    backgroundColor: Colors.gray,
  },
  modal: {
    padding: 0,
    margin: 0,
  },
  modalContent: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    minHeight: height / 4,
    maxHeight: height - 50,
    backgroundColor: 'white',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    left: 0,
    right: 0,
    padding: 16,
  },
  inputs: {
    flex: 1,
  },
});
