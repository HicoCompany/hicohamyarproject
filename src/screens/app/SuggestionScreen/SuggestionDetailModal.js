import { Label } from '@src/components';
import Colors from '@src/utility/Colors';
import React from 'react';
import { Dimensions, ScrollView, StyleSheet, View } from 'react-native';

//
import Modal from 'react-native-modal';
import { Card } from 'react-native-paper';

const { height, width } = Dimensions.get('window');

export default function SuggestionDetailModal({
  visible,
  onDismiss,
  suggestion,
}) {
  return (
    <Modal
      isVisible={visible}
      swipeDirection="down"
      onSwipeComplete={onDismiss}
      animationIn="slideInUp"
      backdropOpacity={0.1}
      onBackdropPress={onDismiss}
      onBackButtonPress={onDismiss}
      deviceHeight={height}
      deviceWidth={width}
      style={styles.modal}
      statusBarTranslucent
      onDismiss={onDismiss}>
      <View style={styles.modalContent}>
        <View style={styles.line} />
        <View style={styles.spaceBetween}>
          <View style={styles.row}>
            <Label style={styles.cardTitleText} color="placeholder">
              عنوان :‌{' '}
            </Label>
            <Label style={styles.cardTitleText}>{suggestion?.Subject}</Label>
          </View>
          <Label>{suggestion.PMessageDate}</Label>
        </View>
        <ScrollView>
          <Card style={[styles.message, styles.sendedMessage]}>
            <Card.Content>
              <Label>{suggestion?.MessageText}</Label>
            </Card.Content>
          </Card>
          {suggestion?.ResponseText ? (
            <Card style={[styles.message, styles.receivedMessage]}>
              <Card.Content>
                <Label>{suggestion?.ResponseText}</Label>
              </Card.Content>
            </Card>
          ) : (
            <Label style={styles.waitingForAnswer}>
              در انتظار پاسخ پشتیبان
            </Label>
          )}
        </ScrollView>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  spaceBetween: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cardTitleText: {
    fontSize: 16,
    textAlign: 'right',
    paddingRight: 10,
  },
  line: {
    width: '20%',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 15,
    marginTop: -5,
    height: 2,
    backgroundColor: Colors.gray,
  },
  modal: {
    padding: 0,
    margin: 0,
  },
  modalContent: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    minHeight: height / 4,
    maxHeight: height - 50,
    backgroundColor: 'white',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    left: 0,
    right: 0,
    padding: 16,
  },
  message: {
    width: '80%',
    borderWidth: 1,
    minWidth: '30%',
    flexGrow: 0,
    paddingHorizontal: 8,
    marginTop: 10,
    borderRadius: 20,
    alignSelf: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  sendedMessage: {
    borderTopRightRadius: 0,
    borderColor: Colors.green,
    marginLeft: width / 10,
  },
  receivedMessage: {
    borderTopLeftRadius: 0,
    borderColor: '#333',
    marginRight: width / 10,
  },
  waitingForAnswer: {
    textAlign: 'center',
    marginTop: 20,
  },
});
