import HeaderBack from '@src/components/HeaderBack';
import Colors from '@src/utility/Colors';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useState,
  Fragment,
} from 'react';
import {
  StatusBar,
  StyleSheet,
  RefreshControl,
  Dimensions,
  View,
  FlatList,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

// ui
import { FAB, Card } from 'react-native-paper';

import store from '@src/redux/store';
import {
  makeSuggestionReadByUser,
  modifyNotificationCount,
} from '@src/redux/actions';
import Label from '@src/components/Label';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useGetSuggestions } from '@src/api/modules/Suggestion';

import AddSuggestionModal from './AddSuggestionModal';
import SuggestionDetailModal from './SuggestionDetailModal';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';

const { height } = Dimensions.get('screen');
export default function SuggestionListScreen() {
  const Alert = useAlert();
  const [detailModal, setDetailModal] = useState({
    isModalOpen: false,
    SuggestionId: null,
  });

  const [newSuggestion, setNewSuggestion] = useState(false);
  const [suggestionState, suggestionHandlers] = useGetSuggestions({
    onError({ message }) {
      Alert.setError(
        DefaultModalActions.Error({
          message,
        }),
      );
    },
  });

  const selectedSuggestion = useMemo(
    () =>
      suggestionState.data?.find?.(
        (v) => v.SuggestionId === detailModal.SuggestionId,
      ) ?? {},
    [detailModal.SuggestionId, suggestionState.data],
  );

  useEffect(() => {
    handleReloadSuggestion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleReloadSuggestion = useCallback(() => {
    suggestionHandlers.handleRefreshing();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleDismissDetail = useCallback(() => {
    setDetailModal((prev) => ({
      ...prev,
      isModalOpen: false,
      // SuggestionId: null,
    }));
  }, []);

  const handleOpenDetail = (SuggestionId, isAnswered) => () => {
    if (isAnswered) {
      store.dispatch(makeSuggestionReadByUser({ SuggestionId }));
      store.dispatch(
        modifyNotificationCount('suggestions', (count) => count - 1),
      );
    }
    setDetailModal({
      isModalOpen: true,
      SuggestionId,
    });
  };

  const handleDismissNewSuggestion = useCallback(() => {
    setNewSuggestion(false);
  }, []);

  const handleSendNewSuggestion = useCallback(() => {
    setNewSuggestion(true);
  }, []);

  const _renderItem = useCallback(({ item }) => {
    const answered = item.VisitAnswer === 3;
    return (
      <Card
        style={[styles.card, answered ? styles.unread : undefined]}
        onPress={handleOpenDetail(item.SuggestionId, answered)}>
        <Card.Title
          title={item.Subject}
          titleStyle={styles.cardTitleText}
          style={styles.cardTitle}
        />
        <Card.Content>
          {/* <Label>{item.Subject}</Label> */}
          <View style={styles.row}>
            <Label color="placeholder">وضعیت : </Label>
            <Label>{item.IsReadText}</Label>
          </View>
          <View style={styles.row}>
            <Label color="placeholder">تاریخ ارسال : </Label>
            <Label>{item.PMessageDate}</Label>
          </View>
        </Card.Content>
      </Card>
    );
  }, []);

  const _renderEmptyComponent = (
    <View style={styles.empty}>
      <Label style={styles.emptyLabel}>
        برای ثبت انتقاد و پیشنهاد جدید روی دکمه زیر لمس کنید.
      </Label>
      <Icon name="plus" size={20} />
    </View>
  );

  const _keyExtractor = useCallback((item) => `${item.SuggestionId}`, []);
  return (
    <Fragment>
      <SafeAreaView style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={'انتقادات و پیشنهادات'} />
        <FlatList
          data={suggestionState.data}
          extraData={suggestionState.data}
          renderItem={_renderItem}
          ListEmptyComponent={_renderEmptyComponent}
          style={styles.list}
          keyExtractor={_keyExtractor}
          contentContainerStyle={styles.listContent}
          numColumns={2}
          refreshControl={
            <RefreshControl
              refreshing={suggestionState.refreshing}
              onRefresh={handleReloadSuggestion}
              colors={[Colors.green]}
            />
          }
        />
        <SuggestionDetailModal
          visible={detailModal.isModalOpen}
          suggestion={selectedSuggestion}
          onDismiss={handleDismissDetail}
        />
        <AddSuggestionModal
          visible={newSuggestion}
          onDismiss={handleDismissNewSuggestion}
          onSubmit={handleReloadSuggestion}
        />

        <FAB
          animated
          style={styles.fab}
          icon="plus"
          label="انتقادات و پیشنهادات جدید"
          color="white"
          onPress={handleSendNewSuggestion}
        />
      </SafeAreaView>
    </Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    flex: 1,
  },
  listContent: {
    padding: 16,
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  fab: {
    position: 'absolute',
    bottom: 20,
    right: 15,
    backgroundColor: Colors.green,
  },
  card: {
    margin: 10,
  },
  read: {},
  unread: {
    borderWidth: 1,
    borderColor: Colors.green,
  },
  cardTitleText: {
    fontSize: 16,
    textAlign: 'right',
    paddingRight: 10,
  },
  cardTitle: {
    minHeight: 20,
  },

  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    height: height / 1.5,
    width: '100%',
  },
  emptyLabel: {
    marginBottom: 10,
  },
});
