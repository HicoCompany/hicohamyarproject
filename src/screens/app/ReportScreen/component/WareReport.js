import React, { Component } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

// ui components
import { Card } from 'react-native-paper';
import { Empty, Loading, Label, LoadMore } from '@components';

//utils
import NetInfo from '@react-native-community/netinfo';
import { hp, normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';

//api
import Repository from '@src/repository/Repository';
import { uniqBy } from 'lodash';
import ErrorDialog from '@src/components/ErrorDialog';

class WareReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loadMore: false,
      refreshing: false,
      error: false,
      message: '',
      data: [],
      page: 0,
      lastListCount: 0,
      connection: true,
    };
  }

  componentDidMount() {
    this.unsubscribeNetInfo = NetInfo.addEventListener(this.handleNetInfo);

    this.getReceivedWare({
      loading: true,
    });
  }

  componentWillUnmount() {
    this.unsubscribeNetInfo?.();
  }

  handleNetInfo = (state) => {
    this.setState({
      connection: state.isConnected,
    });
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  async getReceivedWare({
    loadMore = false,
    loading = false,
    refreshing = false,
  }) {
    if (!this.state.connection) {
      this.setState({
        error: true,
        message: 'لطفا اینترنت خود را بررسی کنید.',
        loading: false,
        refreshing: false,
        loadMore: false,
      });
      return null;
    }

    this.setState({
      error: false,
      loading,
      refreshing,
      loadMore,
    });
    try {
      const response = await Repository.PhoneGetReceivedWareHelperReportApi({
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        Page: this.state.page,
      });
      if (response.ResultID === 100) {
        const data = response.Result || [];
        return this.setState((prevState) => ({
          data: loadMore
            ? uniqBy([...prevState.data, ...data], 'DeliveryWasteID')
            : data,
          lastListCount: data?.length,
          loading: false,
          refreshing: false,
          loadMore: false,
        }));
      }
      this.setState({
        error: true,
        message: response.Text,
        loading: false,
        refreshing: false,
        loadMore: false,
      });
    } catch (error) {
      this.setState({
        error: true,
        loading: false,
        refreshing: false,
        loadMore: false,
        message: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  handleLoadMore = () => {
    this.setState(
      (state) => ({
        page: state.page + 1,
      }),
      () => {
        this.getReceivedWare({ loadMore: true });
      },
    );
  };

  handleRefresh = () => {
    this.setState({ list: [] }, () => {
      this.getReceivedWare({ refreshing: true });
    });
  };

  renderEmpty = () => {
    return this.state.loading && this.state.connection ? (
      <View style={styles.grow}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.grow}>
        <Empty />
      </View>
    );
  };

  renderFooter = () => {
    if (this.state.lastListCount < 14 || !this.state.connection) return null;
    return (
      <LoadMore loading={this.state.loadMore} onPress={this.handleLoadMore} />
    );
  };

  /**
   * @typedef {{BoothTitle: string | null,CollectionDateStr: string | null,CollectionTime: string | null,DeliveryCode: number,DeliveryDateStr: string,DeliveryTime: string,DeliveryTypeStr: string,DeliveryWasteID: number,PersonlName: string,RoleTitle: string | null,StatusStr: string | null,TotalCount: number,TotalPoint: number,TotalPrice: number,WareTitle: string}} WareItem
   * @param {{ item: WareItem}} param0
   * @returns
   */
  renderItem = ({ item }) => {
    return (
      <Card style={styles.itemContainer} activeOpacity={0.6}>
        <Card.Title
          title={item.WareTitle}
          style={styles.itemTitle}
          titleStyle={styles.itemTitleLabel}
        />
        <Card.Content>
          <View style={styles.itemTextContainer}>
            <Label style={styles.itemTextDesc} weight="bold">
              {`${item.DeliveryDateStr} ${item.DeliveryTime}`}
            </Label>
            <Label style={styles.itemTextTitle}>{'تاریخ دریافت کالا : '}</Label>
          </View>
          <View style={styles.itemTextContainer}>
            <Label style={styles.itemTextDesc} weight="bold">
              {item.TotalCount}
            </Label>
            <Label style={styles.itemTextTitle}>{'تعداد : '}</Label>
          </View>
          <View style={styles.itemTextContainer}>
            <Label style={styles.itemTextDesc} weight="bold">
              {item.TotalPoint}
            </Label>
            <Label style={styles.itemTextTitle}>{' امتیاز کل : '}</Label>
          </View>
          <View style={styles.itemTextContainer}>
            <Label
              style={styles.itemTextDesc}
              weight="bold">{`${item.TotalPrice} تومان`}</Label>
            <Label style={styles.itemTextTitle}>{'ارزش کالا : '}</Label>
          </View>
        </Card.Content>
      </Card>
    );
  };

  _keyExtractor = (item, index) => `ware-${item.DeliveryWasteID}-${index}`;

  dismissError = () => this.setState({ error: false, message: '' });

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty}
          ListFooterComponent={this.renderFooter}
          numColumns={1}
          keyExtractor={this._keyExtractor}
          refreshing={this.state.refreshing && this.state.connection}
          onRefresh={this.handleRefresh}
          contentContainerStyle={
            this.state.data.length ? undefined : styles.grow
          }
        />
        <ErrorDialog
          open={this.state.error}
          handleClose={this.dismissError}
          message={this.state.message}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  screenHeader: {
    height: hp('7%'),
    backgroundColor: '#424242',
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenHeaderTitle: {
    fontSize: normalize(13),
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    fontSize: normalize(12),
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },

  itemContainer: {
    flexDirection: 'row',
    margin: 10,
    overflow: 'hidden',
  },
  itemTitle: {
    paddingEnd: 16,
    paddingStart: 0,
    minHeight: undefined,
  },
  itemTitleLabel: {
    fontSize: normalize(13),
    textAlign: 'right',
    overflow: 'hidden',
  },
  itemAction: {
    flexDirection: 'row-reverse',
    backgroundColor: Colors.green,
  },
  itemActionSection: {
    flexDirection: 'row-reverse',
    flex: 1,
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  itemTextContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
  },
  itemTextTitle: {
    fontSize: normalize(10),
    textAlign: 'center',
  },
  itemTextDesc: {
    fontSize: normalize(11),
    textAlign: 'center',
  },
  grow: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(WareReport);
