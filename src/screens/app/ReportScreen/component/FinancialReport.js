import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { connect } from 'react-redux';

//ui components
import { Empty, Loading, Label, LoadMore, ErrorDialog } from '@components';
import { Card } from 'react-native-paper';

//utils
import NetInfo from '@react-native-community/netinfo';
import { hp, normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import EStyleSheet from 'react-native-extended-stylesheet';

//api
import { getFinancialReports } from '@src/api/modules/Report';

class FinancialReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      lastDataCount: 0,
      page: 1,
      loading: false,
      refreshing: false,
      loadMore: false,
      error: false,
      message: '',
      connection: true,
    };
  }

  componentDidMount() {
    this.unsubscribeNetInfo = NetInfo.addEventListener(this.handleNetInfo);
    this.getFinancialReports({ loading: true });
  }

  componentWillUnmount() {
    this.unsubscribeNetInfo?.();
  }

  handleNetInfo = (state) => {
    this.setState({
      connection: state.isConnected,
    });
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  async getFinancialReports({
    loadMore = false,
    loading = false,
    refreshing = false,
  }) {
    if (!this.state.connection) {
      this.setState({
        error: true,
        message: 'لطفا اینترنت خود را بررسی کنید.',
        loading: false,
        refreshing: false,
        loadMore: false,
      });
      return null;
    }

    this.setState({
      error: false,
      message: '',
      loadMore,
      loading,
      refreshing,
    });

    try {
      //TODO API REPORTS
      const response = await getFinancialReports({
        page: this.state.page,
      });
      const list = response || [];
      if (!list) return null;

      this.setState((prev) => ({
        data: loadMore ? [...prev.data, ...list] : list,
        lastDataCount: list.length,
        loadMore: false,
        loading: false,
        refreshing: false,
      }));
    } catch (error) {
      this.setState({
        error: true,
        message: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
        loadMore: false,
        loading: false,
        refreshing: false,
      });
    }
  }

  handleLoadMore = () => {
    this.setState(
      (state) => ({
        page: state.page + 1,
        loadMore: true,
      }),
      () => {
        this.getFinancialReports({
          loadMore: true,
        });
      },
    );
  };

  handleRefresh = () => {
    this.getFinancialReports({
      refreshing: true,
    });
  };

  renderEmpty = () => {
    return this.state.loading && this.state.connection ? (
      <View style={styles.grow}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.grow}>
        <Empty />
      </View>
    );
  };

  renderFooter = () => {
    if (this.state.lastDataCount < 15 || !this.state.connection) return null;
    return (
      <LoadMore loading={this.state.loadMore} onPress={this.handleLoadMore} />
    );
  };

  renderItem = ({ item }) => {
    const { Cash, StrReceivedType, PCreateDate, PersonlName, RoleTitle } = item;

    return (
      <Card style={styles.itemContainer}>
        <Card.Content>
          <View style={styles.itemTextContainer}>
            <View style={styles.row}>
              <Label style={styles.itemTextDesc} weight="bold">
                {PersonlName}
              </Label>
              {RoleTitle && (
                <Label
                  weight="light"
                  color="placeholders"
                  style={styles.itemTitleCaption}>
                  ({RoleTitle})
                </Label>
              )}
            </View>
            <Label style={styles.itemTextTitle}>{'پرسنل واریز کننده : '}</Label>
          </View>
          <View style={styles.itemTextContainer}>
            <Label style={styles.itemTextDesc} weight="bold">
              {StrReceivedType}
            </Label>
            <Label style={styles.itemTextTitle}>{'نحوه دریافت وجه : '}</Label>
          </View>

          <View style={styles.itemTextContainer}>
            <Label style={styles.itemTextDesc} weight="bold">
              {PCreateDate}
            </Label>
            <Label style={styles.itemTextTitle}>{'تاریخ دریافت : '}</Label>
          </View>

          <View style={styles.itemTextContainer}>
            <Label
              style={styles.itemTextDesc}
              weight="bold">{`${Cash} تومان`}</Label>
            <Label style={styles.itemTextTitle}>{'ارزش وجه دریافتی : '}</Label>
          </View>
        </Card.Content>
        {/* <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {DeliveryCode}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'کد تحویل : '}
                        </Text>
                    </View> */}

        {/* <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {Description}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'توضیحات  : '}
                        </Text>
                    </View> */}
        {/* <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {''}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'معادل ریالی : '}
                        </Text>
                    </View> */}
      </Card>
    );
  };
  _keyExtractor = (_, index) => `financial-${index}`;

  dismissError = () => {
    this.setState({
      error: false,
      message: '',
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty}
          ListFooterComponent={this.renderFooter}
          numColumns={1}
          refreshing={this.state.refreshing && this.state.connection}
          onRefresh={this.handleRefresh}
          keyExtractor={this._keyExtractor}
          contentContainerStyle={
            this.state.data.length ? undefined : styles.grow
          }
        />
        <ErrorDialog
          open={this.state.error}
          handleClose={this.dismissError}
          message={this.state.message}
        />
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  screenHeader: {
    height: hp('7%'),
    backgroundColor: '#424242',
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenHeaderTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },

  itemContainer: {
    flexDirection: 'row',
    margin: 10,
  },
  itemTitleContainer: {
    padding: 5,
  },
  itemTitle: {
    fontSize: normalize(13),
    textAlign: 'right',
    paddingHorizontal: 10,
  },
  itemTitleCaption: {
    fontSize: normalize(9),
    paddingEnd: 5,
  },
  itemTextContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  itemTextTitle: {
    fontSize: normalize(10),
    textAlign: 'center',
  },
  itemTextDesc: {
    fontSize: normalize(11),
    textAlign: 'center',
  },
  grow: {
    flex: 1,
  },
  row: {
    flexDirection: 'row-reverse',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(FinancialReport);
