import React, { Component } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

//ui component
import { Button, Card, Divider } from 'react-native-paper';
import { Empty, Loading, Label, LoadMore, ErrorDialog } from '@components';

//utils
import NetInfo from '@react-native-community/netinfo';
import uniqBy from 'lodash/uniqBy';
import { hp, normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';

//api
import Repository from '@src/repository/Repository';
import { getWastesCollectionReport } from '@src/api/modules/Report';

class WasteReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
      loadMore: false,
      list: [],
      lastListCount: 0,
      page: 1,
      error: false,
      message: '',
      connection: true,
    };
  }

  componentDidMount() {
    this.unsubscribeNetInfo = NetInfo.addEventListener(this.handleNetInfo);

    this.getReports({
      loading: true,
    });
  }

  componentWillUnmount() {
    this.unsubscribeNetInfo?.();
  }

  handleNetInfo = (state) => {
    this.setState({
      connection: state.isConnected,
    });
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  async getReports({ loadMore = false, refreshing = false, loading = false }) {
    if (!this.state.connection) {
      this.setState({
        error: true,
        message: 'لطفا اینترنت خود را بررسی کنید.',
        loading: false,
        refreshing: false,
        loadMore: false,
      });
      return null;
    }

    this.setState({
      loadMore,
      refreshing,
      loading,
      error: false,
      message: '',
    });

    try {
      const response = await getWastesCollectionReport({
        page: this.state.page,
      });
      const apiList = response.data;
      if (apiList !== null && apiList.length > 0) {
        return this.setState((prevState) => ({
          list: loadMore
            ? uniqBy([...prevState.list, ...apiList], 'DeliveryWasteID')
            : apiList,
          lastListCount: apiList?.length,
          loadMore: false,
          refreshing: false,
          loading: false,
        }));
      }
      return this.setState({
        loadMore: false,
        refreshing: false,
        loading: false,
      });
    } catch (error) {
      return this.setState({
        loadMore: false,
        refreshing: false,
        loading: false,
        error: true,
        message: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => this.getReports({ loadMore: true }),
    );
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 0,
        list: [],
      },
      () => {
        this.getReports({
          refreshing: true,
        });
      },
    );
  };

  renderEmpty = () => {
    return this.state.loading && this.state.connection ? (
      <View style={styles.grow}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.grow}>
        <Empty />
      </View>
    );
  };

  renderFooter = () => {
    if (this.state.lastListCount < 15 || !this.state.connection) return null;
    return (
      <LoadMore loading={this.state.loadMore} onPress={this.handleLoadMore} />
    );
  };

  // BoothTitle: "غرفه همیار زیست دوست منطقه 3"
  // CollectionDateStr: "1399/12/21"
  // CollectionTime: " 3:29"
  // DeliveryCode: 6
  // DeliveryDateStr: "1399/12/21"
  // DeliveryTime: "03:29"
  // DeliveryTypeStr: "پرسنل غرفه ثابت"
  // DeliveryWasteID: 5533
  // PersonlName: "تست پرسنل غرفه منطقه 3"
  // RoleTitle: "پرسنل غرفه ثابت"
  // StatusStr: "تحویل گردیده"
  // TotalCount: 10
  // TotalPoint: 20
  // TotalPrice: 20000
  // WareTitle: null
  renderItem = ({ item }) => {
    const {
      DeliveryCode,
      DeliveryDateStr,
      DeliveryWasteID,
      PersonlName,
      TotalPrice,
      CollectionDateStr,
      CollectionTime,
      RoleTitle,
    } = item;

    return (
      <Card style={styles.itemContainer}>
        <View style={styles.itemHeader}>
          <View style={styles.itemTitleContainer}>
            <Label color="placeholder">پرسنل : </Label>
            <View>
              <Label style={styles.itemTitle}>{PersonlName}</Label>
              <Label
                weight="light"
                color="placeholders"
                style={styles.itemTitleCaption}>
                {RoleTitle}
              </Label>
            </View>
          </View>
          <View style={[styles.rowBetween, { minWidth: 30 }]}>
            <Label color="placeholder">کد : </Label>
            <Label weight="bold">{DeliveryCode}</Label>
          </View>
        </View>
        <Divider />
        <View style={[styles.itemSection, { padding: 10 }]}>
          <View style={styles.rowBetween}>
            <Label style={styles.itemDate}>تاریخ تحویل : </Label>
            <Label weight="bold">{DeliveryDateStr}</Label>
          </View>
          <View style={styles.rowBetween}>
            <Label style={styles.itemDate}>تاریخ جمع آوری : </Label>
            <Label weight="bold">{`${CollectionDateStr} ${CollectionTime}`}</Label>
          </View>
        </View>
        <Divider />

        <Card.Actions style={styles.itemActionsContainer}>
          <Button
            onPress={() => {
              this.props.navigation.navigate('WasteReportDetail', {
                DeliveryWasteID,
                detail: item,
              });
            }}>
            جزئیات
          </Button>
          <View style={styles.rowBetween}>
            <Label style={styles.itemDate}>قیمت کل : </Label>
            <Label weight="bold">{`${TotalPrice || '...'} تومان`}</Label>
          </View>
        </Card.Actions>
      </Card>
    );
  };

  _keyExtractor = (item) => item.DeliveryWasteID.toString();
  dismissError = () => this.setState({ error: false, message: '' });

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.list}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty}
          ListFooterComponent={this.renderFooter}
          numColumns={1}
          keyExtractor={this._keyExtractor}
          refreshing={this.state.refreshing && this.state.connection}
          onRefresh={this.handleRefresh}
          contentContainerStyle={this.state.list.length > 0 ? {} : styles.grow}
        />
        <ErrorDialog
          open={this.state.error}
          handleClose={this.dismissError}
          message={this.state.message}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  screenHeader: {
    height: hp('7%'),
    backgroundColor: '#424242',
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenHeaderTitle: {
    fontSize: normalize(13),
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    fontSize: normalize(12),
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },
  itemContainer: {
    margin: 10,
    // padding: 10,
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  itemHeader: {
    flexDirection: 'row-reverse',
    flex: 1,
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  itemTitleContainer: {
    flexDirection: 'row-reverse',
  },
  itemTitle: {
    fontSize: normalize(12),
  },
  itemTitleCaption: {
    fontSize: normalize(8),
    paddingEnd: 5,
    // marginTop: 5,
  },
  itemActionsContainer: {
    padding: 5,
    paddingTop: 0,
    justifyContent: 'space-between',
  },
  itemSection: {
    flexDirection: 'row-reverse',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemDate: {
    fontSize: normalize(9),
    // textAlignVertical: 'center',
  },
  rowBetween: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  grow: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(WasteReport);
