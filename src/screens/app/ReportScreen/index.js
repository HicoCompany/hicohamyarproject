import { Tab, Tabs, View } from 'native-base';
import React, { Component } from 'react';
import { StatusBar, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import HeaderBack from '@src/components/HeaderBack';
import TabItem from '@src/components/TabItem';
import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
// import WareReport from './component/WareReport';
// import FinancialReport from './component/FinancialReport';
import WasteReport from './component/WasteReport';
import ComingSoon from '@src/components/ComingSoon';

const { width } = Dimensions.get('screen');

class ReportScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      tabBarIcon: ({ tintColor }) => (
        <TabItem
          name={'گزارشات'}
          source={require('@src/assets/image/ic_report.png')}
          color={tintColor}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    this.tabs = null;
  }

  componentDidMount = () => {
    setTimeout(this.tabs.goToPage.bind(this.tabs, 2));
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  componentWillUnmount = () => {};

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={'گزارشات'} />
        <Tabs
          ref={(tabs) => (this.tabs = tabs)}
          tabContainerStyle={styles.tabs}
          initialPage={1}
          tabBarPosition={'top'}
          tabBarBackgroundColor={Colors.green}
          tabBarUnderlineStyle={styles.tabBarUnderLineColor}>
          <Tab
            heading={'مالی همیار'}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.tabActiveStyle}
            textStyle={styles.tabText}
            activeTextStyle={styles.tabActiveText}>
            <ComingSoon />
          </Tab>
          <Tab
            heading={'کالاهای دریافتی'}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.tabActiveStyle}
            textStyle={styles.tabText}
            activeTextStyle={styles.tabActiveText}>
            <ComingSoon />
          </Tab>
          <Tab
            heading={'پسماند تحویلی'}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.tabActiveStyle}
            textStyle={styles.tabText}
            activeTextStyle={styles.tabActiveText}>
            <WasteReport navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </View>
    );
  }
}
/* <FinancialReport navigation={this.props.navigation} /> */
/* <WareReport navigation={this.props.navigation} / */
const tabWidth = width / 3;
const tabBarConsts = {
  center: tabWidth / 2,
  underlineWidth: 30,
  centerUnderLine: tabWidth / 2 - 15,
};

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  tabs: {
    elevation: 0,
  },
  tabBarUnderLineColor: {
    height: 3,
    borderRadius: 10,
    width: tabBarConsts.underlineWidth,
    bottom: 2,
    marginLeft: tabBarConsts.centerUnderLine,
  },
  tabStyle: {
    backgroundColor: Colors.green,
  },
  tabActiveStyle: {
    backgroundColor: Colors.green,
  },
  tabText: {
    fontSize: normalize(10),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.white,
  },
  tabActiveText: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    textAlign: 'center',
    color: Colors.white,
    fontWeight: 'normal',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(ReportScreen);
