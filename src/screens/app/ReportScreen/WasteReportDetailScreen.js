import React, { Component } from 'react';
import { FlatList, Image, StatusBar, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

// ui components
import { Button, Empty, Loading } from '@components';
import HeaderBack from '@components/HeaderBack';
import Label from '@components/Label';
import { Dialog, Portal, Card } from 'react-native-paper';

// utils
import { normalize, wp } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';

// api
import Repository from '@src/repository/Repository';

//assets
import WASTE from '@src/assets/image/ic_pasmand.png';
import { getWasteCollectionReportDetail } from '@src/api/modules/Report';

class WasteReportDetailScreen extends Component {
  constructor(props) {
    super(props);
    const detail = this.props.route.params.detail;
    this.state = {
      loading: true,
      refreshing: false,
      list: [],
      error: false,
      message: '',
      detail: {
        ...detail,
        HelperName: detail?.HelperName?.trim?.(),
      },
    };
  }

  componentDidMount() {
    this.getWasteReportDetail({
      loading: true,
    });
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  async getWasteReportDetail({ loading = false, refreshing = false }) {
    this.setState({
      loading,
      refreshing,
      error: false,
      message: '',
    });

    try {
      const response = await getWasteCollectionReportDetail({
        deliveryWasteID: this.props.route.params.DeliveryWasteID,
      });

      this.setState({
        list: response.data.ResultList,
        loading: false,
        refreshing: false,
      });
    } catch (error) {
      this.setState({
        loading: false,
        refreshing: false,
        error: true,
        message: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  handleRefresh = () => {
    this.getWasteReportDetail({ refreshing: true });
  };

  renderEmpty = () => {
    return this.state.loading ? (
      <View style={styles.grow}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.grow}>
        <Empty />
      </View>
    );
  };

  renderItem = ({ item }) => {
    return (
      <Card style={styles.itemContainer}>
        <Card.Content style={styles.itemTextContainer}>
          <Image
            source={WASTE}
            resizeMode={'contain'}
            style={styles.itemImage}
          />
          <Label style={styles.itemTitle}>{item.WasteTypeStr}</Label>
        </Card.Content>
        <Card.Actions style={styles.itemAction}>
          <Label
            style={styles.itemActionSection}>{`${item.Price} تومان`}</Label>
          <Label
            style={styles.itemActionSection}>{`${item.Point} امتیاز`}</Label>
          <Label
            style={
              styles.itemActionSection
            }>{`${item.WasteTypeCount} کیلوگرم`}</Label>
        </Card.Actions>
      </Card>
    );
  };

  _keyExtractor = (_, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack
          onBackPress={() => this.props.navigation.goBack()}
          headerTitle={'جزئیات گزارش پسماند تحویلی'}
        />
        <View style={styles.detailContainer}>
          <View style={styles.detailRow}>
            <Label>{this.state.detail.PersonlName}</Label>
            <Label>{'نام پرسنل : '}</Label>
          </View>
          <View style={styles.detailRow}>
            <View style={styles.detailRow}>
              <Label>{`${this.state.detail.TotalPrice} تومان`}</Label>
              <Label>{'مبلغ کل : '}</Label>
            </View>
            <View style={styles.detailRow}>
              <Label>{`${this.state.detail.TotalPoint}`}</Label>
              <Label>{'امتیاز کل : '}</Label>
            </View>
            <View style={styles.detailRow}>
              <Label>{`${this.state.detail.TotalCount}`}</Label>
              <Label>{'وزن کل : '}</Label>
            </View>
          </View>
        </View>
        <FlatList
          data={this.state.list}
          extraData={[
            this.state.list,
            this.state.loading,
            this.state.refreshing,
          ]}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty}
          numColumns={1}
          keyExtractor={this._keyExtractor}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          contentContainerStyle={this.state.list.length > 0 ? {} : styles.grow}
        />
        <Portal>
          <Dialog
            visible={this.state.error && false}
            style={styles.dialogContainer}
            dismissable
            onDismiss={() => {
              this.setState({
                error: false,
              });
            }}>
            <Dialog.Content>
              <Label style={styles.dialogText}>{this.state.message}</Label>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                buttonText={'تلاش مجدد'}
                onPress={() => {
                  this.getWasteReportDetail({ loading: true });
                }}
                style={styles.dialogButton}
              />
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  itemContainer: {
    // flexDirection: 'row',
    margin: 10,
    padding: 0,
    overflow: 'hidden',
  },
  itemImage: {
    height: wp('7%'),
    width: wp('7%'),
    marginLeft: 5,
    // marginHorizontal: 10,
  },
  itemTextContainer: {
    flexDirection: 'row-reverse',
    // alignSelf: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: 8,
  },
  itemTitle: {
    fontSize: normalize(13),
    textAlign: 'center',
    // color: '#035e6f',
    // flex: 1,
    marginRight: 5,
  },
  itemAction: {
    backgroundColor: Colors.green,
    margin: 0,
  },
  itemActionSection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    textAlign: 'center',
    height: '100%',
    color: '#ffffff',
  },
  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  dialogText: {
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
  detailContainer: {
    padding: 16,
    elevation: 3,
    backgroundColor: '#fff',
    borderBottomEndRadius: 6,
    borderBottomStartRadius: 6,
  },
  detailRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  grow: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(WasteReportDetailScreen);
