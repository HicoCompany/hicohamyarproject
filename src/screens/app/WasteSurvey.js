import React, { useCallback, useEffect, useRef, useState } from 'react';
import { StatusBar, StyleSheet, View, Animated, Easing } from 'react-native';

// components
import { Button, Divider, HelperText } from 'react-native-paper';
import { Label, Rating, RadioButton, Loading, ErrorDialog } from '@components';

//utils
import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';

//assets
import WASTE_DELIVERY from '@assets/image/garbage-truck.png';

//api
import {
  getSurveyQuestions,
  useSkipSurvey,
  useSubmitSatisfaction,
  useSubmitSurvey,
} from '@src/api/modules/Survey';
import PushNotification from 'react-native-push-notification';
import { useAlert } from '@src/components/GlobalAlert';

export default function WasteSurvey({ route, navigation }) {
  const Alert = useAlert();
  const [state, setState] = useState({
    questions: [],
    loading: true,
    submitting: false,
    error: false,
    message: '',
  });
  const DeliveryWasteID = route.params?.DeliveryWasteID ?? 9834;
  const prevRouteOnDone = route.params?.onDone;

  const [submitState, submitHandlers] = useSubmitSurvey();

  const [skipState, skipHandlers] = useSkipSurvey();

  const [starState, starHandlers] = useSubmitSatisfaction();

  // "AveragePoints":3,
  //        "BoothTitle":null,
  //        "ExcellentPoints":5,
  //        "GoodPoints":4,
  //        "PersonlName":null,
  //        "QuestionNo":1,
  //        "QuestionText":"برخورد سفیر پاکی چگونه بود ؟",
  //        "QuestionsSurveyHelperId":1,
  //        "RemainPoints":80,
  //        "SurveyHelperID":0,
  //        "WeakPoints":2

  const [rate, setRate] = useState(0);
  const animation = useRef(new Animated.Value(0)).current;

  const timingFunction = useRef(
    Animated.timing(animation, {
      toValue: 1,
      duration: 500,
      easing: Easing.inOut(Easing.ease),
      useNativeDriver: true,
    }),
  ).current;

  const translateY = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [200, 0],
  });

  const imageScale = animation.interpolate({
    inputRange: [0, 1],
    outputRange: [1, 0.8],
  });

  useEffect(() => {
    handleLoadQuestions();
    if (route.params?.id) {
      PushNotification.cancelLocalNotification?.(route.params.id);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLoadQuestions = () => {
    getSurveyQuestions({
      deliveryWasteId: DeliveryWasteID,
    })
      .then(({ data }) => {
        setState({
          loading: false,
          error: false,
          questions: data.map((v) => ({
            ...v,
            id: v.QuestionsSurveyHelperId,
            question: v.QuestionText,
          })),
        });
      })
      .catch((e) => {
        setState((prevState) => ({
          ...prevState,
          loading: false,
          error: true,
          message: e?.message || 'خطایی رخ داد لطفا اینترنت خود را بررسی کنید.',
        }));
      });
  };

  const handleSubmit = useCallback(() => {
    submitHandlers
      .handleFetch({
        data: {
          DeliveryWasteID,
          QuestionHelperList: state.questions.map((v) => ({
            QuestionsSurveyHelperId: v.QuestionsSurveyHelperId,
            QuestionsSurveyHelperAnswersId: v.selectedAnswer,
          })),
        },
      })
      .then(() => {
        Alert.addToast({
          message: 'نظر شما با موفقیت ثبت شد.',
          timeout: 'short',
          type: 'success',
        });
        prevRouteOnDone?.();
        const timer = setTimeout(() => {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
          clearTimeout(timer);
        }, 1000);
      })
      .catch(() => {});

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.questions, DeliveryWasteID]);

  const handleSkip = useCallback(() => {
    skipHandlers
      .handleFetch({
        data: {
          DeliveryWasteID,
        },
      })
      .then(() => {
        Alert.addToast({
          message:
            'نظرسنجی رد شد برای ارائه نظر خود به صفحه درخواست ها مراجعه کنید.',
          timeout: 'long',
          type: 'info',
        });
        prevRouteOnDone?.();
        const timer = setTimeout(() => {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
          clearTimeout(timer);
        }, 1000);
      })
      .catch((e) => {
        const timer = setTimeout(() => {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
          clearTimeout(timer);
        }, 2000);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [DeliveryWasteID]);

  const handleRateChange = useCallback(
    (value) => {
      setRate(value);
      starHandlers
        .handleFetch({ data: { DeliveryWasteID, Star: value } })
        .catch(() => {});
      timingFunction.start();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [DeliveryWasteID],
  );

  const handleItemRateChange = (id) => (value) => {
    setState((prevState) => ({
      ...prevState,
      questions: prevState.questions.map((v, i, arr) => {
        if (v.id === id) {
          return {
            ...v,
            selectedAnswer: value,
          };
        }
        return v;
      }),
    }));
  };

  const handleDismissError = useCallback(() => {
    submitHandlers.setState((prev) => ({ ...prev, error: false, message: '' }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItem = useCallback(
    ({ item }) => (
      <View style={styles.listItem}>
        <Label style={styles.listItemLabel}>{item.QuestionText}</Label>
        <RadioButton.Group
          value={item.selectedAnswer}
          style={styles.radioGroup}
          onChange={handleItemRateChange(item.id)}>
          {item.QuestionsSurveyHelperAnswers.map((answer) => (
            <RadioButton
              label={answer.AnswerText}
              value={answer.QuestionsSurveyHelperAnswersId}
              style={styles.radioItem}
            />
          ))}
        </RadioButton.Group>
      </View>
    ),
    [],
  );

  if (state.loading) {
    return (
      <View style={styles.container}>
        <Loading />
      </View>
    );
  }
  return (
    <>
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <Animated.View
          style={[styles.content, { transform: [{ translateY }] }]}>
          <Animated.Image
            source={WASTE_DELIVERY}
            style={[styles.wasteLogo, { transform: [{ scale: imageScale }] }]}
            resizeMode="contain"
          />
          <Label weight="bold" style={styles.successTitle}>
            جمع آوری پسماند با موفقیت انجام شد.
          </Label>
          <Label weight="bold" style={styles.title}>
            لطفا به کیفیت جمع آوری امتیاز بدهید
          </Label>
          <Rating
            style={styles.rate}
            value={rate}
            onChange={handleRateChange}
            disabled={starState.succeeded}
          />
          {starState.error || starState.succeeded ? (
            <HelperText
              type={starState.error ? 'error' : 'info'}
              style={styles.helperText}
              visible={starState.error || starState.succeeded}>
              {starState.message}
            </HelperText>
          ) : null}
          <Animated.FlatList
            data={state.questions}
            style={[
              styles.list,
              {
                opacity: animation,
              },
            ]}
            contentContainerStyle={styles.listContent}
            ItemSeparatorComponent={Divider}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
          />
        </Animated.View>

        {/* {skipState.error || skipState.succeeded ? (
          <HelperText
            type={skipState.error ? 'error' : 'info'}
            style={styles.helperText}
            visible={skipState.error || skipState.succeeded}>
            {skipState.message}
          </HelperText>
        ) : null} */}
        {submitState.error || submitState.succeeded ? (
          <HelperText
            type={submitState.error ? 'error' : 'info'}
            style={styles.helperText}
            visible={submitState.error || submitState.succeeded}>
            {submitState.message}
          </HelperText>
        ) : null}
        <View style={styles.actions}>
          <Button
            style={styles.actionButton}
            mode="contained"
            labelStyle={styles.successButtonLabel}
            onPress={handleSubmit}
            loading={submitState.loading}
            color={Colors.green}>
            ثبت
          </Button>
          <Button
            style={styles.actionButton}
            mode="outlined"
            icon={skipState.error ? 'refresh' : undefined}
            loading={skipState.loading}
            onPress={handleSkip}
            color={Colors.gray}>
            رد شدن
          </Button>
        </View>
      </View>
      <ErrorDialog
        open={submitState.error}
        message={submitState.message}
        handleClose={handleDismissError}
      />
    </>
  );
}

WasteSurvey.initialParams = {
  DeliveryWasteID: 0,
  // notification id
  id: null,
  onDone: () => {},
};

const buttonHeight = 50;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    width: '100%',
    alignItems: 'center',
    flex: 1,
  },
  successTitle: {
    fontSize: normalize(14),
    textAlign: 'center',
    color: Colors.green,
  },
  title: {
    fontSize: normalize(11),
  },
  wasteLogo: { width: 200, height: 200 },
  list: {
    width: '100%',
  },
  listContent: { padding: 16 },
  listItem: {
    marginBottom: 16,
  },
  listItemLabel: {
    margin: 5,
  },
  radioGroup: {
    alignSelf: 'center',
    flexWrap: 'wrap-reverse',
    width: '100%',
  },
  radioItem: {
    flex: 1,
    borderRadius: 6,
    margin: 6,
    height: buttonHeight,
    justifyContent: 'center',
  },
  actions: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-evenly',
    width: '100%',
    paddingVertical: 20,
  },
  actionButton: {
    width: 150,
    elevation: 0,
  },
  successButtonLabel: {
    color: 'white',
  },
  helperText: {
    textAlign: 'center',
  },
});
