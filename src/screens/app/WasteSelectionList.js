import React, { useCallback } from 'react';
import { View, FlatList } from 'react-native';

//components
import { ButtonSubmit, Label } from '@src/components';
import SelectCounter from '@components/SelectCounter';
import HeaderBack from '@src/components/HeaderBack';

//network
import { useWastes } from '@src/api/modules/Waste';

//utils
import { fileUrlBuilder } from '@src/utility/base';
import { ScaledSheet } from 'react-native-size-matters';
import { useMemo } from 'react';
import EmptyList from '@src/components/EmptyList';
import { useSelector } from 'react-redux';

export default function WasteSelectionList({ navigation }) {
  const [WasteState, WasteHandler] = useWastes({
    page: 1,
    wasteType: 'wastes',
    limit: 30,
  });

  const {
    minWeightOnDeliveryRequest,
    wasteChangeVelocityOnDeliveryRequest,
  } = useSelector((state) => state.global);

  const selectedWastes = useMemo(
    () => WasteState.data.filter((el) => el.Selected && el.Count > 0),
    [WasteState.data],
  );
  const selectedWastesWeight = useMemo(
    () => selectedWastes.reduce((prev, next) => prev + next.Count, 0),
    [selectedWastes],
  );

  const { totalPrice, totalPoint } = useMemo(() => {
    return selectedWastes?.reduce(
      (prev, item) => {
        if (item.Selected) {
          prev.totalPrice += item.Count * item.HelperPrice;
          prev.totalPoint += item.Count * item.Point;
        }

        return prev;
      },
      { totalPrice: 0, totalPoint: 0 },
    );
  }, [selectedWastes]);

  const handleDecreaseItemValue = useCallback(
    (WasteId) => () => {
      WasteHandler.setState((state) => {
        const data = [...state.data];
        const index = data.findIndex((el) => el.WasteId === WasteId);
        if (index == -1) return state;

        data[index] = {
          ...data[index],
          Count: data[index].Count - wasteChangeVelocityOnDeliveryRequest,
        };
        return {
          ...state,
          data,
        };
      });
    },
    [WasteHandler, wasteChangeVelocityOnDeliveryRequest],
  );

  const handleIncreaseItemValue = useCallback(
    (WasteId) => () => {
      WasteHandler.setState((state) => {
        const data = [...state.data];
        const index = data.findIndex((el) => el.WasteId === WasteId);
        if (index == -1) return state;

        data[index] = {
          ...data[index],
          Count: data[index].Count + wasteChangeVelocityOnDeliveryRequest,
        };
        return {
          ...state,
          data,
        };
      });
    },
    [WasteHandler, wasteChangeVelocityOnDeliveryRequest],
  );

  const handleSelectItem = useCallback(
    (WasteId) => () => {
      WasteHandler.setState((state) => {
        const data = [...state.data];
        const index = data.findIndex((el) => el.WasteId === WasteId);
        if (index == -1) return state;

        data[index] = {
          ...data[index],
          Selected: !data[index].Selected,
          Count: 0,
        };
        return {
          ...state,
          data,
        };
      });
    },
    [WasteHandler],
  );

  const _renderItem = useCallback(
    ({ item }) => (
      <SelectCounter
        selected={item.Selected}
        name={item.Title}
        imageSrc={{ uri: fileUrlBuilder(item.File) }}
        onDecrease={handleDecreaseItemValue(item.WasteId)}
        onIncrease={handleIncreaseItemValue(item.WasteId)}
        onSelectionChange={handleSelectItem(item.WasteId)}
        value={item.Count}
      />
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const handleNavigateToForm = useCallback(() => {
    navigation.navigate('WasteCollectionSubmitForm', {
      list: selectedWastes,
    });
  }, [selectedWastes, navigation]);

  return (
    <View style={styles.container}>
      <HeaderBack headerTitle={'انتخاب و تعیین وزن پسماند'} />

      <View style={styles.help}>
        <Label color="placeholder" weight="bold" fontSize={15}>
          راهنما
        </Label>
        <Label color="placeholder" weight="bold" fontSize={11.5}>
          {`۱. پسماند های خود را جهت جمع آوری انتخاب کنید \n۲. وزن پسماند ها به کیلوگرم محاسبه می شود.\n۳. حداقل وزن پسماند ها باید بیشتر از ${minWeightOnDeliveryRequest} کیلوگرم باشند.`}
        </Label>
      </View>
      <FlatList
        data={WasteState.data}
        renderItem={_renderItem}
        keyExtractor={(item) => String(item.WasteId)}
        numColumns={3}
        style={styles.list}
        refreshing={WasteState.loading}
        onRefresh={WasteHandler.handleFetch}
        ListEmptyComponent={
          WasteState.loading ? null : (
            <EmptyList message={'پسماندی برای انتخاب یافت نشد.'} />
          )
        }
      />
      {WasteState.data?.length ? (
        <>
          <View style={styles.listHeader}>
            <Label color="placeholder" weight="bold" fontSize={14}>
              جمع مبلغ:‌ {totalPrice}
            </Label>
            <Label color="placeholder" weight="bold" fontSize={14}>
              جمع امتیاز:‌ {totalPoint}
            </Label>
          </View>
          <ButtonSubmit
            onPress={handleNavigateToForm}
            disabled={selectedWastesWeight < minWeightOnDeliveryRequest}>
            تکمیل درخواست
          </ButtonSubmit>
        </>
      ) : null}
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  listHeader: {
    backgroundColor: '#efefef',
    margin: '8@s',
    paddingHorizontal: '8@s',
    height: '40@s',
    borderRadius: '6@s',
    flexDirection: 'row-reverse',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    elevation: 1,
  },
  help: {
    paddingTop: '8@s',
    padding: '12@s',
    elevation: 1,
    backgroundColor: '#f1f1f1',
  },
  list: {
    flexGrow: 1,
  },
});
