import React, { Fragment, useMemo, useRef, useCallback, useState } from 'react';
import { findNodeHandle, RefreshControl, StyleSheet, View } from 'react-native';

//components
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Gender from '@src/components/Gender';
import TextInput from '@src/components/TextInput';
import { ButtonSubmit, Container, Field, Label, Select } from '@src/components';
import { useProfile, useUpdateProfile } from '@src/api/modules/Helper';
import { Avatar, Button, Divider, HelperText } from 'react-native-paper';

//services
import { useGetBank } from '@src/api/modules/Bank';
import { useGetMainArea } from '@src/api/modules/Area';

//utils
import { useFormik } from 'formik';
import {
  isShebaValid,
  verifyCardNumber,
  verifyIranianNationalId,
  digitsArToFa,
  digitsFaToEn,
} from '@persian-tools/persian-tools';
import Colors from '@src/utility/Colors';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import { scale, ScaledSheet } from 'react-native-size-matters';

import * as Yup from 'yup';

const ProfileSchema = Yup.object().shape({
  bank: Yup.object().shape({
    BankID: Yup.string(),
  }),
  accountNumber: Yup.string()
    .max(16, 'شماره کارت وارد شده بیشتر از ۱۶ رقم است.')
    .min(16, 'لطفا ۱۶ رقم شماره کارت را وارد کنید.')
    .test('isValidAccountNumber', 'شماره کارت معتبر نیست.', (accountNumber) =>
      accountNumber?.length ? verifyCardNumber(accountNumber) : true,
    ),
  shaba: Yup.string()
    .min(24, 'لطفا ۲۴ رقم شماره شبا را وارد کنید.')
    .max(24, 'شماره شبا وارد شده بیشتر از ۲۴ رقم است.')
    .test('isShebaValid', 'شماره شبا معتبر نیست.', (sheba) =>
      sheba?.length
        ? isShebaValid(`IR${digitsFaToEn(digitsArToFa(sheba))}`)
        : true,
    )
    .nullable(),
  email: Yup.string().email('ایمیل درست نیست.').nullable(true),
  nationalId: Yup.string()
    .test('nationalIdValidation', 'کدملی صحیح نیست.', (value) => {
      if (value == null || value?.length == 0) return true;

      return verifyIranianNationalId(digitsFaToEn(digitsArToFa(value)));
    })
    .nullable(true),
  area: Yup.object()
    .shape({
      MainAreaCode: Yup.string(),
      MainAreaTitle: Yup.string(),
    })
    .test(
      'validateArea',
      'لطفا منطقه خود را انتخاب کنید',
      (area) => (area?.MainAreaCode ?? '').trim().length,
    ),
});

const FIELD_LABELS_PROPS = { fontSize: 14, weight: 'bold' };

export default function ProfileScreen() {
  /** @type {{current: KeyboardAwareScrollView}} */
  const scrollRef = useRef();

  const Alert = useAlert();
  const [editable, setEditable] = useState(false);
  const [state, handler] = useProfile();
  const [areaState] = useGetMainArea({
    initialData: [],
  });

  const [bankState] = useGetBank();

  const data = state.data ?? {};

  const defaultSelectedBank = useMemo(
    () =>
      bankState.data
        ? bankState.data?.find?.((el) => el.BankID === state.data?.BankID)
        : {},
    [bankState.data, state.data],
  );

  const formik = useFormik({
    initialValues: {
      name: data.Name,
      family: data.Family,
      email: data.Email,
      nationalId: data.NationalCode,
      area: {
        MainAreaCode: data.MainAreaID,
        Title: data.MainAreaTitle,
      },
      shaba: data?.ShabaNumber?.replace?.('IR', ''),
      gender: data.Gender,
      accountOwner:
        data.AccountOwner?.length > 0 ? data.AccountOwner : undefined,
      accountNumber:
        data.AccountNumber?.length > 0 ? data.AccountNumber : undefined,
      bank: defaultSelectedBank,
    },
    enableReinitialize: true,
    onSubmit: async () => {
      try {
        await updateHandler.handleFetch();
        setEditable(false);
        Alert.addToast({
          message: 'پروفایل با موفقیت ویرایش شد.',
          timeout: 'short',
          type: 'success',
        });
        await handler.handleFetch().catch(() => {});
      } catch (error) {
        if (error.response?.GuildId) {
          formik.setFieldError('guildId', error.response?.GuildId[0]);
        }
        if (error.response?.MainAreaID) {
          formik.setFieldError('area', error.response?.MainAreaID[0]);
        }
        if (error.response?.NationalCode?.length) {
          formik.setFieldError('nationalId', error.response?.NationalCode[0]);
        }
        if (error.response?.MainAreaID?.length) {
          formik.setFieldError('', error.response?.MainAreaID[0]);
        }
        if (error.response?.Email?.length) {
          formik.setFieldError('area', error.response?.Email[0]);
        }
        if (error.response?.ShabaNumber?.length) {
          formik.setFieldError('shaba', error.response?.ShabaNumber[0]);
        }

        // Alert.setError(
        //   DefaultModalActions.Error({
        //     message: error.message ?? JSON.stringify(error.data),
        //     onDismiss: () => {},
        //   }),
        // );
      }
    },
    validationSchema: ProfileSchema,
    validateOnChange: false,
    validateOnMount: false,
  });

  const [updateState, updateHandler] = useUpdateProfile({
    AccountNumber: formik.values.accountNumber ?? undefined,
    AccountOwner: formik.values.accountOwner ?? undefined,
    BankID: formik.values.bank?.BankID ?? undefined,
    Email: formik.values.email?.length ? formik.values.email : undefined,
    Family: formik.values.family ?? undefined,
    Name: formik.values.name ?? undefined,
    Gender: formik.values.gender ?? undefined,
    NationalCode: formik.values.nationalId
      ? digitsFaToEn(digitsArToFa(formik.values.nationalId))
      : undefined,
    ShabaNumber: formik.values.shaba?.length
      ? `IR${formik.values.shaba}`
      : undefined,
    MainAreaID: formik.values.area?.MainAreaCode,
    GuildId: data?.GuildId,
  });

  const handleInputChange = (name) => (value) => {
    formik.handleChange({ target: { name, value } });
  };

  const handleShebaChange = (value) => {
    formik.setFieldValue('shaba', value.replace('IR', ''));
  };

  const handleSubmit = () => {
    if (!editable) return setEditable(true);
    formik.submitForm();
  };

  const cancelEditing = useCallback(() => {
    Alert.setAction(
      DefaultModalActions.Ask({
        message:
          'آیا از لغو ویرایش پروفایل مطمئن هستید؟ اطلاعات ویرایش شده به حالت اول بر می گردند',
        mode: 'error',
        onConfirm: () => setEditable(false),
      }),
    );
  }, [Alert]);

  const handleInputFocus = useCallback((event) => {
    scrollRef.current?.scrollToFocusedInput?.(findNodeHandle(event.target));
  }, []);

  const setScrollRef = useCallback((ref) => {
    if (ref) {
      scrollRef.current = ref;
    }
  }, []);

  const renderEdit = useMemo(
    () => (
      <Fragment>
        <View style={styles.row}>
          <View style={styles.inlineInput}>
            <Label style={styles.label} fontSize={14} weight="bold">
              نام :
            </Label>
            <TextInput
              value={formik.values.name}
              placeholder="نام"
              onChangeText={handleInputChange('name')}
              style={styles.input}
              onFocus={handleInputFocus}
            />
          </View>
          <View style={styles.inlineInput}>
            <Label style={styles.label} fontSize={14} weight="bold">
              نام خانوادگی :
            </Label>
            <TextInput
              value={formik.values.family}
              placeholder="نام خانوادگی"
              onChangeText={handleInputChange('family')}
              style={styles.input}
              onFocus={handleInputFocus}
            />
          </View>
        </View>

        <Label style={styles.label} fontSize={14} weight="bold">
          کد ملی :
        </Label>
        <TextInput
          value={formik.values.nationalId}
          placeholder="کد ملی"
          onChangeText={handleInputChange('nationalId')}
          style={styles.input}
          onFocus={handleInputFocus}
          keyboardType="number-pad"
          maxLength={10}
        />
        {formik.errors.nationalId && (
          <HelperText type="error" style={styles.error}>
            {formik.errors.nationalId}
          </HelperText>
        )}
        <Label style={styles.label} fontSize={14} weight="bold">
          جنسیت :
        </Label>
        <Gender
          value={formik.values.gender}
          onChange={handleInputChange('gender')}
        />
        <View>
          <Label style={styles.label} fontSize={14} weight="bold">
            منطقه :
          </Label>
          <Select
            inputStyle={styles.areaInput}
            placeholder="منطقه"
            data={areaState.data ?? []}
            value={formik.values.area}
            valueKey={'MainAreaCode'}
            getLabel={(el) => el?.Title}
            labelKey={'Title'}
            onChange={handleInputChange('area')}
            style={styles.select}
          />
        </View>
        {formik.errors.area && (
          <HelperText type="error" style={styles.error}>
            {formik.errors.area}
          </HelperText>
        )}
        <Label style={styles.label} fontSize={14} weight="bold">
          ایمیل :
        </Label>
        <TextInput
          value={formik.values.email}
          placeholder="ایمیل"
          onChangeText={handleInputChange('email')}
          style={styles.input}
          keyboardType="email-address"
          onFocus={handleInputFocus}
        />
        {formik.errors.email && (
          <HelperText type="error" style={styles.error}>
            {formik.errors.email}
          </HelperText>
        )}

        <Label
          style={styles.title}
          fontSize={16}
          weight="bold"
          color="placeholder">
          اطلاعات حساب بانکی:
        </Label>

        <View>
          <Label style={styles.label} fontSize={14} weight="bold">
            بانک :
          </Label>
          <Select
            inputStyle={styles.areaInput}
            placeholder="بانک"
            data={bankState.data ?? []}
            value={formik.values.bank}
            valueKey={'BankID'}
            getLabel={(el) => el?.Title}
            labelKey={'Title'}
            onChange={handleInputChange('bank')}
            style={styles.select}
            loading={bankState.loading}
          />
        </View>

        <Label style={styles.label} fontSize={14} weight="bold">
          صاحب حساب :
        </Label>
        <TextInput
          value={formik.values.accountOwner}
          placeholder="صاحب حساب"
          onChangeText={handleInputChange('accountOwner')}
          style={styles.input}
          onFocus={handleInputFocus}
        />
        <Label style={styles.label} fontSize={14} weight="bold">
          شماره کارت :
        </Label>
        <TextInput
          value={formik.values.accountNumber}
          placeholder="شماره کارت"
          onChangeText={handleInputChange('accountNumber')}
          style={styles.input}
          keyboardType="number-pad"
          onFocus={handleInputFocus}
        />
        {formik.errors.accountNumber ? (
          <HelperText type="error" style={styles.error}>
            {formik.errors.accountNumber}
          </HelperText>
        ) : null}
        <Label style={styles.label} fontSize={14} weight="bold">
          شماره شبا :
        </Label>
        <View style={styles.shabaContainer}>
          <Label>IR</Label>
          <TextInput
            mode="none"
            value={formik.values.shaba}
            placeholder="شماره شبا"
            onChangeText={handleShebaChange}
            style={styles.shabaInput}
            onFocus={handleInputFocus}
            keyboardType="numeric"
            maxLength={26}
          />
        </View>
        {formik.errors.shaba ? (
          <HelperText type="error" style={styles.error}>
            {formik.errors.shaba}
          </HelperText>
        ) : null}
      </Fragment>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [areaState.data, bankState.data, formik.values, formik.errors],
  );

  const renderView = useMemo(
    () => (
      <Fragment>
        <Field
          title={'کد ملی'}
          value={state.data?.NationalCode ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Field
          title={'جنسیت'}
          value={state.data.GenderTitle ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Field
          title={'ایمیل'}
          value={state.data?.Email ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Field
          title={'نوع کاربری'}
          value={state.data?.GuildTitle ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Field
          title={'منطقه'}
          value={state.data?.MainAreaTitle ?? '-----------'}
          justifyContent="flex-start"
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Label
          style={styles.title}
          fontSize={16}
          weight="bold"
          color="placeholder">
          اطلاعات حساب بانکی:
        </Label>
        <Field
          title={'صاحب حساب'}
          value={state.data?.AccountOwner ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Field
          title={'شماره کارت'}
          value={state.data?.AccountNumber ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
        <Field
          title={'شماره شبا'}
          value={state.data?.ShabaNumber ?? '-----------'}
          titleProps={FIELD_LABELS_PROPS}
          valueProps={FIELD_LABELS_PROPS}
        />
      </Fragment>
    ),
    [state.data],
  );

  return (
    <Container>
      <Container.Header title="پروفایل" />
      <View style={styles.profileHeader}>
        <Avatar.Icon
          style={styles.avatar}
          icon={'account'}
          color="white"
          size={scale(55)}
        />
        <View style={styles.col}>
          <Label weight="bold" fontSize={15}>
            {state.data?.Name} {state.data?.Family}
          </Label>
          <Label weight="bold" color="placeholder" fontSize={11}>
            {state.data?.Mobile}
          </Label>
        </View>
      </View>
      <Divider />
      <KeyboardAwareScrollView
        innerRef={setScrollRef}
        enableOnAndroid
        extraHeight={scale(50)}
        extraScrollHeight={scale(50)}
        style={styles.content}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollToOverflowEnabled
        refreshControl={
          <RefreshControl
            refreshing={state.loading}
            onRefresh={handler.handleFetch}
          />
        }
        contentContainerStyle={styles.scrollContent}>
        <View>{editable ? renderEdit : renderView}</View>
      </KeyboardAwareScrollView>
      <View style={styles.actions}>
        {editable && (
          <Button
            color={Colors.error}
            style={styles.cancel}
            contentStyle={styles.cancelContent}
            mode="text"
            onPress={cancelEditing}>
            لغو
          </Button>
        )}
        <ButtonSubmit
          onPress={handleSubmit}
          loading={updateState.loading}
          growWidth
          disabled={updateState.loading}>
          {editable ? 'ثبت اطلاعات' : 'ویرایش'}
        </ButtonSubmit>
      </View>
    </Container>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  scroll: {},
  content: {
    flex: 1,
  },
  scrollContent: {
    paddingBottom: 400,
    padding: '16@s',
  },
  profileHeader: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    margin: '12@s',
    // justifyContent: 'space-between',
  },
  avatar: {
    marginLeft: '16@s',
  },

  row: {
    flexDirection: 'row-reverse',
  },
  inlineInput: {
    flexDirection: 'column',
    alignItems: 'stretch',
    flex: 1,
    margin: '8@s',
  },
  col: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },

  title: {
    // backgroundColor: 'red',
    marginTop: '20@s',
  },
  label: {
    margin: '5@s',
  },
  areaInput: {
    // width: '100%',
    textAlign: 'right',
    borderLeftColor: 'white',
    borderLeftWidth: 2,
    height: '40@s',
    margin: '8@s',
  },
  select: {
    height: '40@s',
    width: '100%',
    marginBottom: '8@s',
  },
  input: {
    elevation: 0,
  },
  shabaInput: {
    elevation: 0,
    textAlign: 'left',
    flex: 1,
    paddingLeft: '16@s',
  },
  actions: {
    flexDirection: 'row',
    height: '45@s',
    overflow: 'hidden',
    elevation: 100,
    zIndex: 10,
  },
  cancel: {
    flex: 0.5,
    height: '100%',
    borderRadius: 0,
    // alignItems: 'center',
    // justifyContent: 'center',
    padding: 0,
  },
  cancelContent: {
    width: '100%',
    height: '100%',
    padding: 0,
    borderRadius: 0,
  },
  shabaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: '6@s',
    paddingLeft: '16@s',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.gray,
  },
  error: {
    textAlign: 'center',
  },
});
