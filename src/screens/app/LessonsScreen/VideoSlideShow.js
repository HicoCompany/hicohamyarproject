import React, { useCallback } from 'react';
import { FlatList, View } from 'react-native';

//components
import { Label, Loading } from '@src/components';
import { Card } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//api
import { useVideoLessons } from '@src/api/modules/Learning';

//utils
import { dimensions } from '@src/utility/Constants';
import { ScaledSheet } from 'react-native-size-matters';
import Colors from '@src/utility/Colors';
import { useNavigation } from '@react-navigation/native';
import EmptyList from '@src/components/EmptyList';

export default function VideoSlideShow() {
  const navigation = useNavigation();
  const [{ data, loading, error, message }] = useVideoLessons();

  const handleItemPress = (item) => () => {
    navigation.navigate('VideoLessonDetail', { detail: item });
  };

  const _renderItem = useCallback(
    ({ item }) => (
      <Card style={styles.item} onPress={handleItemPress(item)}>
        <Icon
          name="play"
          size={30}
          color={Colors.whiteText}
          style={styles.playIcon}
        />
        <View style={styles.itemContent}>
          <Label weight="bold" style={styles.itemTitle}>
            {item.MessageTitle}
          </Label>
        </View>
      </Card>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  if (loading) {
    return <Loading style={styles.loading} />;
  }

  if (error) {
    <View style={styles.container}>
      <Label>{message}</Label>
    </View>;
  }

  if (!data.length) {
    return null;
  }

  return (
    <FlatList
      data={data}
      renderItem={_renderItem}
      keyExtractor={(item) => item.LearningFileId}
      style={styles.container}
      contentContainerStyle={styles.content}
      horizontal
      pagingEnabled
    />
  );
}

const itemHeight = dimensions.screen.height / 5;
const styles = ScaledSheet.create({
  container: {
    height: dimensions.screen.height / 4,
    backgroundColor: '#efefef',
  },
  content: {
    minWidth: dimensions.screen.width,
    paddingLeft: 16,
  },
  loading: {
    height: dimensions.screen.height / 4,
  },
  item: {
    width: dimensions.screen.width - 50,
    height: itemHeight,
    backgroundColor: '#e2e2e2',
    borderRadius: 6,
    alignSelf: 'center',
    marginRight: 16,
    overflow: 'hidden',
  },
  itemContent: {
    position: 'absolute',
    backgroundColor: 'grey',
    left: 0,
    right: 0,
    bottom: 0,
    padding: 16,
  },
  itemTitle: {
    color: Colors.whiteText,
  },
  playIcon: {
    position: 'absolute',
    alignSelf: 'flex-end',
    right: 3,
    top: 3,
    // elevation: 10,
    textShadowColor: 'rgba(0,0,0,0.5)',
    textShadowRadius: 3,
  },
});
