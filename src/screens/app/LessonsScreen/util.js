export async function learningFilesListResponseTransformer(data) {
  data.Result = data.map((el) =>
    el.ContentType?.includes('video')
      ? {
          ...el,
          VideoURL: el.FileURL,
          FileURL: undefined,
        }
      : el,
  );

  return data;
}
