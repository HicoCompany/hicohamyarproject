import Colors from '@src/utility/Colors';
import { StyleSheet } from 'react-native';

export const gradients = {
  colors: ['#38ef7d', '#38ef7d'],
  viewedColor: ['#F2F2F2', '#DBDBDB', '#EAEAEA'],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 0 },
  style: StyleSheet.absoluteFill,
};
// background: linear-gradient(to bottom, #D5DEE7 0%, #E8EBF2 50%, #E2E7ED 100%), linear-gradient(to bottom, rgba(0,0,0,0.02) 50%, rgba(255,255,255,0.02) 61%, rgba(0,0,0,0.02) 73%), linear-gradient(33deg, rgba(255,255,255,0.20) 0%, rgba(0,0,0,0.20) 100%);
//  background-blend-mode: normal,color-burn;

// background-image: linear-gradient(-225deg, #3D4E81 0%, #5753C9 48%, #6E7FF3 100%);
