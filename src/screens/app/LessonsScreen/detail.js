import Colors from '@src/utility/Colors';
import React, { useCallback, useEffect, useMemo } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';

// ui components
import {
  Caption,
  Title,
  Paragraph,
  IconButton,
  Divider,
} from 'react-native-paper';
import { HeaderBack, Label } from '@components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
//utils
import Share from 'react-native-share';
import { decode } from 'html-entities';

//constants
import { modifyNotificationCount } from '@src/redux/actions';
import { useDispatch } from 'react-redux';
import {
  useSetLearningToRead,
  useToggleLearningLike,
} from '@src/api/modules/Learning';

import { useAlert } from '@src/components/GlobalAlert';
import { fileUrlBuilder } from '@src/utility/base';

import PICTURE_ICON from '@assets/image/picture.png';

/**
 * @typedef {{VideoURL: string,ContentType:string,FileURL:string,validVideo: boolean,MessageTitle:string,LikeCount: number,ViewCount: number,MessageText: string,IsLiked: boolean,IsViewed: boolean}} params
 * @param {import('@react-navigation/stack').StackScreenProps<{LessonsDetail: params},'LessonsDetail'>} props
 */
export default function LessonDetail({ route: { params }, navigation }) {
  const dispatch = useDispatch();
  const Alert = useAlert();
  const [, likeHandler] = useToggleLearningLike({
    id: params.LearningFileId,
  });

  // eslint-disable-next-line no-unused-vars
  const [_, readHandler] = useSetLearningToRead({
    id: params.LearningFileId,
  });

  const {
    // VideoURL,
    // ContentType,
    // FileURL,
    // validVideo,
    ImagePath,
    MessageTitle,
    LikeCount,
    ViewCount,
    MessageText,
    IsLiked,
    IsViewed,
    LearningFileId,
  } = params;

  useEffect(() => {
    handleFileView();
  }, [handleFileView]);

  const handleShareFile = useCallback(() => {
    Share.open({
      title: MessageTitle,
      message: `${MessageTitle}\n${MessageText}`,
    }).catch(() => {});
  }, [MessageText, MessageTitle]);

  const handleFileView = useCallback(() => {
    if (IsViewed) return null;
    readHandler
      .handleFetch({
        data: {
          LearningFileId,
          FormName: LessonDetail.FormName,
        },
      })
      .then(() => {
        navigation.setParams({
          ...params,
          IsViewed: true,
          ViewCount: ViewCount + 1,
        });

        dispatch(
          modifyNotificationCount('lessons', (count) =>
            count > 0 ? count - 1 : 0,
          ),
        );
        params?.refreshPrevScreen?.();
      })
      .catch(() => {});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [IsViewed, LearningFileId, ViewCount, params]);

  const handleFileLike = useCallback(() => {
    likeHandler
      .handleFetch()
      .then(async (res) => {
        Alert.addToast({
          message: res.data,
          type: 'success',
          timeout: 'short',
        });
        navigation.setParams({
          ...params,
          IsLiked: !IsLiked,
          LikeCount: IsLiked ? LikeCount - 1 : LikeCount + 1,
        });

        params?.refreshPrevScreen?.();
      })
      .catch((e) => {});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [IsLiked, LearningFileId, LikeCount, params]);

  const title = useMemo(
    () => decode(MessageTitle).replace(/(<([^>]+)>)/gi, ''),
    [MessageTitle],
  );
  const message = useMemo(
    () => decode(MessageText).replace(/(<([^>]+)>)/gi, ''),
    [MessageText],
  );

  return (
    <View style={styles.container}>
      <HeaderBack headerTitle="جزئیات آموزش" />
      <ScrollView contentContainerStyle={styles.content}>
        <TouchableOpacity activeOpacity={1}>
          <View style={styles.mediaContainer}>
            {ImagePath?.length ? (
              <Image
                source={{
                  uri: fileUrlBuilder(ImagePath),
                }}
                defaultSource={PICTURE_ICON}
                style={styles.media}
              />
            ) : null}
          </View>

          <View style={styles.content}>
            <Title style={styles.title}>{title}</Title>
            <View style={styles.row}>
              <View style={styles.row}>
                <Icon name="heart" style={styles.icon} />
                <Label weight="bold">{LikeCount}</Label>
                <Caption> لایک شده</Caption>
              </View>
              <View style={styles.row}>
                <Icon name="eye" style={styles.icon} />
                <Label weight="bold">{ViewCount}</Label>
                <Caption> خوانده شده</Caption>
              </View>
            </View>
            <Divider style={styles.divider} />
            <Paragraph style={styles.text}>{message}</Paragraph>
          </View>
        </TouchableOpacity>
      </ScrollView>
      <View style={styles.actionsContainer}>
        <IconButton
          icon={IsLiked ? 'heart' : 'heart-outline'}
          size={30}
          color={IsLiked ? Colors.error : '#444'}
          onPress={handleFileLike}
        />
        <IconButton
          icon={'share-variant'}
          size={30}
          color={'#444'}
          onPress={handleShareFile}
        />
      </View>
    </View>
  );
}

LessonDetail.initialParams = {
  VideoURL: '',
  ContentType: '',
  FileURL: '',
  validVideo: false,
  MessageTitle: '',
  LikeCount: 0,
  ViewCount: 0,
  MessageText: '',
  IsLiked: false,
  IsViewed: false,
  LearningFileId: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  content: {
    width: '100%',
    padding: 16,
    paddingBottom: 100,
  },
  mediaContainer: {
    overflow: 'hidden',
    borderRadius: 6,
    backgroundColor: '#efefef',
  },
  media: {
    aspectRatio: 16 / 9,
    resizeMode: 'contain',
  },
  title: {
    textAlign: 'right',
  },
  text: {
    textAlign: 'right',
  },
  actionsContainer: {
    ...StyleSheet.absoluteFill,
    top: undefined,
    bottom: 0,
    backgroundColor: '#fff',
    height: 65,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    // marginBottom: -10,
    justifyContent: 'space-between',
  },
  row: {
    flexDirection: 'row-reverse',
    minWidth: '50%',
    alignItems: 'center',
  },
  icon: {
    marginLeft: 5,
    color: Colors.gray,
  },
  error: {
    ...StyleSheet.absoluteFill,
    textAlign: 'center',
    textAlignVertical: 'center',
    width: '100%',
    height: '100%',
  },
  divider: {
    marginVertical: 16,
  },
});
