import React, { useCallback } from 'react';
import { StyleSheet, FlatList, View, Dimensions } from 'react-native';

//ui components
import { Card, Divider, Button } from 'react-native-paper';
import { Empty, HeaderBack, Label } from '@components';

import LinearGradient from 'react-native-linear-gradient';

//utils
import { learningFilesListResponseTransformer } from './util';
import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';

//api
import { gradients } from './const';
import VideoSlideShow from './VideoSlideShow';
import { useLessons } from '@src/api/modules/Learning';
import EmptyList from '@src/components/EmptyList';
import { fileUrlBuilder } from '@src/utility/base';

import PICTURE_ICON from '@assets/image/picture.png';

const { height } = Dimensions.get('window');

/**
 * @param {{navigation: import('@react-navigation/stack').StackNavigationProp<any,any>}}
 */
export default function Lessons({ navigation }) {
  const [listState, listHandlers] = useLessons({
    transformResponse: learningFilesListResponseTransformer,
  });

  const handleOpenDetail = (data) => () => {
    navigation.navigate('LessonDetail', {
      ...data,
      refreshPrevScreen: listHandlers.handleRefreshing,
    });

    const timeout = setTimeout(() => {
      listHandlers.setState((prev) => ({
        ...prev,
        data: prev.data.map((el) =>
          el.LearningFileId === data.LearningFileId
            ? { ...el, IsViewed: true }
            : el,
        ),
      }));
      clearTimeout(timeout);
    }, 1000);
  };

  const _renderItem = useCallback(
    ({ item }) => {
      return (
        <Card
          onPress={handleOpenDetail(item)}
          elevation={item.IsViewed ? 1 : 2}
          style={[
            styles.item,

            item.ImageThumbPath ? undefined : styles.itemWithoutCover,
          ]}>
          <Card.Cover
            style={styles.media}
            source={{
              uri: fileUrlBuilder(item.ImagePath),
            }}
            defaultSource={PICTURE_ICON}
            progressiveRenderingEnabled
            resizeMode="cover"
          />

          <View style={styles.itemContent}>
            <LinearGradient
              {...gradients}
              colors={item.IsViewed ? gradients.viewedColor : gradients.colors}
            />
            <Label style={styles.title} weight="bold">
              {item.MessageTitle}
            </Label>
            <Button
              onPress={handleOpenDetail(item)}
              style={styles.detail}
              color={'#333'}
              compact
              icon="text">
              جزئیات
            </Button>
          </View>
        </Card>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <View style={styles.container}>
      <HeaderBack headerTitle="آموزش های شهروندی" />
      <FlatList
        data={listState.data}
        renderItem={_renderItem}
        refreshing={listState.loading || listState.refreshing}
        onRefresh={listHandlers.handleRefreshing}
        keyExtractor={keyExtractor}
        contentContainerStyle={styles.list}
        ItemSeparatorComponent={Divider}
        ListHeaderComponent={VideoSlideShow}
        ListEmptyComponent={() =>
          !listState.loading ? (
            <EmptyList message="آموزشی برای نمایش یافت نشد" />
          ) : null
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    // alignItems: 'center',
    paddingBottom: 20,
  },
  item: {
    width: '90%',
    minHeight: 200,
    marginTop: 16,
    alignSelf: 'center',
    overflow: 'hidden',
    marginBottom: 16,
  },
  itemWithoutCover: { minHeight: 50, height: 50 },
  itemContent: {
    // position: 'absolute',
    // bottom: 0,
    width: '100%',
    height: 50,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    paddingHorizontal: 8,
  },
  media: {
    // ...StyleSheet.absoluteFill,
    width: '100%',
    aspectRatio: 16 / 9,
    alignSelf: 'center',
  },
  title: {
    fontSize: normalize(13),
    height: '100%',
    textAlignVertical: 'center',
    color: '#333',
  },
  detail: {},
  modalContent: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    minHeight: height / 1.5,
    maxHeight: height - 50,
    backgroundColor: 'white',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    left: 0,
    right: 0,
    padding: 16,
  },
  line: {
    // borderWidth: 1,
    width: '20%',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 15,
    marginTop: -5,
    height: 2,
    backgroundColor: Colors.gray,
  },
  modal: {
    padding: 0,
    margin: 0,
  },
});

const keyExtractor = (item) => item.LearningFileId.toString();
