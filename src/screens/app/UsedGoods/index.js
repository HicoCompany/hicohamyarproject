import React, { useCallback } from 'react';
import { View, FlatList, RefreshControl } from 'react-native';

//components
import { ButtonSubmit, Label } from '@src/components';
import SelectCounter from '@components/SelectCounter';
import HeaderBack from '@src/components/HeaderBack';

//network
import { useWastes } from '@src/api/modules/Waste';

//utils
import { fileUrlBuilder } from '@src/utility/base';
import { ScaledSheet } from 'react-native-size-matters';
import { useMemo } from 'react';
import EmptyList from '@src/components/EmptyList';

const VELOCITY_CHANGE = 1;
export default function UsedGoods({ navigation }) {
  const [WasteState, WasteHandler] = useWastes({
    page: 1,
    wasteType: 'usedGoods',
    limit: 30,
  });

  const selectedWastes = useMemo(
    () => WasteState.data.filter((el) => el.Selected && el.Count > 0),
    [WasteState.data],
  );

  // const { totalPrice, totalPoint } = useMemo(() => {
  //   return selectedWastes?.reduce(
  //     (prev, item) => {
  //       if (item.Selected) {
  //         prev.totalPrice += item.Count * item.HelperPrice;
  //         prev.totalPoint += item.Count * item.Point;
  //       }

  //       return prev;
  //     },
  //     { totalPrice: 0, totalPoint: 0 },
  //   );
  // }, [selectedWastes]);

  const handleDecreaseItemValue = useCallback(
    (WasteId) => () => {
      WasteHandler.setState((state) => {
        const data = [...state.data];
        const index = data.findIndex((el) => el.WasteId === WasteId);
        if (index == -1) return state;

        data[index] = {
          ...data[index],
          Count: data[index].Count - VELOCITY_CHANGE,
        };
        return {
          ...state,
          data,
        };
      });
    },
    [WasteHandler],
  );

  const handleIncreaseItemValue = useCallback(
    (WasteId) => () => {
      WasteHandler.setState((state) => {
        const data = [...state.data];
        const index = data.findIndex((el) => el.WasteId === WasteId);
        if (index == -1) return state;

        data[index] = {
          ...data[index],
          Count: data[index].Count + VELOCITY_CHANGE,
        };
        return {
          ...state,
          data,
        };
      });
    },
    [WasteHandler],
  );

  const handleSelectItem = useCallback(
    (WasteId) => () => {
      WasteHandler.setState((state) => {
        const data = [...state.data];
        const index = data.findIndex((el) => el.WasteId === WasteId);
        if (index == -1) return state;

        data[index] = {
          ...data[index],
          Selected: !data[index].Selected,
          Count: 0,
        };
        return {
          ...state,
          data,
        };
      });
    },
    [WasteHandler],
  );

  const _renderItem = useCallback(
    ({ item }) => (
      <SelectCounter
        selected={item.Selected}
        name={item.Title}
        imageSrc={{ uri: fileUrlBuilder(item.File) }}
        onDecrease={handleDecreaseItemValue(item.WasteId)}
        onIncrease={handleIncreaseItemValue(item.WasteId)}
        onSelectionChange={handleSelectItem(item.WasteId)}
        value={item.Count}
      />
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const handleNavigateToForm = useCallback(() => {
    navigation.navigate('UsedGoodSubmitFormScreen', {
      list: selectedWastes,
    });
  }, [selectedWastes, navigation]);

  return (
    <View style={styles.container}>
      <HeaderBack headerTitle={''} />
      <Label
        style={styles.title}
        weight="bold"
        fontSize={14}
        color="placeholder">
        کالا های خود را جهت جمع آوری انتخاب کنید
      </Label>

      <FlatList
        data={WasteState.data}
        renderItem={_renderItem}
        keyExtractor={(item) => String(item.WasteId)}
        numColumns={3}
        style={styles.list}
        refreshControl={
          <RefreshControl
            refreshing={WasteState.loading || WasteState.refreshing}
            onRefresh={WasteHandler.handleRefreshing}
          />
        }
        ListEmptyComponent={
          <EmptyList message={'کالای جهت انتخاب یافت نشد.'} />
        }
      />
      {WasteState.data?.length ? (
        <ButtonSubmit
          onPress={handleNavigateToForm}
          disabled={selectedWastes.length < 1}>
          تکمیل درخواست
        </ButtonSubmit>
      ) : null}
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  title: {
    height: '40@s',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  listHeader: {
    backgroundColor: '#efefef',
    margin: '8@s',
    paddingHorizontal: '8@s',
    height: '40@s',
    borderRadius: '6@s',
    flexDirection: 'row-reverse',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    elevation: 1,
  },
  help: {
    paddingTop: '8@s',
    padding: '12@s',
    elevation: 1,
    backgroundColor: '#f1f1f1',
  },
  list: {
    flexGrow: 1,
  },
});
