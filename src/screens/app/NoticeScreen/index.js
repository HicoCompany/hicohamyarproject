import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  BackHandler,
  StyleSheet,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal, Badge } from 'react-native-paper';
import { connect, useSelector } from 'react-redux';
import HeaderMain from '@src/components/HeaderMain';
import TabItem from '@src/components/TabItem';
import { hp, normalize, wp } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import { Button, Loading } from '@src/components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Notification from '@src/api/modules/Notification';
import { fileUrlBuilder } from '@src/utility/base';
import EmptyList from '@src/components/EmptyList';

//assets

class NoticeScreen extends Component {
  static navigationOptions = () => {
    return {
      tabBarIcon: ({ color }) => <TabItemWithBadge color={color} />,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      refreshing: false,
      loadingData: false,

      apiError: false,
      apiErrorDesc: '',
    };
  }

  componentDidMount = () => {
    this.getApi();
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  getApi = async () => {
    await this.phoneGetNotificationForHelperApi();
  };

  async phoneGetNotificationForHelperApi() {
    try {
      const { data } = await Notification.GetNotificationForHelper();

      this.setState({
        list: data,
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  handleLoadMore = () => {
    // if (this.state.countList > 0) {
    //     this.setState({
    //         offset: this.state.offset + this.state.pageSize, loadingData: true
    //     }, async () => {
    //         await this.phoneGetNotificationForHelperApi();
    //     });
    // }
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true,
        isLoadingPage: true,
        list: [],
      },
      () => {
        this.phoneGetNotificationForHelperApi();
      },
    );
  };

  renderEmpty = () => {
    return this.state.isLoadingPage ? (
      <View style={{ flex: 1 }}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <EmptyList message={'اطلاع رسانی برای نمایش یافت نشد'} />
    );
  };

  renderFooter = () => {
    if (!this.state.loadingData) {
      return null;
    }
    return <ActivityIndicator size={'small'} color={'black'} />;
  };

  renderItem(item) {
    const { CreateDateStr, ImagePath, Like, Title } = item;
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        activeOpacity={0.7}
        onPress={() => {
          this.props.navigation.navigate('NoticeDetail', {
            detail: item,
            refreshPrevScreen: this.handleRefresh,
          });
        }}>
        <View style={styles.itemLeftContainer}>
          <Text style={styles.itemTextTitle}>{Title}</Text>
          <Text style={styles.itemTextDate}>{`تاریخ : ${CreateDateStr}`}</Text>
          <View style={styles.itemSocial}>
            <Icon name="eye" style={[styles.IconDisabled]} size={25} />
            <Text style={styles.itemTextTitle}>{item.View}</Text>
            <Icon name="heart" style={[styles.IconRed]} size={25} />
            <Text style={styles.itemTextTitle}>{Like}</Text>
          </View>
        </View>
        <View style={styles.itemRightContainer}>
          <Image
            source={{ uri: fileUrlBuilder(ImagePath) }}
            resizeMode={'contain'}
            style={styles.itemImage}
          />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderMain
          onMenuPress={() => BackHandler.exitApp()}
          onPinPress={() => this.props.navigation.navigate('boothDynamic')}
          headerTitle={'اطلاع رسانی'}
        />
        {/* <HeaderMain
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                /> */}
        {/* <View style={styles.screenHeader}>
                    <Text style={styles.screenHeaderTitle}>
                        {'لیست پیام ها ، اطلاعیه ها ، طرح های تشویقی و ...'}
                    </Text>
                </View> */}
        <FlatList
          data={this.state.list}
          renderItem={({ item }) => this.renderItem(item)}
          ListEmptyComponent={() => this.renderEmpty()}
          ListFooterComponent={() => this.renderFooter()}
          numColumns={1}
          keyExtractor={(item, index) => item.NoticesID.toString()}
          refreshing={this.state.refreshing}
          onRefresh={() => this.handleRefresh()}
          onEndReached={() => this.handleLoadMore()}
          onEndReachedThreshold={0.08}
          contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
        />
        <Portal>
          <Dialog
            visible={this.state.apiError}
            style={styles.dialogContainer}
            dismissable
            onDismiss={() => {
              this.setState({
                apiError: false,
              });
            }}>
            <Dialog.Content>
              <Text style={styles.dialogText}>{this.state.apiErrorDesc}</Text>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                buttonText={'تلاش مجدد'}
                onPress={() => {
                  this.setState(
                    {
                      apiError: false,
                      isLoadingPage: true,
                      list: [],
                    },
                    () => {
                      this.getApi();
                    },
                  );
                }}
                style={styles.dialogButton}
              />
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

function TabItemWithBadge({ color }) {
  const { notices, loading } = useSelector(
    (state) => state.global.notificationsCount,
  );
  return (
    <View style={tabItemStyle.container}>
      <TabItem
        name={'اطلاع رسانی'}
        source={require('@src/assets/image/TabLearn.png')}
        color={color}
      />
      {loading || notices ? (
        <View style={tabItemStyle.badgeContainer}>
          <ActivityIndicator
            style={styles.loading}
            size={15}
            color="white"
            animating={loading}
          />
          {!loading && (
            <Badge size={20} style={tabItemStyle.badge}>
              {notices}
            </Badge>
          )}
        </View>
      ) : null}
    </View>
  );
}
const tabItemStyle = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
  },
  badge: {
    position: 'absolute',
    backgroundColor: Colors.green,
    color: 'white',
  },
  loading: {
    position: 'absolute',
  },
  badgeContainer: {
    position: 'absolute',
    left: 40,
    top: 10,
    height: 20,
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    overflow: 'hidden',
    backgroundColor: Colors.green,
  },
});

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.disabled,
  },
  screenHeader: {
    height: hp('7%'),
    backgroundColor: '#424242',
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenHeaderTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },

  itemContainer: {
    flexDirection: 'row',
    height: hp('20%'),
    margin: 5,
    borderRadius: 10,
    borderColor: Colors.buttonlightdark,
    backgroundColor: Colors.backgroundColor,
    borderWidth: 1,
  },
  itemSocial: {
    marginTop: hp('5%'),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  itemRightContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 10,
  },
  itemImage: {
    height: wp('30%'),
    width: wp('35%'),
    borderRadius: 10,
    marginRight: 10,
  },
  itemLeftContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  itemTextTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },

    color: Colors.buttonlightdark,
  },
  itemTextDate: {
    fontSize: normalize(10),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.buttonlightdark,
  },
  IconDisabled: {
    color: Colors.disabled,

    paddingHorizontal: 5,
  },
  IconRed: {
    color: Colors.red,
    paddingHorizontal: 5,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
  separate: {
    marginBottom: 20,
    marginTop: 20,
    height: 0.8,
    width: wp('70%'),
    backgroundColor: Colors.buttonlightdark,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(NoticeScreen);
