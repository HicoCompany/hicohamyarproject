import React, { Component } from 'react';
import {
  ScrollView,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import { Button, Loading } from '@src/components';
import { normalize, wp } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import HeaderBack from '@src/components/HeaderBack';
import { fetchNotificationCount } from '@src/redux/actions';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Notification from '@src/api/modules/Notification';
import { fileUrlBuilder } from '@src/utility/base';
import {
  DefaultModalActions,
  getGlobalAlert,
} from '@src/components/GlobalAlert';

class NoticeDetailScreen extends Component {
  FormName = 'جزئیات اطلاع رسانی';
  GlobalAlert = getGlobalAlert();

  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      detail: {
        CreateDateStr: '',
        ImagePath: '',
        NoticesID: 1,
        Text: '',
        Title: '',
        Like: 0,
      },
      Like: 0,
    };
  }

  componentDidMount = () => {
    this.getApi();
    this.props.fetchNotificationCount();
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  getApi = async () => {
    await this.phoneGetNotificationByIDApi();
  };
  async PhoneAddLikeNotificationForHelperApi() {
    try {
      const { data } = await Notification.AddLikeNotificationForHelper({
        data: {
          NoticesID: this.props.route.params.detail.NoticesID,
        },
      });

      this.setState({
        Like: this.state.Like === 1 ? 0 : 1,
        isLoadingPage: false,
      });
      this.GlobalAlert.addToast({
        message: data,
        type: 'success',
        timeout: 'short',
      });
      this.props.route.params?.refreshPrevScreen?.();
    } catch (error) {
      this.setState({
        isLoadingPage: false,
      });
      this.GlobalAlert.setError(
        DefaultModalActions.Error({
          message: error.message,
        }),
      );
    }
  }
  async phoneGetNotificationByIDApi() {
    try {
      const { data } = await Notification.GetNotificationByID({
        params: {
          noticesID: this.props.route.params.detail.NoticesID,
        },
      });

      this.setState({
        detail: data,
        Like: data.Like,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  render() {
    const { ImagePath, Title } = this.props.route.params.detail;
    return this.state.isLoadingPage ? (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={Title} />
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={Title} />
        <ScrollView>
          <Image
            style={styles.image}
            resizeMode={'cover'}
            source={{ uri: fileUrlBuilder(ImagePath), cache: 'force-cache' }}
          />
          <Text style={styles.Title}>{this.state.detail.Title}</Text>
          <TouchableOpacity
            onPress={() => this.PhoneAddLikeNotificationForHelperApi()}>
            <Icon
              name="heart"
              size={30}
              style={this.state.Like === 1 ? styles.IconRed : styles.IconGray}
            />
          </TouchableOpacity>
          <Text style={styles.desc}>{this.state.detail.Text}</Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  image: {
    minWidth: '100%',
    aspectRatio: 1,
  },
  Title: {
    fontSize: normalize(15),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '700',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    textAlign: 'center',
    color: Colors.green,
    margin: wp('5%'),
    marginTop: wp('2%'),
  },
  desc: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'right',
    color: Colors.textGray,
    margin: wp('5%'),
    marginTop: wp('2%'),
  },

  IconRed: {
    textAlign: 'center',
    color: Colors.red,
    borderRadius: 5,
  },
  IconGray: {
    textAlign: 'center',
    color: Colors.gray,
    borderRadius: 5,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {
  fetchNotificationCount,
};

export default connect(mapStateToProps, mapActionToProps)(NoticeDetailScreen);
