import React, { Component } from 'react';
import { View, StatusBar, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import Colors from '@src/utility/Colors';
import { exitApp, fetchSuggestions } from '@src/redux/actions';
//ui
import TabItem from '@components/TabItem';
import HeaderMain from '@components/HeaderMain';
import Section from '@src/components/sections';
import { Container, Content } from 'native-base';
import { Badge } from 'react-native-paper';
//assets
import SUGGESTIONS from '@src/assets/image/suggestion.png';
import USER_INFORMATION from '@src/assets/image/InformationUser.png';
import INVITE_FRIEND from '@src/assets/image/InvitedToFriend.png';
import ABOUT_US from '@src/assets/image/AboutUs.png';
import MANAGE_USER_ICON from '@src/assets/image/TabManageUser.png';
import SUPPORT_ICON from '@src/assets/image/support.png';
import TEACHING_ICON from '@src/assets/image/teaching.png';
import GUID_ICON from '@src/assets/image/Guid.png';
import LOGOUT_ICON from '@src/assets/image/logout.png';
import { Persistor } from '@src/redux/store';

class ManageUserScreen extends Component {
  static navigationOptions = () => {
    return {
      tabBarIcon: ({ color }) => (
        <TabItem
          name={'مدیریت کاربر '}
          source={MANAGE_USER_ICON}
          color={color}
        />
      ),
    };
  };

  onLogOutClick = () => {
    Persistor.purge();
  };

  handleBoothsScreenNavigation = () => {
    this.props.navigation.navigate('BoothsScreen');
  };

  handleNavigationToProfile = () => this.props.navigation.navigate('Profile');
  handleNavigationToIntroduction = () =>
    this.props.navigation.navigate('Introduction');
  handleNavigationToSupport = () => this.props.navigation.navigate('Support');
  handleNavigationToSuggestions = () =>
    this.props.navigation.navigate('Suggestions');
  handleNavigationToGuid = () => this.props.navigation.navigate('Guid');
  handleNavigationToLessons = () => this.props.navigation.navigate('Lessons');
  handleNavigationToAbout = () => this.props.navigation.navigate('About');

  render() {
    // const newUpdates = this.props.suggestions.data.filter(
    //   (v) => v.VisitAnswer === 3,
    // ).length;

    // const { suggestions, lessons } = this.props.notificationCount;

    return (
      <Container>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderMain
          onPinPress={this.handleBoothsScreenNavigation}
          headerTitle={'مديريت كاربر'}
        />
        <Content>
          <View style={styles.container}>
            <Section
              label="اطلاعات کاربر"
              source={USER_INFORMATION}
              onPress={this.handleNavigationToProfile}
            />
            <Section
              label="معرفی به دوستان"
              source={INVITE_FRIEND}
              onPress={this.handleNavigationToIntroduction}
            />
            <Section
              label="ارتباط به پشتیبان"
              source={SUPPORT_ICON}
              onPress={this.handleNavigationToSupport}
            />
            <Section
              label="انتقادات و پشنهادات"
              source={SUGGESTIONS}
              onPress={this.handleNavigationToSuggestions}
              badge={this.props.notificationsCount.suggestions}
              loading={this.props.notificationsCount.loading}
            />

            <Section
              label="راهنما"
              source={GUID_ICON}
              onPress={this.handleNavigationToGuid}
            />
            <Section
              label="آموزش های شهروندی"
              source={TEACHING_ICON}
              onPress={this.handleNavigationToLessons}
              badge={this.props.notificationsCount?.lessons}
              loading={this.props.notificationsCount?.loading}
            />
            <Section
              label="درباره ما"
              source={ABOUT_US}
              onPress={this.handleNavigationToAbout}
            />
            <Section
              label="خروج از حساب کاربری"
              source={LOGOUT_ICON}
              onPress={this.onLogOutClick}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 16,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    notificationsCount: state.global.notificationsCount,
  };
};

const mapActionToProps = {
  fetchSuggestions,
};

export default connect(mapStateToProps, mapActionToProps)(ManageUserScreen);
