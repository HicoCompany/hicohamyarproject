import React, { Component } from 'react';
import { Image, ScrollView, StatusBar, Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { Loading } from '@src/components';
import { hp, normalize, wp } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import HeaderBack from '@src/components/HeaderBack';
import { fileUrlBuilder } from '@src/utility/base';
import { getAdvertisementDetail } from '@src/api/modules/Advertisement';
import {
  DefaultModalActions,
  getGlobalAlert,
} from '@src/components/GlobalAlert';

class AdvertismentDetailScreen extends Component {
  GlobalAlert = getGlobalAlert();
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      detail: {
        CreateDateStr: '',
        ImagePath: '',
        AdvertisementID: 1,
        Text: '',
        Title: '',
        Like: 0,
      },
      Like: 0,
    };
  }

  componentDidMount = () => {
    this.getApi();
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  getApi = async () => {
    await this.PhoneGetAdvertisementByIDForHelperApi();
  };

  async PhoneGetAdvertisementByIDForHelperApi() {
    const { AdvertisementID } = this.props.route.params.item;

    try {
      const { data } = await getAdvertisementDetail({
        advertisementID: AdvertisementID,
      });

      this.setState({
        detail: data,
        Like: data.Like,
        loading: false,
      });
    } catch (error) {
      this.GlobalAlert.setError(
        DefaultModalActions.Error({
          message: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
        }),
      );
      this.setState({
        loading: false,
      });
    }
  }

  render() {
    const { ImagePath, Title } = this.props.route.params.item;
    return this.state.loading ? (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={Title} />
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={Title} />
        <ScrollView>
          <Image
            style={styles.image}
            resizeMode={'cover'}
            source={{ uri: fileUrlBuilder(ImagePath) }}
          />
          <Text style={styles.Title}>{this.state.detail.Title}</Text>
          <Text style={styles.desc}>{this.state.detail.Text}</Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  image: {
    height: hp('40%'),
    width: wp('90%'),
    margin: wp('5%'),
    marginBottom: wp('2%'),
  },
  Title: {
    fontSize: normalize(15),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '700',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    textAlign: 'center',
    color: Colors.green,
    margin: wp('5%'),
    marginTop: wp('2%'),
  },
  desc: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'right',
    color: Colors.textGray,
    margin: wp('5%'),
    marginTop: wp('2%'),
  },

  IconRed: {
    textAlign: 'center',
    color: Colors.red,
    borderRadius: 5,
  },
  IconGray: {
    textAlign: 'center',
    color: Colors.gray,
    borderRadius: 5,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(AdvertismentDetailScreen);
