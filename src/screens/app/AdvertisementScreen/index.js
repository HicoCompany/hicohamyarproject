import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import HeaderMain from '@components/HeaderMain';
import TabItem from '@components/TabItem';
import { hp, normalize, wp } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import { Loading } from '@components';
import { getAdvertisements } from '@src/api/modules/Advertisement';
import { fileUrlBuilder } from '@src/utility/base';
import {
  DefaultModalActions,
  getGlobalAlert,
} from '@src/components/GlobalAlert';
import EmptyList from '@src/components/EmptyList';

class AdvertismentScreen extends Component {
  GlobalAlert = getGlobalAlert();
  static navigationOptions = () => {
    return {
      tabBarIcon: ({ color }) => (
        <TabItem
          name={'تبلیغات'}
          source={require('@src/assets/image/TabAdv.png')}
          color={color}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      list: [],
      loadMore: false,
      refreshing: false,
      loading: false,
    };
  }

  componentDidMount = () => {
    this.getData({
      loading: true,
    })();
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  getData = ({
    loadMore = false,
    refreshing = false,
    loading = false,
  }) => async () => {
    try {
      this.setState({
        loadMore,
        refreshing,
        loading,
      });
      const { data } = await getAdvertisements();
      this.setState({
        list: data,
        loadMore: false,
        refreshing: false,
        loading: false,
      });
    } catch (error) {
      this.GlobalAlert.setError(
        DefaultModalActions.Error({
          message: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
        }),
      );
      this.setState({
        loadMore: false,
        refreshing: false,
        loading: false,
      });
    }
  };

  handleLoadMore = () => {
    // if (this.state.countList > 0) {
    //     this.setState({
    //         offset: this.state.offset + this.state.pageSize, loadingData: true
    //     }, async () => {
    //         await this.PhoneGetAdvertisementForHelperApi();
    //     });
    // }
  };

  handleRefresh = () => {};

  renderEmpty = () => {
    const newLocal = 'تبلیغی برای نمایش یافت نشد';
    return this.state.loading ? (
      <View style={styles.grow}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <EmptyList message={newLocal} />
    );
  };

  renderFooter = () => {
    if (!this.state.loadMore) {
      return null;
    }
    return <ActivityIndicator size={'small'} color={'black'} />;
  };

  renderItem = ({ item }) => {
    const {
      CreateDateStr,
      ImagePath,
      // Like,
      // AdvertisementID,
      Title,
      //View
    } = item;
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        activeOpacity={0.7}
        onPress={() => {
          this.props.navigation.navigate('AdvertisementDetail', {
            item,
          });
        }}>
        <View style={styles.itemLeftContainer}>
          <Text style={styles.itemTextTitle}>{Title}</Text>
          <Text style={styles.itemTextDate}>{`تاریخ : ${CreateDateStr}`}</Text>
        </View>
        <View style={styles.itemRightContainer}>
          <Image
            source={{ uri: fileUrlBuilder(ImagePath) }}
            resizeMode={'contain'}
            style={styles.itemImage}
          />
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderMain
          onMenuPress={() => BackHandler.exitApp()}
          onPinPress={() => this.props.navigation.navigate('boothDynamic')}
          headerTitle={'تبلیغات'}
        />
        <FlatList
          data={this.state.list}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty}
          numColumns={1}
          keyExtractor={(item) => item.AdvertisementID.toString()}
          refreshing={this.state.refreshing}
          onRefresh={this.getData({
            refreshing: true,
          })}
          // ListFooterComponent={() => this.renderFooter()}
          // onEndReachedThreshold={0.08}
          contentContainerStyle={this.state.list.length > 0 ? {} : styles.grow}
        />
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.disabled,
  },
  screenHeader: {
    height: hp('7%'),
    backgroundColor: '#424242',
    justifyContent: 'center',
    alignItems: 'center',
  },
  screenHeaderTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },

  itemContainer: {
    flexDirection: 'row',
    height: hp('20%'),
    margin: 5,
    borderRadius: 10,
    borderColor: Colors.buttonlightdark,
    backgroundColor: Colors.backgroundColor,
    borderWidth: 1,
  },
  itemSocial: {
    marginTop: hp('5%'),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  itemRightContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 10,
  },
  itemImage: {
    height: wp('30%'),
    width: wp('35%'),
    borderRadius: 10,
    marginRight: 10,
  },
  itemLeftContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  itemTextTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },

    color: Colors.buttonlightdark,
  },
  itemTextDate: {
    fontSize: normalize(10),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.buttonlightdark,
  },
  IconDisabled: {
    color: Colors.disabled,

    paddingHorizontal: 5,
  },
  IconRed: {
    color: Colors.red,
    paddingHorizontal: 5,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
  separate: {
    marginBottom: 20,
    marginTop: 20,
    height: 0.8,
    width: wp('70%'),
    backgroundColor: Colors.buttonlightdark,
    alignSelf: 'center',
  },
  grow: {
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(AdvertismentScreen);
