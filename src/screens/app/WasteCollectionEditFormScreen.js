/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  StatusBar,
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  Image,
} from 'react-native';
import {
  Button,
  Card,
  Dialog,
  HelperText,
  IconButton,
} from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';

import { useFormik } from 'formik';

import Colors from '@src/utility/Colors';
import HeaderBack from '@src/components/HeaderBack';
import TextInput from '@components/TextInput';
import Label from '@src/components/Label';
import { Loading, Select } from '@src/components';

import {
  useDeliveryTime,
  useUpdateWasteRequest,
  useWasteCollectionDetail,
  useWastes,
} from '@src/api/modules/Waste';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import AddressInput from '@src/components/Addresses';
import { selectAddress } from '@src/redux/slices/AddressesSlice';
import { useDispatch, useSelector } from 'react-redux';
import { fileUrlBuilder } from '@src/utility/base';
import { Months } from '@src/utility/Constants';

/**
 * @typedef {{ DeliverDateCreate: string,CreateDateStr: string,CreateTimeStr: string,DeliveryDateStr: string,DeliveryTimeStr: string,DeliveryWasteID: number,StatusStr:string,Status: number}} Request
 * @param {{navigation: import('@react-navigation/stack').NavigationStackScreenProps<any,{request: Request}>}} param0
 */
export default function EditRequestScreen({ navigation, route }) {
  const dayRef = useRef();
  const Alert = useAlert();
  const dispatch = useDispatch();
  const [wasteDialog, setWasteDialog] = useState(false);
  const { request = {}, sendResult = () => {} } = route.params;
  const MIN_WASTE_WEIGHT = useSelector(
    (state) => state.global.minWeightOnDeliveryRequest,
  );

  const dateSelectRef = useRef();

  const [wastesState, wastesHandler] = useWastes(
    {
      wasteType: 'wastes',
    },
    {
      requestOnMount: false,
      onError(e) {
        Alert.addToast({
          message: e.message,
          type: 'error',
          timeout: 'short',
        });
      },
    },
  );

  const [detailState] = useWasteCollectionDetail(
    {
      deliveryWasteId: request?.DeliveryWasteID,
    },
    {
      onSuccess({ data }) {
        if (data) {
          convertWasteArray(data.Result);
          dispatch(selectAddress(data.HelperAddressId));
          setFieldValue('addressId', data.HelperAddressId);

          timesHandler
            .handleFetch({
              params: {
                mainAreaID: data.MainAreaCode,
              },
            })
            .then(({ data: timeData }) => {
              setSelectedTime(data, timeData?.Result);
            });
        }
      },
      onError(e) {
        Alert.addToast({
          message: e.message,
          type: 'error',
          timeout: 'short',
        });
      },
    },
  );

  function setSelectedTime(detail, times = []) {
    const selectedTime = {
      FromTime: detail.SettingCollectFromTime,
      ToTime: detail.SettingCollectToTime,
      DeliverDateStr: detail.DeliveryDateStr,
      SettringCollectID: `${detail.DeliveryDateStr}-10`,
    };
    const foundTimeInTimesList = times.find(
      (el) =>
        el.DeliverDateStr === selectedTime.DeliverDateStr &&
        el.FromTime === selectedTime.FromTime &&
        el.ToTime === selectedTime.ToTime,
    );

    const [Year, Month, MDay] = selectedTime.DeliverDateStr?.split('/');

    const finalSelectedTime = {
      ...selectedTime,
      ...foundTimeInTimesList,
      Year,
      Month,
      MDay: parseInt(MDay, 10),
      MonthName: Months[parseInt(Month - 1, 10)],
    };

    setFieldValue('selectedDate', finalSelectedTime);

    if (foundTimeInTimesList) {
      const timePeriodList = times.filter(
        (el) => el.DeliverDateStr === foundTimeInTimesList.DeliverDateStr,
      );
      setFieldValue('timePeriodList', timePeriodList);
      setFieldValue('selectedDateTime', foundTimeInTimesList);
    } else {
      setFieldValue('timePeriodList', [finalSelectedTime]);
      setFieldValue('selectedDateTime', finalSelectedTime);
    }
  }

  const { values, setFieldValue, handleSubmit } = useFormik({
    initialValues: {
      addressId: undefined,
      areaCode: undefined,
      deliverDate: '',
      deliverDay: '',
      deliverTime: '',
      selectedDate: {},
      selectedDateTime: {},
      timePeriodList: [],
      items: [],
      FormName: this.FormName,
    },
    onSubmit: () => {
      updateHandler
        .handleFetch()
        .then(() => {
          sendResult();
          Alert.addToast({
            message: 'با موفقیت ویرایش شد.',
            type: 'success',
            timeout: 'short',
          });
          navigation.goBack();
        })
        .catch((e) => {
          Alert.setAction(
            DefaultModalActions.RetryOrBack({
              message: e.message,
              onRefresh: handleSubmit,
            }),
          );
        });
    },
  });

  const itemsToShow = useMemo(() => values.items.filter((v) => v.selected), [
    values.items,
  ]);

  const selectedWastesIdAndCount = useMemo(
    () =>
      itemsToShow.map((v) => ({
        WasteId: v.WasteId,
        WasteTypeCount: parseFloat(v.WasteTypeCount, 10).toFixed(1),
      })),
    [values.items],
  );

  const selectedWasteWeightsSum = useMemo(
    () =>
      itemsToShow.reduce(
        (weight, item) => weight + parseFloat(item.WasteTypeCount),
        0,
      ),
    [itemsToShow],
  );

  const [{ loading: submitLoading }, updateHandler] = useUpdateWasteRequest({
    addressId: values?.addressId,
    date: values.selectedDateTime?.DeliverDateStr,
    timeId: values.selectedDateTime?.SettringCollectID,
    requestId: detailState?.data?.DeliveryWasteID,
    wastes: selectedWastesIdAndCount,
  });

  const [timesState, timesHandler] = useDeliveryTime(
    {},
    {
      initialData: {
        uniqByDeliverDateStr: [],
        list: [],
      },
      defaultLoading: true,
      onError(e) {
        Alert.setError(
          DefaultModalActions.Error({
            message: e.message,
          }),
        );
      },
    },
  );

  useEffect(() => {
    if (timesState.data && detailState.data) {
      const time = (timesState.data.Result ?? []).find(
        (v) =>
          v.FromTime === detailState.data?.SettingCollectFromTime &&
          v.ToTime === detailState.data?.SettingCollectToTime,
      );

      if (time) {
        setFieldValue('day', { id: time?.Day, label: time?.DayStr });
        setFieldValue('time', time);
      }
    }
  }, [timesState.data]);

  // we need this because server not sending data correctly
  /**
   * this function get selected wasts from request detail
   * and all wasts from another endpoint and merge them to gether
   *
   * */
  const convertWasteArray = (wastes = []) => {
    wastesHandler.handleFetch().then(({ data }) => {
      const selected = wastes.map((w) => w.WasteTypeStr?.trim());

      const res = data.map((v) => {
        const waste = selected.indexOf(v.Title.trim());
        if (waste >= 0) {
          console.log(wastes?.[waste]);
          return {
            ...v,
            selected: true,
            WasteTypeCount: String(wastes?.[waste]?.WasteTypeCount),
          };
        } else {
          return v;
        }
      });
      setFieldValue('items', res);
    });
  };

  const handleOpenWasteTypeDialog = useCallback(() => {
    setWasteDialog(true);
  }, []);
  const handleCloseWasteTypeDialog = useCallback(() => {
    setWasteDialog(false);
  }, []);

  const handleDiscardChanges = useCallback(() => {
    setFieldValue(
      'items',
      values.items.map((v) => ({ ...v, temporarySelected: undefined })),
    );
    handleCloseWasteTypeDialog();
  }, [values.items]);

  const handleSaveWastes = useCallback(() => {
    setFieldValue(
      'items',
      values.items.map((v) =>
        v.temporarySelected !== undefined
          ? { ...v, selected: v.temporarySelected }
          : v,
      ),
    );
    handleCloseWasteTypeDialog();
  }, [values.items]);

  const handleRemoveWaste = (wasteId) => () => {
    setFieldValue(
      'items',
      values.items.filter((v) => v.WasteId !== wasteId),
    );
  };
  const handleChangeWasteAmount = (wasteId) => (value) => {
    setFieldValue(
      'items',
      values.items.map((v) =>
        v.WasteId === wasteId ? { ...v, WasteTypeCount: value } : v,
      ),
    );
  };

  const _renderWastList = () => {
    return (
      <>
        <View style={styles.wastTitleContainer}>
          <Label>پسماند ها</Label>
          <Button icon="plus" size={18} onPress={handleOpenWasteTypeDialog}>
            اضافه کردن پسماند
          </Button>
        </View>
        {itemsToShow.map((item) => {
          console.log('items: ', item);
          return (
            <Card key={`${item.WasteId}`} style={styles.item}>
              <Card.Content style={styles.itemContent}>
                <Label color="text" style={styles.itemLabel}>
                  {item.Title}
                </Label>
                <View style={styles.itemInputContainer}>
                  <TextInput
                    style={styles.itemInput}
                    placeholder="وزن"
                    value={item.WasteTypeCount}
                    defaultValue={'0'}
                    onChangeText={handleChangeWasteAmount(item.WasteId)}
                    keyboardType="number-pad"
                  />
                  <Label>کیلوگرم</Label>
                </View>
                <IconButton
                  icon="close"
                  size={19}
                  color={Colors.error}
                  onPress={handleRemoveWaste(item.WasteId)}
                />
              </Card.Content>
            </Card>
          );
        })}
      </>
    );
  };

  const handleAddressChange = useCallback((item) => {
    timesHandler
      .handleFetch({
        params: {
          mainAreaID: item?.area?.id,
        },
      })
      .then(() => {
        setFieldValue('addressId', item.id);
        setFieldValue('delivery', undefined);
        setFieldValue('time', undefined);
        dayRef.current.setVisibility(true);
      })
      .catch((e) => {});
  }, []);

  const handleDateChange = useCallback(
    (value) => {
      const timePeriodList = timesState.data.list.filter(
        (el) => el.DeliverDateStr === value.DeliverDateStr,
      );
      setFieldValue('selectedDate', value);
      setFieldValue('timePeriodList', timePeriodList);
      setFieldValue('selectedDateTime', timePeriodList[0]);
    },
    [timesState.data],
  );

  const handleCollectionDateTimeChange = useCallback((value) => {
    setFieldValue('selectedDateTime', value);
  }, []);

  const handleSelectDayOnVisibility = useCallback(() => {
    if (!values.addressId) {
      Alert.addToast({
        message: 'لطفا آدرس خود را وارد کنید.',
        timeout: 'long',
      });
      return false;
    }
    // handleScrollToBottom();
    return true;
  }, [values.addressId]);

  const handleSelectTimeOnVisibility = useCallback(() => {
    if (!values.addressId) {
      Alert.addToast({
        message: 'لطفا آدرس خود را وارد کنید.',
        timeout: 'long',
      });
      return false;
    }

    if (!values.selectedDate?.DeliverDateStr) {
      Alert.addToast({
        message: 'لطفا روز جمع آوری را انتخاب کنید',
        timeout: 'long',
      });

      return false;
    }
  }, [values.addressId, values.selectedDate]);

  const _renderHeaderComponent = () => {
    return (
      <>
        <Label style={styles.title}>آدرس</Label>
        <AddressInput onChange={handleAddressChange} />
        <Label style={styles.title}>تاریخ مراجعه سفیر</Label>
        <View style={styles.selectContainer}>
          <Select
            data={timesState.data.uniqByDeliverDateStr || []}
            getLabel={(el) =>
              el?.DayStr ? `${el.DayStr} ${el.MDay} ${el.MonthName}` : null
            }
            valueKey={'DeliverDateStr'}
            placeholder="روز جمع آوری را انتخاب کنید"
            onChange={handleDateChange}
            value={values.selectedDate}
            inputStyle={styles.selectInput}
            style={styles.select}
            disabled={timesState.error}
            selectRef={dateSelectRef}
            shouldChangeVisibility={handleSelectDayOnVisibility}
          />
        </View>

        <View style={styles.selectContainer}>
          <Select
            data={values.timePeriodList}
            getLabel={(el) =>
              el.FromTime ? `از ${el?.FromTime} - ${el?.ToTime}` : null
            }
            valueKey={'SettringCollectID'}
            placeholder="ساعت مراجعه سفیر را انتخاب کنید"
            onChange={handleCollectionDateTimeChange}
            value={values.selectedDateTime}
            inputStyle={styles.selectInput}
            style={styles.select}
            shouldChangeVisibility={handleSelectTimeOnVisibility}
          />
        </View>
      </>
    );
  };

  const handleAddWaste = (waste) => () => {
    setFieldValue(
      'items',
      values.items.map((v) =>
        v.WasteId === waste.WasteId
          ? {
              ...v,
              temporarySelected:
                v.temporarySelected !== undefined
                  ? !v.temporarySelected
                  : !v.selected,
            }
          : v,
      ),
    );
  };

  const _renderWasteItem = ({ item }) => {
    const selected =
      item[
        item.temporarySelected !== undefined ? 'temporarySelected' : 'selected'
      ];
    return (
      <View style={styles.wasteType}>
        <View style={styles.wasteTypeImage}>
          <Image
            source={{
              uri: fileUrlBuilder(item.File),
              cache: 'force-cache',
            }}
            style={styles.wasteIcon}
          />
          <Label>{item.Title}</Label>
        </View>
        <IconButton
          icon={selected ? 'close' : 'plus'}
          onPress={handleAddWaste(item)}
          color={Colors[selected ? 'danger' : 'green']}
          size={20}
        />
      </View>
    );
  };

  const _wasteItemKeyExtractor = useCallback(
    (item) => `waste-type-${item.WasteId}`,
    [],
  );

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={Colors.statusBar}
        barStyle="light-content"
        animated
      />
      <HeaderBack headerTitle={'ویرایش درخواست'} />

      {detailState.loading ? (
        <View style={styles.loadingContainer}>
          <Loading
            message="در حال دریافت اطلاعات"
            messageColor={Colors.green}
          />
        </View>
      ) : (
        <ScrollView contentContainerStyle={styles.content}>
          {_renderHeaderComponent()}
          <View>{_renderWastList()}</View>
        </ScrollView>
      )}

      <HelperText style={styles.helper}>
        حداقل وزن وارد شده برای جمع آوری پسماند ها {MIN_WASTE_WEIGHT} کیلوگرم
        است
      </HelperText>
      <View style={styles.bottomSheet}>
        <Button
          mode="contained"
          labelStyle={styles.labelButton}
          loading={submitLoading}
          onPress={handleSubmit}
          disabled={
            selectedWasteWeightsSum < MIN_WASTE_WEIGHT ||
            detailState.loading ||
            submitLoading
          }
          style={[styles.actionButtons, styles.submitButton]}>
          ذخیره
        </Button>
        <Button
          mode="contained"
          color={Colors.white}
          onPress={() => navigation.goBack()}
          style={[styles.actionButtons, styles.cancelButton]}>
          لغو
        </Button>
      </View>
      <Dialog
        visible={wasteDialog}
        onDismiss={handleCloseWasteTypeDialog}
        style={styles.dialog}>
        <View style={styles.dialogTitleContainer}>
          <Label weight="bold" style={styles.dialogTitle}>
            انتخاب کنید
          </Label>

          <View style={styles.row}>
            <Button onPress={handleSaveWastes}>ذخیره</Button>
            <Button color={Colors.error} onPress={handleDiscardChanges}>
              لغو
            </Button>
          </View>
        </View>
        <FlatList
          data={values.items}
          extraData={values.items}
          renderItem={_renderWasteItem}
          keyExtractor={_wasteItemKeyExtractor}
          refreshing={wastesState.loading}
          contentContainerStyle={styles.wasteTypeListContent}
          persistentScrollbar
        />
      </Dialog>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    marginBottom: 30,
  },
  content: {
    padding: 16,
    minHeight: '100%',
    overflow: 'hidden',
  },
  bottomSheet: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    overflow: 'hidden',
    backgroundColor: 'white',
    // zIndex: 10,
    borderTopColor: Colors.gray,
    borderTopWidth: StyleSheet.hairlineWidth,
    bottom: 0,
  },
  labelButton: {
    color: 'white',
  },
  actionButtons: {
    borderRadius: 0,
    flex: 1,
    height: 50,
    justifyContent: 'center',
    padding: 5,
  },
  submitButton: { flex: 1 },
  cancelButton: { flex: 1.2 },
  inputRowContainer: {
    flexDirection: 'row',
    // alignItems: 'center',
  },
  addressInput: {
    flex: 1,
    textAlign: 'right',
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
    minHeight: 50,
  },
  selectInput: {
    width: '100%',
    textAlign: 'right',
    borderLeftColor: 'white',
    borderLeftWidth: 2,
    height: 50,
    marginVertical: 5,
  },
  selectLabel: {
    textAlign: 'right',
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
    borderLeftColor: 'white',
    borderLeftWidth: 2,
    height: 50,
  },
  selectRightGrow: {
    flex: 1,
  },
  title: {
    padding: 5,
    marginTop: 16,
  },
  or: {
    width: '100%',
    textAlign: 'center',
    marginVertical: 16,
  },
  button: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.green,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderRadius: 6,
    color: 'white',
    alignSelf: 'center',
  },
  wastListContent: { padding: 5, paddingTop: 10, paddingBottom: 30 },
  item: {
    marginBottom: 5,
  },
  itemContent: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    height: 55,
    elevation: 0,
  },
  itemLabel: {
    width: 150,
  },
  itemInputContainer: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  itemInput: {
    width: 80,
    elevation: 0,
    height: 45,
    marginLeft: 10,
    textAlign: 'center',
    textAlignVertical: 'center',
    alignItems: 'center',
  },
  wastTitleContainer: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor: 'gray',
    paddingVertical: 0,
    paddingStart: 12,
    marginTop: 16,
  },
  wasteType: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: 50,
  },
  wasteTypeListContent: {
    minHeight: 300,
    padding: 16,
  },
  wasteTypeImage: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  wasteIcon: {
    width: 30,
    height: 30,
    marginStart: 16,
    backgroundColor: 'white',
    borderRadius: 6,
  },
  dialog: {
    maxHeight: 400,
  },
  dialogTitleContainer: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomColor: Colors.gray,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  dialogTitle: {
    fontSize: 16,
  },
  row: {
    flexDirection: 'row',
  },
  helper: {
    textAlign: 'center',
  },
});
