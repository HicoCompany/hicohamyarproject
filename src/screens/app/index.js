//dashboard
import Dashboard from './Dashboard';

//notice
import NoticeDetail from './NoticeScreen/NoticeDetailScreen';

//advertisement
import AdvertisementDetail from './AdvertisementScreen/AdvertisementDetailScreen';

// wastes
import WasteSelectionList from './WasteSelectionList';
import WasteCollectionSubmitForm from './WasteCollectionSubmitForm';
import WasteCollectionDetail from './WasteCollectionDetailScreen';
import WasteCollections from './WasteCollectionsScreen';
import WasteCollectionEditForm from './WasteCollectionEditFormScreen';
import WasteCollectionSurvey from './WasteSurvey';
import WasteCollectionRequestSubmission from './WasteCollectionSubmitForm/WasteCollectionRequestSubmission';

//reports
import Report from './ReportScreen';
import WasteReportDetail from './ReportScreen/WasteReportDetailScreen';
import WareReportDetail from './ReportScreen/WareReportDetailScreen';

//other
import About from './AboutScreen';
import Guid from './GuidScreen';
import Profile from './ProfileScreen';
import Introduction from './IntroductionScreen';
import Support from './SupportScreen';
import Lessons from './LessonsScreen';
import LessonDetail from './LessonsScreen/detail';
import VideoLessonDetail from './LessonsScreen/VideoLessonDetailScreen';

import CharityScreen from './creditManagementScreen/CharityScreen';
import BoothsScreen from './BoothsScreen';

//Suggestions
import Suggestions from './SuggestionScreen/SuggestionListScreen';

//products
import ProductScreen from './creditManagementScreen/Products/ProductScreen';
import EditProductScreen from './creditManagementScreen/Products/EditProductScreen';
import RequestProductScreen from './creditManagementScreen/Products/RequestProductScreen';
import SelectedProductScreen from './creditManagementScreen/SelectedProductScreen';
import SpecialServiceScreen from './creditManagementScreen/SpecialServiceScreen';
import CashWithdrawalScreen from './creditManagementScreen/CashWithdrawalScreen';

// used goods
import UsedGoodsScreen from './UsedGoods';
import UsedGoodSubmitFormScreen from './UsedGoods/UsedGoodSubmitFormScreen';

// TODO check if app work without this screen : no component navigate to this screens
// all this screen moved to !NotUsedScreens and after a while if they were no needed should deleted
// import DeliveredWaste from './registerPasmandScreen/deliveredwasteScreen/DeliveredwasteScreen';
// import DeliveryTime from './registerPasmandScreen/deliveryTimeScreen/DeliveryTimeScreen';
// import SelectPackage from './registerPasmandScreen/selectPackageScreen/SelectPackageScreen';
// import Wastes from './WasteListScreen';
// import WasteDetail from './WasteListScreen/WasteDetailScreen';
// import SupportingHicoScreen from './creditManagementScreen/supportingHicoScreen/SupportingHicoScreen';
// import ConfirmScreen from './ConfirmScreen';

export const AppScreens = {
  Dashboard,
  NoticeDetail,
  AdvertisementDetail,
  WasteSelectionList,
  WasteCollectionSubmitForm,
  WasteCollectionDetail,
  WasteCollections,
  WasteCollectionEditForm,
  WasteCollectionSurvey,
  WasteReportDetail,
  WasteCollectionRequestSubmission,
  WareReportDetail,
  About,
  Guid,
  Profile,
  CashWithdrawalScreen,
  CharityScreen,
  SpecialServiceScreen,
  Suggestions,
  BoothsScreen,
  ProductScreen,
  EditProductScreen,
  RequestProductScreen,
  SelectedProductScreen,
  Report,
  Introduction,
  Support,
  Lessons,
  LessonDetail,
  VideoLessonDetail,

  //used goods
  UsedGoods: UsedGoodsScreen,
  UsedGoodSubmitFormScreen,
};
