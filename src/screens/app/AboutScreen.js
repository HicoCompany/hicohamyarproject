import React from 'react';
import { View, Image, Linking, StatusBar, StyleSheet } from 'react-native';

import Colors from '@src/utility/Colors';
import HeaderBack from '@components/HeaderBack';

import { config } from '@src/utility/Constants';

//ui components
import { Button, Card } from 'react-native-paper';
import Label from '@src/components/Label';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';
import { Loading } from '@src/components';

//assets
import { organization_logo } from '@src/assets/image';

import { useGetSettings } from '@src/api/modules/Helper';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';

export default function AboutScreen({ navigation }) {
  const Alert = useAlert();
  const [state, handler] = useGetSettings({
    onError: ({ message }) => {
      Alert.setError(
        DefaultModalActions.Error({
          message: message,
        }),
      );
    },
  });

  const handleClick = () => {
    if (state.data?.Web) {
      if (/^\S+@\S+\.\S+$/.test(state.data?.Web)) {
        Linking.openURL(`mailto:` + state.data?.Web);
      } else {
        Linking.openURL(state.data?.Web);
      }
    }
  };
  const handleClickDial = () => {
    if (state.data?.Tel) {
      Linking.openURL(`tel:` + state.data?.Tel);
    }
  };

  // const handleBackButton = () => {
  //   navigation.goBack();
  //   return true;
  // };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Colors.statusBar} barStyle="light-content" />
      <HeaderBack headerTitle={'درباره ما'} />
      <ScrollView style={styles.list} contentContainerStyle={styles.content}>
        <Image source={organization_logo} style={styles.ImgProduct} />
        {state.loading ? (
          <View>
            <Loading style={styles.Box} />
          </View>
        ) : (
          <Card style={styles.Box}>
            <Card.Content>
              <Label textBreakStrategy="highQuality">{state.data.about}</Label>
              <Label style={styles.title}>ارتباط با ما:</Label>
              <View style={styles.contact}>
                <Button
                  labelStyle={styles.buttonLabel}
                  style={styles.button}
                  mode="outlined"
                  compact
                  onPress={handleClickDial}>
                  <Label>{state.data.Tel}</Label>
                </Button>
                <Label style={styles.or}>و</Label>
                <Button
                  style={styles.button}
                  mode="outlined"
                  compact
                  labelStyle={styles.buttonLabel}
                  onPress={handleClick}>
                  <Label>{state.data.Web}</Label>
                </Button>
              </View>
            </Card.Content>
          </Card>
        )}
      </ScrollView>
      <Label style={styles.version}>
        {`نسخه اپلیکیشن : ${config.VERSION}`}
      </Label>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#50b3ae',
  },
  content: {
    alignItems: 'center',
    paddingVertical: 20,
    paddingBottom: 50,
  },
  contact: {
    flexDirection: 'row',
    marginBottom: 20,
    justifyContent: 'center',
  },
  version: {
    height: 50,
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    textAlignVertical: 'center',
    width: '100%',
  },
  ImgProduct: {
    height: 200,
    aspectRatio: 1,
    resizeMode: 'contain',
    tintColor: Colors.text,
  },
  Box: {
    width: '90%',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: '#8a987f',
    marginTop: 20,
    minHeight: 200,
  },
  button: {
    height: 40,
  },
  or: {
    height: 40,
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingHorizontal: 10,
  },
  buttonLabel: {
    textTransform: 'lowercase',
  },
  title: {
    textAlign: 'center',
    paddingTop: 20,
    paddingBottom: 10,
  },
});
