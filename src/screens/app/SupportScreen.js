import HeaderBack from '@src/components/HeaderBack';
import Colors from '@src/utility/Colors';
import React, { useCallback, useRef, useState, Fragment } from 'react';
import {
  StatusBar,
  StyleSheet,
  View,
  Linking,
  KeyboardAvoidingView,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

// ui
import { Button } from 'react-native-paper';
import Label from '@src/components/Label';
import TextInput from '@src/components/TextInput';

//assets
import { normalize } from '@src/responsive';
import { useAlert, DefaultModalActions } from '@src/components/GlobalAlert';
import { useSubmitSupportMessage } from '@src/api/modules/Helper';
import { scale } from 'react-native-size-matters';

SupportScreen.FormName = 'ارتباط با پشتیبان';
export default function SupportScreen({ navigation }) {
  const Alert = useAlert();
  const [state, setState] = useState({
    title: '',
    message: '',
    errors: {},
  });

  const [submitState, submitHandler] = useSubmitSupportMessage({
    message: state.message,
    title: state.title,
  });

  const handleChangeTitle = useCallback((title) => {
    setState((prev) => ({ ...prev, title }));
  }, []);

  const handleChangeMessage = useCallback((message) => {
    setState((prev) => ({ ...prev, message }));
  }, []);

  const handleSubmit = () => {
    handleConfirmModalOpening({ loading: true });
    submitHandler
      .handleFetch()
      .then(() => {
        setState({ title: '', message: '' });
        Alert.addToast({
          message: 'با موفقیت ارسال شد',
          timeout: 'short',
          type: 'success',
        });
      })
      .catch(({ done, message, data }) => {
        if (done == true) {
          Alert.setError(
            DefaultModalActions.Error({
              message: data?.Message?.[0] ?? message,
              refreshHandler: handleSubmit,
            }),
          );
        } else {
          Alert.setAction(
            DefaultModalActions.RetryOrBack({
              message: data?.Message?.[0] ?? message,
              refreshHandler: handleSubmit,
            }),
          );
        }
      });
  };

  const inputRef = useRef();

  const handleFocusOnMessage = useCallback(() => {
    inputRef.current.focus();
  }, [inputRef]);

  const handleConfirmModalOpening = ({ loading }) => {
    Alert.setAction(
      DefaultModalActions.Ask({
        onConfirm: handleSubmit,
        onDenied: Alert.clearAction,
        message: 'آیا از ارسال پیام خود مطمئن هستید؟',
        loading,
      }),
    );
  };

  const handleCall = useCallback(() => {
    Linking.canOpenURL('tel://0613').then(() => {
      Linking.openURL('tel://0613');
    });
  }, []);
  const handleCancel = useCallback(() => {
    setState({ message: '', title: '' });
  }, []);

  return (
    <Fragment>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={'height'}
        keyboardVerticalOffset={scale(30)}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={'ارتباط با پشتیبان'} />
        <View style={styles.content}>
          <View style={styles.contentBody}>
            <View style={[styles.row, styles.titleInput]}>
              <Label style={styles.cardTitleText} color="placeholder">
                عنوان :
              </Label>
              <TextInput
                placeholder="عنوان"
                autoFocus
                mode="none"
                value={state.title}
                blurOnSubmit={false}
                onChangeText={handleChangeTitle}
                onSubmitEditing={handleFocusOnMessage}
                style={styles.input}
              />
            </View>
            <TextInput
              placeholder="پیام خود را بنویسید"
              mode="none"
              onChangeText={handleChangeMessage}
              value={state.message}
              inputRef={inputRef}
              style={styles.multilineTextInput}
              onSubmitEditing={handleConfirmModalOpening}
              multiline
            />
            <View style={styles.spaceBetween}>
              <View style={styles.row}>
                <Button
                  mode="outlined"
                  onPress={handleConfirmModalOpening}
                  disabled={!state.message || !state.title}
                  loading={submitState.loading}>
                  ارسال
                </Button>
                {state.title || state.message ? (
                  <Button color={Colors.error} onPress={handleCancel}>
                    پاک کردن فرم
                  </Button>
                ) : null}
              </View>
              <Button color={Colors.blue} onPress={handleCall}>
                تماس با پشتیبان
              </Button>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  contentHeader: {
    flex: 1,
    padding: 16,
    backgroundColor: Colors.green,
    alignItems: 'center',
    overflow: 'hidden',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  headerImageContainer: {
    overflow: 'hidden',
    borderRadius: 10,
    maxHeight: 350,
  },
  headerImage: {
    maxWidth: '100%',
    maxHeight: '100%',
    backgroundColor: 'white',
    overflow: 'hidden',
    borderRadius: 40,
  },
  contentBody: {
    padding: 16,
    flex: 1,
    justifyContent: 'flex-start',
    // alignItems: 'center',
    // paddingTop: 100,
  },
  bodyTitle: {
    marginTop: 20,
    marginRight: 20,
    fontSize: normalize(10),
    marginBottom: 30,
  },

  row: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  spaceBetween: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cardTitleText: {
    fontSize: 16,
    textAlign: 'right',
    paddingRight: 10,
  },
  message: {
    width: '80%',
    borderWidth: 1,
    minWidth: '30%',
    flexGrow: 0,
    paddingHorizontal: 8,
    marginTop: 10,
    borderRadius: 20,
    alignSelf: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  titleInput: {
    backgroundColor: '#efefef',
    // padding: 16,
    borderRadius: 6,
    marginBottom: 6,
  },
  multilineTextInput: {
    flex: 1,
    textAlignVertical: 'top',
    backgroundColor: '#efefef',
    padding: 16,
    borderRadius: 6,
    marginBottom: 6,
  },
  input: {
    flex: 1,
  },
});
