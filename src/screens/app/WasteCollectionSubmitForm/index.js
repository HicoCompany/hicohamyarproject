import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  View,
  StatusBar,
  Dimensions,
  Platform,
  StyleSheet,
  ScrollView,
} from 'react-native';

// components
import { Select, Label, RadioButton, Container } from '@src/components';
import { Button, DataTable, HelperText } from 'react-native-paper';
import TabItem from '@src/components/TabItem';
import AddressInput from '@components/Addresses';

// redux
import { useSelector } from 'react-redux';

//api
import {
  useDeliveryTime,
  useSubmitWasteCollection,
} from '@src/api/modules/Waste';

// utils
import Colors from '@src/utility/Colors';
import { normalize, wp } from '@src/responsive';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import moment from 'moment-jalaali';
import { scale } from 'react-native-size-matters';

const momentDate = moment();
const todayDate = {
  day: momentDate.jDate(),
  month: momentDate.jMonth() + 1,
  year: momentDate.jYear(),
  dateString: momentDate.format('jYYYY/jMM/jDD'),
};

/**
 * @typedef {import('@react-navigation/native').RouteProp<{WasteCollection: {}},'WasteCollection'>} Route
 * @param {{navigation: import('@react-navigation/stack').StackNavigationProp<any,''>,route: Route}}
 */
export default function WasteCollectionSubmitRequestScreen({
  navigation,
  route,
}) {
  const dateSelectRef = useRef();
  const scrollRef = useRef();
  const Alert = useAlert();
  const selectedAddress = useSelector((store) => store.addresses.selected) ?? {
    area: undefined,
  };

  const [state, setState] = useState({
    list: [],
    // for saving the date to filter list of collectionTime for time period select list
    selectedDate: {},
    timePeriodList: [],
    //for submit
    selectedDateTime: {},
    focused: false,
  });

  const [timesState, timesHandler] = useDeliveryTime(
    { areaId: selectedAddress?.area?.id },
    {
      initialData: {
        uniqByDeliverDateStr: [],
        list: [],
      },
      defaultLoading: true,
      onError: (e) => {
        Alert.setError(
          DefaultModalActions.Error({
            message: e.message,
          }),
        );
      },
    },
  );

  // we need today time to see
  //todayTimes
  const canRequestFastCollection = useMemo(() => {
    if (timesState.loading)
      return { todayTimes: [], canRequestFastCollection: false };

    const times = timesState.data.list.filter(
      (el) => el.DeliverDateStr == todayDate.dateString,
    );

    return times.length > 0 ? true : false;

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timesState.loading]);

  const [fastCollection, setFastCollection] = useState(undefined);
  const [submitState, submitHandler] = useSubmitWasteCollection();

  useEffect(() => {
    setState((prev) => ({ ...prev, list: route.params?.list }));
  }, [route.params]);

  useEffect(() => {
    if (selectedAddress?.area?.id) {
      timesHandler.handleFetch();
      return;
    }
    timesHandler.setState((prevState) => ({
      ...prevState,
      data: {
        uniqByDeliverDateStr: [],
        list: [],
      },
      done: false,
      error: false,
    }));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedAddress?.area?.id]);

  // i want to change to fastCollection to true if there is collectionTime today
  // if not the user only can use select collection time تعیین زمان جمع آوری
  useEffect(() => {
    if (!timesState.loading) {
      if (timesState.data.ResultID === -1000) {
      }
      if (canRequestFastCollection) {
        setFastCollection(true);
      } else {
        setFastCollection(false);
      }
    } else {
      setFastCollection(undefined);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timesState.loading]);

  const handleDateChange = useCallback(
    (value) => {
      const timePeriodList = timesState.data.list.filter(
        (el) => el.DeliverDateStr === value.DeliverDateStr,
      );
      setState((prev) => ({
        ...prev,
        selectedDate: value,
        timePeriodList,
        selectedDateTime: timePeriodList[0],
      }));
    },
    [timesState.data],
  );

  const handleCollectionDateTimeChange = useCallback((value) => {
    setState((prev) => ({
      ...prev,
      selectedDateTime: value,
    }));
  }, []);

  const { score, cost } = useMemo(() => {
    return state.list.reduce(
      (a, v) => ({
        score: a.score + v.HelperPrice * v.Count,
        cost: a.cost + v.Point * v.Count,
      }),
      { score: 0, cost: 0 },
    );
  }, [state.list]);

  const handleSubmit = () => {
    var list = state.list.map((item) => {
      return {
        WasteId: item.WasteId,
        WasteTypeCount: parseFloat(item.Count),
      };
    });

    const requestBody = {
      AddressId: selectedAddress.id,
      DeliveredWasteList: list,
    };

    if (!fastCollection) {
      requestBody.SettringCollectID = state.selectedDateTime.SettringCollectID;
      requestBody.PDeliveryDate = state.selectedDateTime.DeliverDateStr;
      requestBody.DeliveredWasteRequestTypeId = 1;
    } else {
      // requestBody.SettringCollectID = null;
      requestBody.SettringCollectID = state.selectedDateTime.SettringCollectID;
      // requestBody.PDeliveryDate = null;
      requestBody.DeliveredWasteRequestTypeId = 2;
    }

    submitHandler
      .handleFetch({
        data: requestBody,
      })
      .then(({ data }) => {
        Alert.addToast({
          message: 'با موفقیت ثبت شد.',
          timeout: 'short',
          type: 'success',
        });
        if (fastCollection) {
          return navigation.replace('WasteCollectionRequestSubmission', {
            DeliveryWasteID: data,
            type: 'success',
          });
        }
        navigation.replace('WasteCollectionDetail', {
          DeliveryWasteID: data,
          apiSuccess: 1,
        });
      })
      .catch((e) => {
        Alert.setError(
          DefaultModalActions.Error({
            message: e.message,
          }),
        );
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  };

  const handleDismissDateSelect = useCallback(() => {
    dateSelectRef.current?.setVisibility(false);
    scrollRef.current?.scrollToEnd({ animated: true, duration: 500 });
  }, []);
  const handleScrollToBottom = useCallback(() => {
    scrollRef.current?.scrollToEnd({ animated: true, duration: 500 });
  }, []);

  const handleNavigateToBoothsScreen = useCallback(() => {
    navigation.navigate('BoothsScreen');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSelectDayOnVisibility = useCallback(() => {
    if (!selectedAddress.id) {
      Alert.addToast({
        message: 'لطفا آدرس خود را وارد کنید.',
        timeout: 'long',
      });
      return false;
    }
    handleScrollToBottom();
    return true;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedAddress.id]);
  const handleSelectTimeOnVisibility = useCallback(() => {
    if (!selectedAddress.id) {
      Alert.addToast({
        message: 'لطفا آدرس خود را وارد کنید.',
        timeout: 'long',
      });
      return false;
    }

    if (!state.selectedDate?.DeliverDateStr) {
      Alert.addToast({
        message: 'لطفا روز جمع آوری را انتخاب کنید',
        timeout: 'long',
      });

      return false;
    }
    handleDismissDateSelect();
    handleScrollToBottom();
    return true;

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedAddress.id, state.selectedDate]);

  const isThereTimeToSelect = useMemo(() => {
    if (!timesState.done) {
      return true;
    } else if (
      timesState.loading ||
      timesState.data?.uniqByDeliverDateStr.length
    ) {
      return true;
    }
    return false;
  }, [timesState.data, timesState.done, timesState.loading]);

  const _renderReserveService = () => {
    // if it was undefined or true dont return any thing
    // because we dont want to show time selection component during collectionTimes loading
    if (fastCollection != false) return null;
    if (timesState.data?.ResultID === -1000 || !isThereTimeToSelect) {
      return (
        <View style={styles.useBoothContainer}>
          <Label weight="bold" style={styles.boothInfoMessage}>
            {timesState.data?.Text}
          </Label>
        </View>
      );
    }
    return (
      <>
        <View style={styles.selectContainer}>
          <Select
            data={timesState.data.uniqByDeliverDateStr || []}
            getLabel={(el) =>
              el?.DayStr ? `${el.DayStr} ${el.MDay} ${el.MonthName}` : null
            }
            valueKey={'DeliverDateStr'}
            placeholder="روز جمع آوری را انتخاب کنید"
            onChange={handleDateChange}
            value={state.selectedDate}
            inputStyle={styles.selectInput}
            style={styles.select}
            disabled={timesState.error}
            selectRef={dateSelectRef}
            shouldChangeVisibility={handleSelectDayOnVisibility}
          />
        </View>

        <View style={styles.selectContainer}>
          <Select
            data={state.timePeriodList}
            getLabel={(el) =>
              el.FromTime ? `از ${el?.FromTime} - ${el?.ToTime}` : null
            }
            valueKey={'SettringCollectID'}
            placeholder="ساعت مراجعه سفیر را انتخاب کنید"
            onChange={handleCollectionDateTimeChange}
            value={state.selectedDateTime}
            inputStyle={styles.selectInput}
            style={styles.select}
            // disabled={!state.selectedDate?.DeliverDateStr}
            shouldChangeVisibility={handleSelectTimeOnVisibility}
          />
        </View>
      </>
    );
  };

  const handleCollectionModeChange = useCallback((value) => {
    setFastCollection(value);
    const timeout = setTimeout(() => {
      scrollRef.current?.scrollToEnd({ animated: true });
      clearTimeout(timeout);
    }, 500);
  }, []);

  return (
    <Container style={styles.container}>
      <StatusBar backgroundColor={Colors.statusBar} barStyle="light-content" />
      <Container.Header title={' انتخاب زمان تحویل '} />
      <ScrollView
        contentContainerStyle={
          !fastCollection ? styles.scrollContent : undefined
        }
        ref={scrollRef}>
        <View style={styles.Title}>
          <View style={styles.line} />
          <View>
            <Label style={styles.textTitle}>آیتم های انتخابی</Label>
          </View>
        </View>
        <DataTable>
          {state.list.map((data) => (
            <DataTable.Row
              style={styles.dataTableRow}
              key={`WasteTypeID-${data.WasteId}`}>
              <Label numeric style={styles.tableCell}>
                {`${data.Point * data.Count ?? '-'} امتیاز - ${
                  data.HelperPrice * data.Count ?? '-'
                } تومان`}
              </Label>
              <Label numeric style={styles.tableCell}>{`${
                data.Count ?? '-'
              } کیلوگرم`}</Label>
              <Label style={styles.tableCell} numeric>
                {data.Title}
              </Label>
            </DataTable.Row>
          ))}
        </DataTable>
        <View style={styles.TotalCard}>
          <View style={styles.buttonContainer}>
            <View style={styles.BtnSubmit1}>
              <Label style={styles.textFooter}>
                {`مجموع امتیاز این درخواست: ${score}`}
              </Label>
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <View style={styles.BtnSubmit1}>
              <Label style={styles.textFooter}>
                {`مجموع مبلغ این درخواست : ${cost} تومان`}
              </Label>
            </View>
          </View>
        </View>

        <View style={styles.Title}>
          <View style={styles.line} />
          <View>
            <Label style={styles.textTitle}>آدرس مکان جمع آوری</Label>
          </View>
        </View>

        <View style={styles.TotalCard}>
          <AddressInput />
        </View>

        {isThereTimeToSelect ? (
          <Fragment>
            {!selectedAddress.id && (
              <HelperText style={styles.requestModeInfo}>
                برای تعیین زمان مراجعه باید آدرس خود را وارد کنید
              </HelperText>
            )}
            <View style={styles.Title}>
              <View style={styles.line} />
              <Label style={styles.textTitle}>تعیین زمان مراجعه</Label>
            </View>
            <RadioButton.Group
              style={styles.requestMode}
              value={fastCollection}
              onChange={handleCollectionModeChange}>
              <RadioButton
                style={styles.requestModeItem}
                labelStyle={styles.requestModeItemLabel}
                side="right"
                value={true}
                label={'جمع آوری سریع'}
                // disabled={timesState.loading || !canRequestFastCollection}
                disabled={timesState.loading}
              />
              <RadioButton
                style={styles.requestModeItem}
                labelStyle={styles.requestModeItemLabel}
                side="left"
                value={false}
                disabled={timesState.loading}
                label={'تعیین زمان مراجعه'}
              />
            </RadioButton.Group>
            <HelperText style={styles.requestModeInfo}>
              با انتخاب گزینه "جمع آوری سریع" امروز و در اسرع وقت سفیر برای جمع
              آوری مراجعه خواهد کرد.
            </HelperText>
            <HelperText style={styles.requestModeInfo}>
              با انتخاب گزینه "تعیین زمان مراجعه" می توانید تاریخ و ساعت مراجعه
              سفیر را مشخص کنید.
            </HelperText>
          </Fragment>
        ) : null}

        {_renderReserveService()}
      </ScrollView>

      {isThereTimeToSelect || fastCollection ? (
        <View style={{ zIndex: 1000 }}>
          <Button
            onPress={handleSubmit}
            style={styles.submitButton}
            contentStyle={{ height: '100%', width: '100%' }}
            labelStyle={styles.submitButtonLabel}
            disabled={
              (!fastCollection
                ? !state.selectedDateTime?.SettringCollectID
                : false) || !selectedAddress.id
            }
            loading={submitState.loading}
            icon={submitState.error ? 'refresh' : undefined}
            mode="contained">
            ثبت درخواست
          </Button>
        </View>
      ) : null}
    </Container>
  );
}

WasteCollectionSubmitRequestScreen.navigationOptions = () => {
  return {
    tabBarIcon: ({ tintColor }) => (
      <TabItem
        name={'زمان تحویل پسماند '}
        source={require('@src/assets/image/ic_residue.png')}
        color={tintColor}
      />
    ),
  };
};

const styles = StyleSheet.create({
  grow: { flex: 1 },
  header: {
    backgroundColor: Colors.green,
  },
  Btn: {
    padding: 8,
  },
  tableCell: {
    flex: 1,
    // textAlign: 'center',
    textAlignVertical: 'center',
  },
  textTitle: {
    color: 'black',
  },
  Title: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingTop: 15,
  },
  line: {
    width: '50%',
    borderTopWidth: 1.5,
    borderColor: Colors.green,
  },
  BoxDaysWeek: {
    width: Dimensions.get('window').width * 0.4,
    height: 100,
    backgroundColor: '#e0e0e0',
    // alignItems: 'space-between',
    borderRadius: 10,
    marginHorizontal: Dimensions.get('window').width * 0.01,
  },
  textDaysWeek: {
    color: 'black',
  },
  TotalDate: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    width: '100%',
  },
  Week: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  Date: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderTopWidth: 1,
    borderColor: 'white',
  },
  BtnFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputFooter: {
    backgroundColor: 'white',
    width: 200,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    paddingRight: 10,
  },
  textFooter: {
    color: 'white',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: undefined,
    paddingHorizontal: 16,
  },
  TotalCard: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  CardSelect: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 5,
    padding: 8,
    paddingVertical: 12,
  },
  BtnSubmit1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.green,
    borderRadius: 10,
    padding: 7,
    width: '90%',
  },

  pickerContainer: {},
  pickerStyle: {
    // borderBottomWidth: 0.8,
    // borderColor: '#e2e2e2',
    width: wp('50%'),
    // justifyContent: 'center',
  },
  pickerHeaderStyle: {
    backgroundColor: Colors.purple,
  },
  pickerHeaderTextStyle: {
    width: wp('50%'),
    backgroundColor: 'transparent',
    fontSize: 16,
    color: 'white',
    textAlign: 'right',
  },
  pickerBackHeaderTextStyle: {
    width: wp('50%'),
    backgroundColor: 'transparent',
    fontSize: 16,
    color: 'white',
    textAlign: 'left',
  },
  pickerTextStyle: {
    fontSize: 16,
    color: '#34495e',
    textAlign: 'center',
  },
  pickerItemStyle: {
    width: wp('90%'),
    backgroundColor: 'transparent',
  },
  pickerItemTextStyle: {
    fontSize: 16,
    width: wp('65%'),
    backgroundColor: 'transparent',
    color: '#34495e',
    textAlign: 'right',
  },
  pickerSeparator: Platform.select({
    android: {
      height: 1,
      width: wp('90%'),
      backgroundColor: '#e2e2e2',
    },
    ios: {},
  }),
  inputTitle: {
    width: '100%',
    fontSize: normalize(12),
    color: Colors.textBlack,
    textAlign: 'center',
    marginBottom: 10,
  },
  input: {
    width: '95%',
    alignSelf: 'center',
    marginBottom: 10,
    paddingVertical: 10,
  },
  calenderIcon: {
    color: '#50b3ae',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
  },
  listItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 13,
  },
  scrollContent: { paddingBottom: 300 },
  listItems: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  selectContainer: {
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginTop: 16,
  },
  select: {
    minHeight: 50,
    width: '100%',
  },
  selectInput: {
    width: '100%',
    textAlign: 'right',
    borderLeftColor: 'white',
    borderLeftWidth: 2,
    height: 50,
  },
  areaInput: {
    width: '100%',
    textAlign: 'right',
    borderLeftColor: 'white',
    borderLeftWidth: 2,
    height: 50,
  },
  submitButton: {
    width: '100%',
    borderRadius: 0,
    height: 50,
    justifyContent: 'center',
    zIndex: 100,
    elevation: 1,
  },
  submitButtonLabel: {
    color: '#ffffff',
  },
  useBoothContainer: {
    minHeight: 250,
    width: '100%',
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  boothButton: {
    width: 200,
    elevation: 0,
  },
  boothInfoMessage: {
    marginBottom: scale(15),
  },
  requestMode: {
    flexDirection: 'row-reverse',
    // margin: 0,
    alignSelf: 'center',
    overflow: 'hidden',
  },
  requestModeItem: {
    flex: 1,
  },
  requestModeItemLabel: {
    fontSize: normalize(11),
  },
  requestModeInfo: {
    // flexDirection: 'row-reverse',
    textAlign: 'right',
  },
  dataTableRow: { margin: 0, width: '100%', padding: 0 },
});
