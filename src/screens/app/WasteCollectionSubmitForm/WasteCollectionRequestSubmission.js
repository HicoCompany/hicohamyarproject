import React, { useCallback } from 'react';
import { Image } from 'react-native';

import { Label, Container } from '@src/components';

//navigation
import { TransitionPresets } from '@react-navigation/stack';

//assets
import GarbageTruck from '@assets/image/garbage-truck.png';
import { ScaledSheet } from 'react-native-size-matters';
import Colors from '@src/utility/Colors';
import { Button } from 'react-native-paper';
import { StackActions } from '@react-navigation/routers';
/**
 *
 * @param {import('@react-navigation/stack').StackScreenProps<{ SuccessSubmission: {DeliveryWasteID: number},'SuccessSubmission'}>} param0
 * @returns
 */
function WasteCollectionRequestSubmission({ navigation, route }) {
  const handleNavigateToDashboard = useCallback(() => {
    navigation.dispatch(StackActions.popToTop());
  }, [navigation]);

  return (
    <Container style={styles.container}>
      <Image source={GarbageTruck} style={styles.image} />
      <Label style={styles.serial}>
        شماره پیگیری درخواست: {route.params?.DeliveryWasteID}
      </Label>
      <Label style={styles.message} weight="bold">
        درخواست شما با موفقیت ثبت شد و در انتظار تایید سفیر می باشد نتیجه تایید
        تا یک ساعت دیگر به شما ارسال خواهد شد.
      </Label>
      <Button
        mode="contained"
        color={Colors.green}
        style={styles.button}
        labelStyle={styles.buttonLabel}
        onPress={handleNavigateToDashboard}>
        برگشت به صفحه اصلی
      </Button>
    </Container>
  );
}

WasteCollectionRequestSubmission.navigationOptions =
  TransitionPresets.ModalPresentationIOS;

const styles = ScaledSheet.create({
  container: {
    padding: '16@s',
  },
  image: {
    marginTop: '100@s',
    height: '200@s',
    alignItems: 'center',
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  serial: {
    backgroundColor: Colors.text,
    color: Colors.whiteText,
    height: '30@s',
    minWidth: '200@s',
    alignSelf: 'center',
    textAlign: 'center',
    textAlignVertical: 'center',
    borderRadius: '6@s',
  },
  message: {
    flex: 1,
    // textAlignVertical: '',
    // paddingBottom: 100,
    paddingTop: '40@ms',
  },
  button: {
    borderRadius: 16,
    width: '150@s',
    alignSelf: 'center',
    marginBottom: '40@s',
  },
  buttonLabel: {
    color: Colors.whiteText,
  },
});

export default WasteCollectionRequestSubmission;
