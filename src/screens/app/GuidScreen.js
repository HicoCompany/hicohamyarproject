import React, { Component } from 'react';
import { StatusBar, View, ActivityIndicator, Text } from 'react-native';
import { connect } from 'react-redux';

import Colors from '@src/utility/Colors';
import HeaderBack from '@components/HeaderBack';
import { Container } from 'native-base';

import TabItem from '@components/TabItem';

import { WebView } from 'react-native-webview';
import { config } from '@src/utility/Constants';
import EStyleSheet from 'react-native-extended-stylesheet';

const INJECTED_JAVASCRIPT = `(function() {
  const style = document.createElement('style');
  style.type = 'text/css';
  style.innerText = ' * { overflow-x: hidden; padding: 0;margin:0; }';
  document.head.appendChild(style);
})();`;

class GuidScreen extends Component {
  static navigationOptions = () => {
    return {
      tabBarIcon: ({ tintColor }) => (
        <TabItem
          name={'راهنما'}
          source={require('@src/assets/image/ic_notice.png')}
          color={tintColor}
        />
      ),
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      horizontal: false,
      list: '',
    };
  }

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };

  renderActivityIndicator = () => (
    <View style={styles.loadingContainer}>
      <ActivityIndicator color={Colors.green} size={36} />
    </View>
  );

  renderError = () => (
    <View style={styles.loadingContainer}>
      <Text style={styles.errorMessage}>ارتباط دستگاه خود را بررسی کنید</Text>
    </View>
  );

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack headerTitle={'راهنما'} />
        <View style={styles.content}>
          <WebView
            // source={{ uri: `${config.BASE_URL}/Help/Helper/Help/Help.html` }}
            source={{
              uri: `${config.BASE_URL}${this.props.appSettings.guideLink}`,
            }}
            style={styles.webViewContainer}
            renderLoading={this.renderActivityIndicator}
            cacheEnabled={false}
            cacheMode="LOAD_NO_CACHE"
            startInLoadingState
            renderError={this.renderError}
            injectedJavaScript={INJECTED_JAVASCRIPT}
          />
        </View>
      </Container>
    );
  }
}

const styles = EStyleSheet.create({
  content: {
    height: '100%',
    width: '100%',
    flex: 1,
  },
  webViewContainer: {
    width: '100%',
    height: '100%',
  },
  loadingContainer: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorMessage: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
    appSettings: {
      privacyLink: state.global.privacyLink,
      guideLink: state.global.guideLink,
      marketLink: state.global.marketLink,
    },
  };
};

const mapActionToProps = {};
export default connect(mapStateToProps, mapActionToProps)(GuidScreen);
