import React, { Component } from 'react';
import { View, StatusBar, ScrollView } from 'react-native';

import { connect } from 'react-redux';
import Colors from '@src/utility/Colors';
import { normalize, wp } from '@src/responsive';
import { Dialog, Portal, Card, Button, Divider } from 'react-native-paper';

import TabItem from '@components/TabItem';
import HeaderBack from '@components/HeaderBack';
import { Label, Loading } from '@components';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { getWasteCollectionDetail } from '@src/api/modules/Waste';
import { ScaledSheet } from 'react-native-size-matters';

class RequestDetailsScreen extends Component {
  static navigationOptions = () => {
    return {
      tabBarIcon: ({ tintColor }) => (
        <TabItem
          name={'نوع پسماند'}
          source={require('@src/assets/image/ic_residue.png')}
          color={tintColor}
        />
      ),
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      //address
      MainAreaTitle: null,
      HomeNo: null,
      Address: null,
      //address
      CreateDateStr: '',
      DeliverDateCreate: '',
      CreateTimeStr: '',
      DeliveryDateStr: '',
      DeliveryTimeStr: '',
      DeliveryWasteID: 0,
      DeliveryCode: '',
      StatusStr: '',
      refreshing: false,
      loadingData: false,
      laps: [],
      apiError: false,
      apiErrorDesc: '',
      apiSuccess: true,
      apiSuccessDesc: 'درخواست شما با موفقیت ثبت شد',
    };
  }

  componentDidMount = async () => {
    const { DeliveryWasteID, apiSuccess } = this.props.route.params;
    if (apiSuccess == 1) {
      this.setState({ apiSuccess: true });
    } else {
      this.setState({ apiSuccess: false });
    }
    await this.setState({ DeliveryWasteID: DeliveryWasteID });
    this.getApi();
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  };

  getApi = async () => {
    await this.PhoneGetDeliverWasteByDeliveryWasteIDApi();
  };
  async PhoneGetDeliverWasteByDeliveryWasteIDApi() {
    try {
      const response = await getWasteCollectionDetail({
        deliveryWasteId: this.state.DeliveryWasteID,
      });
      const data = response.data;

      this.setState({
        laps: data.Result ?? [],
        StatusStr: data.StatusStr,
        CreateDateStr: data.CreateDateStr,
        DeliverDateCreate: data.DeliverDateCreate,
        CreateTimeStr: data.CreateTimeStr,
        DeliveryDateStr: data.DeliveryDateStr,
        DeliveryTimeStr: data.DeliveryTimeStr,
        SettingCollectFromAndToTime: data.SettingCollectFromAndToTime,
        DeliveryWasteID: data.DeliveryWasteID,
        DeliveryCode: data.DeliveryCode,

        //address
        Address: data.HelperAddressStreet,
        HomeNo: data.HelperAddressHomeNo,
        MainAreaTitle: data.MainAreaTitle,
      });

      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: error.message ?? 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  lapsList() {
    return this.state.laps.map((data) => {
      return (
        <Card
          style={styles.item}
          elevation={1.5}
          key={`Waste-${data.DeliveryWasteItemID}`}>
          <Card.Content style={styles.itemContent}>
            <Label style={styles.text}>
              {data.Points} امتیاز - {data.HelperPrice} تومان
            </Label>
            <Label style={styles.text}> {data.WasteTypeCount} کیلوگرم </Label>
            <Label style={styles.text}>{data.WasteTypeStr}</Label>
          </Card.Content>
        </Card>
      );
    });
  }

  handleNavigateToMain = () => {
    this.props.navigation.navigate('main');
  };

  render() {
    if (this.state.isLoadingPage) {
      return <Loading />;
    }

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack
          onBackPress={this.handleNavigateToMain}
          headerTitle={'مشاهده جزئیات سفارش '}
        />
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.content}>
          {this.state.DeliveryCode ? (
            <View style={styles.titleBar}>
              <Label style={styles.titleBarValue} weight="bold">
                کد تحویل پسماند :
              </Label>
              <Label style={styles.titleBarValue} weight="bold">
                {this.state.DeliveryCode}
              </Label>
            </View>
          ) : null}

          <View style={styles.title}>
            <Label style={styles.value}>آدرس و زمان تحویل</Label>
            <Icon
              name="calendar"
              color={Colors.green}
              style={styles.titleIcon}
              size={20}
            />
          </View>
          <Card style={styles.CardTime} elevation={3}>
            <Label style={styles.value}>
              {`${this.state.Address} پلاک ${this.state.HomeNo} - ${this.state.MainAreaTitle}`}
            </Label>
            <Divider />
            <Label style={styles.value}>
              {(this.state.DeliveryDateStr || '') +
                ' ' +
                (this.state.SettingCollectFromAndToTime || '')}
            </Label>
          </Card>
          <View style={styles.title}>
            <Label style={styles.value}> نوع پسماند </Label>
            <Icon
              name="basket"
              color={Colors.green}
              style={styles.titleIcon}
              size={20}
            />
          </View>
          {this.lapsList()}
        </ScrollView>
        <Button
          mode="contained"
          style={styles.footerButton}
          labelStyle={styles.labelButton}
          onPress={this.handleNavigateToMain}>
          بازگشت به صفحه اصلی
        </Button>

        <Portal>
          <Dialog
            visible={this.state.apiSuccess}
            style={styles.dialogContainer}
            onDismiss={() => {
              this.setState({
                apiSuccess: false,
              });
            }}>
            <Dialog.Content style={styles.dialogContent}>
              <Label style={styles.dialogText}>
                {this.state.apiSuccessDesc}
              </Label>
              <Button
                onPress={() => {
                  this.setState({
                    apiSuccess: false,
                  });
                }}
                style={styles.dialogButton}>
                تایید
              </Button>
            </Dialog.Content>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
  },
  scrollView: {
    width: '100%',
  },
  content: {
    padding: '16@s',
    paddingBottom: 100,
    width: '100%',
  },
  titleBar: {
    height: 40,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.green,
    borderRadius: 20,
    paddingHorizontal: 16,
    marginVertical: 5,
  },
  titleBarValue: {
    color: '#fff',
    flex: 1,
  },
  footerButton: {
    minWidth: '50%',
    height: 45,
    justifyContent: 'center',
    position: 'absolute',
    bottom: 30,
    borderRadius: 50,
  },
  labelButton: {
    color: '#fff',
  },
  item: {
    height: 50,
    width: '100%',
  },
  itemContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 5,
    padding: 8,
    paddingVertical: 12,
  },
  CardTime: {
    // flex: 1,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    // borderRadius: 5,
    paddingHorizontal: 12,
    paddingVertical: 10,
    marginBottom: 5,
  },
  title: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginBottom: 3,
    height: 40,
    alignItems: 'center',
  },
  titleIcon: {
    // width: 30,
    // height: 30
    marginLeft: 5,
  },
  value: {
    flex: 1,
    textAlign: 'right',
    marginVertical: 5,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
  },
  dialogContent: {
    margin: 0,
    alignItems: 'center',
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(mapStateToProps, mapActionToProps)(RequestDetailsScreen);
