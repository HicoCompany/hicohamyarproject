import React, { useCallback, useMemo, useState } from 'react';
import { FlatList, Modal, Share, StyleSheet, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Field, Label } from '@components';
import { config } from '@src/utility/Constants';
import { Button, Card, Divider, FAB, List } from 'react-native-paper';
import { clearRequests } from '@src/redux/slices/development/index.develop';
import Colors from '@src/utility/Colors';
import { safeJsonParse } from '@src/utility/base';
import { ScaledSheet } from 'react-native-size-matters';

function Requests() {
  const dispatch = useDispatch();
  const { requests, Mobile, auth } = useSelector((store) => ({
    requests: store?.dev?.requests,
    Mobile: store.userData.mobileNumber,
    auth: store.userData.auth ?? {},
  }));
  // const length = useMemo(() => `درخواست ها ${requests?.length}`, [requests]);
  const [modal, setModal] = useState(false);

  const handleModal = useCallback(() => {
    setModal((state) => !state);
  }, []);

  const renderFab = (
    <FAB
      icon={modal ? 'close' : 'server'}
      style={[styles.fab, modal ? styles.fabInModal : {}]}
      onPress={handleModal}
      animated
    />
  );

  const handleClearData = () => {
    dispatch(clearRequests());
  };

  const handleShareToken = useCallback(() => {
    Share.share({
      title: 'به اشتراک گذاری توکن با برنامه نویس',
      message: auth.token,
    });
  }, [auth.token]);
  const handleShareRequest = (item, token) => () => {
    if (item) {
      delete item?.requestStr;
      delete item?.responseStr;
      Share.share({
        title: 'به اشتراک گذاری نتیجه با برنامه نویس',
        message: JSON.stringify({
          ...item,
          token,
          response: safeJsonParse(item.response),
          request: safeJsonParse(item.request),
        }),
      });
    }
  };

  const renderHeader = useMemo(
    () => (
      <Card>
        <Card.Title title="Requests" />
        <Card.Content>
          <Field title="baseUrl" value={config.API_URL} flexDirection="row" />
          <Field title="mobile" value={Mobile} flexDirection="row" />
          <Button
            mode="contained"
            labelStyle={styles.labelStyle}
            onPress={handleShareToken}>
            به اتشراک گذاری توکن
          </Button>
        </Card.Content>
      </Card>
    ),
    [Mobile, handleShareToken],
  );

  const _renderItems = useCallback(
    ({ item }) => {
      return (
        <Card style={styles.item}>
          <Card.Content>
            <Field title={'url'} value={item.url} flexDirection="row" />
            <Field title={'method'} value={item.method} flexDirection="row" />
            <Field
              title={'status code'}
              value={item.code}
              flexDirection="row"
            />
            <List.Section>
              <List.Accordion
                title={
                  <Label fontSize={15} weight="bold">
                    Request
                  </Label>
                }>
                <Label style={styles.data}>{item.requestStr}</Label>
              </List.Accordion>

              <List.Accordion
                title={
                  <Label fontSize={15} weight="bold">
                    Response
                  </Label>
                }>
                <Label style={styles.data}>{item.responseStr}</Label>
              </List.Accordion>
            </List.Section>
            {item.error && (
              <Field title={'error'} value={item.error} flexDirection="row" />
            )}
            <Field title={'time'} value={item.time} flexDirection="row" />
            <Button
              mode="outlined"
              onPress={handleShareRequest(item, auth.token)}>
              ارسال برای برنامه نویس
            </Button>
          </Card.Content>
        </Card>
      );
    },
    [auth.token],
  );

  return (
    <View style={styles.container}>
      <Modal visible={modal} animationType="fade">
        <FlatList
          data={requests}
          renderItem={_renderItems}
          style={styles.list}
          keyExtractor={(item) => item.key}
          ItemSeparatorComponent={Divider}
          ListHeaderComponent={renderHeader}
        />
        {renderFab}
        <FAB
          icon={'cancel'}
          color={Colors.white}
          style={[styles.fab, styles.clearButton]}
          onPress={handleClearData}
        />
      </Modal>
      {!modal && renderFab}
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    position: 'absolute',
    zIndex: 1,
    left: '12@s',
    bottom: '80@s',
    height: '45@s',
    borderRadius: '25@s',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    padding: 0,
  },
  list: {
    flex: 1,
  },
  fab: {
    width: 50,
    height: 50,
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.purple,
    zIndex: 1000,
  },
  fabInModal: {
    position: 'absolute',
    bottom: 50,
    alignSelf: 'center',
  },
  labelStyle: { color: 'white' },
  data: {
    paddingHorizontal: 20,
    lineHeight: 24,
  },
  item: {
    borderWidth: 1,
    margin: 5,
  },
  clearButton: {
    position: 'absolute',
    bottom: 50,
    right: 50,
    backgroundColor: Colors.red,
  },
});

export default Requests;
