import store from '@src/redux/store';

function stringify(json) {
  const data = JSON.stringify(json, null, 2);
  if (data) {
    return data?.replace(/\\n|"|! |{|}|,/g, '');
  }
  return data;
}

/**
 * @param {{ url, request, response, error, code, method }} param0
 */
export function saveToDev(data) {
  // we do this because sometimes there development will not added to project
  const addRequest = require('@src/redux/slices/development').addRequest;
  if (addRequest) {
    const requestStr = stringify(data.request);
    const responseStr = stringify(data.response);
    const request = JSON.stringify(data.request);
    const response = JSON.stringify(data.response);
    store.dispatch(
      addRequest({ requestStr, responseStr, ...data, request, response }),
    );
  }
}
