import { CommonActions } from '@react-navigation/native';
import { createRef } from 'react';
import ProxyPolyfillBuilder from 'proxy-polyfill/src/proxy';
const proxyPolyfill = ProxyPolyfillBuilder();

/** * @typedef {React.RefObject<import('@react-navigation/native').NavigationContainerRef>} NavigationRef
 * @type {NavigationRef | undefined}
 */
export const navigationRef = createRef();
export const navigationStatus = new proxyPolyfill(
  { ready: false, actions: [] },
  {
    set: (target, prop, value) => {
      if (value === true) {
        target.actions = [
          ...target.actions.map((action, i) => {
            navigationRef.current.dispatch(action);
            // target.actions[i] = null;
            return null;
          }),
        ];
      }
      target[prop] = value;
    },
  },
);

// this function is because of an error from react navigation that says navigation object isn't initialized
// call this function in initial screen
export function handleMakeNavigationReady() {
  // setTimeout(() => {
  navigationStatus.ready = true;
  // }, SPLASH_TIMEOUT);
}

export function navigationToWithDelay(name, params) {
  if (navigationStatus.ready) {
    navigationRef.current?.dispatch?.(CommonActions.navigate({ name, params }));
  } else {
    navigationStatus.actions.push(CommonActions.navigate({ name, params }));
  }
}
