import React, { useEffect, useMemo } from 'react';
import { StyleSheet, Platform, View, StatusBar } from 'react-native';

import { AuthScreens } from './auth';
import { AppScreens } from './app';
import { StandaloneScreens } from './standalone';
import Splash from './splash';
import { useDispatch, useSelector } from 'react-redux';
import { enableScreens } from 'react-native-screens';
// import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import Colors from '@src/utility/Colors';
import { hp } from '@src/responsive';
import Requests from './dev/Requests';
import { fetchGlobalDetails } from '@src/redux/actions';
import { handleMakeNavigationReady } from './navigation';
import {
  TransitionPresets,
  createStackNavigator,
} from '@react-navigation/stack';

enableScreens();

const Stack = createStackNavigator();

const screenOptions = {
  headerShown: false,
  contentStyle: { backgroundColor: '#ffffff' },
  animationEnabled: true,
  detachPreviousScreen: false,
  cardOverlayEnabled: false,
  stackAnimation: 'fade',
  direction: 'rtl',
  replaceAnimation: 'push',
  obscureBackground: false,
  cardStyle: {
    backgroundColor: Colors.backgroundColor,
  },
  ...TransitionPresets.FadeFromBottomAndroid,
};

const SPLASH_TIMEOUT = 800;

export function RootStack() {
  const dispatch = useDispatch();
  /**
   * @type {{current: Navigator}}
   */
  const { active, loadingDetails, showSplash } = useSelector((store) => ({
    active: store.userData?.auth?.active,
    showSplash: store.global.loading || store.global.error,
    loadingDetails: store.global.loading,
  }));

  useEffect(() => {
    if (loadingDetails !== null && loadingDetails === false) {
      setTimeout(() => {
        handleMakeNavigationReady();
      }, SPLASH_TIMEOUT);
    }
  }, [loadingDetails]);

  const Screens = useMemo(
    () =>
      Object.entries(
        active ? AppScreens : AuthScreens,
      ).map(([name, component]) => (
        <Stack.Screen
          key={name}
          name={name}
          component={component}
          options={component.navigationOptions}
          initialParams={component.initialParams}
        />
      )),
    [active],
  );
  const StandAlone = useMemo(
    () =>
      Object.entries(StandaloneScreens).map(([name, component]) => (
        <Stack.Screen
          key={name}
          name={name}
          component={component}
          options={component.navigationOptions}
          initialParams={component.initialParams}
        />
      )),
    [],
  );

  const Routes = (
    <>
      <View style={styles.headerBack} />
      <StatusBar
        animated
        backgroundColor={Colors.green}
        barStyle="light-content"
      />
      <Stack.Navigator
        screenOptions={screenOptions}
        mode="modal"
        initialRouteName={active ? 'Dashboard' : 'Intro'}>
        {Screens}
        {StandAlone}
      </Stack.Navigator>
      <Requests />
    </>
  );

  if (showSplash) return <Splash />;
  return Routes;
}

const styles = StyleSheet.create({
  headerBack: {
    backgroundColor: Colors.green,
    height: 100,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        paddingTop: 25,
        height: hp('12%'),
      },
      android: {
        height: hp('10%'),
      },
    }),
  },
});
