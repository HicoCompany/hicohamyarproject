import React, { useCallback, useRef, useState } from 'react';
import { ScrollView, View } from 'react-native';

//redux
import { addAddress } from '@src/redux/slices/AddressesSlice';
import { useDispatch } from 'react-redux';

//custom components
import { HeaderBack, Label, Select } from '@src/components';
import BottomSubmit from '@components/Buttons/BottomSubmit';
import TextInput from '@src/components/TextInput';

//utils
import { scale, ScaledSheet } from 'react-native-size-matters';
import { useFormik } from 'formik';
import { Button, HelperText } from 'react-native-paper';

import { dimensions } from '@src/utility/Constants';
import { useGetMainArea } from '@src/api/modules/Area';
import { useAddAddress } from '@src/api/modules/Address';
import { DefaultModalActions, useAlert } from '@src/components/GlobalAlert';
import Map from '@src/components/Map';
import Colors from '@src/utility/Colors';
import { useGetLocation } from '../MapAreaScreen/mapUtils';

/**
 *
 * @typedef {{callBack: () => void}} params
 * @typedef {import('@react-navigation/native').RouteProp<{Address: params},'Address'>} Route
 * @param {{navigation: import('@react-navigation/native').NavigationProp<ant,any>,route: Route}} param0
 */
export default function Address({ navigation, route }) {
  const dispatch = useDispatch();
  /** @type {import('react').Ref<MapView>} */
  const mapRef = useRef();
  const postalCodeRef = useRef();
  const houseNumberRef = useRef();
  const Alert = useAlert();
  const tempLocation = useRef();
  const [isAddressSelected, setIsAddressSelected] = useState(true);

  const [areaState, areaHandler] = useGetMainArea({
    onError: (state) => {
      Alert.addToast({
        message: state.message,
        type: 'error',
        timeout: 'short',
      });
    },
    onSuccess: (state) => {
      const final = transformAreaRequestResponse(state.data);
      return (prev) => ({
        ...prev,
        data: final,
      });
    },
    initialData: {
      list: [],
      areaList: [],
      area: {
        id: null,
        name: null,
      },
    },
  });

  const formik = useFormik({
    initialValues: {
      address: '',
      area: areaState.data.area,
      postalCode: '',
      houseNumber: '',
      position: null,
    },
    validate: (values) => {
      const errors = {};

      if (values.address.trim().length < 10) {
        errors.address = 'لطفا آدرس خود را وارد کنید';
      }

      if (values.position == null) {
        errors.position = 'موقعیت خود را بر روی نقشه مشخص کنید';
      }

      if (values.area == null) {
        errors.area = 'منطقه را انتخاب کنید';
      }

      return errors;
    },
    async onSubmit({ area, address, houseNumber, position, postalCode }) {
      try {
        const id = (await handler.handleFetch()).data;

        dispatch(
          addAddress({
            id,
            area,
            homeNo: houseNumber,
            position: {
              longitude: position?.longitude,
              latitude: position?.latitude,
            },
            street: address,
            postalCode,
          }),
        );
        Alert.addToast({
          message: state.message,
          type: 'success',
          timeout: 'short',
        });
        navigation.goBack();
      } catch (e) {
        Alert.setError(
          DefaultModalActions.Error({
            message: e.message,
          }),
        );
      }
    },
    validateOnBlur: false,
    validateOnMount: false,
    validateOnChange: false,
  });

  const [state, handler] = useAddAddress(
    {
      longitude: formik.values.position?.longitude,
      latitude: formik.values.position?.latitude,
    },
    formik.values.area?.id,
    formik.values.address,
    formik.values.houseNumber,
    formik.values.postalCode,
  );

  const { getAreaAndAddress, state: addressState } = useGetLocation();

  const focusOnUnit = useCallback(() => {
    houseNumberRef.current.focus();
  }, []);

  const focusOnPostalCode = useCallback(() => {
    postalCodeRef.current.focus();
  }, []);

  const handleAreaChange = (value) => {
    const item = areaState.data.areaList.find(
      (v) => v.MainAreaCode === value.MainAreaCode,
    );
    areaHandler.setState((prev) => ({
      ...prev,
      data: {
        ...prev.data,
        area: {
          ...item,
          id: item.MainAreaId,
          name: item.MainAreaCode,
        },
      },
    }));
    formik.setFieldValue('area', {
      id: item.MainAreaId,
      name: item.MainAreaCode,
    });
    focusOnPostalCode();

    // handleFetchCollectionTime(item?.MainAreaCode);
  };

  const transformAreaRequestResponse = useCallback(
    (data, areaCode) => {
      const list = data;
      const fromMap = data.filter(
        (el) => el.MainAreaCode == String(areaCode),
      )?.[0];

      // default area
      // array restructured because when modifying the array we don't want to modify response.Result and list
      const selected =
        fromMap ?? data.find((el) => `${el.Selected}` === '1') ?? list[0];

      formik.setFieldValue('area', {
        id: selected.MainAreaId,
        name: selected.MainAreaCode,
      });
      return {
        areaList: list,
        area: {
          ...selected,
          id: selected.MainAreaId,
          name: selected.MainAreaCode,
        },
      };
    },
    [formik],
  );

  const handleSetAddressAndArea = useCallback(
    ({ area, address }) => {
      formik.setFieldValue('address', address);
      formik.setFieldValue('position: ', tempLocation.current);

      const final = transformAreaRequestResponse(areaState.data.areaList, area);
      areaHandler.setState((prev) => ({
        ...prev,
        data: final,
      }));
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [transformAreaRequestResponse],
  );

  const handleLocation = useCallback(() => {
    formik.setFieldValue('position', tempLocation.current);
    setIsAddressSelected(true);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLocationChange = useCallback((location) => {
    //{"latitude": 31.3149892726385, "latitudeDelta": 0.1839046251655887, "longitude": 48.67896295956791, "longitudeDelta": 0.3015362676226516}
    tempLocation.current = location;
    setIsAddressSelected(false);
  }, []);

  return (
    <View style={styles.container}>
      <HeaderBack headerTitle="ثبت آدرس جدید" />

      <ScrollView
        style={styles.content}
        contentContainerStyle={styles.scrollContent}>
        <View style={styles.mapContainer}>
          <Map onIdle={handleLocationChange} mapRef={mapRef} />
          <View style={styles.PointerContainer}>
            <View style={styles.pointer} />
          </View>
          <Button
            icon={isAddressSelected ? 'check' : 'circle'}
            color={Colors.success}
            size={scale(25)}
            onPress={handleLocation}
            loading={addressState.loading}
            style={styles.mapButton}>
            {isAddressSelected ? 'انتخاب شد' : 'انتخاب موقعیت'}
          </Button>
        </View>
        <View style={styles.inputsContainer}>
          <Label weight="bold" style={styles.headerTitle}>
            آدرس و منطقه خود را وارد کنید
          </Label>
          <TextInput
            placeholder="آدرس"
            mode="default"
            style={[styles.inputs, styles.multiline]}
            multiline
            value={formik.values.address}
            onChangeText={formik.handleChange('address')}
            autoFocus
          />
          {formik.errors.address && (
            <HelperText style={styles.helperTextError} type="error">
              {formik.errors.address}
            </HelperText>
          )}
          <Select
            inputStyle={styles.areaInput}
            placeholder="منطقه"
            data={areaState.data?.areaList ?? []}
            value={areaState.data?.area}
            valueKey={'MainAreaCode'}
            getLabel={(el) => el?.Title}
            labelKey={'Title'}
            onChange={handleAreaChange}
            style={styles.select}
          />
          {formik.errors.area && (
            <HelperText style={styles.helperTextError} type="error">
              {formik.errors.area}
            </HelperText>
          )}

          <View style={styles.row}>
            <View style={styles.grow}>
              <TextInput
                placeholder="کد پستی"
                mode="default"
                style={styles.inputsWithCol}
                onChangeText={formik.handleChange('postalCode')}
                keyboardType="numeric"
                inputRef={postalCodeRef}
                onSubmitEditing={focusOnUnit}
                value={formik.values.postalCode}
                maxLength={10}
              />
              {formik.errors.postalCode && (
                <HelperText style={styles.helperTextError} type="error">
                  {formik.errors.postalCode}
                </HelperText>
              )}
            </View>
            <View style={styles.grow}>
              <TextInput
                placeholder="پلاک"
                mode="default"
                style={styles.inputsWithCol}
                onChangeText={formik.handleChange('houseNumber')}
                keyboardType="numeric"
                inputRef={houseNumberRef}
                value={formik.values.houseNumber}
              />
              {formik.errors.houseNumber && (
                <HelperText style={styles.helperTextError} type="error">
                  {formik.errors.houseNumber}
                </HelperText>
              )}
            </View>
          </View>
        </View>
      </ScrollView>
      <BottomSubmit
        onPress={formik.submitForm}
        mode="contained"
        loading={state.loading}>
        ثبت آدرس
      </BottomSubmit>
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  mapContainer: {
    alignItems: 'stretch',
    width: '100%',
    height: '250@s',
    overflow: 'hidden',
    backgroundColor: 'red',
    justifyContent: 'center',
  },
  content: {
    flex: 4,
    // minHeight: '100%',
  },
  scrollContent: {
    paddingBottom: 400,
  },
  inputsContainer: {
    padding: '16@s',
  },
  inputs: {
    minHeight: '40@s',
    flex: 1,
    margin: '8@s',
    marginBottom: 0,
  },
  inputsWithCol: {
    minHeight: '40@s',
    margin: '8@s',
    marginBottom: 0,
    flexWrap: 'wrap',
  },
  multiline: {
    paddingVertical: '8@s',
    flex: 4,
  },
  row: {
    flexDirection: 'row-reverse',
    width: '100%',
    justifyContent: 'space-evenly',
    // alignItems: '',
  },
  areaInput: {
    // width: '100%',
    textAlign: 'right',
    borderLeftColor: 'white',
    borderLeftWidth: 2,
    height: '40@s',
    margin: '8@s',
  },
  grow: {
    flexGrow: 1,
    flexShrink: 0,
    maxWidth: dimensions.window.width / 2.25,
  },
  helperTextError: {
    textAlign: 'center',
  },
  select: {
    height: '40@s',
    width: '100%',
    marginBottom: '8@s',
  },
  headerTitle: {
    fontSize: '13@s',
    textAlign: 'center',
    margin: '8@s',
  },
  locateContainer: {
    marginVertical: '10@s',
  },
  locate: {
    marginVertical: '10@s',
  },
  PointerContainer: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    position: 'absolute',
    width: '45@ms',
    height: '45@ms',
    borderRadius: '25@ms',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  pointer: {
    width: '5@ms',
    height: '5@ms',
    borderRadius: '3@ms',
    backgroundColor: '#4BB543',
  },
  mapButton: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    backgroundColor: Colors.white,
    margin: '12@s',
  },
});
