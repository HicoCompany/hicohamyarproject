import React, { useCallback, useEffect, useRef, useState } from 'react';
import { View } from 'react-native';
import { MAP_API_KEY } from 'config';

//redux
import { useDispatch, useSelector } from 'react-redux';
import { setSdiKey } from '@src/redux/actions';

//ui components
import Icon from 'react-native-vector-icons/Feather';
import Map from '@src/components/Map';
import { Button } from 'react-native-paper';

//utils
import { getApiKey, latLonToPixels, requestArea } from './mapUtils';
import Colors from '@src/utility/Colors';
import { ScaledSheet } from 'react-native-size-matters';

/**
 *
 * @param {{navigation: import('@react-navigation/stack').NavigationStackScreenProps}} param0
 */

export default function MapAreaScreen({ navigation, route }) {
  const api_key = useSelector((store) => store.userData.sdi_api_key);

  const [data, setData] = useState({
    area: '',
    areaId: null,
    address: '',
    areaLoading: false,
    areaError: false,
    addressLoading: false,
    position: {
      x: null,
      y: null,
    },
  });

  /**
   * @type {{current: MapView}}
   */
  const mapRef = useRef();
  const { type, onGoBack, list } = route.params;
  const navigationType = useRef(type).current;
  const dispatch = useDispatch();

  useEffect(() => {
    // let focusOnUserLocationDone = false;
    // getLocationPermissions()
    //   .then(() => {
    //     getLocationByRequestUpdate(
    //       (v) => {
    //         if (v.x) {
    //           setUserLocation({
    //             latitude: v.y,
    //             longitude: v.x,
    //           });
    //           if (!focusOnUserLocationDone) {
    //             mapRef.current?.animateCamera?.({
    //               center: {
    //                 latitude: v.y,
    //                 longitude: v.x,
    //               },
    //               zoom: 15,
    //             });
    //             focusOnUserLocationDone = true;
    //           }
    //         }
    //       },
    //       { fastestInterval: 500, interval: 1000, numUpdates: 5 },
    //     ).catch(() => {});
    //   })

    handleGetApiKey();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleMapPermissionError = useCallback(
    (error) => {
      if (error === 'restricted') {
        navigation.goBack();
      }
    },
    [navigation],
  );

  const handleGetApiKey = async () => {
    try {
      const res = await getApiKey();
      dispatch(setSdiKey(res.api_key));
      return res;
    } catch (e) {
      return e;
    }
  };

  const handleGetArea = (x, y) => {
    if (!x || !y) {
      setData((state) => ({
        ...state,
        areaError: true,
        areaLoading: false,
        area: 'امکان دریافت منطقه وجود ندارد',
        areId: null,
        position: {
          x,
          y,
        },
      }));
      return;
    }
    const lngLat = latLonToPixels(x, y);

    setData((state) => ({
      ...state,
      areaError: false,
      areaLoading: true,
      area: '...',
      areId: null,
      position: {
        x,
        y,
      },
    }));
    requestArea({
      api_key,
      ...lngLat,
    })
      .then((response) => {
        setData((state) => ({
          ...state,
          area: response.area,
          areaId: response.areaId,
          areaLoading: false,
        }));
      })
      .catch((e) => {
        setData((state) => ({
          ...state,
          areaLoading: false,
          areaError: true,
          area:
            e.message === 'error' ? 'امکان دریافت منطقه وجود ندارد' : e.message,
        }));
        // Alert.alert(e.message, e.error.message);
      });
  };

  const getAddress = (x, y) => {
    setData((state) => ({
      ...state,
      addressError: false,
      addressLoading: true,
    }));
    fetch(`http://map.ahvaz.ir/api/v2/reverse?lat=${y}&lng=${x}`, {
      headers: {
        'Api-Key': MAP_API_KEY,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setData((state) => ({
          ...state,
          address: res.formatted_address,
          addressLoading: false,
        }));
      })
      .catch(() => {
        setData((state) => ({
          ...state,
          addressLoading: false,
          addressError: true,
        }));
      });
  };

  // const openDetail = () => {
  //   setOpenMapModal(true);
  // };

  // const closeDetail = () => {
  //   setOpenMapModal(false);
  // };

  const handleMapIdle = useCallback(
    (position) => {
      if (position.longitude && position.latitude) {
        // openDetail();
        if (api_key) {
          handleGetArea(position?.longitude, position?.latitude);
        } else {
          handleGetApiKey().then(() => {
            handleGetArea(position?.longitude, position?.latitude);
          });
        }
        getAddress(position?.longitude, position?.latitude);
      } else {
        mapRef.current?.getCamera?.().then((camera) => {
          // handleGetArea(camera.center?.longitude,camera.center?.longitude);
          handleMapIdle(camera.center);
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [api_key],
  );

  const handleSubmit = () => {
    if (!data.address || !data.area) {
      mapRef.current?.getCamera?.().then((camera) => {
        handleMapIdle(camera.center);
      });
      return;
    }
    if (onGoBack) {
      onGoBack({
        area: {
          code: null,
          id: null,
        },
        address: '',
        position: data.position,
      });
      navigation.goBack();
      return;
    }
    if (navigationType === 1) {
      navigation.navigate('WasteCollectionSubmitForm', {
        list: list.filter((el) => el.count > 0),
        AreaId: data.areaId,
        AreaName: data.area,
        Address: data.address,
      });
    } else {
      navigation.navigate('Register', {
        AreaId: data.areaId,
        AreaName: data.area,
        Address: data.address,
      });
    }
  };
  // const handleTouchStart = () => {
  //   // closeDetail();
  // };

  return (
    <View style={styles.container}>
      <Map
        onIdle={handleMapIdle}
        // tileUrl="http://map.ahvaz.ir/basetile/tiles/?x={x}&y={y}&z={z}"
        mapRef={mapRef}
        onPermissionError={handleMapPermissionError}
      />
      {/* <Button
        style={styles.button}
        androidRippleColor="#333"
        disabled={data.areaLoading || data.addressLoading || data.areaError}
        loading={!data.areaError && (data.areaLoading || data.addressLoading)}
        icon={'check'}
        onPress={handleSubmit}>
        انتخاب موقیت
      </Button> */}
      <Button
        style={styles.button}
        icon="check"
        mode="contained"
        color="white"
        onPress={handleSubmit}>
        انتخاب موقیت
      </Button>
      <View style={styles.PointerContainer}>
        <View style={styles.pointer} />
      </View>
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  PointerContainer: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    position: 'absolute',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pointer: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: '#4BB543',
  },
  bottomBar: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    backgroundColor: 'rgba(255,255,255,0.8)',
    padding: 16,
  },
  button: {
    position: 'absolute',
    bottom: 10,
    width: 300,
    height: '50@s',
    backgroundColor: Colors.white,
    borderRadius: '6@s',
    justifyContent: 'center',
  },
  // checkButton: {
  //   backgroundColor: 'white',
  //   width: 50,
  //   height: 50,
  //   borderRadius: 25,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   overflow: 'hidden',
  //   color: '#4BB543',
  //   right: 10,
  //   bottom: 10,
  //   position: 'absolute',
  //   zIndex: 10,
  // },
  title: {
    color: '#666',
    fontFamily: 'IRANYekanRegularMobile(FaNum)',
    fontSize: 16,
  },
  value: {
    color: '#333',
    fontFamily: 'IRANYekanMobileBold(FaNum)',
    fontSize: 13,
    textAlign: 'right',
    paddingRight: 5,
  },
  row: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  marker: {
    padding: 5,
  },
});

// {
/* <Animated.View style={[styles.bottomBar, { height, opacity }]}>
        <Text style={styles.title}>آدرس:‌</Text>
        <Text style={styles.value}>{data.address || '...'}</Text>
        <View style={styles.row}>
          <Text style={styles.title}>منطقه:‌</Text>
          <Text style={styles.value}>{data.area ?? '...'}</Text>
        </View>
      </Animated.View> */
// }
// {
/* <Button
        style={styles.checkButton}
        androidRippleColor="#333"
        disabled={data.areaLoading || data.addressLoading || data.areaError}
        onPress={handleSubmit}>
        {!data.areaError && (data.areaLoading || data.addressLoading) ? (
          <ActivityIndicator color="#4BB543" />
        ) : (
          <Icon
            name={data.areaError ? 'slash' : 'check'}
            size={20}
            color={data.areaError ? Colors.gray : '#4BB543'}
          />
        )}
      </Button> */
//}
