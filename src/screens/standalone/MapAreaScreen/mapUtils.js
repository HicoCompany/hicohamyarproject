import { DefaultModalActions } from '@src/components/GlobalAlert';
import { NeshanMapModule } from '@src/components/NeshanMap/NeshanMapModule';
import { permissionPrompt } from '@src/utility/permissionPrompt';
import { MAP_API_KEY } from 'config';
import { SDI_API_ADDRESS } from 'config';
import { useEffect, useState } from 'react';
import { NativeEventEmitter, NativeModules } from 'react-native';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import RNFetchBlob from 'react-native-fetch-blob';
import RNPermissions from 'react-native-permissions';

const Request = RNFetchBlob.config({
  trusty: true,
  timeout: 5000,
});
/* eslint-disable radix */
export function latLonToPixels(lng, lat) {
  var TILE_SIZE = 256;
  var siny = Math.sin((lat * Math.PI) / 180);
  siny = Math.min(Math.max(siny, -0.9999), 0.9999);
  var pointX = TILE_SIZE * (0.5 + lng / 360);
  var pointY =
    TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI));
  //Get BBOX pixel in range 266 tileSize
  var r = 6378137 * Math.PI * 2;
  var x = (lng / 360) * r;
  var sin = Math.sin((lat * Math.PI) / 180);
  var y = ((0.25 * Math.log((1 + sin) / (1 - sin))) / Math.PI) * r;
  var bbox = x - 10 + ',' + (y - 10) + ',' + (x + 10) + ',' + (y + 10);

  return {
    bbox,
    pointX,
    pointY,
  };
}

export const getApiKey = () =>
  new Promise(async (res, rej) => {
    try {
      const response = await Request.fetch(
        'POST',
        `${SDI_API_ADDRESS}/geoapi/user/login/`,
        {
          'Content-Type': 'application/json',
        },
        JSON.stringify({
          appId: 'mobilegis',
          password: '123456',
          username: 'zistdoost',
        }),
      );
      const parsed = await response.json();
      res({
        api_key: parsed.api_key,
      });
    } catch (e) {
      rej(e);
    }
  });

export const requestArea = ({ api_key, pointY, pointX, bbox }) =>
  new Promise((res, rej) => {
    var url =
      'http://sdi.ahvaz.ir/wms/?format=image/png&service=WMS&version=1.1.1&request=GetFeatureInfo&srs=EPSG:3857&transparent=true&width=256&height=256&layers=GeoTajak:boundary97_project_1203&STYLES=boundary97_project_12034199&authkey=' +
      api_key +
      '&FEATURE_COUNT=50&Y=' +
      parseInt(pointY) +
      '&X=' +
      parseInt(pointX) +
      '&INFO_FORMAT=application/json&QUERY_LAYERS=GeoTajak:boundary97_project_1203&TILED=true&BBOX=' +
      bbox;
    Request.fetch('GET', url, {
      'Content-Type': 'application/json',
    })
      .then((data) => data.json())
      .then((jsonResponse) => {
        if (jsonResponse != null) {
          if (jsonResponse.features.length > 0) {
            res({
              area: jsonResponse.features[0].properties.name,
              areaId: jsonResponse.features[0].properties.objectid,
            });
          } else {
            rej({
              message: 'منطقه انتخابی خارج از اهواز می باشد',
            });
          }
        }
      })
      .catch((e) => {
        rej({
          message: 'error',
          error: e,
        });
      });
    // var req = new XMLHttpRequest();

    // req.responseType = 'json';
    // req.open('GET', url, true);

    // req.onload = function () {
    //   var jsonResponse = req.response;
    //   if (jsonResponse != null)
    //     if (jsonResponse.features.length > 0) {
    //       res({
    //         area: jsonResponse.features[0].properties.name,
    //       });
    //     } else {
    //       rej({
    //         message: 'منطقه انتخابی خارج از اهواز می باشد',
    //       });
    //     }
    // };

    // req.onerror = (e) => {
    // rej({
    //   message: 'error',
    //   error: e,
    // });
    // };
    // req.send(null);
  });

/**
 * @typedef {{x: number;y: number}}  Coordinate
 * @param {Coordinate} bottomLeft
 * @param {Coordinate} topRight
 * @param {Coordinate} point
 * @returns {boolean}
 */
export function inBoundingBox(bottomLeft, topRight, point) {
  let isLongInRange = point.x >= bottomLeft.x && point.x <= topRight.x;
  let isLatiInRange = point.y >= bottomLeft.y && point.y <= topRight.y;
  return isLongInRange && isLatiInRange;
}

export function getLocationByRequestUpdate(
  callback,
  { fastestInterval, interval, numUpdates } = {
    interval: 1000,
    fastestInterval: 500,
    numUpdates: 4,
  },
) {
  try {
    let eventEmitter = new NativeEventEmitter(NativeModules.NativeModules);
    eventEmitter.addListener('lastLocation', callback);
    NeshanMapModule.getLastLocationByListening({
      interval,
      fastestInterval,
      numUpdates,
    })
      .then(() => {
        eventEmitter.removeAllListeners('lastLocation');
      })
      .catch(() => {
        eventEmitter.removeAllListeners('lastLocation');
      });
  } catch (e) {
    callback?.(e);
  }
}

export const getLocationPermissions = (alert) =>
  new Promise((res, rej) => {
    const permissions = ['location', 'coarseLocation'];
    RNPermissions.checkMultiple(permissions)
      .then(async (statuses) => {
        if (statuses.location !== 'authorized') {
          permissionPrompt('location', 'نقشه', alert)
            .then(() => {
              RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
                interval: 1000,
                fastInterval: 0,
              })
                .then(res)
                .catch(rej);
            })
            .catch((error) => rej(error));
        } else {
          RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
            interval: 1000,
            fastInterval: 0,
          })
            .then(res)
            .catch(rej);
        }
      })
      .catch(rej);
  });

export const isPermissionsAuthorized = () =>
  new Promise((res, rej) => {
    const permissions = ['location', 'coarseLocation'];
    RNPermissions.checkMultiple(permissions)
      .then(async (statuses) => {
        if (statuses.location !== 'authorized') {
          res(false);
        } else {
          res(true);
        }
      })
      .catch(rej);
  });

/**
 *
 * @param {Alert} alert
 * @returns
 */
export const askForPermissions = (alert) =>
  new Promise(async (res, rej) => {
    const permissions = ['location', 'coarseLocation'];
    const restrictionMessage = `برای استفاده از نقشه لطفا به تنظیمات رفته و مجوز های نقشه را تایید کنید.`;
    const warn = `در صورتی که اجازه دسترسی را ندهید نمی توانید از این صفحه استفاده کنید.`;

    try {
      const permission = await RNPermissions.request(permissions[0]);
      if (permission === 'denied') {
        alert.addToast({
          message: warn,
          type: 'warning',
          timeout: 'short',
        });
        askForPermissions();
        rej(warn);
      } else if (permission === 'restricted') {
        alert.addToast({
          message: restrictionMessage,
          type: 'info',
          timeout: 'long',
        });
        rej(restrictionMessage);
      } else if (permission === 'authorized') {
        res('authorized');
        alert.addToast({
          message: 'دسترسی با موفقیت داده شد.',
          type: 'success',
          timeout: 'short',
        });
      }
    } catch (e) {
      rej(e);
    }
  });

export function useGetLocation() {
  const [state, setState] = useState({
    loading: false,
    error: false,
    message: '',
    area: null,
    address: null,
  });
  const [apiKey, setApiKey] = useState('');

  useEffect(() => {
    handleGetApiKey();
  }, []);

  const handleGetApiKey = async () => {
    try {
      const res = await getApiKey();
      setApiKey(res.api_key);
      return res;
    } catch (e) {
      return e;
    }
  };

  /**
   *
   * @param {number} longitude
   * @param {number} latitude
   * @returns {Promise<{address: string,area: string}>}
   */
  const getAreaAndAddress = async (longitude, latitude) => {
    if (!longitude || !latitude) {
      setState((prev) => ({
        ...prev,
        error: true,
        loading: false,
        position: {
          longitude,
          latitude,
        },
      }));
      return;
    }

    setState((prev) => ({
      ...prev,
      error: false,
      loading: true,
      position: {
        longitude,
        latitude,
      },
    }));

    try {
      const area = await fetchArea(longitude, latitude);
      const address = await fetchAddress(longitude, latitude);

      setState((prev) => ({
        ...prev,
        loading: false,
        error: false,
        message: '',
      }));

      return {
        address,
        area,
      };
    } catch (e) {
      setState((prev) => ({
        ...prev,
        loading: false,
        error: true,
        message: e,
      }));
      return e;
    }
  };

  const fetchArea = (longitude, latitude) => {
    const lngLat = latLonToPixels(longitude, latitude);
    return requestArea({
      api_key: apiKey,
      ...lngLat,
    })
      .then((response) => {
        setState((prev) => ({
          ...prev,
          area: response.area,
          loading: false,
        }));

        return response.area;
      })
      .catch((e) => {
        return e.message === 'error'
          ? 'امکان دریافت منطقه وجود ندارد'
          : e.message;
      });
  };

  const fetchAddress = (longitude, latitude) => {
    return fetch(
      `http://map.ahvaz.ir/api/v2/reverse?lat=${latitude}&lng=${longitude}`,
      {
        headers: {
          'Api-Key': MAP_API_KEY,
        },
      },
    )
      .then((res) => res.json())
      .then((res) => {
        setState((prev) => ({
          ...prev,
          address: res.formatted_address,
        }));

        return res.formatted_address;
      })
      .catch(() => {
        return 'خطای دریافت آدرس توسط موقعیت.';
      });
  };

  return {
    getAreaAndAddress,
    state,
  };
}
