import React, { Fragment, useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { Label } from '@src/components';
import { useNavigation } from '@react-navigation/native';
import Colors from '@src/utility/Colors';

export default function LocateFromMap({
  onGoBack,
  pillRound = false,
  label = 'انتخاب موقعیت از نقشه',
  params,
  header,
  style,
}) {
  const navigation = useNavigation();

  const handleNavigateToMapScreen = useCallback(() => {
    navigation.navigate('MapAreaScreen', {
      onGoBack,
      ...params,
    });
  }, [navigation, params, onGoBack]);

  return (
    <Fragment>
      {header}
      <TouchableRipple
        style={[styles.button, pillRound ? styles.pillRound : undefined, style]}
        onPress={handleNavigateToMapScreen}>
        <Label style={styles.button}>{label}</Label>
      </TouchableRipple>
    </Fragment>
  );
}

const styles = StyleSheet.create({
  button: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.green,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderRadius: 6,
    color: 'white',
    alignSelf: 'center',
  },
  pillRound: { borderRadius: 20, overflow: 'hidden' },
});
