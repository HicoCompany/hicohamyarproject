import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import NeshanMap from '@components/NeshanMap';
import { permissionPrompt } from '@src/utility/permissionPrompt';
import RNPermissions from 'react-native-permissions';
import Icon from 'react-native-vector-icons/Feather';
import { Button } from 'native-base';
import Animated, { EasingNode, interpolateNode } from 'react-native-reanimated';
import {
  getApiKey,
  latLonToPixels,
  requestArea,
} from '@src/screens/registerPasmandScreen/mapAreaScreen/mapUtils';
import { NESHAN_API_KEY } from 'config';
import { useDispatch, useSelector } from 'react-redux';
import { setSdiKey } from '@src/redux/actions';
import Colors from '@src/utility/Colors';
import Constants from '@src/utility/Constants';
import { useTransition } from 'react-native-redash/lib/module/v1';

/**
 *
 * @param {{navigation: import('@react-navigation/stack').NavigationStackScreenProps}} param0
 */

export default function MapAreaScreen({ navigation, route }) {
  const api_key = useSelector((store) => store.userData.sdi_api_key);
  const [data, setData] = useState({
    area: '',
    areaId: null,
    address: '',
    areaLoading: false,
    areaError: false,
    addressLoading: false,
  });
  const [openMapModal, setOpenMapModal] = useState(false);
  const transition = useTransition(openMapModal, {
    duration: 500,
    easing: EasingNode.inOut(EasingNode.ease),
  });
  let opacity = transition;
  let height = useMemo(
    () =>
      interpolateNode(transition, {
        inputRange: [0, 1],
        outputRange: [0, 200],
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [openMapModal],
  );

  const mapRef = useRef();
  const { type, onGoBack, list } = route.params;
  const navigationType = useRef(type).current;
  const dispatch = useDispatch();

  useEffect(() => {
    getLocationPermissions();
    handleGetApiKey();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetApiKey = async () => {
    try {
      const res = await getApiKey();
      dispatch(setSdiKey(res.api_key));
    } catch (e) {}
  };

  const handleGetArea = (x, y) => {
    const lngLat = latLonToPixels(x, y);
    setData((state) => ({
      ...state,
      areaError: false,
      areaLoading: true,
    }));

    requestArea({
      api_key,
      ...lngLat,
    })
      .then((response) => {
        setData((state) => ({
          ...state,
          area: response.area,
          areaId: response.areaId,
          areaLoading: false,
        }));
      })
      .catch((e) => {
        setData((state) => ({
          ...state,
          areaLoading: false,
          areaError: true,
          area:
            e.message === 'error' ? 'امکان دریافت منطقه وجود ندارد' : e.message,
        }));
      });
  };

  const getAddressFromNeshan = (x, y) => {
    setData((state) => ({
      ...state,
      addressError: false,
      addressLoading: true,
    }));
    fetch(`https://api.neshan.org/v2/reverse?lat=${y}&lng=${x}`, {
      headers: {
        'Api-Key': NESHAN_API_KEY,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setData((state) => ({
          ...state,
          address: res.formatted_address,
          addressLoading: false,
        }));
      })
      .catch(() => {
        setData((state) => ({
          ...state,
          addressLoading: false,
          addressError: true,
        }));
      });
  };

  const openDetail = () => {
    setOpenMapModal(true);
  };

  const closeDetail = () => {
    setOpenMapModal(false);
  };

  const getLocationPermissions = useCallback(() => {
    const permissions = ['location', 'coarseLocation'];
    RNPermissions.checkMultiple(permissions)
      .then(async (statuses) => {
        if (statuses.location !== 'authorized') {
          permissionPrompt('location', 'نقشه', () => {
            navigation.goBack();
          });
        }
      })
      .catch((error) => {});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleMapIdle = useCallback(
    (position) => {
      openDetail();
      if (api_key) {
        handleGetArea(position?.x, position?.y);
      }
      getAddressFromNeshan(position?.x, position?.y);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [api_key],
  );

  const handleSubmit = useCallback(() => {
    if (!data.address || !data.area) {
      mapRef.current?.getLocationFromModule?.().then((position) => {
        handleMapIdle(position);
      });
      return;
    }
    if (onGoBack) {
      onGoBack({
        area: {
          code: data.area,
          id: data.areaId,
        },
        address: data.address,
      });
      navigation.goBack();
      return;
    }
    if (navigationType === 1) {
      navigation.navigate('WasteCollectionSubmitForm', {
        list: list.filter((el) => el.count > 0),
        AreaId: data.areaId,
        AreaName: data.area,
        Address: data.address,
      });
    } else if (navigationType == 2) {
      navigation.navigate('Register', {
        AreaId: data.areaId,
        AreaName: data.area,
        Address: data.address,
      });
    }
  }, [
    data.address,
    data.area,
    data.areaId,
    handleMapIdle,
    list,
    navigation,
    navigationType,
    onGoBack,
  ]);
  const handleTouchStart = () => {
    closeDetail();
  };

  return (
    <View style={styles.container}>
      <NeshanMap
        onMapIdle={handleMapIdle}
        onTouchStart={handleTouchStart}
        isRotatable={false}
        location={{
          ...Constants.DEFAULT_LOCATION,
          duration: 0,
        }}
        ref={mapRef}
      />

      <Animated.View style={[styles.bottomBar, { height, opacity }]}>
        <Text style={styles.title}>آدرس:‌</Text>
        <Text style={styles.value}>{data.address || '...'}</Text>
        <View style={styles.row}>
          <Text style={styles.title}>منطقه:‌</Text>
          <Text style={styles.value}>{data.area || '...'}</Text>
        </View>
      </Animated.View>
      <Button
        style={styles.checkButton}
        androidRippleColor="#333"
        disabled={data.areaLoading || data.addressLoading || data.areaError}
        onPress={handleSubmit}>
        {!data.areaError && (data.areaLoading || data.addressLoading) ? (
          <ActivityIndicator color="#4BB543" />
        ) : (
          <Icon
            name={data.areaError ? 'slash' : 'check'}
            size={20}
            color={data.areaError ? Colors.gray : '#4BB543'}
          />
        )}
      </Button>
      <View style={styles.PointerContainer}>
        <View style={styles.pointer} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  PointerContainer: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    position: 'absolute',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pointer: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: '#4BB543',
  },
  bottomBar: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    backgroundColor: 'rgba(255,255,255,0.8)',
    padding: 16,
  },
  checkButton: {
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    color: '#4BB543',
    right: 10,
    bottom: 10,
    position: 'absolute',
    zIndex: 10,
  },
  title: {
    color: '#666',
    fontFamily: 'IRANYekanRegularMobile(FaNum)',
    fontSize: 16,
  },
  value: {
    color: '#333',
    fontFamily: 'IRANYekanMobileBold(FaNum)',
    fontSize: 13,
    textAlign: 'right',
    paddingRight: 5,
  },
  row: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
});
