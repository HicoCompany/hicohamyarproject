import MapAreaScreen from './MapAreaScreen';
import AddressForm from './AddressForm';

export const StandaloneScreens = {
  MapAreaScreen,
  AddressForm,
};
