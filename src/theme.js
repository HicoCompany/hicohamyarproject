import { configureFonts, DefaultTheme } from 'react-native-paper';
import Colors from './utility/Colors';

// ^2.16.0
const customFonts = {
  android: {
    light: {
      fontFamily: 'IRANYekanLightMobile(FaNum)',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'IRANYekanMobileBold(FaNum)',
      fontWeight: 'normal',
    },
    regular: {
      fontFamily: 'IRANYekanRegularMobile(FaNum)',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'IRANYekanLightMobile(FaNum)',
      fontWeight: 'normal',
    },
  },
  ios: {
    light: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: 'lighter',
    },
    medium: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: '400',
    },
    regular: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: '100',
    },
  },
};

/**
 * @type DefaultTheme
 */
export const theme = {
  ...DefaultTheme,
  roundness: 6,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.green,
    background: Colors.backgroundColor,
    error: Colors.error,
    backdrop: Colors.backdropColor,
  },
  fonts: configureFonts(customFonts),
};
