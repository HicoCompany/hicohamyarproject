import { useCallback, useEffect, useState } from 'react';
import { NetInfoStateType, useNetInfo } from '@react-native-community/netinfo';

/**
 * @typedef {import('axios').AxiosRequestConfig} AxiosConfig
 * @typedef {'loading' | 'refreshing' | 'loadingMore'} RequestType
 * @typedef {{done: boolean,succeeded: boolean,loading: boolean, refreshing: boolean,loadingMore: boolean,error: boolean,requestType: RequestType,message: string,data: any}} State
 * @typedef {{handleRefreshing: () => Promise<State>,handleLoadMore: (config: AxiosConfig) => Promise<State>,handleFetch: (config: AxiosConfig) => Promise<State>,setState: import('react').Dispatch<import('react').SetStateAction<State>>}} Handlers
 * @param {{requestOnMount: boolean,request: (config: AxiosConfig) => Promise<import('axios').AxiosResponse<any>>.transformResponse: (data: any) => Promise<any>} & AxiosConfig} param
 * @returns {[State,Handlers]}
 */
export default function useRequest(
  { requestOnMount = true, request, url, transformResponse, ...config },
  {
    responseMessageProperty = 'Text',
    responseSuccessProperty = 'Success',
    initialData,
    defaultLoading,
  },
) {
  const [state, setState] = useState({
    loading: defaultLoading || requestOnMount,
    refreshing: false,
    loadingMore: false,
    error: false,
    requestType: null,
    message: '',
    data: initialData ?? null,
    succeeded: false,
  });
  const { isConnected, type } = useNetInfo();

  useEffect(() => {
    if (requestOnMount) {
      handleRequest('loading', config);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   *
   * @param {RequestType} requestType
   * @description handle all request: this function get a request type and based on it changes the state for example if its loading then the loading state will change to true on request start
   */
  const handleRequest = useCallback(
    (requestType = 'loading', handlerConfig) =>
      new Promise(async (res, rej) => {
        //check if user connected to network if connected then request else reject/setState to error true
        if (isConnected || type === NetInfoStateType.unknown) {
          //change state to requesting based on requestType
          const initialState = {
            loading: false,
            refreshing: false,
            loadingMore: false,
            // [requestType]: true,
            error: false,
            errorType: null,
            message: '',
            data: initialData,
            succeeded: false,
          };

          initialState[requestType] = true;
          setState(initialState);
          try {
            const response = await request({
              ...config,
              ...handlerConfig,
            });

            const data =
              (await transformResponse?.(response.data)) ?? response.data;

            // create the data/state object
            const newState = {
              loading: false,
              refreshing: false,
              loadingMore: false,
              error: false,
              // message: response.data?.[responseMessageProperty],
              data,
              requestType,
              succeeded: true,
              done: true,
            };

            //check if the request is successful based on status code and success property from the response
            if (response.data?.[responseSuccessProperty] === 1) {
              res(newState);
              setState(newState);
            } else {
              // if it wasn't successful change the error property to true and resolve/setState
              newState.error = true;
              newState.succeeded = false;

              rej(newState);
              setState(newState);
            }
          } catch (e) {
            const newState = {
              loading: false,
              refreshing: false,
              loadingMore: false,
              error: true,
              requestType,
              data: initialData,
              succeeded: false,
              message: e.response?.data?.[responseMessageProperty],
              done: false,
              Error: e,
            };

            // check again the connection to realize the reason behind error and if it was connected check status code
            if (!isConnected) {
              !newState.message &&
                (newState.message =
                  'لطفا ارتباط خود را با اینترنت بررسی کنید.');
              setState(newState);
              return rej(newState);
            }
            if (e.response.status === 400 || e.response.status > 500) {
              !newState.message &&
                (newState.message =
                  'خطایی رخ داد لطفا از طریق صفحه مدیریت کاربر به پشتیبانی اطلاع بدهید');
              setState(newState);
              return rej(newState);
            } else {
              !newState.message &&
                (newState.message =
                  'خطایی رخ داد لطفا از طریق صفحه مدیریت کاربر به پشتیبان اطلاع بدهید');
              setState(newState);
              return rej(newState);
            }
          }
        } else {
          setState({
            loading: false,
            refreshing: false,
            loadingMore: false,
            error: true,
            requestType,
            message: 'لطفا ارتباط خود را با اینترنت بررسی کنید.',
            data: initialData,
            succeeded: false,
          });
        }
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isConnected, request, type, transformResponse, config.data, config.params],
  );
  /**
   *
   * @param {AxiosConfig} Config
   */
  const handleFetch = useCallback(
    (Config) => handleRequest('loading', Config),
    [handleRequest],
  );

  /**
   *
   * @param {AxiosConfig} Config
   */
  const handleRefreshing = useCallback(
    (Config) => handleRequest('refreshing', Config),
    [handleRequest],
  );

  /**
   *
   * @param {AxiosConfig} Config
   * @description YOU SHOULD ADD THE PROPERTY RELATED TO LOAD MORE TO THE CONFIG
   */
  const handleLoadMore = useCallback(
    (Config) => handleRequest('loadingMore', Config),
    [handleRequest],
  );

  return [state, { handleFetch, handleRefreshing, handleLoadMore, setState }];
}
