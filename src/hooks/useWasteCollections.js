import { REQUESTS } from '@src/api';
import useRequest from './useRequest';

export function useSubmitWasteCollection() {
  return useRequest(
    {
      request: REQUESTS.Waste.submitCollectionRequestInstantaneous,
      requestOnMount: false,
    },
    { initialData: {} },
  );
}

export function useGetWasteCollectionTimes() {
  return useRequest(
    {
      request: REQUESTS.Waste.getWasteCollectionTimes,
      requestOnMount: true,
      transformResponse: async (data) => data.Result,
    },
    { initialData: [] },
  );
}
