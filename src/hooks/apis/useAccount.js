import { Api } from '@src/api';
import useRequest from './useRequest';

// Mobile
// DeviceID
// FromName
export function useLogin({ phoneNumber, deviceId }) {
  return useRequest({
    request: Api.Account.Login,
    requestOnMount: false,
    data: {
      Mobile: phoneNumber,
      DeviceId: deviceId,
    },
  });
}

export function useAuthenticate({ phoneNumber, code }) {
  return useRequest({
    request: Api.Account.Authentication,
    requestOnMount: false,
    data: {
      Mobile: phoneNumber,
      Code: code,
    },
  });
}
