import { useCallback, useEffect, useState } from 'react';
import { NetInfoStateType, useNetInfo } from '@react-native-community/netinfo';

/**
 * @typedef {import('axios').AxiosRequestConfig} AxiosConfig
 * @typedef {'loading' | 'refreshing' | 'loadingMore'} RequestType
 * @typedef {{done: boolean,succeeded: boolean,loading: boolean, refreshing: boolean,loadingMore: boolean,error: boolean,requestType: RequestType,message: string,data: any}} State
 * @typedef {{handleRefreshing: () => Promise<State>,handleLoadMore: (config: AxiosConfig) => Promise<State>,handleFetch: (config: AxiosConfig) => Promise<State>,setState: import('react').Dispatch<import('react').SetStateAction<State>>}} Handlers
 * @param {{onError(state: State) : void,onSuccess(state:State): void,requestOnMount: boolean,request: (config: AxiosConfig) => Promise<import('axios').AxiosResponse<any>>.transformResponse: (data: any) => Promise<any>,initialData: any,defaultLoading: boolean} & AxiosConfig} param
 * @returns {[State,Handlers]}
 */
export function useRequest({
  requestOnMount = true,
  request,
  url,
  transformResponse,
  onError,
  onSuccess,
  initialData,
  defaultLoading,
  ...config
}) {
  // responseMessageProperty = 'Message',
  // responseSuccessProperty = 'Success',
  const [state, setState] = useState({
    loading: defaultLoading || requestOnMount,
    refreshing: false,
    loadingMore: false,
    error: false,
    requestType: null,
    message: '',
    data: initialData ?? null,
    succeeded: false,
  });
  const { isConnected, type } = useNetInfo();

  useEffect(() => {
    if (requestOnMount) {
      handleRequest('loading', config).catch(onError ?? (() => {}));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   *
   * @param {RequestType} requestType
   * @description handle all request: this function get a request type and based on it changes the state for example if its loading then the loading state will change to true on request start
   */
  const handleRequest = useCallback(
    (requestType = 'loading', handlerConfig) =>
      new Promise(async (res, rej) => {
        //check if user connected to network if connected then request else reject/setState to error true
        if (isConnected || type === NetInfoStateType.unknown) {
          //change state to requesting based on requestType
          const initialState = {
            loading: false,
            refreshing: false,
            loadingMore: false,
            // [requestType]: true,
            error: false,
            errorType: null,
            message: '',
            data: initialData,
            succeeded: false,
          };

          initialState[requestType] = true;
          setState(initialState);
          try {
            const response = await request({
              ...config,
              ...handlerConfig,
            });

            const data =
              (await transformResponse?.(response.data)) ?? response.data;

            // create the data/state object
            const newState = {
              loading: false,
              refreshing: false,
              loadingMore: false,
              error: false,
              data,
              requestType,
              succeeded: true,
              done: true,
            };
            const stateSetter = onSuccess?.(newState);
            setState(stateSetter ? stateSetter : newState);
            res(newState);
          } catch (e) {
            const newState = {
              loading: false,
              refreshing: false,
              loadingMore: false,
              error: true,
              requestType,
              data: initialData,
              response: e.data,
              succeeded: false,
              message: e.message ?? undefined,
              done: false,
              Error: e,
            };

            // check again the connection to realize the reason behind error and if it was connected check status code
            if (!isConnected) {
              !newState.message &&
                (newState.message =
                  'لطفا ارتباط خود را با اینترنت بررسی کنید.');
            }
            if (e.response?.status === 400 || e.response?.status > 500) {
              if (!newState.message) {
                newState.message =
                  'خطایی رخ داد لطفا از طریق صفحه مدیریت کاربر به پشتیبانی اطلاع بدهید';
              }
            }

            setState(newState);
            return rej(newState);
          }
        } else {
          const error = {
            loading: false,
            refreshing: false,
            loadingMore: false,
            error: true,
            requestType,
            message: 'لطفا ارتباط خود را با اینترنت بررسی کنید.',
            data: initialData,
            succeeded: false,
          };
          setState(error);
          onError?.(error);
        }
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isConnected, request, type, transformResponse, config.data, config.params],
  );
  /**
   *
   * @param {AxiosConfig} Config
   */
  const handleFetch = useCallback(
    (Config) => handleRequest('loading', Config),
    [handleRequest],
  );

  /**
   *
   * @param {AxiosConfig} Config
   */
  const handleRefreshing = useCallback(
    (Config) => handleRequest('refreshing', Config),
    [handleRequest],
  );

  /**
   *
   * @param {AxiosConfig} Config
   * @description YOU SHOULD ADD THE PROPERTY RELATED TO LOAD MORE TO THE CONFIG
   */
  const handleLoadMore = useCallback(
    (Config) => handleRequest('loadingMore', Config),
    [handleRequest],
  );

  return [state, { handleFetch, handleRefreshing, handleLoadMore, setState }];
}
