import { axios } from '@src/config/api';
// /**
//  * @typedef {{'Account' | 'Advertisement' | 'App' | 'Area' | 'Bank' | 'Booth' | 'Charity' | 'CreditManagement' |'DiscountCard' | 'FinancialContractor' | 'Helper' | 'HelperDeliverWaste' | 'HelperFinancial' | 'HelperLearningFiles' | 'HelperSuggestion' | 'Notification' | 'Package' | 'Payment' | 'Survey' | 'Ware' | 'WastType'}} Groups
//  * @type {{[key: string]: {[key: string]: (conf: import('./utils').AxiosConfig) => import('axios').AxiosPromise<any>}}}
//  */

/**
 * @template {RequestBody = string,ResponseBody= number}
 * @typedef {import('axios').AxiosRequestConfig & {params: RequestBody,data: RequestBody}} RequestParam
 * @typedef {import('axios').AxiosPromise<ResponseBody>} RequestReturn
 */
const Services = {
  Account: {
    requestCode: (conf) =>
      axios('/Account/RequestCodeHelper', { method: 'post', ...conf }),
    verifyUser: (conf) =>
      axios('/Account/VerifyHelper', { method: 'post', ...conf }),

    // LoginContractor: (conf) =>
    //   axios('/Account/LoginContractor', { method: 'post', ...conf }),
    // ChangePasswordContractor: (conf) =>
    //   axios('/Account/ChangePasswordContractor', { method: 'post', ...conf }),
    // AuthCheck: (conf) =>
    //   axios('/Account/AuthCheck', { method: 'get', ...conf }),
    // Protected: (conf) =>
    //   axios('/Account/Protected', { method: 'get', ...conf }),
  },
  Advertisement: {
    GetAdvertisementForHelper: (conf) =>
      axios('/Advertisement/GetAdvertisementForHelper', {
        method: 'get',
        ...conf,
      }),
    GetAdvertisementByIDForHelper: (conf) =>
      axios('/Advertisement/GetAdvertisementByIDForHelper', {
        method: 'get',
        ...conf,
      }),
    GetAdvertisementForContractor: (conf) =>
      axios('/Advertisement/GetAdvertisementForContractor', {
        method: 'get',
        ...conf,
      }),
    GetAdvertisementByIDForContractor: (conf) =>
      axios('/Advertisement/GetAdvertisementByIDForContractor', {
        method: 'get',
        ...conf,
      }),
  },
  App: {
    GetAppSetting: (conf) =>
      axios('/App/GetAppSetting', { method: 'get', ...conf }),
    GetGlobalDetails: (conf) =>
      axios('/App/GetGlobalDetails', { method: 'get', ...conf }),
    // SettingForContractor: (conf) =>
    //   axios('/App/SettingForContractor', { method: 'get', ...conf }),

    //TODO useless
    // ShowAppRules: (conf) =>
    //   axios('/App/ShowAppRules', { method: 'get', ...conf }),
    // ShowHelpForHelper: (conf) =>
    //   axios('/App/ShowHelpForHelper', { method: 'get', ...conf }),
  },
  Area: {
    GetMainAreas: (conf) =>
      axios('/Area/GetMainAreas', { method: 'get', ...conf }),
  },
  Bank: {
    GetBank: (conf) => axios('/Bank/GetBank', { method: 'get', ...conf }),
  },
  Booth: {
    GetAllBoothes: (conf) =>
      axios('/Booth/GetAllBoothes', { method: 'get', ...conf }),
    GetBoothesItemByBoothesID: (conf) =>
      axios('/Booth/GetBoothesItemByBoothesID', { method: 'get', ...conf }),
    GetAllBoothesTitle: (conf) =>
      axios('/Booth/GetAllBoothesTitle', { method: 'get', ...conf }),
    GetBootheInfoByBoothId: (conf) =>
      axios('/Booth/GetBootheInfoByBoothId', { method: 'get', ...conf }),
  },
  Charity: {
    GetCharity: (conf) =>
      axios('/Charity/GetCharity', { method: 'get', ...conf }),
    AddCharityPaymentToHelperByContractor: (conf) =>
      axios('/Charity/AddCharityPaymentToHelperByContractor', {
        method: 'post',
        ...conf,
      }),
  },
  // "Contractor": {
  //     "AddHelperByContractor":(conf) => axios('/Contractor/AddHelperByContractor', { method: 'post', ...conf }),
  //     "AddDeliverWasteByContractor":(conf) => axios('/Contractor/AddDeliverWasteByContractor', { method: 'post', ...conf }),
  //     "UpdateDeliverWasteItemByContractor":(conf) => axios('/Contractor/UpdateDeliverWasteItemByContractor', { method: 'put', ...conf }),
  //     "RegisterDeliverWasteByContractor":(conf) => axios('/Contractor/RegisterDeliverWasteByContractor', { method: 'post', ...conf }),
  //     "AddTransferWasteForContractor":(conf) => axios('/Contractor/AddTransferWasteForContractor', { method: 'post', ...conf }),
  //     "AddRequestDeliverWasteByFixedBoothPerosnel":(conf) => axios('/Contractor/AddRequestDeliverWasteByFixedBoothPerosnel', { method: 'post', ...conf }),
  //     "GetCalculationPointandPriceForContractor":(conf) => axios('/Contractor/GetCalculationPointandPriceForContractor', { method: 'post', ...conf }),
  //     "GetRequestWareHelperItemForContractor":(conf) => axios('/Contractor/GetRequestWareHelperItemForContractor', { method: 'get', ...conf }),
  //     "ShowHelpForContractor":(conf) => axios('/Contractor/ShowHelpForContractor', { method: 'get', ...conf }),
  //     "GetInfoCooperation":(conf) => axios('/Contractor/GetInfoCooperation', { method: 'get', ...conf }),
  //     "SettingsForContractor":(conf) => axios('/Contractor/SettingsForContractor', { method: 'get', ...conf }),
  //     "InfoContractor":(conf) => axios('/Contractor/InfoContractor', { method: 'get', ...conf }),
  //     "AddLocationContractor":(conf) => axios('/Contractor/AddLocationContractor', { method: 'post', ...conf }),
  //     "DeliverForTransferByContractorIDForTransfer":(conf) => axios('/Contractor/DeliverForTransferByContractorIDForTransfer', { method: 'get', ...conf }),
  //     "GetDeliverWasteForTransfer":(conf) => axios('/Contractor/GetDeliverWasteForTransfer', { method: 'get', ...conf }),
  //     "GetHelper":(conf) => axios('/Contractor/GetHelper', { method: 'get', ...conf }),
  //     "GetInfoCooperationByContractor":(conf) => axios('/Contractor/GetInfoCooperationByContractor', { method: 'get', ...conf })
  // },
  // "ContractorDeliverWaste": {
  //     "AddRequestDeliverWasteForContractor":(conf) => axios('/ContractorDeliverWaste/AddRequestDeliverWasteForContractor', { method: 'post', ...conf }),
  //     "DelRequestDeliverWasteForContractor":(conf) => axios('/ContractorDeliverWaste/DelRequestDeliverWasteForContractor', { method: 'delete', ...conf }),
  //     "GetDeliveredWasteContractorReport":(conf) => axios('/ContractorDeliverWaste/GetDeliveredWasteContractorReport', { method: 'get', ...conf }),
  //     "GetDeliveredWasteReportItemContractor":(conf) => axios('/ContractorDeliverWaste/GetDeliveredWasteReportItemContractor', { method: 'get', ...conf }),
  //     "GetDeliverWasteForContractor":(conf) => axios('/ContractorDeliverWaste/GetDeliverWasteForContractor', { method: 'get', ...conf }),
  //     "GetContractor":(conf) => axios('/ContractorDeliverWaste/GetContractor', { method: 'get', ...conf }),
  //     "GetTransferWasteForContractor":(conf) => axios('/ContractorDeliverWaste/GetTransferWasteForContractor', { method: 'get', ...conf }),
  //     "GetDeliverWasteByDeliveryWasteIDForContractor":(conf) => axios('/ContractorDeliverWaste/GetDeliverWasteByDeliveryWasteIDForContractor', { method: 'get', ...conf }),
  //     "DeleteDeleiverWasteItemByID":(conf) => axios('/ContractorDeliverWaste/DeleteDeleiverWasteItemByID', { method: 'delete', ...conf }),
  //     "ConfirmRequestBySafir":(conf) => axios('/ContractorDeliverWaste/ConfirmRequestBySafir', { method: 'post', ...conf })
  // },
  CreditManagement: {
    GetInfoCreditManagement: (conf) =>
      axios('/CreditManagement/GetInfoCreditManagement', {
        method: 'get',
        ...conf,
      }),
    GetStockAndPointOfHelper: (conf) =>
      axios('/CreditManagement/GetStockAndPointOfHelper', {
        method: 'get',
        ...conf,
      }),
    GetInfoCreditManagementContractor: (conf) =>
      axios('/CreditManagement/GetInfoCreditManagementContractor', {
        method: 'get',
        ...conf,
      }),
    GetHelperCreditManagement: (conf) =>
      axios('/CreditManagement/GetHelperCreditManagement', {
        method: 'get',
        ...conf,
      }),
    GetPaymentDetails: (conf) =>
      axios('/CreditManagement/GetPaymentDetails', { method: 'get', ...conf }),
  },
  DiscountCard: {
    GetDiscountCard: (conf) =>
      axios('/DiscountCard/GetDiscountCard', { method: 'get', ...conf }),
    GetDiscountCardForContractor: (conf) =>
      axios('/DiscountCard/GetDiscountCardForContractor', {
        method: 'get',
        ...conf,
      }),
    AddDiscountCardToHelperByContractor: (conf) =>
      axios('/DiscountCard/AddDiscountCardToHelperByContractor', {
        method: 'post',
        ...conf,
      }),
  },
  FinancialContractor: {
    GetFinancialContractorReport: (conf) =>
      axios('/FinancialContractor/GetFinancialContractorReport', {
        method: 'post',
        ...conf,
      }),
  },
  Helper: {
    RegisterHelper: (conf) =>
      axios('/Helper/RegisterHelper', { method: 'post', ...conf }),
    UpdateHelperAddres: (conf) =>
      axios('/Helper/UpdateHelperAddres', { method: 'put', ...conf }),
    GetInfoNaturalPerson: (conf) =>
      axios('/Helper/GetInfoNaturalPerson', { method: 'get', ...conf }),
    UpdateNaturalPerson: (conf) =>
      axios('/Helper/UpdateNaturalPerson', { method: 'put', ...conf }),
    AddReagentCodeHelper: (conf) =>
      axios('/Helper/AddReagentCodeHelper', { method: 'put', ...conf }),
    GetDetermineDeliveryTime: (conf) =>
      axios('/Helper/GetDetermineDeliveryTime', { method: 'get', ...conf }),

    GetTimeOfSettringCollect: (conf) =>
      axios('/Helper/GetTimeOfSettringCollect', { method: 'get', ...conf }),
    SettingsForHelper: (conf) =>
      axios('/Helper/SettingsForHelper', { method: 'get', ...conf }),
    SettingForHelper: (conf) =>
      axios('/Helper/SettingForHelper', { method: 'get', ...conf }),
    AddProblem: (conf) =>
      axios('/Helper/AddProblem', { method: 'post', ...conf }),
    AddSupportMessageHelper: (conf) =>
      axios('/Helper/AddSupportMessageHelper', { method: 'post', ...conf }),
    ShowAppGuide: (conf) =>
      axios('/Helper/ShowAppGuide', { method: 'get', ...conf }),
    AddSatisfactionHelper: (conf) =>
      axios('/Helper/AddSatisfactionHelper', { method: 'post', ...conf }),
    GetAllSettingCollectTodayRangeTime: (conf) =>
      axios('/Helper/GetAllSettingCollectTodayRangeTime', {
        method: 'get',
        ...conf,
      }),
  },
  // "HelperContractor": {
  //     "GetLocationContractor":(conf) => axios('/HelperContractor/GetLocationContractor', { method: 'get', ...conf }),
  //     "GetHelperDetails":(conf) => axios('/HelperContractor/GetHelperDetails', { method: 'get', ...conf }),
  //     "GetAllHelperWithPoint":(conf) => axios('/HelperContractor/GetAllHelperWithPoint', { method: 'post', ...conf })
  // },
  WasteCollection: {
    /**
     * get delivery times
     * @param {import('axios').AxiosRequestConfig} conf
     */
    GetMoreDetermineDeliveryTime: (conf) =>
      axios('/Helper/GetMoreDetermineDeliveryTime', { method: 'get', ...conf }),
    GetDeliverWasteHelper: (conf) =>
      axios('/HelperDeliverWaste/GetDeliverWasteHelper', {
        method: 'get',
        ...conf,
      }),
    GetDeliverWasteByDeliveryWasteID: (conf) =>
      axios('/HelperDeliverWaste/GetDeliverWasteByDeliveryWasteID', {
        method: 'get',
        ...conf,
      }),
    GetLastDeliverWasteNotSurveyByHelper: (conf) =>
      axios('/HelperDeliverWaste/GetLastDeliverWasteNotSurveyByHelper', {
        method: 'get',
        ...conf,
      }),
    AddRequestDeliverWaste: (conf) =>
      axios('/HelperDeliverWaste/AddRequestDeliverWaste', {
        method: 'post',
        ...conf,
      }),
    UpdateRequestDeliverWaste: (conf) =>
      axios('/HelperDeliverWaste/UpdateRequestDeliverWaste', {
        method: 'put',
        ...conf,
      }),
    DelRequestDeliverWaste: (conf) =>
      axios('/HelperDeliverWaste/DelRequestDeliverWaste', {
        method: 'delete',
        ...conf,
      }),
    AddInstantaneousRequestDeliverWaste: (conf) =>
      axios('/HelperDeliverWaste/AddInstantaneousRequestDeliverWaste', {
        method: 'post',
        ...conf,
      }),
    GetInfoCooperation: (conf) =>
      axios('/HelperDeliverWaste/GetInfoCooperation', {
        method: 'get',
        ...conf,
      }),
    GetDeliveredWasteReport: (conf) =>
      axios('/HelperDeliverWaste/GetDeliveredWasteReport', {
        method: 'get',
        ...conf,
      }),
    GetDeliveredWasteReportItem: (conf) =>
      axios('/HelperDeliverWaste/GetDeliveredWasteReportItem', {
        method: 'get',
        ...conf,
      }),
    GetReceivedWareHelperReportItem: (conf) =>
      axios('/HelperDeliverWaste/GetReceivedWareHelperReportItem', {
        method: 'get',
        ...conf,
      }),
  },
  HelperFinancial: {
    GetFinancialHelperReport: (conf) =>
      axios('/HelperFinancial/GetFinancialHelperReport', {
        method: 'post',
        ...conf,
      }),
  },
  HelperLearningFiles: {
    GetLearningFilesForHelper: (conf) =>
      axios('/HelperLearningFiles/GetLearningFilesForHelper', {
        method: 'get',
        ...conf,
      }),
    GetLearningVideoFilesForHelper: (conf) =>
      axios('/HelperLearningFiles/GetLearningVideoFilesForHelper', {
        method: 'get',
        ...conf,
      }),
    ChangeLikeLearningFileForHelper: (conf) =>
      axios('/HelperLearningFiles/ChangeLikeLearningFileForHelper', {
        method: 'put',
        ...conf,
      }),
    ViewLearningFileForHelper: (conf) =>
      axios('/HelperLearningFiles/ViewLearningFileForHelper', {
        method: 'put',
        ...conf,
      }),
  },
  HelperSuggestion: {
    AddSuggestionForHelper: (conf) =>
      axios('/HelperSuggestion/AddSuggestionForHelper', {
        method: 'post',
        ...conf,
      }),
    UpdateSuggestionForHelper: (conf) =>
      axios('/HelperSuggestion/UpdateSuggestionForHelper', {
        method: 'put',
        ...conf,
      }),
    DeleteSuggestionForHelper: (conf) =>
      axios('/HelperSuggestion/DeleteSuggestionForHelper', {
        method: 'delete',
        ...conf,
      }),
    GetSuggestionForHelper: (conf) =>
      axios('/HelperSuggestion/GetSuggestionForHelper', {
        method: 'get',
        ...conf,
      }),
    GetStatusSuggestionForHelper: (conf) =>
      axios('/HelperSuggestion/GetStatusSuggestionForHelper', {
        method: 'get',
        ...conf,
      }),
    UpdateAnswerSuggestionStatus: (conf) =>
      axios('/HelperSuggestion/UpdateAnswerSuggestionStatus', {
        method: 'put',
        ...conf,
      }),
  },
  Notification: {
    GetNotificationForHelper: (conf) =>
      axios('/Notification/GetNotificationForHelper', {
        method: 'get',
        ...conf,
      }),
    GetNotificationByID: (conf) =>
      axios('/Notification/GetNotificationByID', { method: 'get', ...conf }),
    AddLikeNotificationForHelper: (conf) =>
      axios('/Notification/AddLikeNotificationForHelper', {
        method: 'put',
        ...conf,
      }),
    GetCountOfNotViewdItem: (conf) =>
      axios('/Notification/GetCountOfNotViewdItem', { method: 'get', ...conf }),
    GetNotificationForContractor: (conf) =>
      axios('/Notification/GetNotificationForContractor', {
        method: 'get',
        ...conf,
      }),
    GetNotificationByIDForContractor: (conf) =>
      axios('/Notification/GetNotificationByIDForContractor', {
        method: 'post',
        ...conf,
      }),
    AddLikeNotificationForContractor: (conf) =>
      axios('/Notification/AddLikeNotificationForContractor', {
        method: 'post',
        ...conf,
      }),
    UpdateHelperFireBaseId: (conf) =>
      axios('/Helper/UpdateHelperFireBaseId', { method: 'post', ...conf }),
    SendTestNonfiction: (conf) =>
      axios('/Helper/TestFirebaseSurvey', { method: 'post', ...conf }),
  },
  Package: {
    GetPackage: (conf) =>
      axios('/Package/GetPackage', { method: 'post', ...conf }),
    AddRequestPackage: (conf) =>
      axios('/Package/AddRequestPackage', { method: 'post', ...conf }),
    GetListRequestPackage: (conf) =>
      axios('/Package/GetListRequestPackage', { method: 'post', ...conf }),
    DeliveryRequestPackage: (conf) =>
      axios('/Package/DeliveryRequestPackage', { method: 'post', ...conf }),
    PackageRegistration: (conf) =>
      axios('/Package/PackageRegistration', { method: 'post', ...conf }),
    SearchHelper: (conf) =>
      axios('/Package/SearchHelper', { method: 'post', ...conf }),
    SearchHelperNew: (conf) =>
      axios('/Package/SearchHelperNew', { method: 'post', ...conf }),
    GetPackageForContractor: (conf) =>
      axios('/Package/GetPackageForContractor', { method: 'post', ...conf }),
  },
  Payment: {
    AddCashpayment: (conf) =>
      axios('/Payment/AddCashpayment', { method: 'post', ...conf }),
    AddSupportingHico: (conf) =>
      axios('/Payment/AddSupportingHico', { method: 'post', ...conf }),
    AddCharityPayment: (conf) =>
      axios('/Payment/AddCharityPayment', { method: 'post', ...conf }),
    AddCashpaymentToHelper: (conf) =>
      axios('/Payment/AddCashpaymentToHelper', { method: 'post', ...conf }),
  },
  Survey: {
    QuestionsSurveyHelper: (conf) =>
      axios('/Survey/QuestionsSurveyHelper', { method: 'get', ...conf }),
    AddReqSurveyHelper: (conf) =>
      axios('/Survey/AddReqSurveyHelper', { method: 'post', ...conf }),
    SkipSurveyHelper: (conf) =>
      axios('/Survey/SkipSurveyHelper', { method: 'post', ...conf }),
  },
  Ware: {
    GetWare: (conf) => axios('/Ware/GetWare', { method: 'get', ...conf }),
    AddRequestWare: (conf) =>
      axios('/Ware/AddRequestWare', { method: 'post', ...conf }),
    AddRequestWareHelperByContractor: (conf) =>
      axios('/Ware/AddRequestWareHelperByContractor', {
        method: 'post',
        ...conf,
      }),
    DeliverWareHelperByContractor: (conf) =>
      axios('/Ware/DeliverWareHelperByContractor', { method: 'post', ...conf }),
    AddRequestWareHelperByHelper: (conf) =>
      axios('/Ware/AddRequestWareHelperByHelper', { method: 'post', ...conf }),
    ShowRequestWareDetial: (conf) =>
      axios('/Ware/ShowRequestWareDetial', { method: 'get', ...conf }),
    ShowRequestWareDetialForPersonnel: (conf) =>
      axios('/Ware/ShowRequestWareDetialForPersonnel', {
        method: 'post',
        ...conf,
      }),
    EditRequestWareHelperByHelper: (conf) =>
      axios('/Ware/EditRequestWareHelperByHelper', { method: 'post', ...conf }),
    GetWareOfBooth: (conf) =>
      axios('/Ware/GetWareOfBooth', { method: 'post', ...conf }),
    ShowWareDetailsHeler: (conf) =>
      axios('/Ware/ShowWareDetailsHeler', { method: 'get', ...conf }),
    ShowAllWareDetailsHeler: (conf) =>
      axios('/Ware/ShowAllWareDetailsHeler', { method: 'get', ...conf }),
    ShowAllReciveWare: (conf) =>
      axios('/Ware/ShowAllReciveWare', { method: 'get', ...conf }),
    DeleteRequestWareHelper: (conf) =>
      axios('/Ware/DeleteRequestWareHelper', { method: 'delete', ...conf }),
    GetDeliveredWareContractor: (conf) =>
      axios('/Ware/GetDeliveredWareContractor', { method: 'get', ...conf }),
    GetReceivedWareContractorReport: (conf) =>
      axios('/Ware/GetReceivedWareContractorReport', {
        method: 'get',
        ...conf,
      }),
    GetReceivedWareContractorReportItem: (conf) =>
      axios('/Ware/GetReceivedWareContractorReportItem', {
        method: 'post',
        ...conf,
      }),
    GetWareForContractor: (conf) =>
      axios('/Ware/GetWareForContractor', { method: 'get', ...conf }),
    GetWareForPersonnel: (conf) =>
      axios('/Ware/GetWareForPersonnel', { method: 'get', ...conf }),
    GetRequestWareHelperForContractor: (conf) =>
      axios('/Ware/GetRequestWareHelperForContractor', {
        method: 'post',
        ...conf,
      }),
  },
  WastType: {
    GetWast: (conf) => axios('/WastType/GetWast', { method: 'get', ...conf }),
    GetWastTypeInFoByWastType: (conf) =>
      axios('/WastType/GetWastTypeInFoByWastType', { method: 'get', ...conf }),
    GetWastTypeForContractor: (conf) =>
      axios('/WastType/GetWastTypeForContractor', { method: 'get', ...conf }),
  },

  Address: {
    /**
     * @param {import('axios').AxiosRequestConfig} conf
     */
    getAll: (conf) =>
      axios('/HelperAddress/GetAllHelperAddress', { method: 'get', ...conf }),
    add: (conf) =>
      axios('/HelperAddress/AddHelperAddress', { method: 'post', ...conf }),
    update: (conf) =>
      axios('/HelperAddress/UpdateHelperAddress', { method: 'put', ...conf }),
    delete: (conf) =>
      axios('/HelperAddress/DelHelperAddress', { method: 'delete', ...conf }),
  },
  Guild: {
    getMain: (conf) =>
      axios('/SepGuild/GetMainGuilds', { method: 'get', ...conf }),
    getSub: (conf) =>
      axios('/SepGuild/GetSubGuilds', { method: 'get', ...conf }),
  },
};

export default Services;
