import Service from '../services';
import { useRequest } from '../utils';
import DeviceInfo from 'react-native-device-info';

const deviceId = DeviceInfo.getUniqueID();

/** @typedef {{verifyId: string,nationalCode: string,gender: number,name: string,family: string,reagentCode: number,guildId: number, mainAreaId: number}} RegisterRequestBody*/

/**  @param {RegisterRequestBody} data */
export function registerHelper(data = {}) {
  return Service.Helper.RegisterHelper({
    data: {
      VerifiyId: data.verifyId,
      VerifyId: data.verifyId,
      NationalCode: data.nationalCode,
      Gender: data.gender,
      Name: data.name,
      Family: data.family,
      ReagentCode: data.reagentCode,
      GuildId: data.guildId,
      MainAreaId: data.mainAreaId,
    },
  });
}

/**
 *
 * @param {RegisterRequestBody} data
 */
export function useRegisterHelper(data = {}) {
  return useRequest({
    request: Service.Helper.RegisterHelper,
    data: {
      VerifiyId: data.verifyId,
      VerifyId: data.verifyId,
      NationalCode: data.nationalCode,
      Gender: data.gender,
      Name: data.name,
      Family: data.family,
      ReagentCode: data.reagentCode,
      GuildId: data.guildId,
      MainAreaId: data.mainAreaId,
    },
    requestOnMount: false,
  });
}

export function getProfile() {
  return Service.Helper.GetInfoNaturalPerson();
}

export function useProfile() {
  return useRequest({
    request: Service.Helper.GetInfoNaturalPerson,
    initialData: {},
  });
}

/** @param {{Name: string,Family: string,NationalCode: number,Email: string,ShabaNumber: string,AccountOwner: string,AccountNumber: string,BankID: number, Gender: 1 | 2,GuildId: number}} data */
export function updateProfile(data) {
  return Service.Helper.UpdateNaturalPerson({
    data: {
      DeviceID: deviceId,
      ...data,
    },
  });
}

/**
 * @param {{Name: string,Family: string,NationalCode: number,Email: string,ShabaNumber: string,AccountOwner: string,AccountNumber: string,BankID: number, Gender: 1 | 2,GuildId: number}} data
 * */
export function useUpdateProfile(data, options) {
  return useRequest({
    ...options,
    request: Service.Helper.UpdateNaturalPerson,
    data: {
      DeviceID: deviceId,
      ...data,
    },
    requestOnMount: false,
  });
}

export function useSubmitSupportMessage({ title, message }, options) {
  return useRequest({
    ...options,
    request: Service.Helper.AddSupportMessageHelper,
    data: {
      Message: `موضوع : ${title}   
    ${message}`,
    },
    initialData: {},
    requestOnMount: false,
  });
}

export function getSettings() {
  return Service.Helper.SettingsForHelper();
}

export function useGetSettings(options) {
  return useRequest({
    ...options,
    request: Service.Helper.SettingsForHelper,
    initialData: {},
  });
}
