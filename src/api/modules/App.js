import { useRequest } from '../utils';
import services from '../services';

export function getAppSetting() {
  return services.App.GetAppSetting();
}

export function useGetAppSetting() {
  return useRequest({
    request: services.App.GetAppSetting,
    requestOnMount: true,
  });
}

/**
 * @typedef {{HelperVersion: string,SafirVersion: string,"PrivacyLink": string,"HelperGuideLink": string,"SafirGuideLink": string,"HelperMarketLink": string | null,"SafirMarketLink": string | null,HelperWelcomeMessage: string}} GlobalDetail
 * @returns {import('axios').AxiosPromise<GlobalDetail>}
 */
export function getGlobalDetails() {
  return services.App.GetGlobalDetails();
}

export function useGetGlobalDetails() {
  return useRequest({
    request: services.App.GetGlobalDetails,
    requestOnMount: true,
  });
}
