import Services from '../services';
import uniqBy from 'lodash/uniqBy';
import { useRequest } from '../utils';

export function useGetSuggestions() {
  return useRequest(
    {
      request: Services.HelperSuggestion.GetSuggestionForHelper,
      transformResponse: async (data) => uniqBy(data, 'SuggestionId'),
    },
    {
      initialData: [],
    },
  );
}

export function useChangeSuggestionStatus({ suggestionId }) {
  return useRequest(
    {
      request: Services.HelperSuggestion.UpdateAnswerSuggestionStatus,
      requestOnMount: false,
      data: { SuggestionId: suggestionId },
    },
    {
      initialData: {
        Result: {},
      },
    },
  );
}

export function useSubmitNewSuggestion({ title, message }) {
  return useRequest(
    {
      request: Services.HelperSuggestion.AddSuggestionForHelper,
      requestOnMount: false,
      data: { MessageText: message, Subject: title },
    },
    {
      initialData: {
        Result: {},
      },
    },
  );
}
