import { useRequest } from '../utils';
import Services from '../services';
import { uniqBy } from 'lodash-es';
import { Months } from '@src/utility/Constants';
import { func } from 'prop-types';

/**
 * @description get wastes list
 * @param {{ wasteType: 'wastes' | 'usedGoods',page: number,limit: number }} param0
 */
export async function getWastes({ wasteType = 'wastes', page, limit = 20 }) {
  return Services.WastType.GetWast({
    params: {
      wasteTypeId: wasteType == 'usedGoods' ? 71 : 70,
      pageNumber: page,
      pageSize: limit,
    },
  });
}

/**
 * @description get wastes list
 * @param {{ wasteType: 'wastes' | 'usedGoods',page: number,limit: number }} param0
 */
export function useWastes({ wasteType = 'wastes', page, limit = 20 }, options) {
  return useRequest({
    ...options,
    request: Services.WastType.GetWast,
    initialData: [],
    params: {
      wasteTypeId: wasteType == 'usedGoods' ? 71 : 70,
      pageNumber: page,
      pageSize: limit,
    },
  });
}

/**
 * @description get waste collection times
 */
export function getDeliveryTime(areaId) {
  return Services.WasteCollection.GetMoreDetermineDeliveryTime({
    params: {
      mainAreaID: areaId,
    },
  });
}

/**
 * @description get waste collection times
 */
export function useDeliveryTime({ areaId }, args) {
  return useRequest({
    ...args,
    request: Services.WasteCollection.GetMoreDetermineDeliveryTime,
    params: {
      mainAreaID: `${areaId}`,
    },
    requestOnMount: false,
    transformResponse: async (data) => {
      if (data.ResultID == -1000) {
        return {
          ...data,
          uniqByDeliverDateStr: [],
          list: [],
        };
      }
      const response = (data?.Result ?? [])
        .map((el) => {
          const [Year, Month, MDay] = el.DeliverDateStr?.split('/');
          return {
            ...el,
            Year,
            Month,
            MDay: parseInt(MDay, 10),
            MonthName: Months[parseInt(Month - 1, 10)],
          };
        })
        .sort((a, b) => a.Index - b.Index);

      const uniqItems = uniqBy(response, 'DeliverDateStr');
      return {
        ...data,
        uniqByDeliverDateStr: uniqItems,
        list: response,
      };
    },
  });
}
// get / api / Helper / GetMoreDetermineDeliveryTime;

export function useSubmitWasteCollection() {
  return useRequest({
    request: Services.WasteCollection.AddInstantaneousRequestDeliverWaste,
    requestOnMount: false,
    initialData: {},
  });
}

export function getWasteCollectionDetail({ deliveryWasteId }) {
  return Services.WasteCollection.GetDeliverWasteByDeliveryWasteID({
    params: {
      deliveryWasteID: deliveryWasteId,
    },
  });
}

export function useWasteCollectionDetail({ deliveryWasteId }, args) {
  return useRequest({
    ...args,
    request: Services.WasteCollection.GetDeliverWasteByDeliveryWasteID,
    params: {
      deliveryWasteID: deliveryWasteId,
    },
    requestOnMount: true,
  });
}

export function getWasteRequestsInformation() {
  return Services.WasteCollection.GetInfoCooperation();
}

export function getWasteCollectionRequests() {
  return Services.WasteCollection.GetDeliverWasteHelper();
}

export function cancelWasteCollectionRequest({ deliveryWasteId } = {}) {
  return Services.WasteCollection.DelRequestDeliverWaste({
    params: { deliveryWasteID: deliveryWasteId },
  });
}

export function updateWasteRequest({
  timeId,
  requestId,
  areaCode,
  address,
  date,
  wastes,
} = {}) {
  return Services.WasteCollection.UpdateRequestDeliverWaste({
    data: {
      SettringCollectID: timeId,
      DeliveryWasteID: requestId,
      MainAreaID2: areaCode,
      Adress2: address,
      PDeliveryDate: date,
      DeliveredWasteList: wastes,
    },
  });
}

export function useUpdateWasteRequest({
  timeId,
  requestId,
  areaCode,
  address,
  date,
  wastes,
  addressId,
} = {}) {
  return useRequest({
    request: Services.WasteCollection.UpdateRequestDeliverWaste,
    data: {
      AddressId: addressId,
      SettringCollectID: timeId,
      DeliveryWasteID: requestId,
      PDeliveryDate: date,
      DeliveredWasteList: wastes,
    },
    requestOnMount: false,
  });
}
