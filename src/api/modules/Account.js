import { useRequest } from '../utils';
import Services from '../services';
import DeviceInfo from 'react-native-device-info';
import Utils from '@src/utility/Utils';

const DeviceId = DeviceInfo.getUniqueID();

/**
 * @description get code for login or registration
 * @param {{deviceId: string, phoneNumber: number}} data
 */
export function requestCode({ phoneNumber, hash }) {
  return Services.Account.requestCode({
    data: {
      Mobile: Utils.toEnglishDigits(phoneNumber),
      DeviceId,
      hash,
    },
  });
}

/**
 * @description get code for login or registration
 * @param {{deviceId: string, phoneNumber: number}} data
 */
export function useRequestCode({ phoneNumber }) {
  return useRequest({
    request: Services.Account.requestCode,
    requestOnMount: false,
    data: {
      Mobile: Utils.toEnglishDigits(phoneNumber),
      DeviceId,
    },
  });
}

/**
 * @description confirm code that received from getCode/useGetCode and get token
 * @param {{verifyId: string,code: number}} data
 */
export function verifyUser({ code, verifyId }) {
  return Services.Account.verifyUser({
    data: {
      VerifyId: verifyId,
      Code: code,
      DeviceId,
    },
  });
}

/**
 * @description confirm code that received from getCode/useGetCode and get token
 * @param {{verifyId: string,code: number}} param0
 */
export function useVerifyUser({ code, verifyId }) {
  return useRequest({
    request: Services.Account.verifyUser,
    requestOnMount: false,
    data: {
      VerifyId: verifyId,
      VerifiyId: verifyId,
      Code: code,
      DeviceId,
    },
  });
}
