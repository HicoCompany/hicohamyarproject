import { useRequest } from '../utils';
import services from '../services';

function serializeResponse(data) {
  return {
    id: data?.HelperAddressId,
    // userId: data?.HelperId,
    position: {
      latitude: data?.LatitudeLocation,
      longitude: data?.LongitudeLocation,
    },
    area: {
      id: data?.MainAreaId,
      name: data?.MainAreaTitle,
    },
    street: data?.Street,
    homeNo: data?.HomeNo,
    isDeleted: data?.IsDeleted,
  };
}

/**
 * @description get all helper addresses
 */
export function getAllAddresses(config) {
  return services.Address.getAll({
    ...config,
    transformResponse: [(data) => data.map((el) => serializeResponse(el))],
  });
}

/**
 * @description get all helper addresses
 */
export function useGetAllAddresses({ initialData = [] }) {
  return useRequest({
    request: services.Address.getAll,
    transformResponse: async (data) => data.map((el) => serializeResponse(el)),
    initialData,
  });
}

/**
 * @description Add address
 * @param {{latitude: string,longitude: string}} location
 * @param {number} areaId
 * @param {string} street
 * @param {number} homeNo
 * */
export function addAddress(location, areaId, street, homeNo, postalCode) {
  return services.Address.add({
    data: {
      LatitudeLocation: location?.latitude,
      LongitudeLocation: location?.longitude,
      MainAreaId: areaId,
      Street: street,
      HomeNo: homeNo,
      PostalCode: postalCode,
    },
  });
}

/**
 * @description Add address
 * @param {{latitude: string,longitude: string}} location
 * @param {number} areaId
 * @param {string} street
 * @param {number} homeNo
 */
export function useAddAddress(location, areaId, street, homeNo) {
  return useRequest({
    request: services.Address.add,
    requestOnMount: false,
    data: {
      LatitudeLocation: location?.latitude,
      LongitudeLocation: location?.longitude,
      MainAreaId: areaId,
      Street: street,
      HomeNo: homeNo,
    },
  });
}

/**
 * @description Update address
 * @param {number} id
 * @param {{latitude: string,longitude: string}} location
 * @param {number} areaId
 * @param {string} street
 * @param {number} homeNo
 * */
export function updateAddress(id, location, areaId, street, homeNo) {
  return services.Address.update({
    data: {
      HelperAddressId: id,
      LatitudeLocation: location?.latitude,
      LongitudeLocation: location?.longitude,
      MainAreaId: areaId,
      Street: street,
      HomeNo: homeNo,
    },
  });
}

/**
 * @description Update address
 * @param {number} id
 * @param {{latitude: string,longitude: string}} location
 * @param {number} areaId
 * @param {string} street
 * @param {number} homeNo
 */
export function useUpdateAddress(id, location, areaId, street, homeNo) {
  return useRequest({
    request: services.Address.add,
    requestOnMount: false,
    data: {
      HelperAddressId: id,
      LatitudeLocation: location?.latitude,
      LongitudeLocation: location?.longitude,
      MainAreaId: areaId,
      Street: street,
      HomeNo: homeNo,
    },
  });
}

export function deleteAddress({ helperAddressId } = {}) {
  return services.Address.delete({
    params: {
      helperAddressId,
    },
  });
}

export function useDeleteAdress({ helperAddressId } = {}, options) {
  return useRequest({
    request: services.Address.delete,
    requestOnMount: false,
    params: {
      helperAddressId,
    },
  });
}
