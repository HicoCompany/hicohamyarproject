import { useRequest } from '../utils';
import Services from '../services';

// TODO write parameter and return types
/**
 * @description get banks list from this function for class based components
 * @returns {any}
 */
export function getBank() {
  return Services.Bank.GetBank();
}

/**
 * @description use this hooks for better tools in functional components
 */
export function useGetBank() {
  return useRequest({
    request: Services.Bank.GetBank,
    requestOnMount: true,
  });
}
