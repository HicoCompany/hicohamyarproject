import { useRequest } from '../utils';
import Services from '../services';

/**
 * @description get boothes list from this function for class based components
 */
export function getAllBoothes() {
  return Services.Booth.GetAllBoothes();
}

/**
 * @description use this hooks for better tools in functional components
 */
export function useBoothes() {
  return useRequest({
    request: Services.Booth.GetAllBoothes,
    requestOnMount: true,
    initialData: [],
  });
}

/**
 * @description get boothes title list from this function for class based components
 * @returns {any}
 */
export function getAllBoothTitle() {
  return Services.Booth.GetAllBoothesTitle();
}

/**
 * @description use this hooks for better tools in functional components
 * @returns {anu}
 */
export function useGetAllBoothTitle() {
  return useRequest({
    request: Services.Booth.GetAllBoothesTitle,
    requestOnMount: true,
  });
}

/**
 *
 * @param {{id: number | string}} param0
 * @description get boothes info from this function for class based components
 * @returns {any}
 */
export function getBoothInfo({ id }) {
  return Services.Booth.GetBootheInfoByBoothId({
    data: {
      BoothId: id,
    },
  });
}

/**
 *
 * @description use this hooks for better tools in functional components
 * @param {{id: number | string}} param0
 * @returns {any}
 */
export function useGetBoothInfo({ id }) {
  return useRequest({
    request: Services.Booth.GetBootheInfoByBoothId,
    requestOnMount: true,
    data: { id },
  });
}

/**
 *
 * @param {{id: number | string}} param0
 * @description get boothes list from this function for class based components
 * @returns {any}
 */
export function getBoothesByBoothId({ id }) {
  return Services.Booth.GetBoothesItemByBoothesID({
    data: {
      BoothId: id,
    },
  });
}

/**
 *
 * @description use this hooks for better tools in functional components
 * @param {{id: number | string}} param0
 * @returns {any}
 */
export function useGetBoothesByBoothId({ id }) {
  return useRequest({
    request: Services.Booth.GetBoothesItemByBoothesID,
    requestOnMount: true,
    data: { id },
  });
}
