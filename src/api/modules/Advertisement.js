import Services from '../services';

export function getAdvertisements() {
  return Services.Advertisement.GetAdvertisementForHelper();
}

export function getAdvertisementDetail({ advertisementID }) {
  return Services.Advertisement.GetAdvertisementByIDForHelper({
    params: {
      advertisementID,
    },
  });
}
