import Services from '../services';

export function getFinancialReports({ page }) {
  return Services.HelperFinancial.GetFinancialHelperReport({
    params: {
      page,
    },
  });
}

export function getWastesCollectionReport({ page }) {
  return Services.WasteCollection.GetDeliveredWasteReport({
    params: {
      page,
    },
  });
}

export function getWasteCollectionReportDetail({ deliveryWasteID }) {
  return Services.WasteCollection.GetDeliveredWasteReportItem({
    params: {
      deliveryWasteID,
    },
  });
}
