import { useRequest } from '../utils';
import Services from '../services';

/**
 * @description get charities list for help

 */
export function getCharityList() {
  return Services.Charity.GetCharity();
}

/**
 * @description get charities list for help
 */
export function useGetCode() {
  return useRequest({
    request: Services.Charity.GetCharity,
    requestOnMount: true,
  });
}
