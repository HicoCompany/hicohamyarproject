import Services from '../services';
import { useRequest } from '../utils';

export function getSurveyQuestions({ deliveryWasteId }) {
  return Services.Survey.QuestionsSurveyHelper({
    params: {
      deliveryWasteId,
    },
  });
}

export function useSurveyQuestions(deliveryWasteId) {
  return useRequest({
    request: Services.Survey.QuestionsSurveyHelper,
    initialData: [],
    params: {
      deliveryWasteId,
    },
  });
}

export function submitSurvey() {
  return Services.Survey.QuestionsSurveyHelper();
}

export function useSubmitSurvey() {
  return useRequest({
    request: Services.Survey.AddReqSurveyHelper,
    requestOnMount: false,
  });
}

export function submitSatisfaction() {
  return Services.Helper.AddSatisfactionHelper();
}

export function useSubmitSatisfaction() {
  return useRequest({
    request: Services.Helper.AddSatisfactionHelper,
    requestOnMount: false,
  });
}

export function skipSurvey() {
  return Services.Helper.SkipSurveyHelper();
}

export function useSkipSurvey() {
  return useRequest({
    request: Services.Survey.SkipSurveyHelper,
    requestOnMount: false,
  });
}
