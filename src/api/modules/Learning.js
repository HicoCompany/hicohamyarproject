import Services from '../services';
import { useRequest } from '../utils';

export function useVideoLessons() {
  return useRequest(
    {
      request: Services.HelperLearningFiles.GetLearningVideoFilesForHelper,
      transformResponse: (data) => data.Result,
    },
    {
      initialData: [],
    },
  );
}

export function useLessons(options) {
  return useRequest({
    ...options,
    request: Services.HelperLearningFiles.GetLearningFilesForHelper,
    requestOnMount: true,
    initialData: [],
  });
}

export function useToggleLearningLike({ id }, options) {
  return useRequest({
    ...options,
    request: Services.HelperLearningFiles.ChangeLikeLearningFileForHelper,
    requestOnMount: false,
    data: {
      LearningFileId: id,
    },
  });
}
export function useSetLearningToRead({ id }, options) {
  return useRequest({
    ...options,
    request: Services.HelperLearningFiles.ViewLearningFileForHelper,
    requestOnMount: false,
    data: {
      LearningFileId: id,
    },
  });
}
