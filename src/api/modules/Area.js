import { useRequest } from '../utils';
import Services from '../services';

/**
 * @description get areas list from this function for class based components
 * @returns {any}
 */
export function getMainAreas() {
  return Services.Area.GetMainAreas();
}

/**
 * @description use this hooks for better tools in functional components
 */
export function useGetMainArea({ initialData = [], ...args }) {
  return useRequest({
    ...args,
    request: Services.Area.GetMainAreas,
    initialData,
  });
}
