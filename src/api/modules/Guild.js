import Services from '../services';
import { useRequest } from '../utils';

/** @typedef {{GuildId: number,Title: string}} Guild */

/**
 * @description get guild main list
 * @returns {import('axios').AxiosPromise<Guild[]>}
 */
export async function getMainGuild() {
  return Services.Guild.getMain();
}

/**
 * @description get guild main list
 */
export function useMainGuild(args) {
  return useRequest({
    ...args,
    request: Services.Guild.getMain,
  });
}

/**
 * @description get guild sub list
 * @param {{parentId: number}} param0
 */
export async function getSubGuild({ parentId }) {
  return Services.Guild.getSub({
    params: {
      parentGuildId: parentId,
    },
  });
}

/**
 * @description get guild main list
 */
export function useSubGuild({ parentId, ...args }) {
  return useRequest({
    ...args,
    request: Services.Guild.getSub,
    params: {
      parentGuildId: parentId,
    },
    requestOnMount: false,
  });
}
