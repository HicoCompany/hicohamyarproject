import globalStore from '@src/redux/store';
import { saveToDev } from '@src/screens/dev/Requests/utils';
import { config } from '@src/utility/Constants';
import Axios from 'axios';

// eslint-disable-next-line no-unused-vars
import { makeUseAxios, UseAxios } from 'axios-hooks';
import { method } from 'lodash-es';
import { useSelector } from 'react-redux';

export const axios = Axios.create({
  baseURL: config.API_URL,
  timeout: 150000,
  headers: {
    'Cache-Control': 'no-cache',
  },
});

axios.interceptors.request.use(
  (req) => {
    const state = globalStore.getState();
    req.data = {
      Mobile: state.userData.mobileNumber,
      Code: state.userData.codeNumber,
      ...req.data,
    };

    return req;
  },
  (err) => Promise.reject(err),
);

axios.interceptors.response.use(
  (res) => {
    // config.config.
    saveToDev({
      url: res.config.url,
      request: res.config.data,
      response: res.data,
      code: res.status,
      error: false,
      method: res.config.method,
    });
    return res;
  },
  /**
   * @param {import('axios').AxiosError} error
   */
  (error) => {
    saveToDev({
      url: error.config.url,
      request: error.request,
      response: error.response,
      code: error.code,
      error: error.message,
      method: error.config.method,
    });
    throw error;
  },
);

const useAxios = makeUseAxios({
  axios,
});

export function useGetBooths() {
  const data = useSelector((store) => ({
    Mobile: store.userData.mobileNumber,
    Code: store.userData.codeNumber,
  }));
  return useAxios({
    url: 'PhoneGetAllBoothes',
    data,
    method: 'POST',
  });
}

export function useGetGlobalDetails() {
  return useAxios(
    {
      // url: 'PhoneGetDetermineDeliveryTime',
      url: 'PhoneGetGlobalDetails',
      method: 'POST',
    },
    { manual: true, useCache: false },
  );
}

export function getSurveyQuestions() {
  return axios.post('PhoneQuestionsSurveyHelper');
}

/**
 *
 * @param {{DeliveryWasteID: number,QuestionHelperList: {QuestionsSurveyHelperId:number,ExcellentPoints: number,GoodPoints: number,AveragePoints: number,WeakPoints: number}[]}} data
 * @returns
 */
export function submitSurvey(data) {
  return axios.post('PhoneAddReqSurveyHelper', data);
}

const WasteRequests = {
  collectionTime(conf) {
    return axios('PhoneGetMoreDetermineDeliveryTime', {
      ...conf,
      method: 'post',
    });
  },
  submitCollectionRequest(conf) {
    return axios('PhoneAddRequestDeliverWaste', { ...conf, method: 'post' });
  },
  submitCollectionRequestInstantaneous(conf) {
    return axios('PhoneAddInstantaneousRequestDeliverWaste', {
      ...conf,
      method: 'post',
    });
  },
  getWasteCollectionTimes(conf) {
    return axios('PhoneGetAllSettingCollectTodayRangeTime', {
      ...conf,
      method: 'post',
    });
  },
  Survey: {
    notSurveyedList: (conf) =>
      axios('PhoneGetLastDeliverWasteNotSurveyByHelper', {
        ...conf,
        method: 'post',
      }),
    /**
     *
     * @param {{Star: number,DeliveryWasteID: number}} setStartSruveyData
     * @param {import('./hook').AxiosConfig & {data: setStartSruveyData}} conf
     */
    setStar: (conf) =>
      axios('PhoneAddSatisfactionHelper', { ...conf, method: 'post' }),
    /**
     *
     * @param {{DeliveryWasteID: number}} skipSurveyDaTA
     * @param {import('./hook').AxiosConfig & {data: skipSurveyDaTA}} conf
     */
    skipSurvey: (conf) =>
      axios('PhoneChangeDeliverWastestatus', { ...conf, method: 'post' }),
    /**
     *
     * @typedef {{DeliveryWasteID: number,QuestionHelperList: {QuestionsSurveyHelperId:number,ExcellentPoints: number,GoodPoints: number,AveragePoints: number,WeakPoints: number}[]}} surveySubmitData
     * @param {import('./hook').AxiosConfig & {data: surveySubmitData}} conf
     */
    submitSurvey: (conf) =>
      axios('PhoneAddReqSurveyHelper', { ...conf, method: 'post' }),
  },
};

/**
 * @typedef {(config: import('axios').AxiosRequestConfig) => Promise<import('axios').AxiosResponse<any>>} Request
 */
export const REQUESTS = {
  GLOBAL: {
    details: (conf) =>
      axios('PhoneGetGlobalDetails', { ...conf, method: 'post' }),
    // get user profile
    profile: (conf) =>
      axios('PhoneGetInfoNaturalPerson', { ...conf, method: 'post' }),
    // get areas
    areas: (conf) => axios('PhoneGetMainAreas', { ...conf, method: 'post' }),
    //count of lessons/notices/suggestions that user not visited before
    notVisitedContent: (conf) =>
      axios('PhoneGetCountOfNotViewdItem', { ...conf, method: 'post' }),
    getCredits: (data) => axios.post('PhoneGetInfoCreditManagement', data),
  },

  //
  Learning: {
    list: (conf) =>
      axios('PhoneGetLearningFilesForHelper', { ...conf, method: 'post' }),
    videos: (conf) =>
      axios('PhoneGetLearningVideoFilesForHelper', { ...conf, method: 'post' }),
    like: (conf) =>
      axios('PhoneChangeLikeLearningFileForHelper', {
        ...conf,
        method: 'post',
      }),
    status: (conf) =>
      axios('PhoneViewLearningFileForHelper', { ...conf, method: 'post' }),
  },

  //
  Notification: {
    sendTest: (conf) =>
      axios('PhonSendFireBaseNotification', { ...conf, method: 'post' }),
    sendToAllTest: (conf) =>
      axios('PhonSendFireBaseNotificationToAll', { ...conf, method: 'post' }),
    registerToken: (conf) =>
      axios('PhoneUpdateHelperFireBaseId', { ...conf, method: 'post' }),
  },

  //
  Waste: WasteRequests,

  //
  Suggestions: {
    get: (conf) =>
      axios('PhoneGetSuggestionForHelper', { method: 'post', ...conf }),
    /**
     * @param {{ data: {SuggestionId: number} }} conf
     */
    read: (conf) =>
      axios('PhoneUpdateAnswerSuggestionStatus', { method: 'post', ...conf }),
    /**
     * @param {{ data: {SuggestionId: number} }} conf
     */

    /**
     *
     * @description
     *  Subject: title of suggestion
     *  MessageText: suggestion message
     * @param {{data: {Subject: string,MessageText: string}}} conf
     */
    submit: (conf) =>
      axios('PhoneAddSuggestionForHelper', { method: 'post', ...conf }),
  },
  //
  Support: {
    /**
     * @param {{data: {Message: string}}} conf
     */
    submit: (conf) =>
      axios('PhoneAddSupportMessageHelper', { method: 'post', ...conf }),
  },
};

// export const Api = {
//   Account: {
//     // login/register -> send authentication code to phone number
//     Login: (conf) =>
//       axios('/api/Account/RegisterNaturalPerson', { method: 'POST', ...conf }),
//     /**
//      * @description authenticate phone number with code
//      */
//     Authentication: (conf) =>
//       axios('/api/Account/LoginNaturalPerson', { method: 'POST', ...conf }),
//   },
//   App: {
//     // تنظیمات داینامیک اپ مثل آدرس ها مورد نیاز اپ
//     GetAppSetting: (conf) =>
//       axios('/App/GetAppSetting', { method: 'GET', ...conf }),
//     GetGlobalDetails: (conf) =>
//       axios('/App/GetGlobalDetails', { method: 'GET', ...conf }),
//   },
//   Advertisement: {
//     //جزئیات تبلیغات برای همیار
//     GetAdvertisementByIDForHelper: (conf) =>
//       axios('/Advertisement/GetAdvertisementByIDForHelper', {
//         method: 'GET',
//         ...conf,
//       }),
//   },
//   Area: {
//     GetMainAreas: (conf) =>
//       axios('/Area/GetMainAreas', { method: 'GET', ...conf }),
//   },
//   Bank: {
//     GetBank: (conf) => axios('/Bank/GetBank', { method: 'GET', ...conf }),
//   },
//   Booth: {
//     GetAllBooths: (conf) =>
//       axios('/Booth/GetAllBooths', { method: 'GET', ...conf }),
//     /**
//      *
//      * @param {{body: {boothId: number}}} conf
//      */
//     GetBoothItemByID: (conf) =>
//       axios('/Booth/GetBoothesItemByBoothesID', { method: 'GET', ...conf }),
//   },
//   Charity: {
//     Charities: (conf) =>
//       axios('/Charity/GetCharity', { method: 'GET', ...conf }),
//   },
//   Services: {},
// };

// .map((el) => {
//   const address = el.children[1].innerText.trim().replace('/api/');
//   const method = el.children[0].innerText.trim();

//   return [
//     address.split('/')[0],
//     {
//       address,
//       method,
//       request: `(conf) => axios('${address}', { method: '${method}', ...conf })`,
//     },
//   ];
// })
