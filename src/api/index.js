export { default as Api } from './services';
export * from './query';

// [
//   ...document.querySelectorAll(
//     'div#swagger-ui-container.swagger-ui-wrap div#resources_container.container ul#resources li ul li.endpoint ul.operations li div.heading h3',
//   ),
// ].reduce((prevObject, item) => {
//   const address = item.children[1].innerText.trim().replace('/api', '');
//   const method = item.children[0].innerText.trim();
//   const group = address.split('/')[1];
//   if (group !== 'Services') {
//     if (prevObject[group] == undefined) {
//       prevObject[group] = {};
//     }

//     prevObject[group][
//       `${address.split('/')[2]}`
//     ] = `(conf) => axios('${address}', { method: '${method}', ...conf })`;
//   }

//   return prevObject;
// }, {});
