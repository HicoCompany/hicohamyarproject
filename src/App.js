import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import analytics from '@react-native-firebase/analytics';

import { RootStack } from './screens';
import UpdateScreen from './screens/modals/UpdateScreen';
import { navigationRef } from './screens/navigation';
import Providers from './Providers';

function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}

class App extends Component {
  handleNavigationChange = async (prevState, currentState) => {
    const currentRouteName = getActiveRouteName(currentState);
    const previousRouteName = getActiveRouteName(prevState);

    if (previousRouteName !== currentRouteName) {
      await analytics().logScreenView({
        screen_name: currentRouteName,
        screen_class: currentRouteName,
      });
    }
  };

  render() {
    return (
      <Providers>
        <NavigationContainer
          ref={navigationRef}
          onNavigationStateChange={this.handleNavigationChange}>
          <RootStack />
        </NavigationContainer>
        <UpdateScreen />
      </Providers>
    );
  }
}

export default App;
