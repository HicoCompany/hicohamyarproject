// navigation
import { navigationToWithDelay } from '@src/screens/navigation';

// notif
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification, { Importance } from 'react-native-push-notification';
import messaging from '@react-native-firebase/messaging';
//utils
import Colors from '@src/utility/Colors';
import AsyncStorage from '@react-native-community/async-storage';
import { REQUESTS } from '@src/api';
import { config } from '@src/utility/Constants';
import { createRef } from 'react';
import { getGlobalAlert } from '@src/components/GlobalAlert';
import Notification from '@src/api/modules/Notification';

const channelId = 'default';

/**
 *@description when user touches notification this function will called
 * @param {Omit<import('react-native-push-notification').ReceivedNotification, "userInfo">} message
 */
async function handleForegroundNotification(message) {
  MessageActions[message.data.action]?.();
  PushNotification.localNotification({
    ...message,
    title: message.title ?? '',
    message: message.body ?? '',
    color: Colors.green,
    channelId,
    priority: 'max',
    importance: Importance.HIGH,
    onlyAlertOnce: true,
    playSound: true,
    vibrate: true,
    group: 'tafkik-hamyar',
    data: message.data,
  });
}

function handleChanelId() {
  if (AsyncStorage.getItem('chanel_id') !== channelId) {
    PushNotification.channelExists(channelId, (exists) => {
      if (!exists) {
        PushNotification.createChannel(
          {
            channelId,
            channelName: channelId,
            importance: Importance.HIGH,
            playSound: true,
            vibrate: true,
            channelDescription:
              'This chanel is used for tafkik-hamyar app main notifications',
          },
          (created) => {
            if (created) {
              AsyncStorage.setItem('chanel_id', channelId);
            }
          },
        );
      }
    });
  }
}

// this object is used when notification pressed and notification data has an action
const InteractionActions = {
  SurveyHelper: (data) => {
    navigationToWithDelay('WasteCollectionSurvey', data);
    PushNotification.getApplicationIconBadgeNumber((count) => {
      if (count > 0) {
        PushNotification.setApplicationIconBadgeNumber(count - 1);
      }
    });
  },
  NewNotice: (data) => {
    navigationToWithDelay('NoticeDetail', {
      detail: { NoticesID: data.NoticesID },
    });
    PushNotification.getApplicationIconBadgeNumber((count) => {
      if (count > 0) {
        PushNotification.setApplicationIconBadgeNumber(count - 1);
      }
    });
  },
};

// this object is used when message received and message data has an action
const MessageActions = {
  SurveyHelper: () => {
    PushNotification.getApplicationIconBadgeNumber((count) => {
      PushNotification.setApplicationIconBadgeNumber(count + 1);
    });
  },
  NewNotice: () => {
    PushNotification.getApplicationIconBadgeNumber((count) => {
      PushNotification.setApplicationIconBadgeNumber(count + 1);
    });
  },
};

/**
 *@description when user touches notification this function will called
 * @param {Omit<import('react-native-push-notification').ReceivedNotification, "userInfo">} message
 */
async function handleNotifInteraction(notif) {
  if (!notif.foreground) {
    //wait until navigation object initialized/ stacks rendered in screens/index.js
    InteractionActions[notif.data.action]?.(notif.data);
    // notif?.finish?.(PushNotificationIOS.FetchResult.NoData);
  } else {
    InteractionActions[notif.data.action]?.(notif.data);
  }
}

export const FireBasteNotifTokenRef = createRef();
// this function should triggered index.js
export function initNotif() {
  handleChanelId();
  PushNotification.configure({
    onNotification(notif) {
      if (notif.foreground && !notif.userInteraction) {
        handleForegroundNotification(notif);
        // InteractionActions[notif.data.ActionName]?.(notif.data, notif.id);
      } else if (notif.userInteraction) {
        handleNotifInteraction(notif);
      } else {
        MessageActions[notif.data.action]?.();
      }
      notif.finish?.(PushNotificationIOS.FetchResult.NoData);
    },
    onRegister: async (reg) => {
      try {
        AsyncStorage.setItem('FirebaseId', reg.token);
        sendFireBaseIdToServer(reg.token);
        await messaging().subscribeToTopic(`${config.FLAVOR}-all`);
      } catch (e) {}
    },
    popInitialNotification: true,
    permissions: { alert: true, sound: true, badge: 1 },
    requestPermissions: true,
  });
}

export function testAllNotification() {
  REQUESTS.Notification.sendToAllTest().then(({ data }) => {
    getGlobalAlert().addToast({
      message: 'done',
      timeout: 'short',
    });
  });
}

export async function testNotification() {
  try {
    const FirebaseId = await AsyncStorage.getItem('FirebaseId');
    REQUESTS.Notification.sendTest({
      data: { FirebaseId },
    }).then(({ data }) => {
      getGlobalAlert().addToast({
        message: 'done',
        timeout: 'short',
      });
    });
  } catch (e) {}
}

export async function testSurveyNotification() {
  try {
    const FirebaseId = await AsyncStorage.getItem('FirebaseId');
    Notification.SendTestNonfiction({
      data: { FirebaseId, DeliveryWasteID: 260 },
    }).then(({ data }) => {
      getGlobalAlert().addToast({
        message: 'done',
        timeout: 'short',
      });
    });
  } catch (e) {}
}

export async function sendFireBaseIdToServer(FirebaseId) {
  await Notification.UpdateHelperFireBaseId({
    data: {
      FireBaseId: FirebaseId ?? (await AsyncStorage.getItem('FirebaseId')),
    },
  });
}
