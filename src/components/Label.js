import React, { Component, useMemo } from 'react';
// eslint-disable-next-line no-unused-vars
import { Platform, Text, TextPropTypes } from 'react-native';
import Colors from '@src/utility/Colors';
import { ScaledSheet, ms } from 'react-native-size-matters';
import Constants from '@src/utility/Constants';

/**
 *
 * @typedef {{weight: 'normal' | 'bold' | 'light',color: 'placeholder' | 'text',fontSize: number} & TextPropTypes} LabelProps
 * @param {LabelProps} param0
 */
export default function Label({
  style,
  weight = 'normal',
  color = 'text',
  fontSize,
  ...props
}) {
  const s = useMemo(
    () => ({
      fontSize: ms(fontSize ?? 13),
      ...styles[`${weight}${color}Label`],
    }),
    [color, fontSize, weight],
  );

  return <Text {...props} style={[s, style]} />;
}

// for use with animated

export class ClassLabel extends Component {
  style = {
    fontSize: ms(this.props.fontSize ?? 13),
    ...styles[`${this.props.weight}${this.props.color}Label`],
  };

  render() {
    const s = {
      fontSize: ms(this.props.fontSize ?? 13),
      ...styles[`${this.props.weight}${this.props.color}Label`],
    };
    return <Text {...this.props} style={[s, this.props.style]} />;
  }
}

const defaults = {
  normalLabel: {
    color: Colors.text,
    ...Platform.select({
      ios: {
        fontFamily: Constants.Fonts.IOS_IR,
        fontWeight: '500',
      },
      android: {
        fontFamily: Constants.Fonts.IR_M,
      },
    }),
  },
  boldLabel: Platform.select({
    ios: {
      fontFamily: Constants.Fonts.IOS_IR,
      fontWeight: 'bold',
    },
    android: {
      fontFamily: Constants.Fonts.IR_B,
    },
  }),

  lightLabel: Platform.select({
    ios: {
      fontFamily: Constants.Fonts.IOS_IR,
      fontWeight: '100',
    },
    android: {
      fontFamily: Constants.Fonts.IR_L,
    },
  }),
  placeholder: {
    color: Colors.gray,
  },
  text: {
    color: Colors.textBlack,
  },
};

const styles = ScaledSheet.create({
  normaltextLabel: {
    ...defaults.normalLabel,
    ...defaults.text,
  },
  boldtextLabel: {
    ...defaults.boldLabel,
    ...defaults.text,
  },
  lighttextLabel: {
    ...defaults.lightLabel,
    ...defaults.text,
  },
  normalplaceholderLabel: {
    ...defaults.normalLabel,
    ...defaults.placeholder,
  },
  boldplaceholderLabel: {
    ...defaults.boldLabel,
    ...defaults.placeholder,
  },
  lightplaceholderLabel: {
    ...defaults.lightLabel,
    ...defaults.placeholder,
  },
});
