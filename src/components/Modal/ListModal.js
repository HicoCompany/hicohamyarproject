import React, { useEffect, useRef } from 'react';
import { StyleSheet } from 'react-native';
import {
  FlatList,
  gestureHandlerRootHOC,
  PanGestureHandler,
} from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated';

import { dimensions } from '@src/utility/Constants';
import Colors from '@src/utility/Colors';
import { Divider } from 'react-native-paper';

// const minHeight = dimensions.window.height / 4;
const maxHeight = dimensions.window.height - 80;

function ListModal({
  children,
  onDismiss,
  defaultHeight = maxHeight,
  data,
  renderItems,
  renderHeader,
  emptyComponent,
  visible,
  keyExtractor,
}) {
  const panRef = useRef();
  const scrollRef = useRef();
  const initialHeight = useSharedValue(defaultHeight);
  const translateY = useSharedValue(0);
  const height = useSharedValue(defaultHeight);

  useEffect(() => {
    if (visible) {
      handleOpenModal();
    } else {
      handleDismissModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visible]);

  const handleOpenModal = (endCall, velocity) => {
    'worklet';
    translateY.value = withSpring(
      0,
      { overshootClamping: true, velocity },
      endCall,
    );
  };
  function handleDismissModal(endCall, velocity) {
    'worklet';
    translateY.value = withSpring(
      height.value,
      {
        velocity,
        overshootClamping: true,
      },
      (finished) => {
        if (finished) {
          runOnJS(onDismiss)();
        }
      },
    );
  }

  const eventHandler = useAnimatedGestureHandler({
    onStart: (event, ctx) => {
      ctx.translateY = translateY.value;
      ctx.height = height.value;
      const yFromBottom = dimensions.window.height - event.y;
      ctx.isCardTouch = yFromBottom < height.value;
      ctx.isLinePressed = yFromBottom > height.value - 80 && ctx.isCardTouch;
      // calcHeight(event.translationY, ctx.height);
    },
    onActive: (event, ctx) => {
      if (ctx.isCardTouch && event.translationY > 0) {
        translateY.value = event.translationY + ctx.translateY;
      }
    },
    onEnd: (event, ctx) => {
      const _velocity = event.velocityY;
      const _translationY = event.translationY;
      if (_velocity > 10 && _translationY > 30) {
        handleDismissModal(onDismiss, _velocity);
      } else if (_translationY > initialHeight.value / 3) {
        handleDismissModal(onDismiss, _velocity);
      } else {
        translateY.value = withTiming(0);
      }
    },
  });

  const rStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: translateY.value,
        },
      ],
    };
  }, []);

  const rContainerStyle = useAnimatedStyle(() => ({ height: height.value }), [
    height.value,
  ]);

  return (
    <PanGestureHandler
      // simultaneousHandlers={scrollRef}
      ref={panRef}
      onGestureEvent={eventHandler}>
      <Animated.View style={styles.outerContainer}>
        <Animated.View style={[styles.innerContainer, rStyle]}>
          <Animated.View style={[styles.contentContainer, rContainerStyle]}>
            <Divider style={styles.line} />
            <FlatList
              data={data}
              extraData={data}
              renderItem={renderItems}
              keyExtractor={keyExtractor}
              contentContainerStyle={styles.scrollContent}
              showsVerticalScrollIndicator
              bounces={false}
              ListHeaderComponent={renderHeader}
              ListEmptyComponent={emptyComponent}
              overScrollMode="never"
              ref={scrollRef}
            />
            {children}
          </Animated.View>
        </Animated.View>
      </Animated.View>
    </PanGestureHandler>
  );
}

const styles = StyleSheet.create({
  outerContainer: {
    overflow: 'hidden',
    backgroundColor: Colors.backdropColor,
  },
  innerContainer: {
    minHeight: '100%',
    overflow: 'hidden',
  },
  contentContainer: {
    width: '100%',
    maxWidth: 600,
    flex: 1,
    position: 'absolute',
    bottom: 0,
    maxHeight,
    backgroundColor: Colors.backgroundColor,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    overflow: 'hidden',
    paddingTop: 16,
  },
  scrollContent: {
    minHeight: '100%',
    paddingHorizontal: 16,
    paddingBottom: 60,
  },
  line: {
    width: '20%',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 15,
    marginTop: -5,
    height: 2,
    backgroundColor: Colors.gray,
  },
  square: {
    width: 150,
    height: 150,
    backgroundColor: '#28b5b5',
    marginTop: 22,
  },
});
export default gestureHandlerRootHOC(ListModal);
