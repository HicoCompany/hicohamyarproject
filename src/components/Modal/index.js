import React, { Fragment } from 'react';
import {
  StatusBar,
  StyleSheet,
  Modal as ModalComponent,
  View,
  TouchableOpacity,
} from 'react-native';

import Colors from '@src/utility/Colors';
import { dimensions } from '@src/utility/Constants';

import { Label } from '@components';

export default function Modal({
  children,
  onDismiss,
  visible = false,
  hasBackdrop = true,
  swipeDown = false,
}) {
  return (
    // <ModalComponent
    //   style={[
    //     styles.modal,
    // hasBackdrop ? { backgroundColor: Colors.backdropColor } : undefined,
    //   ]}
    //   // swipeThreshold={dimensions.window.height / 2}
    //   swipeDirection={swipeDown ? 'down' : undefined}
    //   onSwipeComplete={swipeDown ? onDismiss : undefined}
    //   isVisible={visible}
    //   backdropOpacity={0.1}
    //   statusBarTranslucent
    //   onBackdropPress={onDismiss}
    //   onBackButtonPress={onDismiss}
    //   avoidKeyboard
    //   animationIn="fadeIn"
    //   animationOut="fadeIn"
    //   hasBackdrop={false}
    //   deviceWidth={dimensions.screen.width}
    //   deviceHeight={dimensions.screen.height}
    //   onDismiss={onDismiss}>
    <ModalComponent
      visible={visible}
      onDismiss={onDismiss}
      onRequestClose={onDismiss}
      transparent
      style={styles.modal}
      statusBarTranslucent
      animationType="fade">
      {/* <TouchableOpacity style={styles.touchable} onPress={onDismiss} /> */}
      {children}
    </ModalComponent>
    // </ModalComponent>
  );
}

const styles = StyleSheet.create({
  modal: {
    padding: 0,
    margin: 0,
    height: '100%',
    width: '100%',
  },
  content: {},
  touchable: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.backdropColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
