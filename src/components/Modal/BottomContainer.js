import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  gestureHandlerRootHOC,
  PanGestureHandler,
} from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated';

import { dimensions } from '@src/utility/Constants';
import Colors from '@src/utility/Colors';

const minHeight = dimensions.window.height / 4;
const maxHeight = dimensions.window.height - 80;
/**
 * @param {React.FC<{onDismiss:() => void;}>} param0
 */
function BottomContainer({ children, onDismiss, defaultHeight = 300 }) {
  // const initialHeight = useSharedValue(propHeight);
  const translateY = useSharedValue(0);
  const height = useSharedValue(defaultHeight);

  const handleDismiss = () => {
    onDismiss?.();
  };
  const eventHandler = useAnimatedGestureHandler({
    onStart: (event, ctx) => {
      ctx.translateY = translateY.value;
      ctx.height = height.value;
      const yFromBottom = dimensions.window.height - event.y;
      ctx.isLinePressed =
        yFromBottom > height.value - 80 && yFromBottom < height.value;
    },
    onActive: (event, ctx) => {
      if (ctx.isLinePressed) {
        if (event.translationY > 0) {
          translateY.value = event.translationY + ctx.translateY;
        }
      }
    },
    onEnd: (event, ctx) => {
      if (
        (ctx.isLinePressed && event.translationY > height.value / 2) ||
        (event.velocityY > 10 && event.translationY > 50)
      ) {
        translateY.value = withSpring(
          height.value,
          { velocity: event.velocityY, overshootClamping: true },
          () => {
            runOnJS(handleDismiss)();
          },
        );
      } else {
        translateY.value = withTiming(0);
      }
    },
  });

  const rStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: translateY.value,
        },
      ],
    };
  }, []);

  const rContainerStyle = useAnimatedStyle(() => ({ height: height.value }), [
    height.value,
  ]);

  const handleLayout = ({ nativeEvent }) => {
    height.value = nativeEvent.layout.height;
  };

  return (
    <PanGestureHandler onGestureEvent={eventHandler}>
      <Animated.View style={styles.outerContainer}>
        <Animated.View style={[styles.innerContainer, rStyle]}>
          <Animated.View
            style={[styles.contentContainer, rContainerStyle]}
            onLayout={handleLayout}>
            <View style={styles.line} />
            {children}
          </Animated.View>
        </Animated.View>
      </Animated.View>
    </PanGestureHandler>
  );
}

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    backgroundColor: Colors.backdropColor,
  },
  innerContainer: {
    flex: 1,
  },
  contentContainer: {
    width: '100%',
    maxWidth: 600,
    flex: 1,
    position: 'absolute',
    bottom: 0,
    minHeight,
    maxHeight,
    backgroundColor: Colors.backgroundColor,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    padding: 16,
  },
  line: {
    width: '20%',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 15,
    marginTop: -5,
    height: 2,
    backgroundColor: Colors.gray,
  },
  square: {
    width: 150,
    height: 150,
    backgroundColor: '#28b5b5',
    marginTop: 22,
  },
});
export default gestureHandlerRootHOC(BottomContainer);
