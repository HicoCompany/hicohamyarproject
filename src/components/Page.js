import React from 'react';
import { View, StyleSheet } from 'react-native';
import BaseExamplePropTypes from './BaseExamplePropTypes';

class Page extends React.Component {
  static propTypes = {
    ...BaseExamplePropTypes,
  };
  render() {
    return (
      <View style={[styles.matchParent]}>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  matchParent: {
    flex: 1,
  }
});

export default Page;

