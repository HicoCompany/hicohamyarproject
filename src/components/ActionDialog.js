import React from 'react';
import { Dialog, Button, Divider } from 'react-native-paper';
import Label from '@components/Label';
import Colors from '@src/utility/Colors';
import { StyleSheet } from 'react-native';

/**
 *
 * @typedef {keyof Colors} DefaultColors
 * @param {{submitColor: DefaultColors,cancelButton: DefaultColors,onDismiss(): void,onSubmit():void,visible: boolean,message: string,loading: boolean}} param0
 */
export default function ActionDialog({
  onDismiss,
  onSubmit,
  visible,
  message,
  loading,
  submitColor = 'blue',
  cancelButton = 'textBlack',
}) {
  return (
    <Dialog visible={visible} onDismiss={onDismiss}>
      <Dialog.Content>
        <Label weight="normal">{message}</Label>
      </Dialog.Content>
      <Divider />
      <Dialog.Actions>
        <Button
          mode="contained"
          style={styles.button}
          contentStyle={styles.contentStyle}
          onPress={onSubmit}
          color={Colors[submitColor]}
          loading={loading}>
          بله
        </Button>
        <Button
          style={styles.button}
          contentStyle={styles.contentStyle}
          onPress={onDismiss}
          color={Colors[cancelButton]}>
          خیر
        </Button>
      </Dialog.Actions>
    </Dialog>
  );
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    elevation: 0,
  },
  contentStyle: {
    fontSize: 14,
  },
});
