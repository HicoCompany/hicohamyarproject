import React, { useContext } from 'react';
import { StyleSheet, View } from 'react-native';
import { TouchableRipple } from 'react-native-paper';

import Colors from '@src/utility/Colors';
import Label from './Label';

const RadioContext = React.createContext(null);

function RadioButtonGroup({ value, onChange, children, style }) {
  const handleChange = (val) => () => {
    onChange(val);
  };

  return (
    <RadioContext.Provider value={{ value, onChange: handleChange }}>
      <View style={[styles.group, style]}>{children}</View>
    </RadioContext.Provider>
  );
}

/**
 *
 * @typedef {import('react-native').StyleProp<import('react-native').ViewProps>} Style
 * @typedef {{checkedColor: string,uncheckedColor: string,labelCheckedColor: string,labelUncheckedColor: string}} Colors
 * @param {{value: string,label:string,style: Style, side: 'right' | 'left' | undefined,labelStyle: Style} & Colors} param0
 */
function RadioButton({
  value,
  label,
  style,
  checkedColor = Colors.green,
  uncheckedColor = '#ffffff',
  labelCheckedColor = '#ffffff',
  labelUncheckedColor = '#000000',
  side,
  labelStyle,
}) {
  const radioContext = useContext(RadioContext);
  const checked = radioContext?.value === value;
  return (
    <TouchableRipple
      onPress={radioContext.onChange(value)}
      style={[
        styles.radio,
        styles[side],
        { backgroundColor: checked ? checkedColor : uncheckedColor },
        style,
      ]}>
      <Label
        style={[
          styles.label,
          labelStyle,
          {
            color: checked ? labelCheckedColor : labelUncheckedColor,
          },
        ]}>
        {label}
      </Label>
    </TouchableRipple>
  );
}
const config = {
  height: 50,
  borderWidth: 1,
};

const styles = StyleSheet.create({
  group: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '90%',
    margin: 5,
  },
  radio: {
    height: config.height,
    justifyContent: 'center',
    borderWidth: config.borderWidth,
    borderColor: '#e2e2e2',
    overflow: 'hidden',
    elevation: 0,
    minWidth: 100,
  },
  right: {
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
  },
  left: {
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    borderLeftWidth: config.borderWidth,
  },
  label: {
    fontSize: 10,
    borderRadius: 0,
    textAlign: 'center',
    padding: 4,
  },
  content: {
    height: config.height,
  },
});

RadioButton.Group = RadioButtonGroup;

export default RadioButton;

/* </
      style={}
      color={checked ? checkedColor : uncheckedColor}
      mode="contained"
      labelStyle={}
      disabled={disabled}
      theme={{ roundness: 0 }}
      contentStyle={styles.content}
      >
      <Label>{label}</Label>
    </Touchable> */
