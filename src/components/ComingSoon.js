import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Avatar } from 'react-native-paper';
import { scale } from 'react-native-size-matters';
import Label from './Label';

export default function ComingSoon() {
  return (
    <View style={styles.cover}>
      <Avatar.Icon icon={'alert'} color="white" size={scale(70)} />
      <Label color="placeholder" fontSize={25} weight="bold">
        به زودی
      </Label>
    </View>
  );
}

const styles = StyleSheet.create({
  cover: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,0.9)',
  },
});
