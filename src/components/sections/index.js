import React from 'react';
import { ActivityIndicator, Image, StyleSheet, View } from 'react-native';

//ui components
import { Card, Badge } from 'react-native-paper';
import Label from '../Label';

//utils
import { normalize, scale, wp } from '@src/responsive';
import Colors from '@src/utility/Colors';

/**
 *
 * @param {{source: import('react-native').ImageSourcePropType,onPress(): void,label: string,disabled: boolean,badge: number, loading: boolean}} param0
 */
export default function Section({
  source,
  onPress,
  label,
  loading,
  badge,
  disabled,
}) {
  return (
    <View style={styles.container}>
      <Card
        style={[styles.card, disabled ? styles.disabled : undefined]}
        onPress={onPress}>
        <Image source={source} style={styles.image} resizeMode="contain" />
        <Card.Content>
          <Label style={styles.label}>{label}</Label>
        </Card.Content>
        {loading || badge ? (
          <View style={styles.badgeContainer}>
            <ActivityIndicator
              style={styles.loading}
              size={20}
              color="white"
              animating={loading}
            />
            {!loading && (
              <Badge size={25} style={styles.badge}>
                {badge}
              </Badge>
            )}
          </View>
        ) : null}
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    width: wp('35'),
    height: wp('35'),
    marginBottom: wp('6'),
    borderRadius: 6,
    borderWidth: StyleSheet.hairlineWidth,
    elevation: 0,
  },
  image: {
    width: scale(50),
    height: scale(50),
    flex: 1,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  label: {
    textAlign: 'center',
    fontSize: normalize(12),
  },
  badge: {
    position: 'absolute',
    backgroundColor: Colors.green,
    color: 'white',
  },
  loading: {
    position: 'absolute',
  },
  badgeContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    height: 25,
    width: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 0,
    borderTopLeftRadius: 6,
    borderBottomRightRadius: 6,
    overflow: 'hidden',
    backgroundColor: Colors.green,
  },
  disabled: {
    opacity: 0.5,
    elevation: 0,
    borderWidth: 0.5,
  },
});
