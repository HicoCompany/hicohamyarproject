import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { IconButton, Surface, TouchableRipple } from 'react-native-paper';
import Label from './Label';

//assets
import PICTURE_ICON from '@assets/image/picture.png';
import Colors from '@src/utility/Colors';
import { ScaledSheet } from 'react-native-size-matters';
import { dimensions } from '@src/utility/Constants';

export default function SelectCounter({
  name,
  value,
  selected,
  imageSrc = '',
  onSelectionChange,
  onIncrease,
  onDecrease,
}) {
  return (
    <TouchableRipple onPress={onSelectionChange}>
      <Surface style={styles.item}>
        {selected ? null : (
          <Image
            style={styles.itemImage}
            source={imageSrc}
            resizeMode="contain"
            defaultSource={PICTURE_ICON}
          />
        )}

        <Label weight="bold" style={styles.name}>
          {name}
        </Label>
        {selected ? (
          <Surface style={styles.counterContainer} pointerEvents="box-none">
            <IconButton icon={'plus'} size={15} onPress={onIncrease} />
            <Label style={styles.counter}>{value}</Label>
            <IconButton
              icon={'minus'}
              size={15}
              onPress={value == 0 ? () => {} : onDecrease}
            />
          </Surface>
        ) : null}
        <Label
          weight="bold"
          fontSize={10}
          style={[
            styles.itemSelectLabel,
            selected ? { color: Colors.error } : undefined,
          ]}>
          {selected ? 'حذف' : 'انتخاب'}
        </Label>
      </Surface>
    </TouchableRipple>
  );
}

const styles = ScaledSheet.create({
  //item
  item: {
    width: dimensions.window.width / 3 + StyleSheet.hairlineWidth,
    aspectRatio: 1,
    margin: StyleSheet.hairlineWidth,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: Colors.gray,
    paddingVertical: '12@s',
  },
  name: {
    textAlign: 'center',
  },
  itemImage: {
    width: '50@s',
    height: '50@s',
    overflow: 'hidden',
  },
  itemSelectLabel: {
    color: Colors.purple,
  },

  counterContainer: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    backgroundColor: '#efefef',
    borderRadius: 6,
    width: '90%',
    alignItems: 'center',
    maxHeight: '35@s',
    flexGrow: 1,
    // borderWidth: StyleSheet.hairlineWidth,
  },
  counter: {
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderLeftWidth: StyleSheet.hairlineWidth,
    borderRightWidth: StyleSheet.hairlineWidth,

    height: '100%',
    margin: 0,
    padding: 0,
  },
});
