import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Card, Divider } from 'react-native-paper';
import Colors from '@src/utility/Colors';
import Label from '../Label';
import Icon from 'react-native-vector-icons/Feather';

const statuses = [
  {
    id: 1,
    backgroundHandler: Colors.blue,
    color: Colors.white,
  },
];

export default function RequestItem({
  status,
  deliverDate,
  statusID,
  onDelete,
  onEdit,
  onDetailClick,
  onSurveyPress,
  date,
  code,
  createdDate,
  collectedDate,
  editable,
  cancelable,
  commentable,
  ...props
}) {
  const isDone = statusID === 2;
  return (
    <Card style={styles.container} {...props}>
      <Card.Content style={styles.content}>
        <View style={styles.row}>
          <View style={styles.grow}>
            <Label style={styles.title} color="placeholder">
              تاریخ درخواست:
            </Label>
            <Label style={styles.value}>{createdDate}</Label>
          </View>
          {isDone && (
            <View style={styles.grow}>
              <Label style={styles.title} color="placeholder">
                تاریخ جمع آوری:
              </Label>
              <Label style={styles.value}>{collectedDate}</Label>
            </View>
          )}
        </View>
        <Label style={styles.title} color="placeholder">
          تاریخ مراجعه سفیر:
        </Label>
        <Label style={styles.value}>{deliverDate}</Label>
        <View style={styles.rowWithGap}>
          <View style={styles.row}>
            <Label style={styles.title} color="placeholder">
              وضعیت درخواست:
            </Label>
            <Label
              style={[
                styles.value,
                { backgroundColor: statuses?.[statusID]?.color },
              ]}>
              {status}
            </Label>
          </View>
          <View style={[styles.row, styles.codeContainer]}>
            <Label style={styles.title} color="placeholder">
              کد تحویل :
            </Label>
            <Label style={styles.value}>{code}</Label>
          </View>
        </View>
        <Divider />
      </Card.Content>
      <Card.Actions style={styles.actions}>
        {cancelable ? (
          <Button
            icon={'close'}
            color={Colors.error}
            onPress={onDelete}
            style={styles.button}>
            لغو
          </Button>
        ) : null}
        {editable ? (
          <Button
            icon="pencil"
            color={Colors.blue}
            onPress={onEdit}
            style={styles.button}>
            ویرایش
          </Button>
        ) : null}
        {commentable ? (
          <Button
            icon={'comment'}
            color={Colors.green}
            onPress={onSurveyPress}
            style={styles.button}>
            نظر سنجی
          </Button>
        ) : null}

        <Button
          icon={() => <Icon name="more-horizontal" />}
          color={Colors.textBlack}
          style={styles.button}
          onPress={onDetailClick}>
          جزئیات
        </Button>
      </Card.Actions>
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
  },
  title: {
    fontSize: 15,
    marginBottom: 5,
  },
  value: {
    marginBottom: 5,
    marginRight: 12,
    textAlign: 'right',
  },
  content: {},
  row: {
    flexDirection: 'row-reverse',
    marginBottom: 10,
  },
  rowWithGap: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: 10,
    flexWrap: 'wrap',
  },
  actions: { height: 50, justifyContent: 'space-around' },
  codeContainer: {
    minWidth: 130,
    borderRadius: 6,
  },
  grow: {
    flex: 1,
  },
  button: {
    flex: 1,
  },
});
