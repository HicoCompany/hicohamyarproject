import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Card } from 'react-native-paper';
import Label from '../Label';

const { width } = Dimensions.get('screen');
export default function RequestHeader({ requestCount, deliveryCount, points }) {
  return (
    <View style={styles.container}>
      <Card style={styles.card}>
        <Label
          color="placeholder"
          style={styles.title}
          textBreakStrategy="balanced">
          تعداد درخواست ها
        </Label>
        <Label weight="bold" style={styles.value}>
          {requestCount}
        </Label>
      </Card>
      <Card style={styles.card}>
        <Label
          color="placeholder"
          style={styles.title}
          textBreakStrategy="balanced">
          درخواست های انجام شده
        </Label>
        <Label weight="bold" style={styles.value}>
          {deliveryCount}
        </Label>
      </Card>
      <Card style={styles.card}>
        <Label
          color="placeholder"
          style={styles.title}
          textBreakStrategy="balanced">
          امتیاز شما
        </Label>
        <Label weight="bold" style={styles.value}>
          {points}
        </Label>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
  },
  card: {
    flexDirection: 'row-reverse',
    width: width / 3.5,
    borderRadius: width / 3.5,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    overflow: 'hidden',
  },
  row: {
    flexDirection: 'row-reverse',
    // justifyContent: 'space-around',
  },
  section: {
    // flex: 1,
    flexDirection: 'row-reverse',
    // justifyContent: 'space-around',
  },
  title: {
    // marginLeft: 10,
    // minWidth: 150,
    fontSize: 13,
    textAlign: 'center',
    maxWidth: '100%',
  },
  value: {
    maxWidth: '100%',
    textAlign: 'center',
  },
});
