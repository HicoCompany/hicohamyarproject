import React from 'react';
import { BackHandler, View } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { hp, normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Label from './Label';
import { IconButton } from 'react-native-paper';

const PinIcon = () => <Icon name={'map-pin'} size={20} color={'#fff'} />;
const ExitIcon = () => <Icon name={'log-out'} size={20} color={'#fff'} />;
const HeaderMain = ({
  headerTitle,
  onMenuPress = BackHandler.exitApp,
  onPinPress,
}) => {
  return (
    <View style={styles.header}>
      <IconButton
        style={styles.icon}
        onPress={onPinPress}
        icon={PinIcon}
        delayPressIn={0}
        delayPressOut={0}
      />
      <Label style={styles.title}>{headerTitle}</Label>
      <IconButton icon={ExitIcon} onPress={onMenuPress} style={styles.icon} />
    </View>
  );
};

const styles = EStyleSheet.create({
  header: {
    height: hp('10%'),
    backgroundColor: Colors.green,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  icon: {},
  title: {
    color: '#ffffff',
    fontSize: normalize(16),
    textAlign: 'center',
    flex: 1,
  },
});

export default connect(
  (state) => ({
    userData: state.userData,
  }),
  null,
)(HeaderMain);
