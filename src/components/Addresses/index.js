import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Button, Card, FAB, TouchableRipple } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import Label from '../Label';
import Modal from '../Modal';

import {
  removeAddress,
  selectAddress,
  setAddressList,
} from '@src/redux/slices/AddressesSlice';
import Field from '../Field';
import { dimensions } from '@src/utility/Constants';
import Colors from '@src/utility/Colors';
import ListModal from '../Modal/ListModal';
import { useNavigation } from '@react-navigation/native';
import { ScaledSheet } from 'react-native-size-matters';

//assets
import NoData from '@assets/image/no_data.png';
import { DefaultModalActions, useAlert } from '../GlobalAlert';
import { useDeleteAdress, useGetAllAddresses } from '@src/api/modules/Address';
import { Loading } from '..';

const addressBuilder = ({ address, area }) => {
  return `${address} منطقه:‌ ${area}`;
};

/**
 *
 * @param {import('@react-navigation/stack').StackScreenProps<{}>} param0
 * @returns
 */
function AddressInput({ onChange }) {
  const [visibility, setVisibility] = useState(false);
  const [animationVisibility, setAnimationVisibility] = useState(false);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const Alert = useAlert();

  /**
   * @type {{selected: Address,list: Address[]}}
   */
  const { list, selected } = useSelector((state) => state.addresses);
  const [addressState] = useGetAllAddresses({ initialData: [] });

  // to show loading on delete button related to address
  const [deletingItem, setDeletingItem] = useState();
  const [deleteState, deleteHandler] = useDeleteAdress();

  useEffect(() => {
    dispatch(setAddressList(addressState.data));

    return () => {
      dispatch(selectAddress(null));
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addressState.data]);

  const handleNavigateToAddNewAddress = useCallback(() => {
    // setWaitingForNavigationBack(false);
    setAnimationVisibility(false);
    setVisibility(false);
    navigation.navigate('AddressForm');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleDismiss = useCallback(() => {
    setAnimationVisibility(false);
    setTimeout(() => {
      setVisibility(false);
    }, 250);
  }, []);

  const handleOpen = useCallback(() => {
    setVisibility(true);
    setTimeout(() => {
      setAnimationVisibility(true);
    }, 100);
  }, []);

  const handleAddressSelection = useCallback(
    (item) => () => {
      dispatch(selectAddress(item.id));
      onChange?.(item);
      handleDismiss();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const handleDeleteAddress = useCallback(
    (id) => {
      setDeletingItem(id);
      deleteHandler
        .handleFetch({
          params: {
            helperAddressId: id,
          },
        })
        .then(() => {
          dispatch(removeAddress(id));
          setDeletingItem(undefined);
          Alert.addToast({
            message: 'آدرس با موفقیت حذف شد',
            type: 'success',
            timeout: 'short',
          });
        })
        .catch((error) => {
          setDeletingItem(undefined);
          Alert.setAction(
            DefaultModalActions.RetryOrBack({
              message: error.message,
              onRefresh: () => handleDeleteAddress(id),
            }),
          );
        });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const handleDeletePress = useCallback(
    (id) => () => {
      Alert.setAction(
        DefaultModalActions.Ask({
          message: 'آیا از حذف این آدرس مطمئنید؟',
          onConfirm: () => {
            handleDeleteAddress(id);
          },
          mode: 'error',
        }),
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const _renderAddress = useCallback(
    ({ item }) => {
      if (item.isDeleted) {
        return null;
      }
      return (
        <Card style={styles.items}>
          <Card.Content style={styles.itemsContent}>
            <Field
              title="آدرس"
              value={item.street}
              justifyContent="flex-start"
            />
            <Field
              title="منطقه"
              value={item.area.id}
              justifyContent="flex-start"
            />
            {/* <Field
              title="کد پستی"
              value={item.postalCode}
              justifyContent="flex-start"
            /> */}
            <Field
              title="پلاک"
              value={item.homeNo}
              justifyContent="flex-start"
            />
          </Card.Content>
          <Card.Actions style={styles.actions}>
            <Button
              mode="outlined"
              color={Colors.text}
              style={styles.action}
              loading={deleteState.loading && deletingItem == item.id}
              onPress={handleDeletePress(item.id)}>
              حذف
            </Button>
            <Button
              onPress={handleAddressSelection(item)}
              mode="contained"
              style={styles.action}
              labelStyle={styles.actionLabel}
              color={Colors.green}>
              انتخاب
            </Button>
          </Card.Actions>
        </Card>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [deleteState.loading, deletingItem],
  );

  // const _renderHeader = useMemo(
  //   () => list.length && <Label>یکی از آدرس های زیر را انتخاب کنید</Label>,
  //   [list.length],
  // );
  const _renderEmptyComponent = useMemo(
    () => (
      <View style={styles.emptyContainer}>
        <Image source={NoData} style={styles.noDataImage} />
        <Label color="placeholder" weight="bold">
          آدرسی پیدا نشد یک آدرس ثبت کنید.
        </Label>
      </View>
    ),
    [],
  );
  return (
    <Fragment>
      <TouchableRipple onPress={handleOpen} style={styles.container}>
        <Label style={styles.text}>
          {selected
            ? addressBuilder({
                address: selected.street,
                area: selected.area.id,
              })
            : 'آدرس خود را وارد کنید.'}
        </Label>
      </TouchableRipple>
      <Modal visible={visibility} onDismiss={handleDismiss}>
        <ListModal
          visible={animationVisibility}
          data={list}
          renderItems={_renderAddress}
          onDismiss={handleDismiss}
          keyExtractor={(item) => `address-${item.id}`}
          emptyComponent={_renderEmptyComponent}
          defaultHeight={dimensions.screen.height}>
          {addressState.loading ? (
            <View style={StyleSheet.absoluteFill}>
              <Loading />
            </View>
          ) : null}
          <FAB
            label="ثبت آدرس جدید"
            style={styles.fab}
            color={Colors.whiteText}
            onPress={handleNavigateToAddNewAddress}
          />
        </ListModal>
      </Modal>
    </Fragment>
  );
}

const styles = ScaledSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '100%',
    elevation: 1,
    minHeight: '50@s',
    justifyContent: 'center',
    paddingHorizontal: '12@s',
    borderRadius: '6@s',
  },
  text: {
    textAlign: 'right',
    textAlignVertical: 'center',
  },
  fab: {
    backgroundColor: Colors.green,
    width: '50%',
    position: 'absolute',
    bottom: '10@s',
    right: '10@s',
  },
  items: {
    marginTop: '16@s',
    overflow: 'hidden',
  },
  itemsContent: {
    flexDirection: 'row-reverse',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  actions: {
    justifyContent: 'center',
  },
  action: {
    margin: '8@s',
    height: '30@s',
    justifyContent: 'center',
    overflow: 'hidden',
    width: '40%',
  },
  actionLabel: {
    color: Colors.white,
  },
  emptyContainer: {
    minHeight: dimensions.window.height / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noDataImage: {
    // maxWidth: '200@s',
    height: '200@s',
    aspectRatio: 1,
    resizeMode: 'contain',
    // backgroundColor: 'grey',
    overflow: 'hidden',
  },
});

export default AddressInput;
