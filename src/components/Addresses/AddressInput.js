import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';

//components
import { TouchableRipple } from 'react-native-paper';
import Label from '../Label';
import { ScaledSheet } from 'react-native-size-matters';

const addressBuilder = ({ address, area }) => {
  return `${address} منطقه:‌ ${area}`;
};

/**
 * @typedef {{ area: {id: number,name: string},homeNo: number,position: { longitude: number,latitude: number },street: string,postalCode: number }} Address
 * @param {{value: Address, onChange : (address: Address) => void}} param0
 */
function AddressInput({ value = {}, onChange }) {
  const navigation = useNavigation();

  const handleAddAdress = useCallback(() => {
    navigation.navigate('AddressForm', {
      callback: onChange,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onChange]);

  return (
    <TouchableRipple onPress={handleAddAdress} style={styles.container}>
      <Label style={styles.text}>
        {value?.street
          ? addressBuilder({
              address: value.street,
              area: value.area.id,
            })
          : 'آدرس خود را وارد کنید.'}
      </Label>
    </TouchableRipple>
  );
}

AddressInput.propTypes = {
  value: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '100%',
    elevation: 1,
    minHeight: '50@s',
    justifyContent: 'center',
    paddingHorizontal: '12@s',
    borderRadius: '6@s',
  },
  text: {
    textAlign: 'right',
    textAlignVertical: 'center',
  },
});

export default AddressInput;
