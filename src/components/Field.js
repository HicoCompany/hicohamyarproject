import React, { useMemo } from 'react';
import { StyleSheet, View } from 'react-native';
import Label from './Label';

/**
 * @param {{titleProps: import('./Label').LabelProps,valueProps:import('./Label').LabelProps, title: string,value:string,justifyContent: string,flexDirection: string,titleSuffix: string,}} param0
 */
export default function Field({
  title,
  value,
  justifyContent = 'space-between',
  flexDirection = 'row-reverse',
  titleSuffix = ' : ',
  titleProps,
  valueProps,
}) {
  const custom = useMemo(
    () => ({
      justifyContent,
      flexDirection,
    }),
    [flexDirection, justifyContent],
  );

  return (
    <View style={[styles.container, custom]}>
      <Label color="placeholder" {...titleProps}>
        {title}
        {titleSuffix}
      </Label>
      <Label textBreakStrategy="highQuality" {...valueProps}>
        {value}
      </Label>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
    flexWrap: 'wrap',
  },
});
