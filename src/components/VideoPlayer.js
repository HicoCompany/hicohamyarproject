import React, { useCallback, useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  ActivityIndicator,
  FAB,
  IconButton,
  TouchableRipple,
} from 'react-native-paper';
import Video, { VideoProperties } from 'react-native-video';
import { normalize, wp } from '@src/responsive';
import { Label } from '@components';

/**
 *
 * @param {import('react-native-video').VideoProperties} props
 * @returns
 */
export default function VideoPlayer({
  style,
  videoStyle,
  autoPlay = false,
  ...props
}) {
  const [state, setState] = useState({
    ready: false,
    error: false,
    errorMessage: '',
    initialized: false,
    hidePlay: false,
    playing: true,
  });
  /**
   * @type {{current: Video}}
   */
  const ref = useRef();
  const videoProgress = useRef();

  useEffect(() => {
    // setTimeout(() => {
    if (state.ready) {
      setState((prev) => ({ ...prev, initialized: true, playing: autoPlay }));
    }
    // }, 10);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.ready]);

  const handleVideoProgress = useCallback((video) => {
    videoProgress.current = video;
  }, []);

  const handlePlay = useCallback(() => {
    // if (state.error) {
    //   setState((prev) => ({ ...prev, playing: !prev.playing }));
    // }
    if (state.playing) {
      setState((prev) => ({ ...prev, hidePlay: false, playing: false }));
    } else {
      ref.current.seek(videoProgress.current?.currentTime ?? 0, 10);
      setState((prev) => ({
        ...prev,
        error: false,
        hidePlay: true,
        playing: true,
      }));
    }
  }, [state.playing]);

  const handleReady = useCallback((d) => {
    setState((prev) => ({
      ...prev,
      ready: true,
    }));
  }, []);
  const handleError = useCallback((e) => {
    setState((prev) => ({
      ...prev,
      error: true,
      playing: false,
      ready: false,
    }));
  }, []);

  return (
    <View style={[styles.container, style]}>
      <Video
        {...props}
        style={[styles.video, videoStyle]}
        ref={ref}
        paused={!state.playing}
        onProgress={handleVideoProgress}
        onLoad={handleReady}
        onError={handleError}
        posterResizeMode="contain"
        muted={!state.initialized}
      />

      <TouchableRipple style={styles.control} onPress={handlePlay}>
        <>
          {state.ready ? (
            <FAB
              icon={state.playing ? 'pause' : 'play'}
              visible={!state.hidePlay}
              style={styles.icon}
              animated
              size={wp(5)}
              color="#ffffff"
            />
          ) : state.error ? (
            <>
              <FAB
                icon={'refresh'}
                visible={!state.hidePlay}
                style={styles.icon}
                animated
                size={wp(5)}
                color="#ffffff"
              />
              <Label style={styles.errorMessage} weight="bold">
                بارگذاری ویدیو با خطا مواجه شد.
              </Label>
            </>
          ) : (
            <>
              <ActivityIndicator
                size={30}
                color={'#fff'}
                style={styles.loading}
              />
              <Label style={styles.errorMessage} weight="bold">
                در حال بارگذاری
              </Label>
            </>
          )}
        </>
      </TouchableRipple>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  video: {
    width: '100%',
    height: '100%',
    // backgroundColor: '#ffffff',
  },
  control: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    // width: 60,
    // height: 60,
    backgroundColor: 'rgba(80,80,80,1)',
    // borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  loading: {
    backgroundColor: '#333',
    padding: 10,
    borderRadius: 50,
  },
  errorMessage: {
    fontSize: normalize(12),
    color: '#333',
    marginTop: 10,
  },
});
