import React from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
const Loading = ({
  message = 'در حال بارگذاری',
  messageColor = Colors.gray,
  style,
}) => {
  return (
    <View style={[loadingStyles.container, style]}>
      <ActivityIndicator size={'large'} color={Colors.green} />
      {message !== undefined && (
        <Text style={[loadingStyles.text, { color: messageColor }]}>
          {message}
        </Text>
      )}
    </View>
  );
};

const loadingStyles = EStyleSheet.create({
  container: {
    backgroundColor: Colors.backgroundColor,
    height: '100%',
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  text: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    color: Colors.green,
    fontSize: normalize(13),
    textAlign: 'center',
  },
});

export { Loading };
