import React from 'react';
import { Image, View } from 'react-native';
import Colors from '@src/utility/Colors';
import Label from './Label';
import { ScaledSheet } from 'react-native-size-matters';

const TabItem = ({ source, name, color }) => (
  <View style={styles.container}>
    <Image
      source={source}
      style={[styles.icon, { tintColor: color }]}
      resizeMode={'contain'}
    />
    <Label fontSize={11} style={[styles.name, { color }]} weight="bold">
      {name}
    </Label>
  </View>
);

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: '22@s',
    width: '22@s',
  },
  name: {
    color: Colors.gray,
    textAlign: 'center',
  },
});

export default TabItem;
