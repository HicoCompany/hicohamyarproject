import Colors from '@src/utility/Colors';
import React from 'react';
import { View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import HeaderBack from './HeaderBack';

export default function Container({ children, style }) {
  return <View style={[styles.container, style]}>{children}</View>;
}

/**
 * @param {{title: string,backTitle: string,onBack:() => void,style: import('react-native').StyleProp}} param0
 */
Container.Header = ({ title, onBack, backTitle, style }) => (
  <HeaderBack
    headerTitle={title}
    onBackPress={onBack}
    backTitle={backTitle}
    style={style}
  />
);

const styles = ScaledSheet.create({
  container: {
    backgroundColor: Colors.backgroundColor,
    flex: 1,
    // padding: '16@s',
    overflow: 'hidden',
  },
});
