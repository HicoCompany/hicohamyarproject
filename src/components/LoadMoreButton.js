import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';

export default function LoadMore({ loading, onPress, disabled }) {
  return (
    <View style={styles.container}>
      <Button
        mode="contained"
        style={styles.button}
        onPress={loading ? undefined : onPress}
        contentStyle={styles.buttonContent}
        loading={loading}
        disabled={disabled}
        labelStyle={styles.buttonLabel}>
        بیشتر
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  label: {},
  loading: {},
  button: {
    borderRadius: 50,
    color: '#ffffff',
    width: 100,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  buttonContent: {
    height: 40,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonLabel: {
    color: '#ffffff',
  },
});
