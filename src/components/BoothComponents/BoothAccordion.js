import React, { useCallback, useState } from 'react';
import { View, Text, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Animated, { EasingNode } from 'react-native-reanimated';
import { useTransition } from 'react-native-redash/lib/module/v1';
import { TouchableRipple } from 'react-native-paper';

//assets
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function BoothAccordion({
  title,
  titleStyle,
  children,
  openByDefault = false,
  IconSource,
  style,
}) {
  const [visible, setVisible] = useState(openByDefault);
  const [calcHeight, setCalcHeight] = useState(10);
  const transition = useTransition(visible, {
    duration: 300,
    ease: EasingNode.inOut(EasingNode.ease),
  });

  const height = Animated.interpolateNode(transition, {
    inputRange: [0, 1],
    outputRange: [0, calcHeight],
  });
  const rotate = Animated.interpolateNode(transition, {
    inputRange: [0, 1],
    outputRange: [0, Math.PI],
  });

  const toggleAccordion = useCallback(() => {
    setVisible((state) => !state);
  }, []);

  const handleLayout = useCallback((event) => {
    if (event.nativeEvent.layout.height) {
      setCalcHeight(event.nativeEvent.layout.height);
    }
  }, []);

  return (
    <View style={styles.container}>
      <TouchableRipple onPress={toggleAccordion}>
        <View style={styles.header}>
          <View style={styles.titleContainer}>
            {IconSource && <Image style={styles.icon} source={IconSource} />}
            <Text style={[styles.title, titleStyle]}>{title}</Text>
          </View>
          <Animated.View style={{ transform: [{ rotate }] }}>
            <Icon name="chevron-down" />
          </Animated.View>
        </View>
      </TouchableRipple>

      <Animated.View
        style={[styles.animatedContainer, { height, opacity: transition }]}>
        <View style={[styles.content, style]} onLayout={handleLayout}>
          {children}
        </View>
      </Animated.View>
    </View>
  );
}

const styles = EStyleSheet.create({
  container: {
    minHeight: 50,
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
    height: 50,
  },
  titleContainer: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    height: 50,
    flex: 1,
  },
  title: {
    fontFamily: '$IR_M',
    flex: 1,
  },
  animatedContainer: {
    overflow: 'hidden',
  },
  content: {
    paddingHorizontal: 32,
  },
  icon: {
    marginStart: 10,
    width: 30,
    height: 30,
  },
});
