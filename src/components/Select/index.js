/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, ActivityIndicator, ScrollView } from 'react-native';
import Label from '@components/Label';

import Animated, { EasingNode, interpolateNode } from 'react-native-reanimated';
import { useTransition } from 'react-native-redash/lib/module/v1';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '@src/utility/Colors';
import SelectOption from './SelectOption';
import { ScaledSheet } from 'react-native-size-matters';

/**
 *
 * @typedef {{label: string,value: any}} SelectValue
 * @param {{loading: boolean,disabled: boolean,shouldChangeVisibility: () => boolean | undefined,onChange: (value: SelectValue) => void,labelKey: string,valueKey: string,data: any[],inputStyle: import('react-native').StyleProp,style: import('react-native').StyleProp,placeholder: string,value: SelectValue}} param0
 */
export default function Select({
  onChange,
  labelKey = 'label',
  getLabel,
  valueKey = 'value',
  data = [],
  inputStyle,
  style,
  placeholder,
  value: SelectValue = { label: '', value: null },
  selectRef,
  loading = false,
  disabled = false,
  shouldChangeVisibility,
}) {
  const [open, setOpen] = useState(false);

  const transition = useTransition(open, {
    duration: 500,
    easing: EasingNode.inOut(EasingNode.ease),
  });

  const height = interpolateNode(transition, {
    inputRange: [0, 1],
    outputRange: [0, 200],
  });

  const handleToggleSelect = useCallback(() => {
    if (!open) {
      const should = shouldChangeVisibility?.();
      if (should == true) {
        setOpen((state) => !state);
      } else if (should == undefined) {
        setOpen((state) => !state);
      }
    } else {
      setOpen((state) => !state);
    }
  }, [open, shouldChangeVisibility]);

  const handleChange = useCallback(
    (value) => {
      onChange?.(value);
      handleCloseSelect();
    },
    [onChange],
  );
  const handleCloseSelect = useCallback(() => {
    setOpen(false);
  }, []);
  const handleSelect = useCallback((value) => {
    setOpen(value);
  }, []);

  const handleLabel = useCallback(
    (value) => {
      if (getLabel) {
        return getLabel(value);
      }
      return value[labelKey];
    },
    [labelKey, getLabel],
  );

  useEffect(() => {
    if (selectRef) {
      selectRef.current = {
        setVisibility: handleSelect,
      };
    }
  }, [selectRef]);

  const renderItems = useMemo(
    () =>
      data.map((el) => {
        const value = el[valueKey];
        return (
          <SelectOption
            isActive={value == SelectValue[valueKey]}
            label={handleLabel(el)}
            key={String(value)}
            onPress={handleChange}
            value={el}
          />
        );
      }),
    [data, SelectValue],
  );

  const _label = useMemo(() => handleLabel(SelectValue), [SelectValue]);

  return (
    <View style={[styles.container, style]}>
      <TouchableRipple
        onPress={handleToggleSelect}
        borderless
        disabled={loading || disabled}>
        <View style={[styles.label, inputStyle]}>
          <Label color={_label ? 'text' : 'placeholder'}>
            {loading ? placeholder : _label || placeholder}
          </Label>
          {loading ? (
            <ActivityIndicator size="small" color={Colors.green} />
          ) : (
            <Icon name={open ? 'chevron-up' : 'chevron-down'} />
          )}
        </View>
      </TouchableRipple>
      <Animated.View style={[styles.options, { height }]}>
        <ScrollView nestedScrollEnabled persistentScrollbar>
          {renderItems}
        </ScrollView>
      </Animated.View>
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    position: 'relative',
  },
  label: {
    flexDirection: 'row-reverse',

    backgroundColor: 'white',
    paddingHorizontal: 16,
    elevation: 1,
    borderWidth: 1,
    borderColor: '#efefef',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlignVertical: 'center',
  },
  itemsContainer: {},
  options: {
    backgroundColor: 'white',
    width: '100%',
    minWidth: '120@s',
    borderRadius: '6@s',
    maxHeight: '200@s',
    position: 'absolute',
    zIndex: 100,
    top: '50@s',
    elevation: 1,
    overflow: 'hidden',
  },
});
