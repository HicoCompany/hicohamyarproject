import Colors from '@src/utility/Colors';
import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import Label from '../Label';

export default function SelectOption({ value, onPress, isActive, label }) {
  const handlePress = useCallback(() => {
    onPress?.(value);
  }, [onPress, value]);

  return (
    <TouchableRipple onPress={handlePress}>
      <Label style={[styles.item, isActive ? styles.selected : undefined]}>
        {label}
      </Label>
    </TouchableRipple>
  );
}

const styles = StyleSheet.create({
  item: {
    height: 50,
    width: '100%',
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#efefef',
    textAlignVertical: 'center',
    minHeight: 50,
  },
  selected: {
    backgroundColor: Colors.green,
    color: 'white',
  },
});
