import React, { useCallback } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { hp, moderateScale, normalize, scale } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Label from './Label';
import Icon from 'react-native-vector-icons/Feather';

const HeaderBack = ({ headerTitle, onBackPress, backTitle, style }) => {
  const navigation = useNavigation();
  const handleBack = useCallback(() => {
    navigation.goBack();
    // onBackPress?.();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={[styles.header, style]}>
      <View style={styles.backContainer}>
        <TouchableOpacity onPress={handleBack}>
          <View style={styles.backButton}>
            <Icon name="chevron-left" style={styles.backIcon} />
            <Label style={styles.backTitle}>{backTitle || 'بازگشت'}</Label>
          </View>
        </TouchableOpacity>
      </View>

      <Label style={styles.title}>{headerTitle}</Label>
    </View>
  );
};

const styles = EStyleSheet.create({
  '@media (min-width: 600) and (min-height: 900)': {
    header: {
      '@media ios': {
        paddingTop: 25,
        height: hp('12%'),
      },
      '@media android': {
        height: hp('10%'),
      },
      backgroundColor: Colors.toolbar,
      flexDirection: 'row',
    },
  },
  header: {
    '@media ios': {
      paddingTop: 21,
      height: hp('15%'),
    },
    '@media android': {
      height: hp('10%'),
    },
    backgroundColor: Colors.green,
    paddingVertical: 10,
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingRight: 16,
    alignItems: 'center',
  },
  title: {
    color: '#ffffff',
    fontSize: normalize(14),
    textAlign: 'right',
    flex: 1,
  },
  backContainer: {
    overflow: 'hidden',
    height: '100%',
  },

  backButton: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
    height: '100%',
    width: scale(70),
  },
  backTitle: {
    color: '#ffffff',
    fontSize: normalize(12),
    textAlign: 'right',
    marginLeft: 3,
  },
  backIcon: { fontSize: moderateScale(20), color: '#ffffff' },
});

export default HeaderBack;
