import { normalize } from '@src/responsive';
import React, { useCallback, useEffect, useState } from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog } from 'react-native-paper';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Label from './Label';

const defaultMessage = 'ارتباط دستگاه خود را بررسی کنید.';
export default function ErrorDialog({
  open,
  autoClose = false,
  handleClose,
  message = defaultMessage,
}) {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (open !== visible) {
      setVisible(open);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  const handleErrorDismiss = useCallback(() => {
    if (autoClose) {
      setVisible(false);
    } else {
      handleClose?.();
    }
  }, [handleClose, autoClose]);

  return (
    <Dialog
      visible={autoClose ? visible : open}
      style={styles.dialogContainer}
      dismissable
      onDismiss={handleErrorDismiss}>
      <Dialog.Content>
        <Label weight="normal" style={styles.dialogText}>
          {message}
        </Label>
      </Dialog.Content>
    </Dialog>
  );
}

const styles = EStyleSheet.create({
  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
    zIndex: 10,
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.textBlack,
  },
});
