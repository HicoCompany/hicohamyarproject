import React from 'react';
import { View, StyleSheet } from 'react-native';

import { IconButton } from 'react-native-paper';
import Colors from '@src/utility/Colors';
import { Label } from 'native-base';
import { normalize } from '@src/responsive';

export default function Rating({
  value,
  onChange,
  style,
  iconSize = 30,
  disabled,
}) {
  const handleRating = (rate) => () => {
    onChange?.(rate);
  };

  return (
    <View style={[styles.rateContainer, style]}>
      <View style={styles.iconContainer}>
        <IconButton
          style={styles.rateIcon}
          icon={value >= 1 ? 'star' : 'star-outline'}
          color={Colors.green}
          size={iconSize}
          onPress={handleRating(1)}
          disabled={disabled}
        />
        {value === 1 && <Label style={styles.label}>خیلی بد</Label>}
      </View>
      <View style={styles.iconContainer}>
        <IconButton
          style={styles.rateIcon}
          icon={value >= 2 ? 'star' : 'star-outline'}
          color={Colors.green}
          onPress={handleRating(2)}
          size={iconSize}
          disabled={disabled}
        />
        {value === 2 && <Label style={styles.label}>بد</Label>}
      </View>
      <View style={styles.iconContainer}>
        <IconButton
          style={styles.rateIcon}
          icon={value >= 3 ? 'star' : 'star-outline'}
          color={Colors.green}
          onPress={handleRating(3)}
          size={iconSize}
          disabled={disabled}
        />
        {value === 3 && <Label style={styles.label}>خوب</Label>}
      </View>
      <View style={styles.iconContainer}>
        <IconButton
          style={styles.rateIcon}
          icon={value >= 4 ? 'star' : 'star-outline'}
          color={Colors.green}
          onPress={handleRating(4)}
          size={iconSize}
          disabled={disabled}
        />
        {value === 4 && <Label style={styles.label}>خیلی خوب</Label>}
      </View>
      <View style={styles.iconContainer}>
        <IconButton
          style={styles.rateIcon}
          icon={value >= 5 ? 'star' : 'star-outline'}
          color={Colors.green}
          onPress={handleRating(5)}
          size={iconSize}
          disabled={disabled}
        />
        {value === 5 && <Label style={styles.label}>عالی</Label>}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rateContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 35,
  },
  rateIcon: {
    margin: 0,
  },
  iconContainer: {
    alignItems: 'center',
    maxWidth: 35,
    overflow: 'visible',
    marginHorizontal: 4,
  },
  label: {
    color: Colors.green,
    fontSize: normalize(12),
    minWidth: 100,
    textAlign: 'center',
    height: 30,
  },
});
