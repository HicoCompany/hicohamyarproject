/* eslint-disable no-shadow */
import Colors from '@src/utility/Colors';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { View, InteractionManager } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native-paper';

/**
 *
 * @param {{onChange: (value) => void,value: string,valueLength: number = 5}} param0
 * @returns
 */
function ConfirmCodeInput({ onChange, value, valueLength = 5 }) {
  const inputs = useRef(Array(valueLength).fill('')).current;
  const inputRef = useRef([]);

  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      inputRef.current[0].focus();
    });
  }, []);

  const code = useMemo(() => {
    let newCode = value.split('');
    const length = newCode.length;
    const distance = valueLength - length;
    newCode = distance ? [...value, ...Array(distance).fill('')] : newCode;
    return newCode;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  const handleRef = useCallback(
    (i) => (ref) => {
      if (ref) {
        inputRef.current[i] = ref;
      }
    },
    [],
  );

  const handleNextInput = useCallback(
    (index) => (value) => {
      if (index < inputRef.current.length - 1 && value) {
        InteractionManager.runAfterInteractions(() => {
          inputRef.current[index + 1].focus();
        });
      }

      if (index == inputRef.current.length - 1 && value) {
        inputRef.current[index].blur();
      }
      const otp = [...code];
      otp[index] = value;
      onChange(otp.join(''));
    },
    [code, onChange],
  );

  const handlePrevInput = useCallback(
    (index, v = '') => ({ nativeEvent: { key } }) => {
      if (index > 0 && key == 'Backspace' && v == '') {
        inputRef.current[index - 1].focus();
      }
    },
    [],
  );

  // change the caret position to end of value
  const handleOnInputFocused = useCallback(
    (index, value) => () => {
      if (value) {
        inputRef.current[index].setNativeProps({
          selection: { start: 1, end: 1 },
        });
      }
    },
    [],
  );

  const renderInputs = useMemo(
    () =>
      inputs.map((_, i) => {
        const itemValue = code[i];
        return (
          <TextInput
            style={styles.input}
            ref={handleRef(i)}
            onChangeText={handleNextInput(i)}
            value={itemValue}
            mode="flat"
            onKeyPress={handlePrevInput(i, itemValue)}
            selectionColor={Colors.green}
            keyboardType={'number-pad'}
            maxLength={1}
            // caretHidden
            onTouchEnd={handleOnInputFocused(i, itemValue)}
            key={`input-${i}`}
            enablesReturnKeyAutomatically
            autoCorrect={false}
            renderToHardwareTextureAndroid
            blurOnSubmit={false}
            focusable
          />
        );
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [code, value],
  );
  return <View style={styles.container}>{renderInputs}</View>;
}

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  input: {
    width: '35@s',
    height: '50@s',
    borderTopLeftRadius: '6@s',
    borderTopRightRadius: '6@s',
    color: '#333',
    margin: '5@s',
    backgroundColor: '#efefef',
    textAlign: 'center',
    fontSize: '16@s',
    padding: 0,
    // lineHeight: '16@s',
    textAlignVertical: 'center',
  },
});

ConfirmCodeInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  valueLength: PropTypes.number,
};

ConfirmCodeInput.defaultProps = {
  valueLength: 5,
};

export default ConfirmCodeInput;
