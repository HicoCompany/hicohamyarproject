import { func, number, string } from 'prop-types';
import React, { Component } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import * as Progress from 'react-native-progress';
import { hp, normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import Constants from '@src/utility/Constants';
import Utils from '@src/utility/Utils';

class ImageUploadProgress extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return Utils.shallowCompare(this, nextProps, nextState);
  }

  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          marginTop: 5,
          marginBottom: 5,
          height: this.props.containerSize,
          width: this.props.containerSize,
        }}>
        <TouchableOpacity
          onPress={this.props.onChooseImageClick}
          activeOpacity={0.6}
          style={[
            {
              borderColor: this.props.borderColor,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
              height: this.props.innerSize,
              width: this.props.innerSize,
              borderRadius: this.props.innerSize / 2,
            },
            this.props.status === Constants.UPLOAD_STATUS.UPLOADING
              ? { borderWidth: 0 }
              : { borderWidth: 2 },
          ]}>
          {(this.props.uri.length === 0 && (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
              }}>
              <Image
                source={require('@src/assets/image/ic_camera.png')}
                resizeMode={'contain'}
                style={[
                  imageUploadProgressStyle.noImage,
                  { tintColor: this.props.noImageTintColor },
                ]}
              />
            </View>
          )) || (
            <Image
              source={{ uri: this.props.uri }}
              resizeMode={'cover'}
              style={{
                height: this.props.imageSize,
                width: this.props.imageSize,
                borderRadius: this.props.imageSize / 2,
              }}
              blurRadius={
                this.props.status === Constants.UPLOAD_STATUS.UPLOADING
                  ? 2.5
                  : 0
              }
            />
          )}
        </TouchableOpacity>
        {this.props.status === Constants.UPLOAD_STATUS.UPLOADING && (
          <Progress.Circle
            progress={this.props.progress}
            animated
            size={this.props.containerSize}
            showsText
            textStyle={imageUploadProgressStyle.imageProgressText}
            color={Colors.purple}
            borderWidth={0}
            thickness={4}
            direction="clockwise"
            style={{
              position: 'absolute',
              top: '50%' - this.props.containerSize,
              left: '50%' - this.props.containerSize,
            }}
          />
        )}
      </View>
    );
  }
}

ImageUploadProgress.propTypes = {
  progress: number,
  status: number,
  uri: string,
  onChooseImageClick: func,
  borderColor: string,
  containerSize: number.isRequired,
  innerSize: number.isRequired,
  imageSize: number.isRequired,
};

ImageUploadProgress.defaultProps = {
  borderColor: Colors.white,
  progress: 0,
  status: Constants.UPLOAD_STATUS.IDLE,
  uri: '',
  onChooseImageClick: () => {},
};

const imageUploadProgressStyle = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },

  noImage: {
    height: hp('8%'),
    width: hp('8%'),
    borderRadius: hp('4%'),
    tintColor: Colors.white,
  },
  imageProgressText: {
    fontSize: normalize(30),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    color: Colors.white,
    textAlign: 'center',
  },
});

export { ImageUploadProgress };
