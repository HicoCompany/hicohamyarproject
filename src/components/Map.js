import { useNavigation } from '@react-navigation/native';
import {
  getLocationByRequestUpdate,
  getLocationPermissions,
} from '@src/screens/standalone/MapAreaScreen/mapUtils';
import Colors from '@src/utility/Colors';
import Constants from '@src/utility/Constants';
import React, {
  createRef,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { StyleSheet, View } from 'react-native';
import MapView, {
  Marker,
  UrlTile,
  PROVIDER_OSMDROID,
} from 'react-native-maps-osmdroid';
import { DefaultModalActions, useAlert } from './GlobalAlert';

//
export default function Map({
  onUserLocationChange,
  onTouch,
  onIdle,
  tileUrl = 'http://c.tile.openstreetmap.org/{z}/{x}/{y}.png',
  mapRef: mapRefFromProps,
  onPermissionError,
  style,
}) {
  const navigation = useNavigation();
  const Alert = useAlert();
  const mapRef = useRef();
  const [userLocation, setUserLocation] = useState({
    latitude: null,
    longitude: null,
  });

  useEffect(() => {
    handleLocationPermissions();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (mapRefFromProps) {
      mapRefFromProps.current = mapRef.current;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mapRef]);

  const handleLocationPermissions = async () => {
    try {
      await getLocationPermissions(Alert);

      getLocationByRequestUpdate(
        (v) => {
          if (v?.x) {
            onUserLocationChange?.({ latitude: v.y, longitude: v.x });
            setUserLocation({ latitude: v.y, longitude: v.x });

            mapRef.current?.animateCamera({
              center: {
                latitude: v.y,
                longitude: v.x,
              },
              zoom: 15,
            });
          }
        },
        {
          fastestInterval: 500,
          interval: 1000,
          numUpdates: 5,
        },
      );
    } catch (error) {
      if (error === 'canceled') {
        Alert.setAction(
          DefaultModalActions.Ask({
            message:
              'این صفحه به اجازه شما برای استفاده از موقعیت مکانی نیاز دارد.',
            onConfirm: handleLocationPermissions,
            ConfirmLabel: 'اجازه می دهم',
            DenyLabel: 'اجازه نمی دهم',
            onDenied: () => {
              navigation.goBack();
            },
          }),
        );

        return;
      }
      onPermissionError?.(error);
    }
  };

  const handleUserMarkerPress = useCallback(
    (v) => {
      mapRef.current?.animateCamera?.({
        center: {
          ...v.nativeEvent.coordinate,
        },
        zoom: 15,
      });
    },
    [mapRef],
  );

  return (
    <MapView
      style={[styles.map, style]}
      provider={PROVIDER_OSMDROID}
      ref={mapRef}
      mapType="none"
      rotateEnabled={false}
      initialRegion={Constants.DEFAULT_LOCATION}
      onRegionChangeComplete={onIdle}
      onTouchStart={onTouch}
      moveOnMarkerPress={false}
      maxZoomLevel={19}
      minZoomLevel={11}>
      <UrlTile urlTemplate={tileUrl} flipY={false} tileSize={512} />
      {userLocation.longitude && (
        <Marker
          coordinate={userLocation}
          onPress={handleUserMarkerPress}
          anchor={{ x: 0.5, y: 0.5 }}>
          <View style={styles.locationIconContainer}>
            <View style={styles.locationIcon} />
          </View>
        </Marker>
      )}
    </MapView>
  );
}

const styles = StyleSheet.create({
  map: {
    width: '100%',
    height: '100%',
  },
  locationIconContainer: {
    width: 20,
    height: 20,
    borderRadius: 30,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 1,
  },
  locationIcon: {
    width: '70%',
    height: '70%',
    borderRadius: 30,
    backgroundColor: Colors.blue,
    elevation: 1,
  },
});
