import { dimensions } from '@src/utility/Constants';
import React from 'react';
import { View } from 'react-native';
import { Avatar } from 'react-native-paper';
import { ScaledSheet } from 'react-native-size-matters';
import Label from './Label';

export default function EmptyList({ message, icon = 'view-grid' }) {
  return (
    <View style={styles.container}>
      <Avatar.Icon icon={icon} color="#efefef" />
      <Label style={styles.message} fontSize={15}>
        {message}
      </Label>
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    minHeight: dimensions.screen.height - 200,
    justifyContent: 'center',
    alignItems: 'center',
    padding: '16@s',
  },
  message: {
    marginTop: '5@s',
  },
});
