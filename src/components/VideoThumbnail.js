import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { createThumbnail } from 'react-native-create-thumbnail';
import { config } from '@src/utility/Constants';
import fs from 'react-native-fs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { FAB } from 'react-native-paper';

export default function VideoThumbnail({ url, name, style, ...props }) {
  const [Thumbnail, setThumbnail] = useState({
    source: undefined,
    loaded: false,
  });

  useEffect(() => {
    const file = fs.downloadFile?.({
      fromUrl: url,
      toFile: `${fs.CachesDirectoryPath}/any.mp4`,
      connectionTimeout: 3000,
      begin: () => {
        fs.stopDownload(file.jobId);
        if (file) {
          createThumbnail({
            url,
            timeStamp: 0,
            cacheName: name
              ? `${config.THUMBNAIL_CACHE_NAME_PREFIX}-${name}`
              : undefined,
          })
            .then((thumbnail) => {
              setThumbnail({ source: { uri: thumbnail.path }, loaded: true });
            })
            .catch(() => {});
        }
      },
      progressInterval: 100,
    });

    file.promise
      .then((data) => {
        setThumbnail({ source: undefined, loaded: false });
      })
      .catch((e) => {});

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={[styles.container, style]}>
      <Image
        {...props}
        style={Thumbnail.loaded ? style : [style, styles.defaultImage]}
        source={Thumbnail.source}
      />
      {!Thumbnail.loaded ? (
        <FAB icon={'play'} color="#ffffff" style={styles.icon} />
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#efefef',
  },
  defaultImage: {
    width: 100,
    height: 100,
    position: 'relative',
    marginBottom: 50,
  },
  icon: {
    position: 'absolute',
    backgroundColor: '#333',
  },
});
