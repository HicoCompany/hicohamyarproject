import React, { Component, createRef } from 'react';
import {
  NativeEventEmitter,
  findNodeHandle,
  NativeModules,
  StyleSheet,
  UIManager,
} from 'react-native';
// eslint-disable-next-line no-unused-vars
import NeshanView, { MapStyle, MapProps, Location } from './NeshanView';
import { NeshanMapModule } from './NeshanMapModule';
/**
 * @typedef {MapProps & {defaultLocation: Location}} Props
 * @extends {Component<MapProps,{location: Location}>}
 */
class MapView extends Component {
  constructor(props) {
    super(props);
    this.mapRef = createRef();
    this.state = {
      location: {
        x: 0,
        y: 0,
        duration: 0.25,
      },
      updatingLocation: false,
      firstUpdate: true,
      key: 'neshan_map_1',
    };
  }

  initializeUserLocation = ({
    startUpdatingLocationAfterInit = false,
    focusOnLastLocation = false,
    addUserMarker = false,
    callOnLocationReady = true,
  }) => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.mapRef.current),
      'initUserLocation',
      [
        startUpdatingLocationAfterInit,
        focusOnLastLocation,
        addUserMarker,
        callOnLocationReady,
      ],
    );
  };

  startUpdatingUserLocation = ({ focusOnLocation = true }) => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.mapRef.current),
      'startUpdatingLocation',
      [focusOnLocation],
    );
    this.setState((state) => ({ ...state, updatingLocation: true }));
  };

  stopUpdatingUserLocation = () => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.mapRef.current),
      'stopUpdatingLocation',
      [],
    );
    this.setState((state) => ({ ...state, updatingLocation: false }));
  };

  focusOnLastLocation = () => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.mapRef.current),
      'focusOnLastLocation',
      [],
    );
  };

  setLocation = ({ x, y, zoom = 15, duration = 0.25 }) => {
    if (x !== undefined && x !== null && y !== undefined && x !== null) {
      UIManager.dispatchViewManagerCommand(
        findNodeHandle(this.mapRef.current),
        'setLocation',
        [x, y, zoom, duration],
      );
    } else {
      console.warn('x,y are required in setLocation');
    }
  };

  setListeners = () => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.mapRef.current),
      'setEventListeners',
      [],
    );
  };

  removeListeners = () => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.mapRef.current),
      'removeEventListeners',
      [],
    );
  };

  getLocationFromModule = () => {
    return NeshanMapModule.getCenterLocation();
  };

  setLocationByRequestUpdate = (
    { fastestInterval, interval, numUpdates } = {
      interval: 1000,
      fastestInterval: 500,
      numUpdates: 4,
    },
  ) => {
    let eventEmitter = new NativeEventEmitter(NativeModules.NativeModules);
    const getLocation = (location) => {
      if (location?.x) {
        NeshanMapModule.setLocation({
          x: location.x,
          y: location.y,
          duration: 0.25,
        })
          .then()
          .catch(() => {});
      }
    };
    eventEmitter.addListener('lastLocation', getLocation);

    NeshanMapModule.getLastLocationByListening({
      interval,
      fastestInterval,
      numUpdates,
    })
      .then(() => {
        eventEmitter.removeAllListeners('lastLocation');
      })
      .catch(() => {
        eventEmitter.removeAllListeners('lastLocation');
      });
  };

  componentDidMount() {
    NeshanMapModule.initModule(findNodeHandle(this.mapRef.current));
    this.initializeUserLocation({
      focusOnLastLocation: true,
      startUpdatingLocationAfterInit: this.state.updatingLocation,
      addUserMarker: false,
      callOnLocationReady: true,
    });
  }

  componentWillUnmount() {
    if (this.state.updatingLocation) {
      this.stopUpdatingUserLocation();
    }
    this.removeListeners();
  }

  _onMapIdle = () => {
    NeshanMapModule.getCenterLocation().then((ec) => {
      this.props.onMapIdle?.(ec);
    });
  };

  _onMapMove = (ec) => {
    this.props.onMapMove?.(ec.nativeEvent);
  };
  _onMapStable = (ec) => {
    this.props.onMapStable?.(ec.nativeEvent);
  };

  _onLocationChanged = (ec) => {
    this.props.onLocationChanged?.(ec.nativeEvent);
  };

  _onPress = (ec) => {
    this.props.onPress?.(ec.nativeEvent);
  };
  _onError = (ec) => {
    this.props.onError?.(ec.nativeEvent);
  };

  _onLocationReady = () => {
    NeshanMapModule.focusOnLastLocation()
      .then()
      .catch((e) => {
        if (e.message === 'location is null') {
          this.setLocationByRequestUpdate({
            fastestInterval: 500,
            interval: 1000,
            numUpdates: 4,
          });
        }
      });
    this.props.onLocationReady?.();
  };

  render() {
    return (
      <NeshanView
        style={styles.map}
        location={this.state.location}
        zoom={15}
        mapStyle={MapStyle.neshan}
        {...this.props}
        ref={this.mapRef}
        onTouchEnd={this._onMapIdle}
        onMapMove={this._onMapMove}
        onMapStable={this._onMapStable}
        onLocationChanged={this._onLocationChanged}
        onPress={this._onPress}
        onLayout={this.setListeners}
        onLocationReady={this._onLocationReady}
        key={this.state.key}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  map: {
    width: '100%',
    height: '100%',
  },
  fab: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    backgroundColor: 'white',
    borderRadius: 50,
    width: 60,
    height: 60,
    zIndex: 10,
    elevation: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fabContent: {
    width: '100%',
    height: '100%',
    backgroundColor: 'red',
  },
});

export default MapView;
