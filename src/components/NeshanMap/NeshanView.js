// eslint-disable-next-line no-unused-vars
import { Component } from 'react';
import { requireNativeComponent } from 'react-native';

/**
 *
 *
 * @typedef {{x: number,y: number,duration: number}} Location
 *  x -> longitude
 *  y -> latitude
 *  duration -> animation duration in seconds
 * @typedef {{
 *  onMapIdle(): void,
 *  onMapStable(): void,
 *  onMapMove(): void,
 *  onPress(): void,
 *  onLocationChanged(): void,
 *  onError():void
 *  location: {x: number,y: number,duration: number},
 *  zoom: number,
 *  mapStyle
 * }} MapProps
 * @extends {Component<MapProps, any, any>}
 */
const Map = requireNativeComponent('NeshanMap');

export const MapStyle = {
  neshan: 0,
  standard_night: 1,
  standard_day: 2,
  dreamy: 3,
  dreamy_gold: 4,
};

export default Map;
