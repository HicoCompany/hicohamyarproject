import Colors from '@src/utility/Colors';
import React from 'react';
import { Button } from 'react-native-paper';
import { ScaledSheet } from 'react-native-size-matters';

export default function ButtonSubmit({
  children,
  onPress,
  style,
  labelStyle,
  growWidth = false,
  ...props
}) {
  return (
    <Button
      mode="contained"
      color={Colors.green}
      {...props}
      onPress={onPress}
      style={[styles.button, growWidth ? styles.grow : undefined, style]}
      labelStyle={[styles.buttonLabel, labelStyle]}>
      {children}
    </Button>
  );
}

const styles = ScaledSheet.create({
  button: {
    borderRadius: 0,
    height: '45@s',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonLabel: {
    color: '#fff',
    fontSize: '13@s',
  },
  content: {},
  grow: {
    flex: 1,
  },
});
