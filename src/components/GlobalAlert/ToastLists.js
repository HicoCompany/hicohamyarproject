import React, { useMemo } from 'react';
import { StyleSheet } from 'react-native';
import { Transition, Transitioning } from 'react-native-reanimated';
import Toast from './Toast';
import Snack from './Snack';

/**
 * @typedef {{type: 'info' | 'warning' | 'error' | 'success' | 'none',mode: 'normal' | 'action',message: string,timeout: 'short' | 'long',action: {label: string, onPress: () => void}}} Toast
 * @param {{list: Toast[],onDismiss: (key:string) => void,transitionRef: any}} param0
 */
export default function ToastLists({ list, onDismiss, transitionRef }) {
  const toastTransition = (
    <Transition.Together>
      <Transition.Change interpolation="easeInOut" />
      <Transition.In type="fade" />
    </Transition.Together>
  );

  const [Toasts, ActionToasts] = useMemo(
    () =>
      list.reduce(
        (prevValue, currentValue) => {
          if (currentValue.mode == 'action') {
            prevValue[1].push(currentValue);
          } else {
            prevValue[0].push(currentValue);
          }
          return prevValue;
        },
        [[], []],
      ),
    [list],
  );

  return (
    <Transitioning.View
      transition={toastTransition}
      style={styles.alertContainer}
      ref={transitionRef}>
      {Toasts.map((el, i) => (
        <Toast {...el} index={i} onDismiss={onDismiss(el.key)} />
      ))}
      {ActionToasts.map((el, i) => (
        <Snack {...el} onDismiss={onDismiss(el.key)} />
      ))}
    </Transitioning.View>
  );
}

const styles = StyleSheet.create({
  alertContainer: {
    position: 'absolute',
    bottom: 100,
    left: 0,
    right: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexShrink: 1,
  },
});
