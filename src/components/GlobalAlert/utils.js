import Colors from '@src/utility/Colors';

export const DefaultModalActions = {
  RetryOrBack: ({ onRefresh, message, onDismiss }) => ({
    message,
    actions: [
      {
        label: 'تلاش مجدد',
        buttonMode: 'contained',
        color: Colors.green,
        onPress: onRefresh,
        labelStyle: {
          color: Colors.whiteText,
        },
      },
      {
        label: 'بستن',
        buttonMode: 'text',
        color: Colors.text,
        onPress: onDismiss,
      },
    ],
  }),
  Ask: ({
    onConfirm,
    onDenied,
    message,
    loading,
    mode,
    ConfirmLabel = 'بله',
    DenyLabel = 'خیر',
  }) => ({
    message,
    actions: [
      {
        label: ConfirmLabel,
        buttonMode: 'contained',
        color: Colors[mode],
        onPress: onConfirm,
        labelStyle: {
          color: Colors.whiteText,
        },
        loading,
      },
      {
        label: DenyLabel,
        buttonMode: 'text',
        color: Colors.text,
        onPress: onDenied,
      },
    ],
  }),
  Error: ({ message = 'خطایی رخ داد', onDismiss }) => ({
    message,
    actions: {
      label: 'بستن',
      buttonMode: 'contained',
      color: Colors.te,
      onPress: onDismiss,
    },
  }),
  Message: ({
    onConfirm,
    message,
    loading,
    mode,
    dismissable = true,
    ConfirmLabel = 'بستن',
  }) => ({
    message,
    dismissable,
    actions: [
      {
        label: ConfirmLabel,
        buttonMode: 'contained',
        color: Colors[mode],
        onPress: onConfirm,
        labelStyle: {
          color: Colors.whiteText,
        },
        loading,
      },
    ],
  }),
};

// label: string,onPress(): void,buttonMode: "text" | "outlined" | "contained", color: string,labelStyle,style,loading?: boolean
