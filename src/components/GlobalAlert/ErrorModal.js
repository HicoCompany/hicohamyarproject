import React from 'react';
import { Button, Dialog, Portal } from 'react-native-paper';
import { Label } from '@components';
import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import { StyleSheet } from 'react-native';

/**
 * @typedef {{label: string,onPress(): void,buttonMode: "text" | "outlined" | "contained", color: string,labelStyle,style}} AlertAction
 * @typedef {{message: string,visible: boolean,onDismiss: () => void} & AlertAction} Error
 * @param {Error} props
 */
export default function ErrorDialog({
  color = Colors.success,
  buttonMode = 'contained',
  ...props
}) {
  const onButtonPress = () => {
    props.onPress?.();
    props.onDismiss();
  };

  return (
    <Portal>
      <Dialog
        visible={props?.message?.length > 0}
        style={styles.dialogContainer}
        dismissable
        onDismiss={props.onDismiss}>
        <Dialog.Content>
          <Label weight="normal" style={styles.dialogText}>
            {props.message}
          </Label>
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            labelStyle={styles.dismissActionLabel}
            mode={'outlined'}
            color={Colors.text}
            onPress={props.onDismiss}
            style={[styles.action, props.style]}>
            بستن
          </Button>
          {props.onPress && props.label ? (
            <Button
              labelStyle={[styles.actionLabel, props.labelStyle]}
              mode={buttonMode}
              color={color}
              onPress={onButtonPress}
              style={[styles.action, props.style]}>
              {props.label}
            </Button>
          ) : null}
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

const styles = StyleSheet.create({
  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0,
    zIndex: 10,
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    textAlign: 'center',
    color: Colors.text,
  },
  action: {
    elevation: 0,
    flex: 1,
    marginHorizontal: 5,
    margin: 0,
  },

  actionLabel: {
    color: Colors.whiteText,
  },
  dismissActionLabel: {
    fontWeight: 'bold',
  },
});
