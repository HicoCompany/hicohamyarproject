import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Label from '../Label';

// * @typedef {{type: 'info' | 'warning' | 'error' | 'success',message: string,timeout: 'short' | 'long'}} Toast

const ToastType = {
  error: {
    iconName: 'alert-circle-outline',
    color: Colors.error,
  },
  warning: {
    iconName: 'alert',
    color: Colors.warn,
  },
  info: {
    iconName: 'information',
    color: Colors.blue,
  },
  success: {
    iconName: 'progress-check',
    color: Colors.green,
  },
  none: undefined,
};

export default function Toast({ type = 'none', message, index }) {
  const showIcon = type != 'none';
  return (
    <View style={[styles.container, { zIndex: index + 10 }]}>
      {showIcon ? (
        <Icon
          name={ToastType[type].iconName}
          color={ToastType[type].color}
          size={25}
          style={styles.icon}
        />
      ) : null}
      <Label
        // eslint-disable-next-line react-native/no-inline-styles
        style={[styles.label, { paddingLeft: showIcon ? 35 : undefined }]}
        numberOfLines={2}
        weight="bold">
        {message}
      </Label>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    minHeight: 45,
    flexShrink: 1,
    maxWidth: '90%',
    backgroundColor: '#efefef',
    justifyContent: 'center',
    elevation: 0.2,
    borderRadius: 50,
    marginVertical: 5,
    overflow: 'hidden',
  },
  label: {
    flexDirection: 'row-reverse',
    minWidth: 100,
    fontSize: normalize(11),
    paddingHorizontal: 10,
    textAlignVertical: 'center',
    textAlign: 'center',
    paddingLeft: 35,
    flexShrink: 1,
    height: '100%',
    overflow: 'hidden',
  },
  error: {},
  warning: {},
  success: {},
  info: {},
  icon: {
    width: 35,
    aspectRatio: 1,
    margin: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
