import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import React from 'react';
import { StyleSheet } from 'react-native';
import { Button, Dialog } from 'react-native-paper';
import Label from '../Label';
import { Modal } from '@components';

/**
 * @typedef {{label: string,onPress(): void,buttonMode: "text" | "outlined" | "contained", color: string,labelStyle: any,style: any,loading: boolean}} AlertAction
 * @typedef {{message: string,dismissable: boolean,actions: AlertAction[]}} Action
 * @param {{onDismiss: () => void} & Action} props
 */
export default function Action(props) {
  const handlePress = (call) => () => {
    call?.();
    props.onDismiss();
  };

  return (
    <Modal
      visible={props.visible}
      onDismiss={props.onDismiss}
      hasBackdrop={false}>
      <Dialog
        visible={props.visible}
        onDismiss={props.onDismiss}
        dismissable={props.dismissable}>
        <Dialog.Content>
          <Label style={styles.text} weight="bold">
            {props?.message}
          </Label>
        </Dialog.Content>
        <Dialog.Actions>
          {props.actions?.map((el, i) => (
            <Button
              loading={el.loading}
              key={`action-${i}`}
              onPress={handlePress(el.onPress)}
              labelStyle={el.labelStyle}
              mode={el.buttonMode}
              color={el.color}
              style={[styles.action, el.style]}>
              {el.label}
            </Button>
          ))}
        </Dialog.Actions>
      </Dialog>
    </Modal>
  );
}

const styles = StyleSheet.create({
  text: {
    margin: 0,
    fontSize: normalize(13),
    color: Colors.text,
  },
  action: {
    elevation: 0,
    flex: 1,
    marginHorizontal: 5,
    margin: 0,
  },
  modal: {
    padding: 0,
    margin: 0,
    height: '100%',
    width: '100%',
  },
});
