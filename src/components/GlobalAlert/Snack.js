import { normalize } from '@src/responsive';
import Colors from '@src/utility/Colors';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button } from 'react-native-paper';
import Label from '../Label';

/**
 *
 * @param {{type: 'none' | 'success' | 'info' | 'warn' | 'error', message: string, index: number,action: {label: string,onPress: () => void}}} param0
 * @returns
 */
export default function Toast({
  type = 'none',
  message,
  index,
  onDismiss,
  action = { label: 'بستن', onPress: onDismiss },
}) {
  return (
    <View style={styles.container}>
      <Label style={styles.label} numberOfLines={2} weight="bold">
        {message}
      </Label>

      {action?.label.length > 0 ? (
        <Button
          onPress={action.onPress}
          color={type != 'none' ? Colors[type] : '#333333'}
          mode="contained"
          labelStyle={styles.buttonLabel}
          style={styles.button}>
          {action.label}
        </Button>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    minHeight: 45,
    flexShrink: 1,
    width: '90%',
    maxWidth: 400,
    backgroundColor: '#efefef',
    justifyContent: 'center',
    elevation: 0.2,
    borderRadius: 50,
    marginVertical: 5,
    overflow: 'hidden',
  },
  label: {
    flexDirection: 'row-reverse',
    fontSize: normalize(11),
    paddingHorizontal: 10,
    textAlignVertical: 'center',
    textAlign: 'right',
    paddingLeft: 35,
    flex: 1,
    height: '100%',
    overflow: 'hidden',
    paddingRight: 20,
  },
  error: {},
  warning: {},
  success: {},
  info: {},
  icon: {
    width: 35,
    aspectRatio: 1,
    margin: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  button: {
    height: '80%',
    minWidth: 60,
    alignSelf: 'center',
    borderRadius: 50,
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonLabel: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#efefef',
    fontWeight: 'bold',
    fontSize: normalize(10),
  },
});
