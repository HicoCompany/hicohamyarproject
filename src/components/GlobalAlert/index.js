import React, { Component, createContext, createRef, useContext } from 'react';
import ToastLists from './ToastLists';
import uuid from 'react-native-uuid';
import ErrorModal from './ErrorModal';
import Action from './Action';

/**
 * @typedef {{label: string,onPress(): void,buttonMode: "text" | "outlined" | "contained", color: string,labelStyle,style,loading?: boolean}} AlertAction
 * @typedef {{message: string,dismissable: boolean,actions: AlertAction[]}} Action
 * @typedef {{type: 'info' | 'warning' | 'error' | 'success' | 'none',mode: 'normal' | 'action',message: string,timeout: 'short' | 'long',action: {label: string, onPress: () => void}}} Toast
 * @typedef {{message: string} & AlertAction} Error
 */

const defaultValue = {
  /**
   * @type {(options: Toast) => void}
   */
  addToast: () => {},
  /**
   * @type {() => void}
   */
  clearToasts: () => {},
  /**
   * @type {(options: Error) => void}
   */
  setError: () => {},
  /**
   * @type {() => void}
   */
  clearError: () => {},
  /**
   * @type {(options: Action) => void}
   */
  setAction: () => {},
  /**
   * @type {() => void}
   */
  clearAction: () => {},
};
const AlertContext = createContext(defaultValue);

export const useAlert = () => useContext(AlertContext);

/**
 * @type {{ addToast: (options: Toast) => void,clearToasts: () => void,setError: (options: Error) => void,clearError: () => void,  setAction: (options: Action) => void,  clearAction: () => void}}
 */
let GlobalAlert = defaultValue;

// to not overwrite the variable
export function getGlobalAlert() {
  return { ...GlobalAlert };
}
export default class AlertProvider extends Component {
  constructor(props) {
    super(props);

    /**
     * @type {{toasts: Toast[],error: Error,action: Action}}
     */
    this.state = {
      error: {
        visible: false,
      },
      toasts: [],
      action: {
        visible: false,
      },
      ready: false,
    };

    /**
     * @type {{current: TransitioningView}}
     */
    this.TransitionRef = createRef();

    GlobalAlert = {
      addToast: this.addToast,
      clearToasts: this.dismissAllToast,
      setError: this.showErrorModal,
      clearError: this.hideError,
    };
  }

  /**
   * @description this function will be used to create a new Toast alert and will be added to Toast start
   * @param {Toast} param
   */
  addToast = ({ timeout, ...options }) => {
    try {
      const key = uuid.v4();
      this.TransitionRef.current?.animateNextTransition?.();
      this.setState((state) => {
        return {
          ...state,
          toasts: [
            ...(state.toasts.length > 4 ? state.toasts.slice(1) : state.toasts),
            { ...options, timeout, key },
          ],
        };
      });

      if (timeout) {
        const typeOfTimeout = typeof timeout;
        // i want the ability to set timeout from addToast
        const time =
          typeOfTimeout == 'string'
            ? timeout == 'long'
              ? 3000 + 500 * this.state.toasts.length
              : 1000 + 500 * this.state.toasts.length
            : timeout;

        const timeoutHandler = setTimeout(() => {
          this.TransitionRef.current?.animateNextTransition?.();
          this.setState((state) => ({
            ...state,
            toasts: state.toasts.filter((el) => el.key != key),
          }));
          clearTimeout(timeoutHandler);
        }, time);
      }
    } catch (e) {}
  };

  handleDismissToast = (key) => () => {
    this.TransitionRef.current?.animateNextTransition?.();
    this.setState((state) => ({
      ...state,
      toasts: state.toasts.filter((el) => el.key != key),
    }));
  };

  dismissAllToast = () => {
    this.setState((state) => ({
      ...state,
      toasts: [],
    }));
  };

  /**
   * @description this function will be used to create a new error modal and will be added to message start
   * @param {Error} param
   */
  showErrorModal = (error) => {
    this.setState((state) => ({
      ...state,
      error: {
        ...state.error,
        ...error,
      },
    }));
  };

  hideError = () => {
    this.setState((state) => ({
      ...state,
      error: {
        visible: false,
      },
      action: null,
    }));
  };

  /**
   * @description this function will be used to create a new action modal and will be added to message start
   * @param {Action} action
   */
  setAction = (action) => {
    this.setState((state) => ({
      ...state,
      error: {
        visible: false,
      },
      action: {
        ...state.action,
        ...action,
        visible: true,
      },
    }));
  };

  clearAction = () => {
    this.setState((state) => ({
      ...state,
      action: {
        ...state.action,
        visible: false,
      },
    }));
  };

  render() {
    return (
      <AlertContext.Provider
        value={{
          addToast: this.addToast,
          clearToasts: this.dismissAllToast,
          setError: this.showErrorModal,
          clearError: this.hideError,
          setAction: this.setAction,
          clearAction: this.clearAction,
        }}>
        {this.props.children}
        <ToastLists
          list={this.state.toasts}
          onDismiss={this.handleDismissToast}
          transitionRef={this.TransitionRef}
        />
        <ErrorModal {...this.state.error} onDismiss={this.hideError} />
        <Action {...this.state.action} onDismiss={this.clearAction} />
      </AlertContext.Provider>
    );
  }
}

export * from './utils';
