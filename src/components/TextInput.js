import Colors from '@src/utility/Colors';
import Constants from '@src/utility/Constants';
import React from 'react';
import { Platform, TextInput as Base } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

Base.defaultProps.selectionColor = 'rgba(0,0,0,0.1)';
/**
 *
 * @param {{weight: 'normal' | 'bold' | 'light',color: 'placeholder' | 'text',mode: 'default' | 'none'} & import('react-native').TextInputProps} param0
 */
export default function TextInput({
  style,
  weight = 'normal',
  color = 'text',
  inputRef,
  mode = 'default',
  ...props
}) {
  return (
    <Base
      // selectionColor={Colors.green}
      {...props}
      ref={inputRef}
      style={[styles[`${mode}${weight}${color}Label`], style]}
    />
  );
}

const defaults = {
  input: {
    textAlign: 'right',
    backgroundColor: 'white',
    borderRadius: '6@s',
    // padding: 0,
    paddingHorizontal: '16@s',
    elevation: '1@ms',
    borderWidth: '1@ms',
    borderColor: '#efefef',
  },
  normalLabel: {
    color: Colors.text,
    ...Platform.select({
      ios: {
        fontFamily: Constants.Fonts.IOS_IR,
        fontWeight: '500',
      },
      android: {
        fontFamily: Constants.Fonts.IR_M,
      },
    }),
  },
  boldLabel: Platform.select({
    ios: {
      fontFamily: Constants.Fonts.IOS_IR,
      fontWeight: 'bold',
    },
    android: {
      fontFamily: Constants.Fonts.IR_B,
    },
  }),

  lightLabel: Platform.select({
    ios: {
      fontFamily: Constants.Fonts.IOS_IR,
      fontWeight: '100',
    },
    android: {
      fontFamily: Constants.Fonts.IR_L,
    },
  }),
  placeholder: {
    color: Colors.gray,
  },
  text: {
    color: Colors.textBlack,
  },
};

const styles = ScaledSheet.create({
  defaultnormaltextLabel: {
    ...defaults.input,
    ...defaults.normalLabel,
    ...defaults.text,
  },
  defaultboldtextLabel: {
    ...defaults.input,
    ...defaults.boldLabel,
    ...defaults.text,
  },
  defaultlighttextLabel: {
    ...defaults.input,
    ...defaults.lightLabel,
    ...defaults.text,
  },
  defaultdnormalplaceholderLabel: {
    ...defaults.input,
    ...defaults.normalLabel,
    ...defaults.placeholder,
  },
  noneboldplaceholderLabel: {
    ...defaults.boldLabel,
    ...defaults.placeholder,
  },
  nonelightplaceholderLabel: {
    ...defaults.lightLabel,
    ...defaults.placeholder,
  },
  nonenormaltextLabel: {
    ...defaults.normalLabel,
    ...defaults.text,
  },
  noneboldtextLabel: {
    ...defaults.boldLabel,
    ...defaults.text,
  },
  nonelighttextLabel: {
    ...defaults.lightLabel,
    ...defaults.text,
  },
  nonenormalplaceholderLabel: {
    ...defaults.normalLabel,
    ...defaults.placeholder,
  },
});
