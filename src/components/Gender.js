import React from 'react';
import PropTypes from 'prop-types';

//components
import RadioButton from './RadioButton';

//utils
import { ScaledSheet } from 'react-native-size-matters';
import Constants from '@src/utility/Constants';
import Colors from '@src/utility/Colors';

export default function Gender({ value, onChange }) {
  return (
    <RadioButton.Group value={value} onChange={onChange} style={styles.gender}>
      <RadioButton
        value={Constants.GENDERS.female}
        label="زن"
        side="left"
        style={styles.genderItem}
        checkedColor={Colors.purple}
        labelStyle={styles.genderItemLabel}
      />
      <RadioButton
        value={Constants.GENDERS.male}
        label="مرد"
        side="right"
        checkedColor={Colors.purple}
        style={styles.genderItem}
        labelStyle={styles.genderItemLabel}
      />
    </RadioButton.Group>
  );
}

Gender.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func.isRequired,
};

const styles = ScaledSheet.create({
  gender: {
    alignSelf: 'center',
    height: '45@s',
    overflow: 'hidden',
    width: '100%',
  },
  genderItem: {
    flex: 1,
    height: '45@s',
  },
  genderItemLabel: {
    fontSize: '12@s',
  },
});
