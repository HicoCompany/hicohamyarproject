/** @format */
// import './wdyr';
import 'react-native-get-random-values';

import { AppRegistry, YellowBox } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import App from './src/App';
import { initNotif } from '@src/notification';
import moment from 'moment-jalaali';
import { name as appName } from './app.json';
// if (__DEV__) {
//   require('react-native-performance-flipper-reporter').setupDefaultFlipperReporter();
// }

moment.loadPersian({ dialect: 'persian-modern' });
YellowBox.ignoreWarnings(['Remote debugger', 'Require cycle:']);

EStyleSheet.build({
  $IR_L: 'IRANYekanLightMobile(FaNum)',
  $IR_M: 'IRANYekanRegularMobile(FaNum)',
  $IR_B: 'IRANYekanMobileBold(FaNum)',
});

initNotif();
AppRegistry.registerComponent(appName, () => App);
