// package ir.hico.tafkik_hamyar

// import android.annotation.SuppressLint
// import android.app.Activity
// import android.content.Context
// import android.content.Intent
// import android.content.IntentSender
// import android.graphics.BitmapFactory
// import android.location.Location
// import android.location.LocationManager
// import android.os.Build
// import android.os.Looper
// import com.facebook.react.bridge.*
// import com.facebook.react.common.MapBuilder
// import com.facebook.react.modules.core.DeviceEventManagerModule
// import com.facebook.react.uimanager.SimpleViewManager
// import com.facebook.react.uimanager.ThemedReactContext
// import com.facebook.react.uimanager.annotations.ReactProp
// import com.facebook.react.uimanager.events.RCTEventEmitter
// import com.google.android.gms.common.api.ApiException
// import com.google.android.gms.common.api.ResolvableApiException
// import com.google.android.gms.location.*
// import com.google.android.gms.tasks.Task
// import org.neshan.core.LngLat
// import org.neshan.layers.Layer
// import org.neshan.layers.VectorElementLayer
// import org.neshan.services.NeshanMapStyle
// import org.neshan.services.NeshanServices
// import org.neshan.styles.AnimationStyleBuilder
// import org.neshan.styles.AnimationType
// import org.neshan.styles.MarkerStyleCreator
// import org.neshan.ui.ClickData
// import org.neshan.ui.ClickType
// import org.neshan.ui.MapEventListener
// import org.neshan.ui.MapView
// import org.neshan.utils.BitmapUtils
// import org.neshan.vectorelements.Marker


// class NeshanMapViewManager(private val reactContext: ReactApplicationContext) : SimpleViewManager<MapView>() {
//     lateinit var map: MapView
//     var layers: MutableMap<String, VectorElementLayer> = mutableMapOf()
//     private var fusedLocationClient: FusedLocationProviderClient? = null
//     private lateinit var locationCallback: LocationCallback
//     private lateinit var  locationRequest: LocationRequest
//     private var focusOnLocation: Boolean? = null

//     private val mActivityEventListener: ActivityEventListener = object : BaseActivityEventListener() {
//         override fun onActivityResult(activity: Activity?, requestCode: Int, resultCode: Int, intent: Intent) {
//             if (requestCode == 10023) {
//                 try {
//                     if(isLocationEnabled()) {
//                         setFusedAndLocationCallback(
//                                 shouldStartLocationUpdate = false,
//                                 focusOnUserLocation = true,
//                                 addMarker = false,
//                                 callOnLocationReady = true
//                         )
//                     }
//                     reactContext.removeActivityEventListener(this);
//                 } catch (ex: Exception) {
//                 }
//             }
//         }
//     }

//     override fun getName() = "NeshanMap"


//     @ReactProp(name = "config")
//     fun setUserLocation(map: MapView, config: ReadableMap) {
//         if(locationRequest != null) {
//             if(config.hasKey("interval")) {
//                 locationRequest.interval = config.getDouble("interval").toLong()
//             }
//             if(config.hasKey("fastInterval")) {
//                 locationRequest.fastestInterval = config.getDouble("fastInterval").toLong()
//             }
//         }
//     }

//     @ReactProp(name = "markers")
//     fun setMarkers(map: MapView, markers: ReadableArray?) {
// //        initMarkersLayer()
// //        clearMarkersLayer()
// //        layers["markers"]?.refresh()
// //
// //        if(markers != null) {
// //            for (i in markers.size()-1 downTo 0  step 1) {
// //                markers.getMap(i)?.let { addMarkerWithCatch(it, "markers") }
// //            }
// //        }

//     }


//     @ReactProp(name = "location")
//     public fun setLocation(map: MapView, location: ReadableMap) {
//         if(location.hasKey("x") && location.hasKey("y")) {
//             val position = LngLat(location.getDouble("x"), location.getDouble("y"))
//             map.setFocalPointPosition(position, location.getDouble("duration").toFloat())
//         }
//     }


//     @ReactProp(name = "zoom")
//     public fun setZoom(map: MapView, zoomLevel: Double = 14.0) {
//             map.setZoom(zoomLevel.toString().toFloat(), 0f)
//     }


//     @ReactProp(name = "isRotatable", defaultBoolean = false)
//     public fun setZoom(map: MapView, isRotatable: Boolean) {
//         map.options.setRotatable(isRotatable)
//     }

//     @ReactProp(name = "mapStyle")
//     public fun  setMapStyle(map: MapView, mapStyle: Double = 2.0) {
//         try {
//             val style = when (mapStyle) {
//                 0.0 -> NeshanMapStyle.NESHAN
//                 1.0 -> NeshanMapStyle.STANDARD_NIGHT
//                 2.0 -> NeshanMapStyle.STANDARD_DAY
//                 3.0 -> NeshanMapStyle.DREAMY
//                 4.0 -> NeshanMapStyle.DREAMY_GOLD
//                 else -> NeshanMapStyle.NESHAN
//             }
//             //            map.layers.add(NeshanServices.createBaseMap(NeshanMapStyle.STANDARD_DAY))
//              map.layers.insert(0,  NeshanServices.createBaseMap(style,reactContext.cacheDir.toString() + "/baseMap", 10))
//         } catch (e:Throwable) {
//         }
//     }

//     override fun getExportedCustomDirectEventTypeConstants(): MutableMap<String, Any>? {
//         return MapBuilder.of(
//                 "press",
//                 MapBuilder.of("registrationName", "onPress"),
//                 "move",
//                 MapBuilder.of("registrationName", "onMapMove"),
//                 "updateLocations",
//                 MapBuilder.of("registrationName", "onLocationChanged"),
//                 "error",
//                 MapBuilder.of("registrationName", "onError"),
//                 "locationReady",
//                 MapBuilder.of("registrationName","onLocationReady")
//         )
//     }

//     override fun createViewInstance(reactContext: ThemedReactContext): MapView {
//         map = MapView(reactContext)

//         map.mapEventListener = object : MapEventListener() {
//             override fun onMapClicked(mapClickInfo: ClickData) {
//                 handleMapClick(mapClickInfo)
//             }

//             override fun onMapMoved() {
//                 var location = Arguments.createMap()
//                 location.putDouble("x", map.focalPointPosition.x)
//                 location.putDouble("y", map.focalPointPosition.y)
//                 location.putDouble("zoom", map.zoom.toDouble())
//                 callEvent("move", location)
//             }
//         }

//         return map
//     }

//     override fun onDropViewInstance(view: MapView) {
//         super.onDropViewInstance(view)
//         reactContext.removeActivityEventListener(mActivityEventListener)
//         map.mapEventListener.delete()
//     }
    
//     fun handleMapClick(mapClickInfo: ClickData) {
//         val event = Arguments.createMap()
//         event.putDouble("x", mapClickInfo.clickPos.x)
//         event.putDouble("y", mapClickInfo.clickPos.y)
//         when(mapClickInfo.clickType) {
//             ClickType.CLICK_TYPE_SINGLE -> event.putString("clickType", "single")
//             ClickType.CLICK_TYPE_LONG -> event.putString("clickType", "long")
//             ClickType.CLICK_TYPE_DOUBLE -> event.putString("clickType", "double")
//             ClickType.CLICK_TYPE_DUAL -> event.putString("clickType", "doul")
//             else -> event.putNull("clickType")
//         }
//         callEvent("press", event)
//     }



//     override fun receiveCommand(root: MapView, commandId: String?, args: ReadableArray?) {
//          when(commandId) {
//              "initUserLocation" -> initUserLocation(args?.getBoolean(0), args?.getBoolean(1),args?.getBoolean(2),args?.getBoolean(3))
//              "startUpdatingLocation" -> startUpdatingLocation(args?.getBoolean(0))
//              "stopUpdatingLocation" -> stopLocationUpdates()
//              "focusOnLastLocation" -> focusOnLastLocation()
//              "setLocation" -> setMapLocation(
//                      x = args!!.getDouble(0),
//                      y = args.getDouble(1),
//                      zoom = args.getDouble(2).toFloat(),
//                      duration = args.getDouble(3).toFloat()
//              )
//              "call_focal_point_position" -> {
//                  var params = Arguments.createMap()
//                  val location = map.focalPointPosition
//                  params.putDouble("x", location.x)
//                  params.putDouble("y", location.y)
//                  callListener("listener_focal_point_position", params)
//              }
//          }
//     }



//     private fun setMapLocation(x: Double, y: Double, zoom: Float, duration: Float) {
//         map.setFocalPointPosition(LngLat(x, y), duration)
//         map.setZoom(zoom, duration)
//     }



//     @SuppressLint("MissingPermission")
//     private fun  focusOnLastLocation() {
//         if(fusedLocationClient != null) {
//             val task =  fusedLocationClient?.lastLocation
//             task?.addOnSuccessListener { location ->
//                if(location != null) {
//                    setMapLocation(
//                            x = location.longitude,
//                            y = location.latitude,
//                            zoom = 15f,
//                            duration = 0.25f
//                    )
//                }
//             }
//         }
//     }



//     private fun addMapLayer(name: String):VectorElementLayer? {
//         if(!layers.containsKey(name)) {
//             layers[name] = NeshanServices.createVectorElementLayer()
//             map.layers.add(layers[name])

//             return layers[name]
//         } else return null
//     }

//     private fun removeMapLayer(name: String) {
//         if(layers.containsKey(name)) {
//             map.layers.remove(layers[name])
//         }
//     }



//     private fun setFusedAndLocationCallback(shouldStartLocationUpdate: Boolean?,focusOnUserLocation: Boolean?,addMarker: Boolean?,callOnLocationReady:Boolean?) {
//         fusedLocationClient = LocationServices.getFusedLocationProviderClient(reactContext)
//         locationCallback = object : LocationCallback() {

//             override fun onLocationResult(locationResult: LocationResult?) {
//                 locationResult ?: return
//                 val results = Arguments.createArray()
//                 for (location in locationResult.locations) {
//                     results.pushMap(getLocation(location))
//                 }
//                 val location = locationResult.locations.last()

//                 if(focusOnLocation == true) {
//                     if (location != null) {
//                         map.setFocalPointPosition(LngLat(location.longitude,location.latitude),0.25f)
//                     }
//                     return
//                 }

//                 val finalResult = Arguments.createMap()
//                 finalResult.putArray("locations", results)
//                 if(addMarker == true) {
//                     addMarker(
//                             image_name = "ic_user_location",
//                             loc = LngLat(location.longitude, location.latitude),
//                             layerName = "user",
//                             withAnimation = false,
//                             clearLayout = true
//                     )
//                 }
//                 callEvent("updateLocations", finalResult)
//             }
//         }
//         if(callOnLocationReady == true) {
//             onLocationReady()
//         }
//         if(shouldStartLocationUpdate == true) {
//             startUpdatingLocation(false)
//         }
//         if(focusOnUserLocation == true) {
//             focusOnLastLocation()
//         }
//     }
//     private fun initUserLocation(
//             shouldStartLocationUpdate: Boolean?,
//             focusOnUserLocation: Boolean?,
//             addMarker: Boolean?,
//             callOnLocationReady:Boolean?
//     ) {
//         locationRequest = LocationRequest.create()
//         locationRequest.interval = 5000
//         locationRequest.fastestInterval = 2000
//         locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

//         if(isLocationEnabled()){
//             setFusedAndLocationCallback(shouldStartLocationUpdate,focusOnUserLocation,addMarker,callOnLocationReady)
//         } else {
//             val builder = LocationSettingsRequest.Builder()
//                     .addLocationRequest(locationRequest)

//             builder.setAlwaysShow(true)

//             val client: SettingsClient = LocationServices.getSettingsClient(reactContext)
//             val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

//             task.addOnSuccessListener { 
//                 setFusedAndLocationCallback(shouldStartLocationUpdate,focusOnUserLocation,addMarker,callOnLocationReady)
//             }

//             task.addOnFailureListener { exception ->
//                 var error = Arguments.createMap()

//                 when ((exception as ApiException).statusCode) {
//                     LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                         try {
//                             reactContext.addActivityEventListener(mActivityEventListener);
//                             // Show the dialog by calling startResolutionForResult(), and check the
//                             // result in onActivityResult().
//                             val rae: ResolvableApiException = exception as ResolvableApiException;
//                             rae.startResolutionForResult(reactContext.currentActivity, 10023);
//                         } catch (sie: IntentSender.SendIntentException) {
//                         }
//                     }

//                     LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
//                         error.putString("message", "can not turn on location go to settings")
//                         error.putString("stackTrace", exception.stackTrace.toString())
//                         callEvent("error", error)
//                     }

//                 }

//             }
//         }


//     }


//     @SuppressLint("MissingPermission")
//     private fun startUpdatingLocation(focus: Boolean?) {
//         focusOnLocation =  focus
//         if(fusedLocationClient != null) {
//             fusedLocationClient?.requestLocationUpdates(locationRequest,
//                     locationCallback,
//                     Looper.getMainLooper())
//             layers["user"] = NeshanServices.createVectorElementLayer()
//             map.layers.add(layers["user"]);
//         }
//     }

//     private fun stopLocationUpdates() {
//         if(fusedLocationClient != null) {
//             fusedLocationClient?.removeLocationUpdates(locationCallback)
//             map.layers.remove(layers["user"]);
//         }
//     }






//     fun initMarkersLayer() {
//         if(!layers.containsKey(name)) {
//             layers["markers"] = NeshanServices.createVectorElementLayer()
//             map.layers.add(layers["markers"])
//         }
//     }

//     fun clearMarkersLayer() {
//         layers["markers"]?.clear()
//     }



//     private fun clearLayer(layerName: String) {
//         layers[layerName]?.clear()
//     }


//     // This method gets a LngLat as input and adds a marker on that position
//     private fun addMarker(image_name: String?, loc: LngLat?, layerName: String?, withAnimation: Boolean, clearLayout: Boolean) {
//         if(image_name != null && loc != null && layerName != null) {


//             val markStCr = MarkerStyleCreator()
//             val image = reactContext.resources.getIdentifier(image_name, "drawable", reactContext.packageName)
//             if(withAnimation) {
//                 val animStBl = AnimationStyleBuilder()
//                 animStBl.fadeAnimationType = AnimationType.ANIMATION_TYPE_SMOOTHSTEP
//                 animStBl.sizeAnimationType = AnimationType.ANIMATION_TYPE_SPRING
//                 animStBl.phaseInDuration = 0.5f
//                 animStBl.phaseOutDuration = 0.5f
//                 val animSt = animStBl.buildStyle()
//                 markStCr.animationStyle = animSt;
//             }
//             markStCr.size = 20f
//             markStCr.bitmap = BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(reactContext.resources, image))
//             val markSt = markStCr.buildStyle()
//             val marker = Marker(loc, markSt)

//             if(clearLayout) {
//                 layers[layerName]?.clear();
//             }
//             layers[layerName]?.add(marker)
//         }
//     }


//     fun isLocationEnabled():Boolean {
//         val locationManager: LocationManager = reactContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//         return  locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
//     }


//     private fun onLocationReady() {
//         callEvent("locationReady", Arguments.createMap())
//     }

//     private fun callEvent(eventName: String, event: WritableMap) {
//         reactContext.getJSModule(RCTEventEmitter::class.java).receiveEvent(map.id, eventName, event)
//     }


//     private fun errorEvent(message: String) {
//         val event:WritableMap = Arguments.createMap()
//         event.putString("message", message)
//         callEvent("error", event)
//     }

//     private fun messageEvent(message: String) {
//         val event:WritableMap = Arguments.createMap()
//         event.putString("message", message)
//         callEvent("message", event)
//     }

//     private fun callListener(name: String, params: WritableMap) {
//         reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit(name, params)
//     }

//     private fun getLocation(location: Location):WritableMap {
//         var customLocation = Arguments.createMap()
//         customLocation.putDouble("accuracy", location.accuracy.toDouble())
//         customLocation.putDouble("altitude", location.altitude)
//         customLocation.putDouble("y", location.latitude)
//         customLocation.putDouble("x", location.longitude)
//         customLocation.putDouble("speed", location.speed.toDouble())
//         customLocation.putDouble("course", location.bearing.toDouble())
//         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//             customLocation.putDouble("courseAccuracy", location.bearingAccuracyDegrees.toDouble())
//         } else {
//             customLocation.putDouble("courseAccuracy", 0.0)
//         }

//         return customLocation
//     }

// }
