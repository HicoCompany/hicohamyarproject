package ir.hico.tafkik_hamyar

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Looper
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices


class NeshanMapModule(private val reactContext: ReactApplicationContext): ReactContextBaseJavaModule(reactContext)
{
    override fun getName() = "NeshanMapModule"
    //  var map: MapView?  = null

    // @ReactMethod
    // public fun initModule(tag: Int) {
    //     reactContext.getNativeModule(UIManagerModule::class.java).prependUIBlock(object : UIBlock {
    //         override fun execute(nativeViewHierarchyManager: NativeViewHierarchyManager?) {
    //             val mapView: MapView = nativeViewHierarchyManager?.resolveView(tag) as MapView
    //             map = mapView
    //         }
    //     })
    // }


    // @ReactMethod
    // public fun getCenterLocation(promise: Promise) {
    //     if(map != null) {
    //         try {
    //             val locationM = map?.focalPointPosition
    //             val location = Arguments.createMap()
    //             if (locationM != null) {
    //                 location.putDouble("x", locationM.x)
    //                 location.putDouble("y", locationM.y)
    //             }
    //             promise.resolve(location)
    //         } catch (e: Exception) {
    //             promise.reject(e)
    //         }
    //     }
    // }

    @ReactMethod
    public fun isLocationEnabled(promise: Promise) {
        try {
            val locationManager: LocationManager = reactContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val data = Arguments.createMap()
            data.putBoolean("network", locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            data.putBoolean("gps", locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            promise.resolve(data)
        } catch (e: Exception) {
            promise.reject(e)
        }   
    }


    @SuppressLint("MissingPermission")
    @ReactMethod
    fun focusOnLastLocation(promise: Promise) {
       if(locationEnabled()) {
           val fusedLocationClient = LocationServices.getFusedLocationProviderClient(reactContext)
           fusedLocationClient.lastLocation.addOnSuccessListener { location ->
              if(location != null) {
                  promise.resolve(getLocation(location))
              } else {
                  val error = Throwable("location is null")
                  promise.reject(error)
              }
           }.addOnFailureListener { err -> promise.reject(err) }
       } else {
           val error = Throwable("gps is not enabled")
           promise.reject(error)
       }
    }


    private fun locationEnabled():Boolean {
        val locationManager: LocationManager = reactContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return  locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }


    private fun getLocation(location: Location):WritableMap {
        var customLocation = Arguments.createMap()
        customLocation.putDouble("accuracy", location.accuracy.toDouble())
        customLocation.putDouble("altitude", location.altitude)
        customLocation.putDouble("y", location.latitude)
        customLocation.putDouble("x", location.longitude)
        customLocation.putDouble("speed", location.speed.toDouble())
        customLocation.putDouble("course", location.bearing.toDouble())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            customLocation.putDouble("courseAccuracy", location.bearingAccuracyDegrees.toDouble())
        } else {
            customLocation.putDouble("courseAccuracy", 0.0)
        }

        return customLocation
    }

    var timesLocationUpdated = 0
    @SuppressLint("MissingPermission")
    @ReactMethod
    private fun getLastLocationByListening(config: ReadableMap, promise: Promise) {
        try{
            val locationRequest = LocationRequest.create()
            if(config.hasKey("interval")) {
                locationRequest.interval = config.getInt("interval").toLong()
            } else {
                locationRequest.interval = 10000
            }

            if(config.hasKey("fastestInterval")) {
                locationRequest.fastestInterval = config.getInt("fastestInterval").toLong()
            } else {
                locationRequest.fastestInterval = 2000
            }
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            if(config.hasKey("numUpdates"))  {
                locationRequest.numUpdates = config.getInt("numUpdates")
            } else {
                locationRequest.numUpdates = 1
            }


            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(reactContext)
            timesLocationUpdated = 0
            val callback = object : LocationCallback() {
               override fun onLocationResult(res: LocationResult?) {
                   if (null != res) {
                       sendEvent(reactContext,"lastLocation",parseLocation(res.locations.last()))
                        timesLocationUpdated++
                        if(timesLocationUpdated >= locationRequest.numUpdates) {
                            fusedLocationClient.removeLocationUpdates(this)
                            promise.resolve("end")
                        }
                   }
                }
            }

            fusedLocationClient?.requestLocationUpdates(locationRequest,
                    callback,
                    Looper.getMainLooper())

        } catch (e: Exception) {
            promise.reject(e)
        }
    }

    // @ReactMethod
    // public fun setLocation(location: ReadableMap, promise: Promise) {
    //     if(map !== null) {
    //         if(location.hasKey("x") && location.hasKey("y")) {
    //            try {
    //                val position = LngLat(location.getDouble("x"), location.getDouble("y"))
    //                map?.setFocalPointPosition(position, location.getDouble("duration").toFloat())
    //                promise.resolve(null)
    //            } catch (e: Exception) {
    //                promise.reject(e)
    //            }

    //         } else {
    //             promise.reject(Exception("x and y are required"))
    //         }
    //     } else {
    //         promise.reject(Exception("init map first"))
    //     }
    // }


    private fun parseLocation(location: Location):WritableMap {
        var customLocation = Arguments.createMap()
        customLocation.putDouble("accuracy", location.accuracy.toDouble())
        customLocation.putDouble("altitude", location.altitude)
        customLocation.putDouble("y", location.latitude)
        customLocation.putDouble("x", location.longitude)
        customLocation.putDouble("speed", location.speed.toDouble())
        customLocation.putDouble("course", location.bearing.toDouble())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            customLocation.putDouble("courseAccuracy", location.bearingAccuracyDegrees.toDouble())
        } else {
            customLocation.putDouble("courseAccuracy", 0.0)
        }

        return customLocation
    }


    private fun sendEvent(reactContext: ReactContext,
                          eventName: String,params: WritableMap?) {
        reactContext
                .getJSModule(RCTDeviceEventEmitter::class.java)
                .emit(eventName, params)
    }

}