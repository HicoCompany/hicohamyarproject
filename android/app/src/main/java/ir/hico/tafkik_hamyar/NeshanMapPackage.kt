package ir.hico.tafkik_hamyar

import com.facebook.react.ReactPackage
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager
import java.util.*


class NeshanMapPackage : ReactPackage {
    override fun createViewManagers(reactContext: ReactApplicationContext): MutableList<ViewManager<*, *>> {
        val views: MutableList<ViewManager<*,*>> = ArrayList()

        // return mutableListOf(NeshanMapViewManager(reactContext))
        // return mutableListOf()

        return views
    }

    override fun createNativeModules(reactContext: ReactApplicationContext): MutableList<NativeModule> {
        val modules: MutableList<NativeModule> = ArrayList()
        modules.add(NeshanMapModule(reactContext))
        return modules
    }
}